#ifndef hnlTRExSelection_h
#define hnlTRExSelection_h

#include "trexSelection.hxx"
#include "DataClassesHNL.hxx"

class hnlTRExSelection: public SelectionBase{
public:
  hnlTRExSelection(bool forceBreak=true);
  virtual ~hnlTRExSelection(){}

  //---- These are mandatory functions
  void DefineSteps();
  inline ToyBoxB* MakeToyBox();
  AnaEventSummaryB* MakeEventSummary(){return new AnaEventSummaryB();}
  bool FillEventSummary(AnaEventB& event, Int_t allCutsPassed[]);
  
  nd280Samples::SampleEnum GetSampleEnum(){return nd280Samples::kUnassigned;}
 
  void InitializeEvent(AnaEventB& event){(void)(event);}
  
protected:
  ///an instance of base selection
  trexSelection _trexSelection;
  
};


class ToyHNL_TRExBox: public ToyTRExBox{

public:

  ToyHNL_TRExBox(){}  
  virtual ~ToyHNL_TRExBox(){};
  
  virtual void Reset(){
    ToyTRExBox::Reset();    
  }

};

inline ToyBoxB* hnlTRExSelection::MakeToyBox() {return new ToyHNL_TRExBox();}

class FillSummaryAction_HNL_TREx: public StepBase{
public:
  using StepBase::Apply;
  bool Apply(AnaEventB& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FillSummaryAction_HNL_TREx();}
};



class ProtonMultiplicityCut: public StepBase{

public:
  ProtonMultiplicityCut(){
    _protonLHoodCut = (Float_t) ND::params().GetParameterD("trexAnalysis.Analysis.ProtonLHoodCut");
  }

  using StepBase::Apply;

  bool Apply(AnaEventB& event, ToyBoxB& box) const;

  StepBase* MakeClone(){return new ProtonMultiplicityCut();}

private:  
  Float_t _protonLHoodCut;

};

class PionCut: public StepBase{

public:
  PionCut(){
    _useOldSecondaryPID = (Float_t) ND::params().GetParameterI("trexAnalysis.Cuts.useOldSecondaryPID");
  }

  using StepBase::Apply;

  bool Apply(AnaEventB& event, ToyBoxB& box) const;

  StepBase* MakeClone(){return new PionCut();}

private:  
  bool _useOldSecondaryPID;

};

class TwoTracksCut: public StepBase{

public:

  using StepBase::Apply;

  bool Apply(AnaEventB& event, ToyBoxB& box) const;

  StepBase* MakeClone(){return new TwoTracksCut();}

};

namespace hnl_trex_selection_utils{
  bool 		PionPID(const AnaTPCPath* path, bool useOldSecondaryPID);
}

#endif 


