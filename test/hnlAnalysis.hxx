#ifndef hnlAnalysis_h
#define hnlAnalysis_h

#include "trexAnalysis.hxx"
#include "hnlTRExSelection.hxx"
#include "hnlGlobalReconSelection.hxx"
#include "AnalysisUtils.hxx"
#include "oaAnalysisHNL_TRExConverter.hxx"
#include "OutputManager.hxx"

class hnlAnalysis: public baseAnalysis {
public:
  hnlAnalysis(AnalysisAlgorithm* ana=NULL);
  virtual ~hnlAnalysis(){}

  //---- These are mandatory functions
  void DefineSelections();
  void DefineConfigurations();
  void DefineMicroTrees(bool addBase=true);
  void DefineTruthTree();

  void FillMicroTrees(bool addBase=true);
  void FillToyVarsInMicroTrees(bool addBase=true);

  bool CheckFillTruthTree(const AnaTrueVertex& vtx);
  void FillTruthTree(const AnaTrueVertex& vtx);
  //--------------------

  void FillCategories();

  // ------- this is to fill TREx info (since cannot inherit from InputManager itself to create AnaTRExEvent)
  void InitializeBunch();


  void FinalizeToy();

  enum enumStandardMicroTrees_hnlAnalysis{

    HMN_true_PiAngle = trexAnalysis::enumStandardMicroTreesLast_trexAnalysis + 1,

    NPiPlus,
    NAssPiPlus,
    NOtherThenPi,
    Pi_true_mom,

    //PionPIDCutTuning
    CutMom,
    Cut1Value,
    PiLikHood,
    PiCutCounter,
    //InvMass
    InvMass,

    //--- Proton  true variables -----
    NProton_true_Paths,
    Proton_true_mom,
    ProtonNu_true_angle,
    Proton_true_pos,
    Proton_true_endpos,
    Proton_true_dir,
    Proton_true_length,
    Proton_true_NNodes,
    Proton_true_ChargeDeposed,

    //General Reco info about Junction
    NProtonPaths,

    //True varibales for junction and paths   
    Total_true_mom,


    //Possible Variables to save about proton or Lowest momntum pos. track        (for future)
    ProtonCandidate_mom,
    LMPT_mom,
    ProtonMu_reco_HighAngle,
    Proton_reco_pos,
    Proton_reco_endpos,
    ProtonPath_reco_length,
    
    //HNL simulation
    prob, 
    hnl_mass, 

    enumStandardMicroTreesLast_hnlAnalysis 
  };

protected:

  const ToyHNL_TRExBox& mybox(){return static_cast<const ToyHNL_TRExBox&>(box());}
 
  
  /// fill vars relevant for TREx studies
  void FillMicroTrees_TREx();
  
  trexAnalysis* _trexAnalysis;

  Float_t _protonLHoodCut;
  Float_t _AssPathDist;
 
  /// whether to use TREx-based selection of global vertexing 
  bool _useTRExSelection;
};


#endif
