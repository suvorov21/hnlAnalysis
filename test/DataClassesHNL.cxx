#include "DataClassesHNL.hxx"


//********************************************************************
AnaHNLTrueVertex::AnaHNLTrueVertex(const AnaHNLTrueVertex& vertex):AnaTRExTrueVertex(vertex){
//********************************************************************

  Mass        = vertex.Mass;
  Weight      = vertex.Weight;
  Probability = vertex.Probability;

}

//********************************************************************
void AnaHNLTrueVertex::Print() const{
  //********************************************************************

  std::cout << "-------- AnaHNLTrueVertex --------- " << std::endl;

  AnaTRExTrueVertex::Print();

  std::cout << "Mass:         " << Mass               << std::endl;
  std::cout << "Weight:       " << Weight             << std::endl;
  std::cout << "Probability:  " << Probability        << std::endl;

}


