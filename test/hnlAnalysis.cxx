#include "hnlAnalysis.hxx"
#include "Parameters.hxx"
#include "UseGlobalAltMomCorrection.hxx"
#include "CategoriesUtils.hxx"
#include "TrackCategoryTools.hxx"
#include "BasicUtils.hxx"
#include "trexUtils.hxx"
#include "trexCategoryUtils.hxx"

//********************************************************************
hnlAnalysis::hnlAnalysis(AnalysisAlgorithm* ana) : baseAnalysis(ana) {
  //********************************************************************

  // Minimum accum level to save event into the output tree
  SetMinAccumCutLevelToSave(ND::params().GetParameterI("hnlAnalysis.MinAccumLevelToSave"));
  
  _useTRExSelection = (bool)ND::params().GetParameterI("hnlAnalysis.TRExAnalysis");
 
  if (_useTRExSelection)
    input().ReplaceConverter("oaAnalysisTree", new oaAnalysisHNL_TRExConverter());

  _protonLHoodCut = (Float_t) ND::params().GetParameterD("hnlAnalysis.Analysis.ProtonLHoodCut");
  _AssPathDist    = (Float_t) ND::params().GetParameterD("hnlAnalysis.Analysis.AssPathDist");


  // Add the package version
  ND::versioning().AddPackage("hnlAnalysis", anaUtils::GetSoftwareVersionFromPath((std::string)getenv("HNLANALYSISROOT")));

  _trexAnalysis = new trexAnalysis(this);
  UseAnalysis(_trexAnalysis);  

}

//********************************************************************
void hnlAnalysis::DefineSelections(){
  //********************************************************************

  bool forceBreak = (bool)ND::params().GetParameterI("hnlAnalysis.ForceBreak");

  // ----- TREx-----------
  if (_useTRExSelection)
    sel().AddSelection("kTRExAnalysis", "HNL trex selection",   new hnlTRExSelection(forceBreak));
  else
    sel().AddSelection("kTRExAnalysis", "HNL GV selection",     new hnlGlobalReconSelection(forceBreak));
    
}


//********************************************************************
void hnlAnalysis::DefineConfigurations(){
  //********************************************************************
  // Some configurations are defined in the base Analysis
  baseAnalysis::DefineConfigurations();


}

//********************************************************************
void hnlAnalysis::DefineMicroTrees(bool addBase){
  //********************************************************************

  // -------- Add variables to the analysis tree ----------------------

  // -------- Add variables to the analysis tree ----------------------
  // Variables from baseAnalysis (run, event, ...)
  _trexAnalysis->DefineMicroTrees(addBase);


  //ProtonTracks true info																				
  AddVarF(output(), Proton_true_mom,      "Proton_true_Momentum");
  AddVarF(output(), ProtonNu_true_angle,  "ProtonNu_true_Angle");

  AddVar4VF(output(), Proton_true_pos,    "Proton_true_pos");
  AddVar4VF(output(), Proton_true_endpos, "Proton_true_endpos");
  AddVar3VF(output(), Proton_true_dir,    "Proton_true_dir");
  //  AddVarF(output(), Proton_true_length, "ProtonPath_true_length");//  There is no info about those parameters in TrueTrack !?
  //  AddVarF(output(), Proton_true_NNodes, "Proton_true_NNodes");//    TODO
  //  AddVarF(output(), Proton_true_ChargeDeposed, "Proton_true_ChargedDeposed");	Do not have this info in TREx yet

  AddVarI(output(),         NPiPlus,          "NVertex_true_PiPlus");
  AddVarI(output(),         NAssPiPlus,       "N AssosiatedPiPlus");
  AddVarI(output(),         NOtherThenPi ,    "NOther then PiPlus Reco particles");   

  //----PionPIDCutTuning----
  AddVarVF(output(),     CutMom,          "", PiCutCounter);
  AddVarVF(output(),     Cut1Value,       "", PiCutCounter);
  AddVarVF(output(),     PiLikHood,       "", PiCutCounter);
  //InvMass
  AddVarF(output(),         InvMass,    "InvMass for Mu- Pi+ channel");


  //Information about TRUE interaction which is not stored in baseAnalysis(TruthTree)

  AddVarI(output(), NProton_true_Paths, "NProton_true_Paths");


  //Possible Variables to save about proton and Lowest momentum positive track        
  AddVarF(output(),   ProtonCandidate_mom,      "ProtonCandidate_mom");
  AddVarF(output(),   LMPT_mom,                 "LMPT_mom");                                                                            //Interesting info about recostructed lowest positive path momentum
  AddVarF(output(),   ProtonMu_reco_HighAngle,  "ProtonMu_reco_HighAngle");//All these variables is caluclated for ProtonCandidate
  AddVar4VF(output(), Proton_reco_pos,          "Proton_reco_pos");
  AddVar4VF(output(), Proton_reco_endpos,       "Proton_reco_endpos");                          
  AddVarF(output(),   ProtonPath_reco_length,   "ProtonPath_reco_length");//TODO Strange things happens there-by default TREx set Length - 0- I got here: -1000!!!


  AddVarF(output(),  prob,     "Interaction/decay(HNL probability)");
  AddVarF(output(),  hnl_mass, "HNL mass");


}

//********************************************************************
void hnlAnalysis::DefineTruthTree(){
  //********************************************************************
  // Variables from base package
  _trexAnalysis->DefineTruthTree();

  AddVarF(output(),  prob,     "Interaction/decay(HNL probability)");
  AddVarF(output(),  hnl_mass, "HNL mass");

}


//********************************************************************
void hnlAnalysis::InitializeBunch(){
  //********************************************************************
  // the event event was created previousely by InputManager
  _trexAnalysis->InitializeBunch();
}


//********************************************************************
void hnlAnalysis::FinalizeToy(){
  //********************************************************************

  AnalysisAlgorithm::FinalizeToy();

  _trexAnalysis->FinalizeToy();

  return;
}


//********************************************************************
void hnlAnalysis::FillMicroTrees(bool addBase){
  //********************************************************************
  // Variables from baseAnalysis (run, event, ...)
  if (addBase) baseAnalysis::FillMicroTreesBase(addBase);
    
  ///ToDo: proper variables filling --> careful with casting if non-TREx selection is used
  if (_useTRExSelection) 
    FillMicroTrees_TREx();

  //Fill variable which need True information
  if(!mybox().MainTrack)
    return;

  AnaTrueTrack *trueTrack = static_cast<AnaTrueTrack*>(mybox().MainTrack->TrueTrack);
  if(!trueTrack)
    return;

  AnaHNLTrueVertex* trueVertex = static_cast<AnaHNLTrueVertex*>(trueTrack->TrueVertex);
  if(!trueVertex)
    return;

  output().FillVectorVar(           weight, trueVertex->Weight);
  output().IncrementCounterForVar(  weight);

  output().FillVar(prob,      trueVertex->Probability);
  output().FillVar(hnl_mass,  trueVertex->Mass);

}


//********************************************************************
void hnlAnalysis::FillToyVarsInMicroTrees(bool addBase){
  //********************************************************************

  (void)addBase;

  // Fill the common variables
  //  if(addBase) baseAnalysis::FillToyVarsInMicroTrees(addBase);


  // Fill all tree variables that vary for each virtual analysis (toy experiment)

  // variables specific for this analysis
  if (mybox().MainTrack)
    output().FillToyVar(trexAnalysis::HMN_mom, mybox().MainTrack->Momentum);    


}

//********************************************************************
bool hnlAnalysis::CheckFillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************

  return (utils::InTPCFiducialVolume(SubDetId::kTPC, vtx.Position));
}


//********************************************************************
void hnlAnalysis::FillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************

  // Fill  the categories according to TREx definitions (FV etc)
  trex_categ_utils::FillTruthTreeCategories(vtx);

  // Event variables
  output().FillVar(run,    input().GetSpill().EventInfo->Run);
  output().FillVar(subrun, input().GetSpill().EventInfo->SubRun);
  output().FillVar(evt,    input().GetSpill().EventInfo->Event);

  // true variables
  output().FillVar(nu_pdg,        vtx.NuPDG);
  output().FillVar(nu_trueE,      vtx.NuEnergy);
  output().FillVar(nu_truereac,   vtx.ReacCode);
  output().FillVar(TruthVertexID, vtx.TruthVertexID);
  output().FillVar(RooVtxIndex,   vtx.RooVtxIndex);
  output().FillVar(RooVtxEntry,   vtx.RooVtxEntry);

  output().FillVectorVarFromArray(nu_truedir,        vtx.NuDir, 3);
  output().FillVectorVarFromArray(nu_vertex_truepos, vtx.Position, 4);

  const AnaHNLTrueVertex& trueVertex = static_cast<const AnaHNLTrueVertex&>(vtx); 

  output().FillVectorVar(weight, trueVertex.Weight);
  output().IncrementCounterForVar(weight);

  output().FillVar(prob,      trueVertex.Probability);
  output().FillVar(hnl_mass,  trueVertex.Mass);


}

//********************************************************************
void hnlAnalysis::FillCategories(){
  //********************************************************************
  // Fill the track categories for color drawing

  // For the muon candidate
  trex_categ_utils::FillCategories(_event, static_cast<AnaTrack*>(mybox().MainTrack),"");

}

//********************************************************************
void hnlAnalysis::FillMicroTrees_TREx(){
  //********************************************************************
  // Muon candidate TRUE variables
  if (mybox().MainTrack){  

    //Select the best End for the HMNT path.
    AnaTPCPath* path =static_cast<AnaTPCPath*>( mybox().MainTrack);
    AnaTPCPathEnd tmp_end;
    for(int i =0; i< path->nPathEnds; i++){
      AnaTPCPathEnd end = path->PathEnds[i];
      if((end.Flag > tmp_end.Flag) && (end.Flag != (1<<4)))
        tmp_end = end;
    }
    AnaTPCJunction* Junction = NULL;
    if(tmp_end.Junction)		Junction = tmp_end.Junction;

    //Fill variable which need True information
    if(mybox().MainTrack->TrueTrack) {
      AnaTrueTrack *true_track = static_cast<AnaTrueTrack*>(mybox().MainTrack->TrueTrack);
      if(true_track) {
        output().FillVar(                 trexAnalysis::HMN_true_mom,     true_track->Momentum);
        output().FillVectorVarFromArray(  trexAnalysis::HMN_true_pos,     true_track->Position,     4);
        output().FillVectorVarFromArray(  trexAnalysis::HMN_true_endpos,  true_track->PositionEnd,  4);
        output().FillVectorVarFromArray(  trexAnalysis::HMN_true_dir,     true_track->Direction,    3);
        ///
        Float_t tmp_array1[3];
        Float_t tmp_array2[3];
        Float_t tmp_array3[3];  	
        for(int i=0; i<3; i++){   		
          tmp_array1[i] = std::abs(mybox().MainTrack->PositionStart[i]-true_track->Position[i]); 	
          tmp_array2[i] = std::abs(mybox().MainTrack->PositionStart[i]-tmp_end.Position[i]);

          tmp_array3[i] = std::abs(tmp_end.Position[i]-true_track->Position[i]);
        }	
        output().FillVectorVarFromArray(  trexAnalysis::HMN_true_posdiff,     tmp_array1,   3);
        output().FillVectorVarFromArray(  trexAnalysis::HMN_end_posdiff,      tmp_array2,   3);
        output().FillVectorVarFromArray(  trexAnalysis::HMN_end_true_posdiff, tmp_array3,   3);

        //TODO find out if this way of TrueVertex choosing is correct since it may not represent End
        AnaTrueVertex* TrueVertex = static_cast<AnaTrueVertex*>(true_track->TrueVertex);
        if (TrueVertex){

          std::set<int> PDG;
          PDG.insert(2212);


          AnaTrueTrackB* ProtonTracks[50];	
          int Pcount = trex_analysis_utils::GetParticlesFromVertex(*TrueVertex, ProtonTracks, PDG, 0);	       
          //Fill True Proton Info
          if(Pcount > 0 ){
            std::sort(&ProtonTracks[0], &ProtonTracks[Pcount], trex_analysis_utils::CompareMomentum);        	
            output().FillVar(Proton_true_mom,ProtonTracks[0]->Momentum);

            output().FillVectorVarFromArray(  Proton_true_pos,    ProtonTracks[0]->Position, 4);
            output().FillVectorVarFromArray(  Proton_true_endpos, ProtonTracks[0]->PositionEnd, 4);
            output().FillVectorVarFromArray(  Proton_true_dir,    ProtonTracks[0]->Direction, 3);

            output().FillVar(ProtonNu_true_angle, utils::Dot(TrueVertex->NuDir, ProtonTracks[0]->Direction));
          }

          AnaTrueTrackB* MuPineutronTracks[50];  

          //loop through trajectories associated with the vertex 
          Float_t Mom = 0;
          int NoRecoAmount =0;
          for (int i=0; i<TrueVertex->nTrueTracks; i++){
            AnaTRExTrueTrack* trueTrack = static_cast<AnaTRExTrueTrack*> (TrueVertex->TrueTracks[i]);
            if(!trueTrack) continue;
            if (trueTrack->ParentPDG != 0) continue; //primary

            if(trueTrack->Charge != 0 )
              Mom += trueTrack->Momentum;

            // this is a bit complicated 
            PDG.clear();
            PDG.insert(211);
            output().FillVar(NPiPlus, trex_analysis_utils::GetParticlesFromVertex(*TrueVertex, MuPineutronTracks, PDG, 0));
            if(Junction)
              if(trueTrack->ReconPaths.size() != 0){
                for(unsigned int i = 0; i < trueTrack->ReconPaths.size(); i++){
                  for(int j = 0; j < Junction->nTPCPaths; j++){ 
                    if(trueTrack->ReconPaths[i]->UniqueID == Junction->TPCPaths[j]->UniqueID){
                      if(trueTrack->PDG == 211){
                        output().FillVar(NAssPiPlus,1);
                        output().FillVar(NOtherThenPi, (Junction->nTPCPaths-1));
                        output().FillVar(HMN_true_PiAngle, (Float_t) std::acos( (utils::ArrayToTVector3(trueTrack->Direction)) * (utils::ArrayToTVector3(true_track->Direction) ) ) ) ;
                        //Fill InvMass
                        TVector3 PiMom = ( (utils::ArrayToTVector3(trueTrack->Direction) ) * (trueTrack->Momentum) );
                        TVector3 MuMom = ( (utils::ArrayToTVector3(true_track->Direction) ) * (true_track->Momentum) );				          			
                        Float_t MomSq = (Float_t) (PiMom + MuMom).Mag2();
                        Float_t EPi = (Float_t) std::sqrt( (trueTrack->Momentum)*(trueTrack->Momentum) + 139.57018*139.57018);
                        //if(true_track->PDG == 13)
                        Float_t EMu = (Float_t) std::sqrt( (true_track->Momentum)*(true_track->Momentum) + 105.6583715*105.6583715);
                        //else
                        //Float_t EMu = 0;

                        output().FillVar(InvMass,	(EPi+EMu)*(EPi+EMu) - MomSq);
                      }	
                    }
                  }
                }		            	
              }

            if(trueTrack->ReconPaths.size()==0){
              NoRecoAmount++;
              output().FillVectorVar( trexAnalysis::NoRecoPDG,       trueTrack->PDG);
              output().FillVectorVar( trexAnalysis::NoRecoMomentum,  trueTrack->Momentum);

              output().FillVectorVar( trexAnalysis::NoRecoLength,				(Float_t)((utils::ArrayToTLorentzVector(trueTrack->Position)).Vect()-
                    (utils::ArrayToTLorentzVector(trueTrack->PositionEnd)).Vect()).Mag());

              output().IncrementCounterForVar( trexAnalysis::NoRecoLength);
            }
          }

          output().FillVar( trexAnalysis::NoRecoAmount, (      trex_analysis_utils::GetParticlesNumberFromVertex(*TrueVertex,0) - NoRecoAmount));
          output().FillVar( trexAnalysis::NVertex_true_tracks, trex_analysis_utils::GetParticlesNumberFromVertex(*TrueVertex,0));
          output().FillVar( trexAnalysis::VertexCleanliness,   trex_analysis_utils::VertexCleanlinessOrComp(tmp_end, TrueVertex, mybox(), false));
          output().FillVar( trexAnalysis::VertexCompleteness,  trex_analysis_utils::VertexCleanlinessOrComp(tmp_end, TrueVertex, mybox(), true ));       
          output().FillVar( trexAnalysis::Total_true_mom,      Mom);
          output().FillVar( NProton_true_Paths,  Pcount);
        }
      }
    }
    //Fill Reco variables
    output().FillVar(                 trexAnalysis::HMN_costheta, mybox().MainTrack->DirectionStart[2]);
    output().FillVectorVarFromArray(  trexAnalysis::HMN_pos,      mybox().MainTrack->PositionStart,   4);		//Those varibale may be should be taken from PathEnd!!
    output().FillVectorVarFromArray(  trexAnalysis::HMN_endpos,   mybox().MainTrack->PositionEnd,     4);			//	
    output().FillVectorVarFromArray(  trexAnalysis::HMN_dir,      mybox().MainTrack->DirectionStart,  3);
    output().FillVar(                 trexAnalysis::HMN_phi,      (Float_t)(utils::ArrayToTVector3(mybox().MainTrack->DirectionStart).Phi()));
    //Lot's of Recon stuff
    if(Junction){

      int Nfitted= 0 , NProtons =0;
      Float_t Tot_reco_mom = 0;
      Float_t lowest_reco_mom = -999.;
      int NAssPaths = 0;
      Float_t MomDiff = 9999.;
      Float_t Lowestangle = 90;
      AnaTPCPath* ProtonPaths[NMAXTPCJUNCTIONPATHS];
      AnaTPCPath* VertexPositivePaths[NMAXTPCJUNCTIONPATHS];
      int PosPCounter = 0;

      for(int i = 0; i < Junction->nTPCPaths; i++){
        AnaTPCPath* tmp_path = Junction->TPCPaths[i];

        //Fill info for Pi+ cut tuning
        if(tmp_path){
          if(trex_selection_utils::GetLeadingCharge(tmp_path) > 0.9){
            output().FillVectorVar(PiLikHood, anaUtils::GetPIDLikelihood(*tmp_path,3));
            Float_t cut1 =  ((anaUtils::GetPIDLikelihood(*tmp_path,0)+anaUtils::GetPIDLikelihood(*tmp_path,3))/(1.-anaUtils::GetPIDLikelihood(*tmp_path,2)));    		
            output().FillVectorVar(Cut1Value, cut1);
            output().FillVectorVar(CutMom, tmp_path->Momentum);     
            output().IncrementCounterForVar(CutMom);
          }
        }
        //Fill momentum difference and Angle
        for(int j = 0; j < Junction->nTPCPaths; j++){
          if(i == j) continue;
          if((trex_selection_utils::MuonPIDCut(tmp_path))&&(trex_selection_utils::MuonPIDCut(Junction->TPCPaths[j]))){
            Float_t diff = std::abs(tmp_path->Momentum - Junction->TPCPaths[j]->Momentum);
            if(diff < MomDiff) MomDiff = diff;	
          }
          Float_t angle =  std::acos(std::abs(utils::Dot(tmp_path->DirectionStart, Junction->TPCPaths[j]->DirectionStart)));
          angle = angle*(180.0/3.14159265359);
          if(angle < Lowestangle) Lowestangle = angle;
        }

        //Fill NAssociated paths
        Float_t diffS = (Float_t) ((utils::ArrayToTLorentzVector(tmp_path->PositionStart)).Vect()-utils::ArrayToTVector3(tmp_end.Position)).Mag();
        Float_t diffE = (Float_t) ((utils::ArrayToTLorentzVector(tmp_path->PositionEnd)).Vect()-utils::ArrayToTVector3(tmp_end.Position)).Mag();			
        if(std::min(diffS,diffE) < _AssPathDist) NAssPaths++; 

        //Fill other
        if(tmp_path->Charge > 0)                           //TODO Enable charge swap check
          VertexPositivePaths[PosPCounter++] = tmp_path;
        if((tmp_path->LikFit)&&(tmp_path->Success)){				
          Nfitted++;
          Tot_reco_mom += tmp_path->Momentum;
          if(tmp_path->Momentum < std::abs(lowest_reco_mom))
            lowest_reco_mom = tmp_path->Momentum;
          if(anaUtils::GetPIDLikelihood(*tmp_path, 2, true) > _protonLHoodCut){
            ProtonPaths[NProtons] = tmp_path;
            NProtons++; 
          }	
        }
      }

      //Fill proton variables
      if(PosPCounter > 0){
        std::sort(VertexPositivePaths, VertexPositivePaths+PosPCounter, AnaTrackB::CompareMomentum); 
        output().FillVar(LMPT_mom,VertexPositivePaths[PosPCounter-1]->Momentum);
      }	
      if(NProtons > 0){
        std::sort(                        ProtonPaths,              ProtonPaths+NProtons,                         AnaTrackB::CompareMomentum);
        output().FillVar(                 ProtonCandidate_mom,      ProtonPaths[0]->Momentum); 	
        output().FillVar(                 ProtonMu_reco_HighAngle,  utils::Dot(mybox().MainTrack->DirectionStart, ProtonPaths[0]->DirectionStart));
        output().FillVectorVarFromArray(  Proton_reco_pos,          ProtonPaths[0]->PositionStart,                4);
        output().FillVectorVarFromArray(  Proton_reco_endpos,       ProtonPaths[0]->PositionEnd,                  4);
        output().FillVar(                 ProtonPath_reco_length,   static_cast<AnaTpcTrack*>(ProtonPaths[0]->TPCSegments[0])->Length	); // !!!See definition
      }
      output().FillVar(NProtonPaths,NProtons);

      //Fill other
      output().FillVar( trexAnalysis::NPaths,           tmp_end.Junction->nTPCPaths);
      output().FillVar( trexAnalysis::NFittedPaths,     Nfitted); 	
      output().FillVar( trexAnalysis::Total_reco_mom,   Tot_reco_mom);
      output().FillVar( trexAnalysis::Lowest_reco_mom,  lowest_reco_mom);
      output().FillVar( trexAnalysis::NAssociatedPaths, NAssPaths);
      output().FillVar( trexAnalysis::MuMomDiff,        MomDiff);
      output().FillVar( trexAnalysis::MuLowestAngle,    Lowestangle);  	

      ///
      Float_t tmp_array[3];  	
      for(int i=0; i<3; i++)   		
        tmp_array[i] = std::abs(Junction->MaximumCoordinates[i]-Junction->MinimumCoordinates[i]); 	
      output().FillVectorVarFromArray( trexAnalysis::VertexExtent, tmp_array, 3);
    }
    else{
      output().FillVar( trexAnalysis::NPaths,                  1);
      output().FillVar( trexAnalysis::NFittedPaths,            1);
      output().FillVar( NProtonPaths,                          0);
      output().FillVar( trexAnalysis::Total_reco_mom,          mybox().MainTrack->Momentum);
      output().FillVar( trexAnalysis::Lowest_reco_mom,         mybox().MainTrack->Momentum);
      output().FillVar( trexAnalysis::NAssociatedPaths,        0);
      output().FillVar( trexAnalysis::trexAnalysis::MuMomDiff, 9999.);
      output().FillVar( trexAnalysis::MuLowestAngle,           90.);  
    }		

  }
}

