#include "hnlTRExSelection.hxx"

//********************************************************************
hnlTRExSelection::hnlTRExSelection(bool forceBreak): SelectionBase(forceBreak) {
  //********************************************************************

  // define the steps
  DefineSteps();  
}

//********************************************************************
void hnlTRExSelection::DefineSteps(){
  //********************************************************************

  // Cuts must be added in the right order
  // last "true" means the step sequence is broken if cut is not passed (default is "false")
  // Add inclusive CC steps to all branches
  
  // Copy all steps from the numuCCSelection
  CopySteps(_trexSelection);
  
  AddSplit(2);
  
  AddStep(0, StepBase::kCut,     "2 tracks in Junction",         								            new TwoTracksCut()             					);
//  AddStep(0,StepBase::kCut,     "looking for 1 Pi+ in path PathEnd "  ,                       new PionCut()     							        );
  
  //AddStep(1,StepBase::kCut,     "looking for 1 Pi+ in path PathEnd "  ,                       new PionCut()     							        );
  //AddStep(1,StepBase::kCut,     "Exactly 2 tracks connected to Junction",                     new TwoTracksCut()             					);
  
  AddStep(1, StepBase::kCut,     "reject any MainTrack with >0 associated protons ",        new ProtonMultiplicityCut()             );

  SetBranchAlias(0,"2track",0);
  SetBranchAlias(1,"Proton",1);
  //SetBranchAlias(2,"Proton",2);
}

//********************************************************************
bool FillSummaryAction_HNL_TREx::Apply(AnaEventB& event, ToyBoxB& box) const{
  //********************************************************************
  if(!box.MainTrack) return 1;
  

  // The lepton candidate is the HMN track in the FV
  event.Summary->LeptonCandidate[nd280Samples::kUnassigned] = box.MainTrack;

  for(int i = 0; i < 4; ++i){
    event.Summary->VertexPosition[nd280Samples::kUnassigned][i] = box.MainTrack->PositionStart[i];
  }

  if(box.MainTrack->TrueTrack) event.Summary->TrueVertex[nd280Samples::kUnassigned] = box.MainTrack->TrueTrack->TrueVertex;

  return 1;
}

//********************************************************************
bool hnlTRExSelection::FillEventSummary(AnaEventB& event, Int_t allCutsPassed[]){
  //********************************************************************

  // The event sample corresponding to this selection
  if(allCutsPassed[0])
    event.Summary->EventSample = nd280Samples::kUnassigned;

  return true; //no sample for this topology so far

}



//**************************************************
bool ProtonMultiplicityCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  ToyHNL_TRExBox& box_trex = static_cast<ToyHNL_TRExBox&>(box);

  if (!box_trex.CandidatePaths[0])
    return false;



  AnaTPCPath* MuonPath = box_trex.CandidatePaths[0];
  AnaTPCPathEnd tmp_end;
  for(int i =0; i< MuonPath->nPathEnds; i++){
    AnaTPCPathEnd end = MuonPath->PathEnds[i];
    if((end.Flag > tmp_end.Flag) && (end.Flag != (1<<4)))
      tmp_end = end;
  }

  if (!tmp_end.Junction)	return false;

  AnaTPCJunction* junction = tmp_end.Junction;
  for(int i =0; i < junction->nTPCPaths; i++){
    AnaTPCPath* path = junction->TPCPaths[i];
    //if(trex_selection_utils::GetLeadingCharge(path) < 1.)  								//TODO 1) Seems these charge check kills lot's of true protons
    //continue;
    if(anaUtils::GetPIDLikelihood(*path, 2, true) > _protonLHoodCut)	//2)Try to look over all pattern and count distance between all protons and junction(may find
      return true;   																									//Use LeapFrog  for protons ?
  }

  return false;

}

//**************************************************
bool PionCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  ToyHNL_TRExBox&    box_trex          = static_cast<ToyHNL_TRExBox&>(box);  

  if (!box_trex.CandidatePaths[0])
    return false;

  AnaTPCPath* MuonPath = box_trex.CandidatePaths[0];
  AnaTPCPathEnd tmp_end;
  for(int i =0; i< MuonPath->nPathEnds; i++){
    AnaTPCPathEnd end = MuonPath->PathEnds[i];
    if((end.Flag > tmp_end.Flag) && (end.Flag != (1<<4)))
      tmp_end = end;
  }
  //Look for all junction path and try to find Pion
  if (tmp_end.Junction){
    AnaTPCJunction* junction = tmp_end.Junction;
    for(int i =0; i < junction->nTPCPaths; i++){
      AnaTPCPath* path = junction->TPCPaths[i];
      if(trex_selection_utils::GetLeadingCharge(path) < 1.)  								
        continue;
      if (hnl_trex_selection_utils::PionPID(path, _useOldSecondaryPID))
        return true;
    } 
  }
  //If failed try to find all path with close to tmp_end Start/End postion TODO Think about LeapFrog(cant see any use now)
  for (int i =0; i < box_trex.nPurePaths; i++){
    AnaTPCPath* path = box_trex.PurePaths[i];
    if(trex_selection_utils::GetLeadingCharge(path) < 1.)  								
      continue;
    if (hnl_trex_selection_utils::PionPID(path, _useOldSecondaryPID)){
      Float_t diffS = ( (utils::ArrayToTLorentzVector(path->PositionEnd) ).Vect()-utils::ArrayToTVector3(tmp_end.Position) ).Mag();
      Float_t diffE = ( (utils::ArrayToTLorentzVector(path->PositionEnd) ).Vect()-utils::ArrayToTVector3(tmp_end.Position) ).Mag();
      if( (diffS  < 100) || (diffE  < 100) )
        return true;
    }			
  }    	
  return false;
}

//**************************************************
bool TwoTracksCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  ToyHNL_TRExBox& box_trex = static_cast<ToyHNL_TRExBox&>(box);

  if (!box_trex.CandidatePaths[0])
    return false;

  AnaTPCPath* MuonPath = box_trex.CandidatePaths[0];
  AnaTPCPathEnd tmp_end;
  for(int i =0; i< MuonPath->nPathEnds; i++){
    AnaTPCPathEnd end = MuonPath->PathEnds[i];
    if((end.Flag > tmp_end.Flag) && (end.Flag != (1<<4)))
      tmp_end = end;
  }

  if (!tmp_end.Junction)	return false;
  if (tmp_end.Junction->nTPCPaths != 2) return false;

  return true;
}

  



//*********************************************************************
bool hnl_trex_selection_utils::PionPID(const AnaTPCPath* path, bool useOldSecondaryPID){
  //*********************************************************************
  if(useOldSecondaryPID){

    if ( anaUtils::GetPIDLikelihood(*path,3) < 0.3  ) return false; //TODO HardCoded for now
    Float_t cut1 = (anaUtils::GetPIDLikelihood(*path,0)+anaUtils::GetPIDLikelihood(*path,3))/(1.-anaUtils::GetPIDLikelihood(*path,2)); 
    if( path->Momentum < 500.  && cut1 < 0.8 ) return false;
    return true;
  }
  //Some tricky way to find Pi+ stolen from numuMultiPi 
  else {
    // For Positive tracks we distinguish pions, electrons and protons.
    Float_t ElLklh = anaUtils::GetPIDLikelihood(*path,1);  
    Float_t ProtonLklh = anaUtils::GetPIDLikelihood(*path,2); 
    Float_t PionLklh = anaUtils::GetPIDLikelihood(*path,3); 
    Float_t norm = ElLklh+ProtonLklh+PionLklh;
    ProtonLklh /= norm; 
    ElLklh /= norm; 
    PionLklh /= norm; 

    if( ProtonLklh > ElLklh && ProtonLklh > PionLklh ) return false; // If the highest probability is a proton continue. 

    // Id associated to the largest of the two probabilities.
    if( PionLklh > ElLklh )
      return true;
    //else 																																//This is interesting for proton selection
    //	if( path->Momentum > 900. ) continue; // This is mainly protons.

  }
  return false;
}

