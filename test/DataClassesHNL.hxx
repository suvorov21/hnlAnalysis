#pragma once
#ifndef DataClassesHNL_hxx
#define DataClassesHNL_hxx

#include "DataClassesTREx.hxx"

/// true vertex with the information specific to HNL search: addded
class AnaHNLTrueVertex: public AnaTRExTrueVertex{
  
public:
  AnaHNLTrueVertex(){
    Mass        = kUnassigned;
    Weight      = kUnassigned;
    Probability = kUnassigned;
  }
  virtual ~AnaHNLTrueVertex(){}
  
  void Print() const;
  
  /// Clone this object.
  virtual AnaHNLTrueVertex* Clone() {
    return new AnaHNLTrueVertex(*this);
  }

  ///This is the information relevant for HNL analsysis, can create one more class for this but seems fine to have it here
  ///Mass of the HNL that corresponds to vertex
  Float_t Mass;
  
  ///These two vars exist e.g. for G4PrimaryVertex (and corresponding rooTracker)
  ///Will be used with more or less the same meaning
   
  /// The weight of the interaction.  This will be set to one if the
  /// interaction is not reweighted.  If the vertex is oversampled, this
  /// will be less than one. This is used if the generator is using
  /// a weighted sample (e.g. over sampling the high energy part of the
  /// neutrino spectrum smooth out the distributions)
  Float_t Weight;

  /// The overall probability of the interaction (HNL decay) that created this vertex.
  /// This includes the effect of the cross section, path length through the
  /// material, etc.  This will be one if it is not filled.
  Float_t Probability;
  


protected:
  /// Copy constructor is protected, as Clone() should be used to copy this object.
  AnaHNLTrueVertex(const AnaHNLTrueVertex& vertex);


}; 


#endif


