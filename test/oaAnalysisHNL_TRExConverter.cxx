#include "oaAnalysisHNL_TRExConverter.hxx"

//*****************************************************************************
bool oaAnalysisHNL_TRExConverter::FillTrueVertexRooInfo(AnaTrueVertex* vertex) {
//*****************************************************************************
 
  if (!vertex)
    return false;
  
  //HNL are saved in NEUT RooTracker format
  if (!Neut)
    return false;
    
  AnaHNLTrueVertex* trueVertex = static_cast<AnaHNLTrueVertex*>(vertex); 
  
  bool status = oaAnalysisTreeConverter::FillTrueVertexRooInfo(trueVertex);
  
  //for the moment
  if (!status)
    return status;
 
  status = false;
  
  //loop throught the vertex: yep, this will be additional loop to base class call, 
  //but dont want to copy the full method to this converter
  for (int roov = 0; roov < NNVtx; roov++) {
    ND::NRooTrackerVtx *lvtx = (ND::NRooTrackerVtx*) (*NVtx)[roov];

    // this is needed otherwise when running with highland for pro6 over a prod5 file
    // it crashes before doing the versioning check
    if ( ! lvtx->StdHepPdg) continue;

    // look for the current vertex
    if (vertex->TruthVertexID != lvtx->TruthVertexID) continue;
    
    //fill the vars
    trueVertex->Weight      = lvtx->EvtWght;
    trueVertex->Probability = lvtx->EvtProb;
    //this how we store it
    trueVertex->Mass        = lvtx->EvtXSec;
    
    
    status = true;
    
    break; 
  }
  
  return status;
   

}



