#include "hnlAnalysisUtils.hxx"
#include "CutUtils.hxx"

//********************************************************************
Float_t hnl_analysis_utils::GetPIDLikelihood(const AnaTrackB& track, int hypo) {
  //********************************************************************

  UInt_t itrk = track.Index;

  if( hypo >= 4 ) return -1.e+6; 
  if( itrk >= NMAXTRACKS ) return -1e+6; // Protection against values out of the vector. 

  Float_t Likelihood[4];

  Double_t prob[4]={1,1,1,1};
  Double_t tmp_prob[3][4];
  Double_t total_prob=0;
  bool found=false;


  // Get the TPC with max nodes. We should make sure that at least the segment in that TPC has the proper PID info
  AnaSubTrackB* subTrack = anaUtils::GetSegmentWithMostNodesInDet(track, SubDetId::kTPC);

  if (!subTrack)
    return -1e+6;;

  SubDetId::SubDetEnum maxnodestpc = SubDetId::GetSubdetectorEnum(subTrack->Detector); 
  
 
  AnaTpcTrackB* segmentsInTPC[3];
  for(int i = 0; i < 3; ++i){
    segmentsInTPC[i] = NULL;
    for (Int_t j=0;j<4;j++){
      Likelihood[j]=-1;
      tmp_prob[i][j] = 1;
    }
  }

  // Loop over TPC segments
  for (int j = 0; j < track.nTPCSegments; ++j){      
    AnaTpcTrackB* TPCSegment = track.TPCSegments[j];
    if (!TPCSegment) continue;
    // Only segments passing the TPC track quality cut will contribute to the likelihood
    if (!cutUtils::TPCTrackQualityCut(*TPCSegment)) continue;

    // Require valid values for all quantities involved
    if( TPCSegment->dEdxexpMuon==-0xABCDEF || TPCSegment->dEdxexpEle==-0xABCDEF || TPCSegment->dEdxexpPion==-0xABCDEF || TPCSegment->dEdxexpProton==-0xABCDEF) continue;
    if( TPCSegment->dEdxMeas ==-0xABCDEF ) continue; 
    if( TPCSegment->dEdxexpMuon==-99999 || TPCSegment->dEdxexpEle==-99999 || TPCSegment->dEdxexpPion==-99999 || TPCSegment->dEdxexpProton==-99999) continue;
    if( TPCSegment->dEdxMeas ==-99999 ) continue; 

    Float_t pulls[4];  

    anaUtils::ComputeTPCPull(*TPCSegment,pulls);
    Float_t pullmu  =     pulls[0];
    Float_t pullele =     pulls[1];
    Float_t pullp   =     pulls[2];
    Float_t pullpi  =     pulls[3];

    if (!TMath::Finite(pullmu) || !TMath::Finite(pullele) || !TMath::Finite(pullp) || !TMath::Finite(pullpi)) continue;
    if (pullmu  != pullmu || pullele != pullele || pullp   != pullp || pullpi  != pullpi) continue;

    SubDetId::SubDetEnum det = SubDetId::GetSubdetectorEnum(TPCSegment->Detector);

    // To avoid mismatching between FlatTree and oaAnalysis we allow only one segment per TPC to be included in the likelihood, the one with more nodes
    if (segmentsInTPC[det-2]){
      if (TPCSegment->NNodes > segmentsInTPC[det-2]->NNodes){ 
        segmentsInTPC[det-2] = TPCSegment;
        tmp_prob[det-2][0] = exp(-pow(pullmu, 2)/2);
        tmp_prob[det-2][1] = exp(-pow(pullele, 2)/2);
        tmp_prob[det-2][2] = exp(-pow(pullp, 2)/2);
        tmp_prob[det-2][3] = exp(-pow(pullpi, 2)/2);
      }            
    }
    else{ 
      segmentsInTPC[det-2] = TPCSegment;      
      tmp_prob[det-2][0] = exp(-pow(pullmu, 2)/2);
      tmp_prob[det-2][1] = exp(-pow(pullele, 2)/2);
      tmp_prob[det-2][2] = exp(-pow(pullp, 2)/2);
      tmp_prob[det-2][3] = exp(-pow(pullpi, 2)/2);
    }
  }

  // Loop over all segments contributing to the likelihood and compute it
  for (int tpc=0;tpc<3;tpc++){
    if (segmentsInTPC[tpc]){ 
      // The pull should be already corrected by all corrections (CT and CT expected)
      prob[0] *= tmp_prob[tpc][0]; 
      prob[1] *= tmp_prob[tpc][1];
      prob[2] *= tmp_prob[tpc][2];
      prob[3] *= tmp_prob[tpc][3];

      if (SubDetId::GetDetectorUsed(segmentsInTPC[tpc]->Detector, maxnodestpc)) found = true;
    }
  }

  // If at least the segment in the TPC with max nodes has a  valid PID info
  if (found){
    for (int h=0;h<4;h++){
      total_prob += prob[h] ;
    }

    if (total_prob>0){
      for (int h=0;h<4;h++){
        Likelihood[h] = prob[h]/total_prob ;
      }
    }
  }



  return Likelihood[hypo];

}


