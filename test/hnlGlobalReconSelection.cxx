#include "hnlGlobalReconSelection.hxx"
#include "baseSelection.hxx"
#include "CutUtils.hxx"
#include "EventBoxUtils.hxx"
#include "VersioningUtils.hxx"
#include "SystematicUtils.hxx"
#include "hnlAnalysisUtils.hxx"

//********************************************************************
hnlGlobalReconSelection::hnlGlobalReconSelection(bool forceBreak): SelectionBase(forceBreak) {
  //********************************************************************
  // Define the steps
  DefineSteps();

  // The detector in which the selection is applied
  _detectorFV = SubDetId::kTPC;
}

//********************************************************************
void hnlGlobalReconSelection::DefineSteps(){
  //********************************************************************

  // Cuts must be added in the right order
  // last "true" means the step sequence is broken if cut is not passed (default is "false")
  AddStep(StepBase::kCut,     "event quality",              new EventQualityCut(),           true);

  // find primary global vertex in FV:: it will be the main one
  AddStep(StepBase::kAction,  "find vertex",                new hnl_global_selection::FindGlobalVertexAction()); 

  // fill track candidates: constituents of the global vertex
  AddStep(StepBase::kAction,  "fill track candidates",      new hnl_global_selection::FindCandidateTracksAction());
  
  // fill external veto tracks
  AddStep(StepBase::kAction,  "fill external veto tracks",  new hnl_global_selection::FindExternalVetoTracksAction());
  
  // fill track candidates: constituents of the global vertex
  AddStep(StepBase::kAction,  "fill event summary",         new hnl_global_selection::FillSummaryAction());
  
  // CUTS:
  // 1. HMN  and HMP candidates exist
  AddStep(StepBase::kCut,     "vertex FV cut",              new hnl_global_selection::TwoProductsWithOppositeChargeCut());
  
  // 2. Veto external tracks
  AddStep(StepBase::kCut,     "external veto cut",          new hnl_global_selection::ExternalVetoTracksCut());
  
  // 3. HNL reconstructed theta w.r.t.  to beam axis 
  AddStep(StepBase::kCut,     "HNL theta cut",              new hnl_global_selection::HNLThetaCut());

  // 4. exactly two products: should be reviewed to make it more fancy (e.g. a fake vertex due to ~collinear tracks) 
  AddStep(StepBase::kCut,     "Global Vertex products cut", new hnl_global_selection::GlobalVertexNProductsCut());
  

  //split the selection into two groups: HMP is a pion/HMN is a pion
  AddSplit(2);

  //5.0
  AddStep(0, StepBase::kCut,     "HMP  PID pion cut",       new hnl_global_selection::PionPIDCut(false));

  // mu- + pi+ and e- + pi+ modes
  AddSplit(0);

  //5.0.0 mu- + pi+
  AddStep(0, 0, StepBase::kCut,     "HMN  PID muon cut",      new hnl_global_selection::MuonPIDCut());
  //5.0.1 e- + pi+
  AddStep(0, 1, StepBase::kCut,     "HMN  PID electron cut",  new hnl_global_selection::ElectronPIDCut());

  
  
  //5.1
  AddStep(0, StepBase::kCut,     "HMN  PID pion cut",       new hnl_global_selection::PionPIDCut());

  // mu+ + pi- and e+ + pi- modes
  AddSplit(1);

  //5.1.0 mu+ + pi-
  AddStep(1, 0, StepBase::kCut,     "HMP  PID muon cut",      new hnl_global_selection::MuonPIDCut(false));
  //5.1.1 e+ + pi-
  AddStep(1, 1, StepBase::kCut,     "HMP  PID electron cut",  new hnl_global_selection::ElectronPIDCut(false));


  SetBranchAlias(0, "mum + pip", 0, 0);
  SetBranchAlias(1, "ele + pip", 0, 1);
  SetBranchAlias(2, "mup + pim", 1, 0);
  SetBranchAlias(3, "pos + pim", 1, 1);
}

//**************************************************
void hnlGlobalReconSelection::InitializeEvent(AnaEventB& event){
  //**************************************************
  //ToDo!
  return; 
}

//********************************************************************
bool hnlGlobalReconSelection::FillEventSummary(AnaEventB& event, Int_t allCutsPassed[]){
  //********************************************************************

  if(allCutsPassed[0] || allCutsPassed[1] || allCutsPassed[2] || allCutsPassed[3]){
    event.Summary->EventSample = nd280Samples::kUnassigned;
  }
  return (event.Summary->EventSample != nd280Samples::kUnassigned);
}

//********************************************************************
bool hnl_global_selection::FillSummaryAction::Apply(AnaEventB& event, ToyBoxB& box) const{
  //********************************************************************
  if (!box.Vertex) return 1;

  for (int i = 0; i < 4; ++i){
    event.Summary->VertexPosition[nd280Samples::kUnassigned][i] = box.Vertex->Position[i];
  }

  event.Summary->TrueVertex[nd280Samples::kUnassigned] = box.Vertex->TrueVertex;

  return true;
}

//**************************************************
bool hnl_global_selection::FindGlobalVertexAction::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  // reset the vertex 
  box.Vertex = NULL;

  //Should be reviewed:
  //we want the primary vertex to be in TPC FV
  //AnaVertex(ices) are already sorted in increasing primary index,  we need the first primary one
  AnaVertexB* vertex_tmp = NULL;
  for (int i=0; i<event.nVertices; i++){
    vertex_tmp = event.Vertices[i];
    if (vertex_tmp)
      break;
  } 
  //should have a valid vertex
  if (!vertex_tmp)
    return false;

  //vertex should be in TPC FV
  if (!utils::InTPCFiducialVolume(SubDetId::kTPC, vertex_tmp->Position))
    return false;

  box.Vertex = vertex_tmp;
  return true;

}

//**************************************************
bool hnl_global_selection::FindCandidateTracksAction::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  //look at the constituents of the global vertex
  if (!box.Vertex)
    return false;

  box.MainTrack = box.HMNtrack = box.HMPtrack = NULL;

  //tracks are already sorted in momentum, so look for HMN and HMP ones
  for (int i=0; i<box.Vertex->nTracks; i++){
    AnaTrackB* track = box.Vertex->Tracks[i];
    if (!track) continue;

    //apply the (TPC) quality cuts,  modified one dealing with the TPC with the max nodes
    if (!hnl_global_actions::TrackQualityCut(*track, _min_tpc_nodes))
      continue;

    //HMN track
    if (track->Charge == -1)
      box.HMNtrack = track;

    if (track->Charge == 1)
      box.HMPtrack = track;

  }

  box.MainTrack = box.HMNtrack;

  return true;

}

//**************************************************
bool hnl_global_selection::TwoProductsWithOppositeChargeCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)(event);

  if (!box.HMPtrack || !box.HMNtrack)
    return false;

  return true;
}

//**************************************************
bool hnl_global_selection::HNLThetaCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)(event);

  if (!box.HMPtrack || !box.HMNtrack)
    return false;
  
  TVector3 vect1 = utils::ArrayToTVector3(box.HMNtrack->DirectionStart);
  vect1 *= box.HMNtrack->Momentum;
  
  TVector3 vect2 = utils::ArrayToTVector3(box.HMPtrack->DirectionStart);
  vect2 *= box.HMPtrack->Momentum;
  
  TVector3 orig = (vect1 + vect2).Unit();
  
  return (orig.Theta()>_min_cos_theta);

}

//**************************************************
bool hnl_global_selection::HNLInvMassCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)(event);

  if (!box.HMPtrack || !box.HMNtrack)
    return false;
  
  Float_t inv_mass = anaUtils::ComputeInvariantMass(*box.HMNtrack, *box.HMPtrack, _massHMN, _massHMP);
  
  
  return (inv_mass>_min_inv_mass && inv_mass<_max_inv_mass);

}

//**************************************************
bool hnl_global_selection::GlobalVertexNProductsCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)(event);
  if (!box.Vertex)
    return false;

  //exactly two constituents
  if (box.Vertex->nTracks!=2)
    return false;

  return true;

}

//**************************************************
bool hnl_global_selection::PionPIDCut::Apply(AnaEventB& event, ToyBoxB& box) const{
//**************************************************

  AnaTrackB* track = _useHMN ? box.HMNtrack : box.HMPtrack;

  if (!track) return false;
  
  return ApplyPionPID(*track); 
}

//**************************************************
bool hnl_global_selection::PionPIDCut::ApplyPionPID(const AnaTrackB& track) const{
  //**************************************************
  
  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(track,0),
    hnl_analysis_utils::GetPIDLikelihood(track,1), 
    hnl_analysis_utils::GetPIDLikelihood(track,2), 
    hnl_analysis_utils::GetPIDLikelihood(track,3)};

  if ((( PIDLikelihood[0] + PIDLikelihood[3])/(1- PIDLikelihood[2]) > _mip_lhood_cut || track.Momentum > _min_mom_cut) &&
      PIDLikelihood[3] > _pion_lhood_cut)
    return true;

  return false;

}

//**************************************************
bool hnl_global_selection::ElectronPIDCut::Apply(AnaEventB& event, ToyBoxB& box) const{
//**************************************************

  AnaTrackB* track = _useHMN ? box.HMNtrack : box.HMPtrack;

  if (!track) return false;
  
  return ApplyElectronPID(*track); 
}


//**************************************************
bool hnl_global_selection::ElectronPIDCut::ApplyElectronPID(const AnaTrackB& track) const{
  //**************************************************
  
  //min momentum cut
  if (track.Momentum<_min_momentum) return false;
  
  if (track.nTPCSegments == 0) return false;

  AnaSubTrackB* subTrack = anaUtils::GetSegmentWithMostNodesInDet(track, SubDetId::kTPC);

  if (!subTrack)
    return false;

  AnaTpcTrack* tpcTrack = static_cast<AnaTpcTrack*>(subTrack); 

  Float_t pulls[4];

  // Pulls are: Muon, Electron, Proton, Pion
  anaUtils::ComputeTPCPull(*tpcTrack,pulls);

  //  Float_t pullmuon = pulls[0];
  Float_t pullelec = pulls[1];
  //  Float_t pullprot = pulls[2];
  //  Float_t pullpion = pulls[3];


  //  if (pullmuon > _pullmu_reject_min && pullmuon < _pullmu_reject_max) return false;
  //  if (pullpion > _pullpi_reject_min && pullpion < _pullpi_reject_max) return false;
  if (pullelec < _pullel_accept_min || pullelec > _pullel_accept_max) return false;

  //  if (pullelec < _pullel_accept_tight_min || pullelec > _pullel_accept_tight_max) return false;

  return true;
}

//**************************************************
bool hnl_global_selection::MuonPIDCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  AnaTrackB* track = _useHMN ? box.HMNtrack : box.HMPtrack;

  if (!track) return false;

  return _useHMN ? ApplyMuonPID(*track) : ApplyAntiMuonPID(*track);
}

//**************************************************
bool hnl_global_selection::MuonPIDCut::ApplyMuonPID(const AnaTrackB& track) const {
  //**************************************************
  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(track,0),
    hnl_analysis_utils::GetPIDLikelihood(track,1), 
    hnl_analysis_utils::GetPIDLikelihood(track,2), 
    hnl_analysis_utils::GetPIDLikelihood(track,3)};

  if (((PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]) > _cut1_mu || track.Momentum > _mom_cut ) && (PIDLikelihood[0]>_cut2_mu))
    return true; 

  return false;

}

//**************************************************
bool hnl_global_selection::MuonPIDCut::ApplyAntiMuonPID(const AnaTrackB& track) const {
  //**************************************************

  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(track,0),
    hnl_analysis_utils::GetPIDLikelihood(track,1), 
    hnl_analysis_utils::GetPIDLikelihood(track,2), 
    hnl_analysis_utils::GetPIDLikelihood(track,3)};

  if (((PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]) > _cut1_antimu || track.Momentum > _mom_cut ) &&
      (PIDLikelihood[0]>_cut2_antimu && PIDLikelihood[0]<_cut3_antimu)){
    return true; 
  }

  return false;

}

//**************************************************
bool hnl_global_actions::TrackQualityCut(AnaTrackB& track, Int_t min_tpc_nodes){
  //**************************************************

  if(track.TPCQualityCut > -1){
    return track.TPCQualityCut;
  }
  // Gets all segments in the TPC with most nodes
  AnaSubTrackB* TPCSegment = anaUtils::GetSegmentWithMostNodesInDet(track, SubDetId::kTPC);

  if (TPCSegment){
    bool passed = (TPCSegment->NNodes>min_tpc_nodes);
    track.TPCQualityCut = passed;
    return passed;
  }

  track.TPCQualityCut = 0;
  return false;

}


