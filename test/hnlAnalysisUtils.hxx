#ifndef hnlAnalysisUtils_h
#define hnlAnalysisUtils_h

#include "trexUtils.hxx"

namespace hnl_analysis_utils{
  Float_t GetPIDLikelihood(const AnaTrackB&,  int hypo);
}

#endif
