#ifndef hnlGlobalReconSelection_h
#define hnlGlobalReconSelection_h

#include "trexSelection.hxx"
#include "Parameters.hxx"

class hnlGlobalReconSelection: public SelectionBase{
public:
  hnlGlobalReconSelection(bool forceBreak=true);
  virtual ~hnlGlobalReconSelection(){}

  //---- These are mandatory functions
  void DefineSteps();
  ToyBoxB* MakeToyBox(){return new ToyTRExBox();}
  bool FillEventSummary(AnaEventB& event, Int_t allCutsPassed[]);
  nd280Samples::SampleEnum GetSampleEnum(){return nd280Samples::kUnassigned;}
  void InitializeEvent(AnaEventB& event);
};

///actions
namespace hnl_global_actions{
  bool TrackQualityCut(AnaTrackB& track, Int_t min_tpc_nodes = 18); 
}


/// out all the classes into a dedicated namespace so to allow solving the name conflicts later
namespace hnl_global_selection{
  class FillSummaryAction: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;  
    StepBase* MakeClone(){return new FillSummaryAction();}
  };
  
  /// find primary global vertex
  class FindGlobalVertexAction: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindGlobalVertexAction();}
  };
  
  /// find external tracks for veto
  class FindExternalVetoTracksAction: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const {(void)(event); (void)(box); return true;}
    StepBase* MakeClone(){return new FindExternalVetoTracksAction();}
  };
  
  /// veto on external tracks
  class ExternalVetoTracksCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const{(void)(event); (void)(box); return true;}
    StepBase* MakeClone(){return new ExternalVetoTracksCut();}
  };
  
  /// cut on HNL angle w.r.t. to beam axis
  class HNLThetaCut: public StepBase{
  public:
    HNLThetaCut(){
      _min_cos_theta = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.HNL.MinCosTheta");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNLThetaCut();}
  private:
    Float_t _min_cos_theta;
    
  };
 
  /// cut on the reconstructed HNL invariant mass
  class HNLInvMassCut: public StepBase{
  public:
    HNLInvMassCut(Float_t mass_center, Float_t massHMN, Float_t massHMP){
      _min_inv_mass = mass_center - (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.HNL.InvMass.Min");
      _max_inv_mass = mass_center + (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.HNL.InvMass.Max");
      _massHMN      = massHMN;
      _massHMP      = massHMP;
      _mass_center  = mass_center;
    }
    HNLInvMassCut(Float_t mass_center, Float_t cut1, Float_t cut2, Float_t massHMN, Float_t massHMP){
       _min_inv_mass  = mass_center - cut1;
       _max_inv_mass  = mass_center + cut2;
       _massHMN       = massHMN;
       _massHMP       = massHMP;
       _mass_center   = mass_center; 
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNLInvMassCut(_mass_center, _min_inv_mass, _max_inv_mass, _massHMN, _massHMP);}
  private:
    Float_t _min_inv_mass;
    Float_t _max_inv_mass;
    Float_t _massHMN;
    Float_t _massHMP;
    Float_t _mass_center;
  };
  
  /// fill HMN and HMP candidates
  class FindCandidateTracksAction: public StepBase{
  public:
    FindCandidateTracksAction(){
      _min_tpc_nodes = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Global.TrackQuality.MinTPCNodes");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindCandidateTracksAction();}
  private:
    Int_t _min_tpc_nodes;
  };
  
  /// HMN and HMP candidates exist
  class TwoProductsWithOppositeChargeCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TwoProductsWithOppositeChargeCut();}
  };
  
  /// HMN and HMP candidates exist
  class GlobalVertexNProductsCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new GlobalVertexNProductsCut();}
  };
  
  /// muon PID
  class MuonPIDCut: public StepBase{
  public:
    MuonPIDCut(bool useHMN = true){
      _useHMN = useHMN;
      _mom_cut      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.MuonPID.MomCut");
      _cut1_mu      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.MuonPID.Cut1");
      _cut2_mu      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.MuonPID.Cut2");
      _cut1_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.AntiMuonPID.Cut1");  
      _cut2_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.AntiMuonPID.Cut2");
      _cut3_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.AntiMuonPID.Cut3");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new MuonPIDCut(_useHMN);}
  
  private:
    bool ApplyMuonPID(const AnaTrackB& track) const;
    bool ApplyAntiMuonPID(const AnaTrackB& track) const;

    bool _useHMN;
    Float_t _cut1_mu;
    Float_t _cut2_mu;
    Float_t _cut1_antimu;
    Float_t _cut2_antimu;
    Float_t _cut3_antimu;
    Float_t _mom_cut; 
  };
  
  /// pion PID
  class PionPIDCut: public StepBase{
  public:
    PionPIDCut(bool useHMN = true){
      _useHMN = useHMN;
      _mip_lhood_cut  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.PionPID.MipLHood");
      _pion_lhood_cut = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.PionPID.PionLHood");
      _min_mom_cut    = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.PionPID.MomentumCut");
      
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new PionPIDCut(_useHMN);}
  
  private:
    bool ApplyPionPID(const AnaTrackB& track) const;
    bool _useHMN;
    Float_t _mip_lhood_cut;
    Float_t _pion_lhood_cut;
    Float_t _min_mom_cut; 
  };
  
  /// electron PID
  class ElectronPIDCut: public StepBase{
  public:
    ElectronPIDCut(bool useHMN = true){
    _useHMN = useHMN;
    _pullmu_reject_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullMuonMin");
    _pullmu_reject_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullMuonMax");
    _pullpi_reject_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullPionMin");
    _pullpi_reject_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullPionMax");
    _pullel_accept_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullElecMin");
    _pullel_accept_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullElecMax");
    _pullel_accept_tight_min  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullElecTightMin");
    _pullel_accept_tight_max  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.PullElecTightMax");

    _min_momentum         = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.MinMomentum");
    _max_momentum_pos     = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.Global.ElePos.MaxMomentum.Pos");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new ElectronPIDCut(_useHMN);}
  
  private:
    bool ApplyElectronPID(const AnaTrackB& track) const;
    bool _useHMN;
    //cuts to be read from NuE selection
    Float_t _pullmu_reject_min;
    Float_t _pullmu_reject_max;
    Float_t _pullpi_reject_min;
    Float_t _pullpi_reject_max;
    Float_t _pullel_accept_min;
    Float_t _pullel_accept_max;  
    Float_t _pullel_accept_tight_min;
    Float_t _pullel_accept_tight_max;
    Float_t _min_momentum;
    Float_t _max_momentum_pos;

  };


}
#endif
