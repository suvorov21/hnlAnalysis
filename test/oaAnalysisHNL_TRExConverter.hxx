#ifndef oaAnalysisHNL_TRExConverter_h
#define oaAnalysisHNL_TRExConverter_h

#include "oaAnalysisTRExConverter.hxx"
#include "DataClassesHNL.hxx"

class oaAnalysisHNL_TRExConverter: public oaAnalysisTRExConverter{

 public:

  oaAnalysisHNL_TRExConverter(){}
  virtual ~oaAnalysisHNL_TRExConverter(){}
 
  //----------------
  virtual AnaTrueVertexB*   MakeTrueVertex()  { return new AnaHNLTrueVertex(); }
  
  /// Override the base class so to be able to retrieve info for HNL
  virtual bool FillTrueVertexRooInfo(AnaTrueVertex* vertex);

};


#endif

