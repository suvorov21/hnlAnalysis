#include "hnlGlobalReconSelection.hxx"
#include "EventBoxUtilsHNL.hxx"
#include "AnalysisUtils.hxx"
#include "hnlAnalysisUtils.hxx"
#include "baseSelection.hxx"
#include "SystIdHNL.hxx"
#include "EventBoxId.hxx"
#include "NuDirUtils.hxx"
#include "KinematicsUtils.hxx"

//********************************************************************
hnlGlobalReconSelection::hnlGlobalReconSelection(bool forceBreak): SelectionBase(forceBreak,  EventBoxId::kEventBoxTracker) {
  //********************************************************************

 // _nBinSigma            = (Int_t)   ND::params().GetParameterD("hnlAnalysis.Veto.FGDTimeBin.NBinSigma");
 // _binWidth             = (Float_t) ND::params().GetParameterD("hnlAnalysis.Veto.FGDTimeBin.BinWidth");
  _binTimeOffsetToBunch = (Float_t) ND::params().GetParameterD("hnlAnalysis.Veto.FGDTimeBin.TimeOffsetToBunch");
  _trim_p0d             = (Float_t) ND::params().GetParameterD("hnlAnalysis.Veto.P0D.Trim");
}


//********************************************************************
void hnlGlobalReconSelection::DefineDetectorFV() {
  //********************************************************************
  SetDetectorFV(SubDetId::kTPC);
}

//********************************************************************
void hnlGlobalReconSelection::DefineSteps(){
  //********************************************************************

  // Cuts must be added in the right order
  // last "true" means the step sequence is broken if cut is not passed (default is "false")
  AddStep(StepBase::kCut,     "event quality",                new EventQualityCut(),           true);
  // find HNL candidate in FV
  AddStep(StepBase::kAction,  "find candidate",               new hnl_global_selection::FindHNLCandidateAction()); 
  // find all tracks in the same TPC as HNL_candidate
  AddStep(StepBase::kAction,  "find TPC tracks",              new hnl_global_selection::FindSameTPCTracks());

  // fill track candidates: constituents of the global vertex
  AddStep(StepBase::kAction,  "fill event summary",           new hnl_global_selection::FillSummaryAction());

  // Cuts 
  // 1. Should have a candidate in FV and with good quality of both daughter tracks 
  AddStep(StepBase::kCut,  "Quality and fiducial",            new hnl_global_selection::HNL_QualityFiducialCut());

  // 2. Should have a candidate in FV and with good quality of both daughter tracks 
//  AddStep(StepBase::kCut,  "Vertex time",                     new hnl_global_selection::HNL_VertexTimeCut());

  // 2. Upstream veto
  AddStep(StepBase::kCut,  "Upstream veto",                   new hnl_global_selection::UpstreamActivityVetoCut());

  // 3. HMP or HMN use the same detector as vertex
  AddStep(StepBase::kCut,  "Track use Vertex TPC",            new hnl_global_selection::EventUseVertexTPC());

  // 4. TPC Additional Activity Cut
  AddStep(StepBase::kCut,  "TPCAdditionalActivity",           new hnl_global_selection::TPCAdditionalActivityCut());

  //split the selection into two groups: HMP is a pion/HMN is a pion
  AddSplit(3);

  //5.0
  AddStep(0, StepBase::kCut,        "HMP  PID pion cut",     new hnl_global_selection::PositivePionPIDCut());
  // mu- + pi+ and e- + pi+ modes
  AddSplit(2, 0);

  //6.0.0 mu- + pi+
  AddStep(0, 0, StepBase::kCut,     "HMN  PID muon cut",            new hnl_global_selection::MuonPIDCut());
  //7.0.0 invariant mass
  AddStep(0, 0, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kMumPip));
  //8.0.0 HNL reconstructed angle theta to Z axis 
  AddStep(0, 0, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kMumPip));
  //9.0.0 Reconstructed relative angle between products of HNL decay 
  AddStep(0, 0, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kMumPip));



  //6.0.1 e- + pi+
  AddStep(0, 1, StepBase::kCut,     "HMN  PID electron cut",        new hnl_global_selection::ElectronPIDCut());
  //7.0.1
  AddStep(0, 1, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kElePip));
  //8.0.1 HNL reconstructed angle theta to Z axis 
  AddStep(0, 1, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kElePip));
  //9.0.1 Reconstructed relative angle between products of HNL decay 
  AddStep(0, 1, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kElePip));

  //5.1
  AddStep(1, StepBase::kCut,        "HMN  PID pion cut",      new hnl_global_selection::NegativePionPIDCut());

  // mu+ + pi- and e+ + pi- modes
  AddSplit(2, 1);

  //6.1.0 mu+ + pi-
  AddStep(1, 0, StepBase::kCut,     "HMP  PID muon cut",            new hnl_global_selection::AntiMuonPIDCut());
  //7.1.0
  AddStep(1, 0, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kMupPim));
  //8.1.0 HNL reconstructed angle theta to Z axis 
  AddStep(1, 0, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kMupPim));
  //9.1.0 Reconstructed relative angle between products of HNL decay 
  AddStep(1, 0, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kMupPim));

  //6.1.1 e+ + pi-
  AddStep(1, 1, StepBase::kCut,     "HMP  PID electron cut",        new hnl_global_selection::PositronPIDCut());
  //7.1.1
  AddStep(1, 1, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kPosPim));
  //8.1.1 HNL reconstructed angle theta to Z axis 
  AddStep(1, 1, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kPosPim));
  //9.1.1 Reconstructed relative angle between products of HNL decay 
  AddStep(1, 1, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kPosPim));

  // study di-muon mode
  //5.2
  AddStep(2, StepBase::kCut,     "HMN  PID muon cut",            new hnl_global_selection::MuonPIDCut());
  //6.2
  AddStep(2, StepBase::kCut,     "HMP  PID muon cut",            new hnl_global_selection::AntiMuonPIDCut());
  // 7.2
  AddStep(2, StepBase::kCut,     "Tracks have segment in ECal",  new hnl_global_selection::ECaLSegmentCut());
  //8.2 NOT electron like track in ECal
  AddStep(2, StepBase::kCut,     "Not EM shower in ECal",        new hnl_global_selection::ECaLShowerCut());
  //9.2 Kinematics cuts
  AddStep(2, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kDiMuon));

  SetBranchAlias(0, "mum + pip", 0, 0);
  SetBranchAlias(1, "ele + pip", 0, 1);
  SetBranchAlias(2, "mup + pim", 1, 0);
  SetBranchAlias(3, "pos + pim", 1, 1);
  SetBranchAlias(4, "mum+mup+nu", 2);
}

//**************************************************
void hnlGlobalReconSelection::InitializeEvent(AnaEventC& eventC){
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  // Create the appropriate EventBox if it does not exist yet
  if (!event.EventBoxes[EventBoxId::kEventBoxTracker])
    event.EventBoxes[EventBoxId::kEventBoxTracker] = new EventBoxHNL();

  EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  // fill P0D veto tracks
  // Don't fill it when already filled by other selection
  if (!EventBox->RecObjectsInGroup[EventBoxHNL::kP0DVetoTracks]){

    AnaTrackB* tracks[NMAXPARTICLES];

    UInt_t count = 0;
    for (int it = 0; it < event.nParticles; it++) {
      AnaTrackB* track = static_cast<AnaTrackB*>(event.Particles[it]);
      if (!track) continue;
      if (!anaUtils::TrackUsesOnlyDet(*track, SubDetId::kP0D)) continue;

      //should have an end close to downstream edge
      if (!hnl_global_selection::IsP0dDownstream(track->PositionStart, _trim_p0d) &&
          !hnl_global_selection::IsP0dDownstream(track->PositionEnd, _trim_p0d)) continue;

      if (count>=NMAXPARTICLES) continue;

      tracks[count] = track;
      ++count;
    }

    // Fill the box
    anaUtils::CreateArray( EventBox->RecObjectsInGroup[EventBoxHNL::kP0DVetoTracks], count);
    anaUtils::CopyArray(tracks, EventBox->RecObjectsInGroup[EventBoxHNL::kP0DVetoTracks], count);
    EventBox->nRecObjectsInGroup[EventBoxHNL::kP0DVetoTracks] = count;

  }

  // Fill FGD time bins   
  Float_t selTime = _bunchingHNL.GetBunchCentralTime(event, event.Bunch); 
  
  EventBox->BunchCentralTime = selTime;  

  // Initialize arrays
  anaUtils::CreateArray(EventBox->FgdTimeBins[0], event.nFgdTimeBins);
  anaUtils::CreateArray(EventBox->FgdTimeBins[1], event.nFgdTimeBins);


  // Loop through the FGD time bins and get those satisfying time cuts
  for( Int_t i = 0; i < event.nFgdTimeBins; i++ ){

    AnaFgdTimeBinB *FgdTimeBin = event.FgdTimeBins[i];

    if (!FgdTimeBin) continue;

    Float_t binTime = FgdTimeBin->MinTime - _binTimeOffsetToBunch;

    
    // Should correspond to the current bunch
    // false --> i.e. not cosmic mode
    if (_bunchingHNL.GetBunch(binTime, event.EventInfo.Run, event.GetIsMC(), false) != event.Bunch) continue;


    EventBox->FgdTimeBins[0][EventBox->nFgdTimeBins[0]++] = FgdTimeBin;
    EventBox->FgdTimeBins[1][EventBox->nFgdTimeBins[1]++] = FgdTimeBin;

  }
  // resize the arrays
  anaUtils::ResizeArray(EventBox->FgdTimeBins[0], EventBox->nFgdTimeBins[0], event.nFgdTimeBins);
  anaUtils::ResizeArray(EventBox->FgdTimeBins[1], EventBox->nFgdTimeBins[1], event.nFgdTimeBins);


  // sort separately FGD1 and FGD2 time bins
  if (EventBox->nFgdTimeBins[0]>0){ 
    std::sort(EventBox->FgdTimeBins[0], EventBox->FgdTimeBins[0] + EventBox->nFgdTimeBins[0], 
        hnl_analysis_utils::CompareFgdBinsNHits(SubDetId::kFGD1));
  }

  if (EventBox->nFgdTimeBins[1]>0){ 
    std::sort(EventBox->FgdTimeBins[1], EventBox->FgdTimeBins[1] + EventBox->nFgdTimeBins[1], 
        hnl_analysis_utils::CompareFgdBinsNHits(SubDetId::kFGD2));
  }


  // True tracks and recon tracks groups
  boxUtils::FillTracksWithTPC(event, SubDetId::kFGD1);
  boxUtils::FillTracksWithTPC(event, SubDetId::kFGD2);
  boxUtils::FillTrajsChargedInTPC(event);

  // tracks for ECal systematics
  boxUtils::FillTracksWithECal(event);
  boxUtils::FillTrajsInECal(event);

  // Fill the true tracks related to the HNL vertex
  boxUtils::FillTrajsChargedFromHNLVertex(event);  

  return; 
}

//********************************************************************
bool hnlGlobalReconSelection::FillEventSummary(AnaEventC& eventC, Int_t allCutsPassed[]){
  //********************************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

 (void)allCutsPassed;
  
  static_cast<AnaEventSummaryB*>(event.Summary)->EventSample =  SampleId::kUnassigned;
  
  return (static_cast<AnaEventSummaryB*>(event.Summary)->EventSample != SampleId::kUnassigned);
}

//********************************************************************
bool hnl_global_selection::FillSummaryAction::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //********************************************************************
 
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyHNLBoxB& box = static_cast<ToyHNLBoxB&>(boxB);
  
  if (!box.Vertex) return 1;

  for (int i = 0; i < 4; ++i){
    static_cast<AnaEventSummaryB*>(event.Summary)->VertexPosition[SampleId::kUnassigned][i] = box.Vertex->Position[i];
  }

  static_cast<AnaEventSummaryB*>(event.Summary)->TrueVertex[SampleId::kUnassigned] = box.Vertex->TrueVertex;

  return true;
}

//**************************************************
bool hnl_global_selection::FindHNLCandidateAction::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB& box = static_cast<ToyHNLBoxB&>(boxB);
  
  // reset the vertex 
  if (box.Vertex) delete box.Vertex;
  box.Vertex = NULL;

  // reset the candidate
  ToyHNLBoxB& hnlBox = static_cast<ToyHNLBoxB&>(box);
  if (hnlBox.hnlCandidate) delete hnlBox.hnlCandidate;
  hnlBox.hnlCandidate = NULL;

  //AnaVertex(ices) are already sorted in increasing primary index,  we need the first primary one
  AnaVertexB* vertex_tmp = NULL;
  for (int i=0; i<event.nVertices; i++){
    vertex_tmp = event.Vertices[i];
    if (vertex_tmp)
      break;
  }

  //should have a valid vertex
  if (!vertex_tmp)
    return false;

  // vertex should have two tracks
  if (vertex_tmp->nParticles !=2) return false;
  if (!vertex_tmp->Particles[0] || !vertex_tmp->Particles[1]) return false;

  // vertex should be in FV of one of the TPC + identify the veto type needed
  bool inFV = false;

  if (hnl_analysis_utils::InTPCFV(SubDetId::kTPC1, vertex_tmp->Position)){
    inFV = true;
    hnlBox.VetoType = SubDetId::kTPC1;
  }
  else if (hnl_analysis_utils::InTPCFV(SubDetId::kTPC2, vertex_tmp->Position)){
    inFV = true;
    hnlBox.VetoType = SubDetId::kTPC2;
  }
  else if (hnl_analysis_utils::InTPCFV(SubDetId::kTPC3, vertex_tmp->Position)){
    inFV = true;
    hnlBox.VetoType = SubDetId::kTPC3;
  }

  if (!inFV) return false;

  // clone vertex instead of use same pointer so it belongs to the box (in case needed)
  box.Vertex = vertex_tmp->Clone(); 

  box.Vertex->Particles[0] = NULL;
  box.Vertex->Particles[1] = NULL;

  for (Int_t i = 0; i < event.nParticles; ++i) {
    if (event.Particles[i]->UniqueID == vertex_tmp->Particles[0]->UniqueID)
      box.Vertex->Particles[0] = event.Particles[i];
    if (event.Particles[i]->UniqueID == vertex_tmp->Particles[1]->UniqueID)
      box.Vertex->Particles[1] = event.Particles[i];
  }

  // in case if one track is placed in the other event
  if (!box.Vertex->Particles[0] || !box.Vertex->Particles[1])
    return false;

  // build a candidate
  hnlBox.hnlCandidate = new AnaHNLCandidate(box.Vertex);

  // fill the main track 
  box.MainTrack = hnlBox.hnlCandidate->trackNegative;
  box.HMNtrack = hnlBox.hnlCandidate->trackNegative;
  box.HMPtrack  = hnlBox.hnlCandidate->trackPositive;

  if (!hnlBox.hnlCandidate->trackNegative || !hnlBox.hnlCandidate->trackPositive)
    return true;
  hnlBox.HMPEndTrackDet   = (Int_t)anaUtils::GetDetector(hnlBox.hnlCandidate->trackPositive->PositionEnd);
  hnlBox.HMNEndTrackDet   = (Int_t)anaUtils::GetDetector(hnlBox.hnlCandidate->trackNegative->PositionEnd);

  hnlBox.HMNStartTrackDet = (Int_t)anaUtils::GetDetector(hnlBox.hnlCandidate->trackNegative->PositionStart);

  // calculate kinanatics in traverse plane
  TVector3 nuVect = anaUtils::GetNuDirRec(hnlBox.hnlCandidate->Vertex->Position);
  Float_t nuArr[3];
  anaUtils::VectorToArray(nuVect, nuArr);

  Float_t Dir[3];
  hnlBox.hnlCandidate->GetDirection(Dir);

  hnlBox.dpt   = anaUtils::GetTransverseMom(nuArr, Dir, hnlBox.hnlCandidate->GetMomentum());
  hnlBox.dphit = anaUtils::GetDPhi(nuArr, hnlBox.hnlCandidate->trackPositive->DirectionStart, hnlBox.hnlCandidate->trackNegative->DirectionStart); 
  //hnlBox.dat   = 

  if (!hnlBox.hnlCandidate->trackNegative->GetTrueParticle() || !hnlBox.hnlCandidate->trackPositive->GetTrueParticle())
    return true;

  hnlBox.HMPEndTrackDetTrue = (Int_t)anaUtils::GetDetector(hnlBox.hnlCandidate->trackPositive->GetTrueParticle()->PositionEnd);
  hnlBox.HMNEndTrackDetTrue = (Int_t)anaUtils::GetDetector(hnlBox.hnlCandidate->trackNegative->GetTrueParticle()->PositionEnd);

  return true;

}

//**************************************************
bool hnl_global_selection::FindSameTPCTracks::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  
  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  // need succesfull HNLcandadate for right TPC selection
  if (!hnlBox->hnlCandidate) 
    return false;

  // initialize array
  anaUtils::CreateArray(hnlBox->SameTPCTracks, event.nParticles);  

  for (int it = 0; it < event.nParticles; it++) {

    AnaTrackB* track = static_cast<AnaTrackB*>(event.Particles[it]);

    if (!track) continue;
    if (anaUtils::GetDetector(track->PositionStart) != hnlBox->VetoType) continue;

    hnlBox->SameTPCTracks[++hnlBox->nSameTPCTracks-1] = track;
    if (!hnl_global_actions::TrackQualityCut(*track, _min_tpc_nodes)) continue;
    if (track->Momentum < _min_momentum) continue;
    ++hnlBox->nSameTPCqualtyTracks;
  }

  anaUtils::ResizeArray(hnlBox->SameTPCTracks, hnlBox->nSameTPCTracks, event.nParticles);

  return true;
}

//**************************************************
bool hnl_global_selection::HNL_QualityFiducialCut::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate) return false;

  if (!hnlBox->hnlCandidate->IsSuccess()) return false;

  //apply the (TPC) quality cuts,  modified one dealing with the TPC with the max nodes
  if (!hnl_global_actions::TrackQualityCut(*hnlBox->hnlCandidate->trackNegative, _min_tpc_nodes))
    return false;

  if (!hnl_global_actions::TrackQualityCut(*hnlBox->hnlCandidate->trackPositive, _min_tpc_nodes))
    return false;

  // low momentum tracks are recognized as recon errors
  if (hnlBox->hnlCandidate->trackNegative->Momentum < _min_momentum) return false;
  if (hnlBox->hnlCandidate->trackPositive->Momentum < _min_momentum) return false;

  // track should go out of vertex but not towards
  if (anaUtils::GetSeparationSquared(hnlBox->hnlCandidate->Vertex->Position, hnlBox->hnlCandidate->trackNegative->PositionStart) >
      anaUtils::GetSeparationSquared(hnlBox->hnlCandidate->Vertex->Position, hnlBox->hnlCandidate->trackNegative->PositionEnd))
    return false;

  if (anaUtils::GetSeparationSquared(hnlBox->hnlCandidate->Vertex->Position, hnlBox->hnlCandidate->trackPositive->PositionStart) >
      anaUtils::GetSeparationSquared(hnlBox->hnlCandidate->Vertex->Position, hnlBox->hnlCandidate->trackPositive->PositionEnd))
    return false;

  // vertex should have proper chi2 and position variance
  AnaVertex* Vertex = static_cast<AnaVertex*>(hnlBox->hnlCandidate->Vertex);
  if (Vertex->Chi2 > _max_chi2) return false;

  Float_t var = sqrt(Vertex->Variance[0]*Vertex->Variance[0] + Vertex->Variance[1]*Vertex->Variance[1] + Vertex->Variance[2]*Vertex->Variance[2]);
  if (var > _max_var) return false;

  return true;
} 

//**************************************************
bool hnl_global_selection::UpstreamActivityVetoCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  
  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate) return false;

  if (!event.EventBoxes[EventBoxId::kEventBoxTracker])
    return false;

  EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  switch (hnlBox->VetoType){
    case SubDetId::kTPC1:
      // no activity in P0D
      return (EventBox->nRecObjectsInGroup[EventBoxHNL::kP0DVetoTracks] == 0);
      break;
      //Cuts on FGD activity,  for the moment use the time-bin with the most hits (and in the bunch window)
    case SubDetId::kTPC2:
      // no activity in FGD1
      return (EventBox->nFgdTimeBins[0] == 0 || 
          EventBox->FgdTimeBins[0][0]->NHits[0] < _max_fgd1_hits);
    case SubDetId::kTPC3:
      // no activity in FGD2
      return (EventBox->nFgdTimeBins[1] == 0 || 
          EventBox->FgdTimeBins[1][0]->NHits[1] < _max_fgd2_hits);
      break;
    default:
      break;
  }
  return false;
}

//**************************************************
bool hnl_global_selection::TPCAdditionalActivityCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate)
    return false;

  if (hnlBox->nSameTPCTracks > 2)
    return false;

  if (hnlBox->nSameTPCTracks == 1 && hnlBox->hnlCandidate->IsSuccess()) {
    Int_t unique = hnlBox->SameTPCTracks[0]->UniqueID;
    if (hnlBox->hnlCandidate->trackPositive->UniqueID == unique || hnlBox->hnlCandidate->trackNegative->UniqueID == unique)
      return true;
    else return false; 
  }

  if (hnlBox->nSameTPCTracks == 2 && hnlBox->hnlCandidate->IsSuccess()) {
    Int_t unique1 = hnlBox->SameTPCTracks[0]->UniqueID;
    Int_t unique2 = hnlBox->SameTPCTracks[1]->UniqueID;

    if ((hnlBox->hnlCandidate->trackPositive->UniqueID == unique1 && hnlBox->hnlCandidate->trackNegative->UniqueID == unique2) ||
        (hnlBox->hnlCandidate->trackPositive->UniqueID == unique2 && hnlBox->hnlCandidate->trackNegative->UniqueID == unique1))
      return true;
    else return false;
  }

  return false;
}

//**************************************************
bool hnl_global_selection::EventUseVertexTPC::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate)
    return false;
  if (!hnlBox->hnlCandidate->trackPositive || !hnlBox->hnlCandidate->trackNegative)
    return false;

  if (SubDetId::GetDetectorUsed(hnlBox->hnlCandidate->trackPositive->Detector, hnlBox->VetoType) || 
      SubDetId::GetDetectorUsed(hnlBox->hnlCandidate->trackNegative->Detector, hnlBox->VetoType))
    return true;

  return false;
}

//**************************************************
bool hnl_global_selection::HNLThetaCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event; 
  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate) return false;
  Float_t Dir[3];
  hnlBox->hnlCandidate->GetDirection(Dir);

  return (Dir[2] > _min_cos_theta);
}

//**************************************************
bool hnl_global_selection::HNLRelAngleCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;
  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  if (!hnlBox->hnlCandidate) return false;
  
  return (std::cos(hnlBox->hnlCandidate->GetRelAngle()) > _min_rel_cos);
}

//**************************************************
bool hnl_global_selection::HNLInvMassCut::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)(event);

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);

  if (!hnlBox->hnlCandidate) return false;

  if (_mode == AnaHNLCandidate::kModeCounter) return false;

  Float_t mass = hnlBox->hnlCandidate->GetMass(_mode);

  return (mass > _min_inv_mass && mass < _max_inv_mass);

}

//**************************************************
bool hnl_global_selection::ECaLSegmentCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************
 
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  (void)(event);

  ToyHNLBoxB& hnlBox = static_cast<ToyHNLBoxB&>(box);

  if (!hnlBox.hnlCandidate) return false;

  AnaTrackB* trackN = hnl_global_selection::GetNegativeTrack(hnlBox);
  AnaTrackB* trackP = hnl_global_selection::GetPositiveTrack(hnlBox);

  if (!trackN || !trackP)
    return false;

  AnaParticleB* HMNsubTrack = anaUtils::GetSegmentWithMostNodesInDet(*trackN, SubDetId::kECAL);
  AnaParticleB* HMPsubTrack = anaUtils::GetSegmentWithMostNodesInDet(*trackP, SubDetId::kECAL);

  // study muon track --> should be long track with interactions in ECaL
  if (!HMNsubTrack || !HMPsubTrack)
    return false;

  AnaECALParticleB* HMNecalTrack = static_cast<AnaECALParticleB*>(HMNsubTrack);
  AnaECALParticleB* HMPecalTrack = static_cast<AnaECALParticleB*>(HMPsubTrack);

  // cross-check
  if (!HMNecalTrack || !HMPecalTrack)
    return false;

  return true;

}

//**************************************************
bool hnl_global_selection::ECaLShowerCut::Apply(AnaEventC& eventC, ToyBoxB& box) const {
  //**************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)(event);

  ToyHNLBoxB& hnlBox = static_cast<ToyHNLBoxB&>(box);

  if (!hnlBox.hnlCandidate) return false;

  AnaTrackB* trackN = hnl_global_selection::GetNegativeTrack(hnlBox);
  AnaTrackB* trackP = hnl_global_selection::GetPositiveTrack(hnlBox);

  if (!trackN || !trackP)
    return false;

  AnaParticleB* HMNsubTrack = anaUtils::GetSegmentWithMostNodesInDet(*trackN, SubDetId::kECAL);
  AnaParticleB* HMPsubTrack = anaUtils::GetSegmentWithMostNodesInDet(*trackP, SubDetId::kECAL);

  // study muon track --> should be long track with interactions in ECaL
  if (!HMNsubTrack || !HMPsubTrack)
    return false;

  AnaECALParticleB* HMNecalTrack = static_cast<AnaECALParticleB*>(HMNsubTrack);
  AnaECALParticleB* HMPecalTrack = static_cast<AnaECALParticleB*>(HMPsubTrack);

  // cross-check
  if (!HMNecalTrack || !HMPecalTrack)
    return false;

  // ECaL segment should be muon like but not EM like
  return(HMNecalTrack->PIDMipEm < 0 && HMPecalTrack->PIDMipEm < 0);

}

//**************************************************
bool hnl_global_selection::PionPIDCut::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;  

  ToyHNLBoxB* hnlBox  = static_cast<ToyHNLBoxB*>(&box);

  AnaTrackB* track    = GetTrack(*hnlBox);

  if (!track) return false;

  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(*track,0),
    hnl_analysis_utils::GetPIDLikelihood(*track,1), 
    hnl_analysis_utils::GetPIDLikelihood(*track,2), 
    hnl_analysis_utils::GetPIDLikelihood(*track,3)};

  if ((( PIDLikelihood[0] + PIDLikelihood[3])/(1- PIDLikelihood[2]) > _mip_lhood_cut || track->Momentum > _min_mom_cut) &&
      PIDLikelihood[3] > _pion_lhood_cut)
    return true;

  return false;
}

//**************************************************
bool hnl_global_selection::ElectronPIDCut::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB* hnlBox  = static_cast<ToyHNLBoxB*>(&box);

  if (!hnlBox->hnlCandidate) return false;

  if (!hnlBox->hnlCandidate->trackNegative) return false;

  AnaTrackB* track    = GetTrack(*hnlBox);

  if (!track) return false;

  if (track->nTPCSegments == 0) return false;

  AnaParticleB* subTrack = anaUtils::GetSegmentWithMostNodesInDet(*track, SubDetId::kTPC);

  if (!subTrack)
    return false;

  AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(subTrack); 

  Float_t pulls[4];

  // Pulls are: Muon, Electron, Proton, Pion
  anaUtils::ComputeTPCPull(*tpcTrack,pulls);

  Float_t pullmuon = pulls[0];
  Float_t pullelec = pulls[1];
  //Float_t pullprot = pulls[2];
  Float_t pullpion = pulls[3];

  if (track->Charge < 0) {
    hnlBox->pullmuon = pulls[0];
    hnlBox->pullelec = pulls[1];
    hnlBox->pullprot = pulls[2];
    hnlBox->pullpion = pulls[3];
  } else hnlBox->pullpos = pulls[1];

  if (pullmuon > _pullmu_reject_min && pullmuon < _pullmu_reject_max) return false;
  if (pullpion > _pullpi_reject_min && pullpion < _pullpi_reject_max) return false;
  if ((pullelec > _pullel_accept_min) && (pullelec < _pullel_accept_max)) return true;

  return false;
}

//**************************************************
bool hnl_global_selection::MuonPIDCut::Apply(AnaEventC& eventC, ToyBoxB& box) const{
  //**************************************************  

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);
  AnaTrackB* track = GetTrack(*hnlBox);

  if (!track) return false;

  //AnaTrackB* tr = static_cast<const AnaTrackB*>(track);

  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(*track,0),
    hnl_analysis_utils::GetPIDLikelihood(*track,1), 
    hnl_analysis_utils::GetPIDLikelihood(*track,2), 
    hnl_analysis_utils::GetPIDLikelihood(*track,3)};

  if (track->Charge < 0) {
    hnlBox->cut1_mu = (PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]);
    hnlBox->cut2_mu = PIDLikelihood[0];
    hnlBox->mip_lhood = (PIDLikelihood[0] + PIDLikelihood[3])/(1- PIDLikelihood[2]);
    hnlBox->pion_lhood = PIDLikelihood[3];
  }
  return track->Charge<0 ? ApplyMuonPID(*track) : ApplyAntiMuonPID(*track);
}

//**************************************************
bool hnl_global_selection::MuonPIDCut::ApplyMuonPID(const AnaTrackB& track) const {
  //**************************************************

  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(track,0),
    hnl_analysis_utils::GetPIDLikelihood(track,1), 
    hnl_analysis_utils::GetPIDLikelihood(track,2), 
    hnl_analysis_utils::GetPIDLikelihood(track,3)};

  if (((PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]) > _cut1_mu || track.Momentum > _mom_cut ) && (PIDLikelihood[0]>_cut2_mu))
    return true; 

  return false;

}

//**************************************************
bool hnl_global_selection::MuonPIDCut::ApplyAntiMuonPID(const AnaTrackB& track) const {
  //**************************************************

  Float_t PIDLikelihood[4] = {hnl_analysis_utils::GetPIDLikelihood(track,0),
    hnl_analysis_utils::GetPIDLikelihood(track,1), 
    hnl_analysis_utils::GetPIDLikelihood(track,2), 
    hnl_analysis_utils::GetPIDLikelihood(track,3)};

  if (((PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]) > _cut1_antimu || track.Momentum > _mom_cut ) &&
      (PIDLikelihood[0]>_cut2_antimu && PIDLikelihood[0]<_cut3_antimu)){
    return true; 
  }

  return false;

}

//**************************************************
bool hnl_global_actions::TrackQualityCut(AnaTrackB& track, Int_t min_tpc_nodes){
  //**************************************************

  if(track.TPCQualityCut > -1){
    return track.TPCQualityCut;
  }
  // Gets all segments in the TPC with most nodes
  AnaParticleB* TPCSegment = anaUtils::GetSegmentWithMostNodesInDet(track, SubDetId::kTPC);

  if (TPCSegment){
    bool passed = (TPCSegment->NNodes>min_tpc_nodes);
    track.TPCQualityCut = passed;
    return passed;
  }

  track.TPCQualityCut = 0;
  return false;

}

// Utils
//**************************************************
AnaTrackB* hnl_global_selection::GetNegativeTrack(ToyHNLBoxB& box){
  //**************************************************
  if (!box.hnlCandidate) return NULL;
  if (!box.hnlCandidate->trackNegative) return NULL;
  return box.hnlCandidate->trackNegative; 
}

//**************************************************
AnaTrackB* hnl_global_selection::GetPositiveTrack(ToyHNLBoxB& box){
  //**************************************************
  if (!box.hnlCandidate) return NULL;
  if (!box.hnlCandidate->trackPositive) return NULL;
  return box.hnlCandidate->trackPositive; 
}

//**************************************************
bool hnl_global_selection::IsP0dDownstream(Float_t* pos, Float_t trim){
  //**************************************************
  return DetDef::p0dmax[2] - pos[2] < trim;   
}

//**************************************************
bool hnl_global_selection::IsFgd1Downstream(Float_t* pos, Float_t trim){ 
  //**************************************************
  return DetDef::fgd1max[2] - pos[2] < trim;   
}

//**************************************************
bool hnl_global_selection::IsFgd2Downstream(Float_t* pos, Float_t trim){
  //**************************************************
  return DetDef::fgd2max[2] - pos[2] < trim;   
}

//**************************************************
bool hnlGlobalReconSelection::IsRelevantRecObjectForSystematicInToy(const AnaEventC& eventC, const ToyBoxB& box, AnaRecObjectC* track, SystId_h syst_index, Int_t branch) const{
  //**************************************************

  (void)eventC;
  (void)branch;

  if (!track) return false;

  ToyBoxB* box_tmp  = const_cast<ToyBoxB*>(&box);

  ToyHNLBoxB* hnlBox  = static_cast<ToyHNLBoxB*>(box_tmp);

  // Get the inputs into the HNL canidate
  AnaTrackB* NTrack = hnl_global_selection::GetNegativeTrack(*hnlBox);
  AnaTrackB* PTrack = hnl_global_selection::GetPositiveTrack(*hnlBox);

  // At first order the dependency is on the HNL candidate`s inputs 
  if (syst_index == SystId::kChargeIDEff || syst_index == SystId::kTpcClusterEff 
      || syst_index == SystId::kTpcFgdMatchEff || syst_index == SystId::kECalPID){
    if (track->UniqueID != NTrack->UniqueID && track->UniqueID != PTrack->UniqueID)
      return false;
  }
  
  return true;
}

//**************************************************
bool hnlGlobalReconSelection::IsRelevantTrueObjectForSystematicInToy(const AnaEventC& eventC, const ToyBoxB& box,
    AnaTrueObjectC* trueTrack, SystId_h syst_index, Int_t branch) const{
  //**************************************************

  (void)eventC;
  (void)branch;

  if (!trueTrack) return false;

  ToyBoxB* box_tmp  = const_cast<ToyBoxB*>(&box);

  ToyHNLBoxB* hnlBox  = static_cast<ToyHNLBoxB*>(box_tmp);

  // Get the inputs into the HNL canidate
  AnaTrackB* NTrack = hnl_global_selection::GetNegativeTrack(*hnlBox);
  AnaTrackB* PTrack = hnl_global_selection::GetPositiveTrack(*hnlBox);

  AnaTrueParticleB* trueNTrack = NULL;
  if (NTrack) trueNTrack = NTrack->GetTrueParticle();

  AnaTrueParticleB* truePTrack = NULL;
  if (PTrack) truePTrack = PTrack->GetTrueParticle(); 

  if (syst_index == SystId::kTpcTrackEff || syst_index == SystId::kTpcECalMatchEff){

    // For the moment apply ony for HNL inputs --> no inefficiency !
    if (truePTrack &&
        trueTrack->ID  == truePTrack->ID) return true; 

    if (trueNTrack &&
        trueTrack->ID  == trueNTrack->ID) return true;   

    // Otherwise don't consider this TrueTrack
    return false;
  }
  else if (syst_index == SystId::kSIPion){

    // If this trueTrack is associated to the MainTrack
    if (truePTrack && trueTrack->ID == truePTrack->ID){
      if (abs(truePTrack->PDG)        == 211) return true;
      if (abs(truePTrack->ParentPDG)  == 211) return true;
      if (abs(truePTrack->GParentPDG) == 211) return true;
      return false;
    }

    if (trueNTrack && trueTrack->ID == trueNTrack->ID){
      if (abs(trueNTrack->PDG)        == 211) return true;
      if (abs(trueNTrack->ParentPDG)  == 211) return true;
      if (abs(trueNTrack->GParentPDG) == 211) return true;
      return false;
    }

    return false;
  }

  return false;
}

//**************************************************
Int_t hnlGlobalReconSelection::GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
//**************************************************
  (void)branch;
  
  std::vector<std::vector<Int_t> > groups(SystIdHNL::SystHNL_EnumLast_SystId, std::vector<Int_t>(1, -1));
 
  // Explicitely set the groups for various systeamtics that need them, 
  // this allows to have a transparent set/control of the systematics by the
  // selection  

  using namespace SystId;
  using namespace anaUtils;
  // --- Systematic
  groups[   kBFieldDist      ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC );
  groups[   kMomScale        ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC );
  groups[   kMomResol        ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC );
  groups[   kTpcPid          ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC );
  groups[   kChargeIDEff     ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC );
  groups[   kTpcClusterEff   ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPCInFGD2FV ); 
  groups[   kTpcTrackEff     ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC         );
  groups[   kTpcFgdMatchEff  ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPCorFGD1    );
  groups[   kTpcECalMatchEff ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithTPC         );
  groups[   kECalPID         ] = CreateVectorI( 1,  EventBoxTracker::kTracksWithECal        );
  // The systematics not mentioned above will get no groups
  Int_t ngroups = 0;
  // Check the systematic index is ok
  try {
    groups.at(systId);
  }
  catch (const std::out_of_range& oor) {
    std::cerr << this->Name() << " GetRelevantTrueObjectGroupsForSystematic: syst index beyond limits "<< systId << " Error " << oor.what() << '\n';
    exit(1);
  } 
  if (groups[systId][0] >= 0) IDs[ngroups++] = groups[systId][0];

  if (systId == SystId::kTpcFgdMatchEff)
    IDs[ngroups++] = EventBoxTracker::kTracksWithTPCorFGD2;
  
  return ngroups;
}

//**************************************************
Int_t hnlGlobalReconSelection::GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
//**************************************************
  (void)branch;
  
  std::vector<std::vector<Int_t> > groups(SystIdHNL::SystHNL_EnumLast_SystId, std::vector<Int_t>(1, -1));
 
  // Explicitely set the groups for various systeamtics that need them, 
  // this allows to have a transparent set/control of the systematics by the
  // selection  

  using namespace SystId;
  using namespace anaUtils;
  // --- Systematic                           
  groups[ kTpcFgdMatchEff   ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesChargedInTPCorFGDInBunch);
  groups[ kTpcTrackEff      ] =  CreateVectorI(1, EventBoxHNL::kTrueTracksChargedFromHNLVertex);
  groups[ kTpcECalMatchEff  ] =  CreateVectorI(1, EventBoxHNL::kTrueTracksChargedFromHNLVertex);
  groups[ kECalTrackEff     ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesInECalInBunch);
  // The systematics not mentioned above will get no groups
  Int_t ngroups = 0;
  // Check the systematic index is ok
  try {
    groups.at(systId);
  }
  catch (const std::out_of_range& oor) {
    std::cerr << this->Name() << " GetRelevantTrueObjectGroupsForSystematic: syst index beyond limits "<< systId << " Error " << oor.what() << '\n';
    exit(1);
  } 
  if (groups[systId][0] >= 0) IDs[ngroups++] = groups[systId][0];
  
  return ngroups;
}

//**************************************************
bool hnlGlobalReconSelection::IsRelevantSystematic(const AnaEventC& eventC, const ToyBoxB& box, SystId_h syst_index, Int_t branch) const{
  //**************************************************

  (void)eventC;
  (void)branch;
  (void)box;

  switch (syst_index){
    case SystId::kBFieldDist:    
    case SystId::kMomResol:          
    case SystId::kMomScale:          
    case SystId::kChargeIDEff:       
    case SystId::kTpcPid:            
    case SystId::kTpcTrackEff:       
    case SystId::kTpcFgdMatchEff:    
    case SystId::kTpcClusterEff:
    case SystIdHNL::kSIPionHNL:
    case SystIdHNL::kGVEffHNL:
    case SystIdHNL::kFluxWeightHNL:
    case SystId::kECalPID:
    case SystId::kTpcECalMatchEff:
    case SystIdHNL::kPileUpHNL:
      return true;
      break;
    default:
      return false;
      break;
  }
  return false;
}
