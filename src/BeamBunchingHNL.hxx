#ifndef BeamBunchingHNL_h
#define BeamBunchingHNL_h

#include "ND280BeamBunching.hxx"
#include "Parameters.hxx"


class BeamBunchingHNL: public ND280BeamBunching {

public:
  
  BeamBunchingHNL(){ 
    _dTOF = 0;
    _hCut = (Float_t)ND::params().GetParameterD("hnlAnalysis.TimeBunching.TimeCutH");
  	_lCut = (Float_t)ND::params().GetParameterD("hnlAnalysis.TimeBunching.TimeCutL");

  }
  
  virtual ~BeamBunchingHNL(){}

  /// The bunch number for a given time, provided the asymmetric cuts around the bunch central time 
  Int_t GetBunchHNL(Float_t tTrack, Int_t run, bool isMC, bool cosmic_mode, Float_t lCut, Float_t hCut) const;

  /// The bunch number for a given time, provided the asymmetric cuts around the bunch central time 
  Int_t GetBunchHNL(Float_t tTrack, Int_t run, bool isMC, bool cosmic_mode) const{
    return GetBunchHNL(tTrack, run, isMC, cosmic_mode, _lCut, _hCut);
  }

  /// Setter
  void SetDeltaTOF(Float_t tof){
    _dTOF = tof;
  }
  
  /// Getter
  Float_t GetDeltaTOF() const{
    return _dTOF;
  }
  
protected:

  /// An additional offset to account for a possible difference in TOF due the massive leptons
  Float_t _dTOF;
  
  /// Cut for retrieving a bunch number
	Float_t _hCut;
	Float_t _lCut;

};

#endif
