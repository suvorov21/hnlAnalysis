#include "hnlAnalysis.hxx"
#include "Parameters.hxx"
#include "UseGlobalAltMomCorrection.hxx"
#include "CategoriesUtils.hxx"
#include "BasicUtils.hxx"
#include "hnlAnalysisUtils.hxx"
#include "hnlCategoryUtils.hxx"

#include "BFieldDistortionSystematicsHNL.hxx"

#include "MomentumScaleSystematics.hxx"
#include "MomentumResolSystematics.hxx"
#include "TPCPIDSystematics.hxx"

#include "ChargeIDEffSystematics.hxx"
#include "TPCTrackEffSystematicsHNL.hxx"
#include "TPCFGDMatchEffSystematicsHNL.hxx"
#include "TPCClusterEffSystematicsHNL.hxx"

#include "FluxWeightSystematicsHNL.hxx"
#include "GVEffSystematicsHNL.hxx"

#include "SIPionSystematicsHNL.hxx"

#include "PileUpSystematicsHNL.hxx"

#include "ECalPIDSystematics.hxx"
#include "ECalTrackEffSystematics.hxx"
#include "TPCECalMatchEffSystematics.hxx"

#include "oaAnalysisHNL_Converter.hxx"

#include "baseToyMaker.hxx"
#include "SystIdHNL.hxx"
#include "EventBoxId.hxx"

//********************************************************************
hnlAnalysis::hnlAnalysis(AnalysisAlgorithm* ana) : baseTrackerAnalysis(ana) {
  //********************************************************************

  // Add the package version
  ND::versioning().AddPackage("hnlAnalysis", anaUtils::GetSoftwareVersionFromPath((std::string)getenv("HNLANALYSISROOT")));  
}

//********************************************************************
bool hnlAnalysis::Initialize(){
  //********************************************************************
  if (!baseTrackerAnalysis::Initialize()) return false;

  // Minimum accum level to save event into the output tree
  SetMinAccumCutLevelToSave(ND::params().GetParameterI("hnlAnalysis.MinAccumLevelToSave"));

  _AntiNu = (bool)ND::params().GetParameterI("hnlAnalysis.AntiNu");

  // Add categories for HMN & HMP analysis
  hnl_categ_utils::AddCategories();
  hnl_categ_utils::AddCategories("HMP");

  return true;

}

//********************************************************************
void hnlAnalysis::DefineInputConverters(){
//********************************************************************

  baseTrackerAnalysis::DefineInputConverters();
 
  // need to replace converter in order to have PASS THROUGH info
  input().ReplaceConverter("oaAnalysisTree", new oaAnalysisHNL_Converter());

 
}


//********************************************************************
void hnlAnalysis::DefineSelections(){
  //********************************************************************
  bool forceBreak = (bool)ND::params().GetParameterI("hnlAnalysis.ForceBreak");

  sel().AddSelection("kHNLAnalysisGV",   "HNL GV selection",         new hnlGlobalReconSelection(forceBreak));

}

//********************************************************************
void hnlAnalysis::DefineConfigurations(){
  //********************************************************************

  //-------  Add and define individual configurations with one systematic only ------------------

  _enableSingleVariationSystConf = (bool)ND::params().GetParameterI("baseAnalysis.Configurations.EnableSingleVariationSystConfigurations");
  _enableSingleWeightSystConf    = (bool)ND::params().GetParameterI("baseAnalysis.Configurations.EnableSingleWeightSystConfigurations");
  _enableAllSystConfig           = (bool)ND::params().GetParameterI("baseAnalysis.Configurations.EnableAllSystematics");


  _ntoys = (Int_t)ND::params().GetParameterI("baseAnalysis.Systematics.NumberOfToys");

  _randomSeed = (Int_t)ND::params().GetParameterI("baseAnalysis.Systematics.RandomSeed");

  //-------  Add and define individual configurations with one systematic only ------------------

   if (_enableSingleVariationSystConf){
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableBFieldDist")){
      AddConfiguration(conf(), bfield_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventVariation(SystId::kBFieldDist,bfield_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableMomScale")){
      AddConfiguration(conf(), momscale_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventVariation(SystId::kMomScale,momscale_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableMomRes")){
      AddConfiguration(conf(), momresol_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventVariation(SystId::kMomResol,momresol_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCPID")){
      AddConfiguration(conf(), tpcpid_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventVariation(SystId::kTpcPid,tpcpid_syst);
    }
  }
  if (_enableSingleWeightSystConf){
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableChargeConf")){
      AddConfiguration(conf(), chargeideff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kChargeIDEff, chargeideff_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCTrackEff")){
      AddConfiguration(conf(), tpctrackeff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kTpcTrackEff, tpctrackeff_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCFGDMatchEff")){
      AddConfiguration(conf(), tpcfgdmatcheff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kTpcFgdMatchEff, tpcfgdmatcheff_syst);
    }
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCClusterEff")){
      AddConfiguration(conf(), tpcclustereff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kTpcClusterEff, tpcclustereff_syst);
    }
    
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableFluxWeight")){
      AddConfiguration(conf(), flux_hnl_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdHNL::kFluxWeightHNL, flux_hnl_syst);
    }
    
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableGVEff")){
      AddConfiguration(conf(), gv_eff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdHNL::kGVEffHNL, gv_eff_syst);
    }
    
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableSIPion")){
      AddConfiguration(conf(), sipion_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdHNL::kSIPionHNL, sipion_syst);
    }

    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCECal")){
      AddConfiguration(conf(), tpc_ecal_matcheff_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kTpcECalMatchEff, tpc_ecal_matcheff_syst);
    }

    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableECalPID")){
      AddConfiguration(conf(), ecal_pid_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystId::kECalPID, ecal_pid_syst);
    }
    
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnablePileUp")){
      AddConfiguration(conf(), pileup_hnl_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdHNL::kPileUpHNL, pileup_hnl_syst);
    }    
  }

  // Enable systematics in the all_syst configuration (created in baseAnalysis)
  if (_enableAllSystConfig){ 
    AddConfiguration(conf(), all_syst, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));  

    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableBFieldDist"))     conf().EnableEventVariation(SystId::kBFieldDist       , all_syst);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableMomRes"))         conf().EnableEventVariation(SystId::kMomResol         , all_syst);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableMomScale"))       conf().EnableEventVariation(SystId::kMomScale         , all_syst);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCPID"))         conf().EnableEventVariation(SystId::kTpcPid           , all_syst);
  }
  
  for (std::vector<ConfigurationBase* >::iterator it= conf().GetConfigurations().begin();it!=conf().GetConfigurations().end();it++){
    Int_t index = (*it)->GetIndex();
    if (index != ConfigurationManager::default_conf && (index != all_syst || !_enableAllSystConfig)) continue;

    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableChargeConf"))     conf().EnableEventWeight(SystId::kChargeIDEff      , index);

    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCTrackEff"))    conf().EnableEventWeight(SystId::kTpcTrackEff      , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCFGDMatchEff")) conf().EnableEventWeight(SystId::kTpcFgdMatchEff   , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCClusterEff"))  conf().EnableEventWeight(SystId::kTpcClusterEff    , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableFluxWeight"))     conf().EnableEventWeight(SystIdHNL::kFluxWeightHNL , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableGVEff"))          conf().EnableEventWeight(SystIdHNL::kGVEffHNL      , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableSIPion"))         conf().EnableEventWeight(SystIdHNL::kSIPionHNL     , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableTPCECal"))        conf().EnableEventWeight(SystId::kTpcECalMatchEff  , index);
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnableECalPID"))        conf().EnableEventWeight(SystId::kECalPID          , index);  
    if (ND::params().GetParameterI("hnlAnalysis.Systematics.EnablePileUp"))         conf().EnableEventWeight(SystIdHNL::kPileUpHNL     , index);  

  }
  
  
  
}

//********************************************************************
void hnlAnalysis::DefineCorrections(){
  //********************************************************************
  baseTrackerAnalysis::DefineCorrections();

  if (ND::params().GetParameterI("baseTrackerAnalysis.Corrections.DisableAllCorrections")){
    // Should not be needed, but just in case !!!
    corr().DisableAllCorrections();
  }
 
  // Make sure to switch OFF regular pile-up
  corr().DisableCorrection("pileup_corr");
}

//********************************************************************
void hnlAnalysis::DefineSystematics(){
  //********************************************************************
 
  //------------ Add Variation systematics ------------
  evar().AddEventVariation(SystId::kBFieldDist,          "BFieldDist",           new BFieldDistortionSystematicsHNL());
  evar().AddEventVariation(SystId::kMomScale,            "MomScale",             new MomentumScaleSystematics());
  evar().AddEventVariation(SystId::kMomResol,            "MomResol",             new MomentumResolSystematics());
  evar().AddEventVariation(SystId::kTpcPid,              "TpcPid",               new TPCPIDSystematics());
  

  //-------------- Add Systematics Weights
  // compute efficiency using truth for eff-like systematics
  _computeEfficiency =  (bool)ND::params().GetParameterI("baseTrackerAnalysis.Systematics.ComputeEfficiency"); 

  _chargeid                 = new ChargeIDEffSystematics(            _computeEfficiency);
  _tpcfgdmatch              = new TPCFGDMatchEffSystematicsHNL(      _computeEfficiency);
  _tpctr                    = new TPCTrackEffSystematics(            _computeEfficiency);
  _tpc_ecal_matcheff        = new TPCECalMatchEffSystematics(        _computeEfficiency);
  _ecal_pid                 = new ECalPIDSystematics(                _computeEfficiency);
  
  
  //BELOW the order is the same as in psyche, so that each throws can be exactly the same as in psyche.
  eweight().AddEventWeight(SystId::kChargeIDEff,       "ChargeIDEff",       _chargeid);
  eweight().AddEventWeight(SystId::kTpcClusterEff,     "TpcClusterEff",     new TPCClusterEffSystematicsHNL());
  eweight().AddEventWeight(SystId::kTpcTrackEff,       "TpcTrackEff",       _tpctr);
  eweight().AddEventWeight(SystId::kTpcFgdMatchEff,    "TpcFgdMatchEff",    _tpcfgdmatch);
  
  eweight().AddEventWeight(SystIdHNL::kFluxWeightHNL,  "FluxWeightHNL",     new FluxWeightSystematicsHNL());
  eweight().AddEventWeight(SystIdHNL::kGVEffHNL,       "GVEffHNL",          new GVEffSystematicsHNL());
  
  eweight().AddEventWeight(SystIdHNL::kSIPionHNL,      "SIPion",            new SIPionSystematicsHNL());
  
  eweight().AddEventWeight(SystId::kTpcECalMatchEff,   "TpcECalMatchEff",   _tpc_ecal_matcheff);
  eweight().AddEventWeight(SystId::kECalPID,           "ECalPID",           _ecal_pid);
  
  eweight().AddEventWeight(SystIdHNL::kPileUpHNL,      "PileUpHNL",         new PileUpSystematicsHNL());
  
}

//********************************************************************
void hnlAnalysis::DefineMicroTrees(bool addBase){
  //********************************************************************
  // Variables from baseAnalysis (run, event, ...)
  if (addBase) 
    baseTrackerAnalysis::DefineMicroTrees(addBase);


  // variable of the HNL candidate
  AddVar4VF   ( output(),   HNL_position,                 ""                                );  // position of the HNL candidate
  AddVar3VF   ( output(),   HNL_direction,                ""                                );  // direction of the HNL candidate
  AddVarFixVF ( output(),   HNL_inv_mass,                 "", AnaHNLCandidate::kModeCounter );  // invariant mass hypothesis
  AddVarF     ( output(),   HNL_constituents_rel_angle,   ""                                );  // relative angle between the two candidates

  // veto objects
  AddVarVI( output(), NFGD1VetoHits,     "", NFGD1VetoBins ); // FGD1 veto hits
  AddVarVI( output(), NFGD2VetoHits,     "", NFGD2VetoBins ); // FGD2 veto hits
  
  AddVarVF( output(), FGD1VetoMinTime,   "", NFGD1VetoBins ); // FGD1 veto hits min time
  AddVarVF( output(), FGD2VetoMinTime,   "", NFGD2VetoBins ); // FGD2 veto hits min time
  
  AddVarVF( output(), FGD1VetoRawCharge, "", NFGD1VetoBins ); // FGD1 veto hits charge
  AddVarVF( output(), FGD2VetoRawCharge, "", NFGD2VetoBins ); // FGD2 veto hits charge

  AddVarI (output(),   nConstituents,     "");

  AddVarF (output(),   dz1, "");
  AddVarF (output(),   dz2, "");

  AddVarF (output(),   VertexChi2, "");
  AddVarF (output(),   VarianceX, "");
  AddVarF (output(),   VarianceY, "");
  AddVarF (output(),   VarianceZ, "");
  AddVarF (output(),   VarianceR, "");

  AddVarMF( output(),   P0DVetoEndPos,    "", NP0DVetoTracks, -30, 4  );  // end position of P0D veto objects
  AddVarMF( output(),   P0DVetoStartPos,  "", NP0DVetoTracks, -30, 4  );  // start position of P0D veto objects

  AddVarI ( output(),   nSameTPCTracks,          ""); // number of tracks in the same TPC as HNL candidate
  AddVarI ( output(),   nSameTPCQualityTracks,   "");

  AddVarF ( output(),   dpt,       "");  // traversity vars
  AddVarF ( output(),   dphit,     "");
  AddVarF ( output(),   dat,       "");
  AddVarF ( output(),   p1,        "");
  AddVarF ( output(),   p2,        "");

  AddVarF  ( output(),  dToF,      "");  // timing vars
  AddVarF  ( output(),  BunchTime, "");

  AddVarF ( output(),   cut1_mu,     ""); // values for PID cuts
  AddVarF ( output(),   cut2_mu,     "");
  AddVarF ( output(),   mip_lhood,   "");
  AddVarF ( output(),   pion_lhood,  "");
  AddVarF ( output(),   pullelec,    "");
  AddVarF ( output(),   pullmuon,    "");
  AddVarF ( output(),   pullpion,    "");
  AddVarF ( output(),   pullprot,    "");
  AddVarF ( output(),   pullpos,     "");

  AddVarI( output(),   HMNEndTrackDet,        "");  // end of track detector
  AddVarI( output(),   HMNEndTrackDetTrue,    "");

  AddVar4VF(output(),  HMNEndTrackPos,        "");
  AddVarI(  output(),  HMNStartTrackDet,      "");

  AddVarI( output(),   HMPEndTrackDet,        "");
  AddVarI( output(),   HMPEndTrackDetTrue,    "");

  // true vars
  AddVar4VF ( output(),   HNL_true_position,                  "" );   // position of the HNL candidate
  AddVar3VF ( output(),   HNL_true_direction,                 "" );   // direction of the HNL candidate
  AddVarF   ( output(),   HNL_true_constituents_rel_angle,    "" );   // relative angle between the two candidates


  AddVarF   ( output(),   HNL_true_prob,                      "");    // probability 
  AddVarF   ( output(),   HNL_true_mass,                      "");    // mass

}

//********************************************************************
void hnlAnalysis::DefineTruthTree(){
  //********************************************************************
  // Variables from base package
  baseTrackerAnalysis::DefineTruthTree();

  AddVar4VF ( output(),   HNL_true_position,                "");   
  AddVar3VF ( output(),   HNL_true_direction,               "");   
  AddVarF   ( output(),   HNL_true_constituents_rel_angle,  "");   

  AddVarF   ( output(),   HNL_true_prob,                    "");
  AddVarF   ( output(),   HNL_true_mass,                    "");

}


//********************************************************************
void hnlAnalysis::FinalizeToy(){
  //********************************************************************

  AnalysisAlgorithm::FinalizeToy();

  return;
}


//********************************************************************
void hnlAnalysis::FillMicroTrees(bool addBase){
  //********************************************************************
  // Fill the common variables defined in baseAnalysis/vXrY/src/baseAnalysis.cxx
  baseTrackerAnalysis::FillMicroTreesBase(addBase);
  //AnaHNLCandidate* hnl = static_cast<const ToyHNLBoxB*>(&box())->hnlCandidate;
  const ToyHNLBoxB* hnlBox = static_cast<const ToyHNLBoxB*>(&box());

  if (!hnlBox) return;

  output().FillVar(nConstituents, hnlBox->constituent);

  output().FillVar(dz1, hnlBox->dz1);
  output().FillVar(dz2, hnlBox->dz2);

  output().FillVar(cut1_mu,    hnlBox->cut1_mu);
  output().FillVar(cut2_mu,    hnlBox->cut2_mu);
  output().FillVar(mip_lhood,  hnlBox->mip_lhood);
  output().FillVar(pion_lhood, hnlBox->pion_lhood);  

  AnaHNLCandidate* hnl = hnlBox->hnlCandidate;

  if (!hnl) return;

  if (!hnl->Vertex) return;

  AnaVertex* Vertex = static_cast<AnaVertex*>(hnl->Vertex);
  output().FillVar(VertexChi2, Vertex->Chi2);  
  output().FillVar(VarianceX, Vertex->Variance[0]);
  output().FillVar(VarianceY, Vertex->Variance[1]);
  output().FillVar(VarianceZ, Vertex->Variance[2]);

  Float_t r = sqrt(Vertex->Variance[0]*Vertex->Variance[0] + Vertex->Variance[1]*Vertex->Variance[1] + Vertex->Variance[2]*Vertex->Variance[2]);
  output().FillVar(VarianceR, r);

  output().FillVar(dpt,   hnlBox->dpt);
  output().FillVar(dphit, hnlBox->dphit);
  output().FillVar(dat,   hnlBox->dat);
  output().FillVar(p1,   hnl->trackNegative->Momentum);
  output().FillVar(p2,   hnl->trackPositive->Momentum);
  output().FillVar(pullelec, hnlBox->pullelec);
  output().FillVar(pullmuon, hnlBox->pullmuon);
  output().FillVar(pullpion, hnlBox->pullpion);
  output().FillVar(pullprot, hnlBox->pullprot);
  output().FillVar(pullpos, hnlBox->pullpos);

  // HNL candidate reco vars
  Float_t Dir[3];
  hnl->GetDirection(Dir);

  output().FillVectorVarFromArray(  HNL_position,               hnl->Vertex->Position,  4);
  output().FillVectorVarFromArray(  HNL_direction,              Dir,                    3);


  Float_t Masses[AnaHNLCandidate::kModeCounter];
  hnl->GetMasses(Masses);

  output().FillVectorVarFromArray(  HNL_inv_mass,               Masses,                 AnaHNLCandidate::kModeCounter);

  output().FillVar(                 HNL_constituents_rel_angle, hnl->GetRelAngle());

  output().FillVar(nConstituents, hnlBox->constituent);
 
  // veto objects
  if (_event->EventBoxes[EventBoxId::kEventBoxTracker]){

    EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(_event->EventBoxes[EventBoxId::kEventBoxTracker]);


    for (int i = 0; i < EventBox->nRecObjectsInGroup[EventBoxHNL::kP0DVetoTracks]; i++){ 
      AnaTrackB* track = static_cast<AnaTrackB*>(EventBox->RecObjectsInGroup[EventBoxHNL::kP0DVetoTracks][i]);
      if (!track) continue;
      output().FillMatrixVarFromArray(  P0DVetoEndPos,    track->PositionStart, 4 );
      output().FillMatrixVarFromArray(  P0DVetoStartPos,  track->PositionEnd,   4 );

      output().IncrementCounterForVar(  P0DVetoEndPos);
    }

    for (int i = 0; i<EventBox->nFgdTimeBins[0]; i++){
      
      output().FillVectorVar( NFGD1VetoHits,     EventBox->FgdTimeBins[0][i]->NHits[0]        );
      
      output().FillVectorVar( FGD1VetoMinTime,   EventBox->FgdTimeBins[0][i]->MinTime         );
      
      output().FillVectorVar( FGD1VetoRawCharge, EventBox->FgdTimeBins[0][i]->RawChargeSum[0] );
    

      output().IncrementCounterForVar( NFGD1VetoHits );
    } 
    
    for (int i = 0; i<EventBox->nFgdTimeBins[1]; i++){
      
      output().FillVectorVar( NFGD2VetoHits,     EventBox->FgdTimeBins[1][i]->NHits[1]        );
      
      output().FillVectorVar( FGD2VetoMinTime,   EventBox->FgdTimeBins[1][i]->MinTime         );
      
      output().FillVectorVar( FGD2VetoRawCharge, EventBox->FgdTimeBins[1][i]->RawChargeSum[1] );
    

      output().IncrementCounterForVar( NFGD2VetoHits );
    } 
  }

  output().FillVar(nSameTPCTracks,          hnlBox->nSameTPCTracks);
  output().FillVar(nSameTPCQualityTracks,   hnlBox->nSameTPCqualtyTracks);

  output().FillVar(HMNEndTrackDetTrue,   hnlBox->HMNEndTrackDetTrue);
  output().FillVar(HMPEndTrackDetTrue,   hnlBox->HMPEndTrackDetTrue);

  output().FillVar(HMNStartTrackDet,     hnlBox->HMNStartTrackDet);
  output().FillVectorVarFromArray(HMNEndTrackPos, hnl->trackNegative->PositionEnd,        4);

  output().FillVar(HMNEndTrackDet,       hnlBox->HMNEndTrackDet);
  output().FillVar(HMPEndTrackDet,       hnlBox->HMPEndTrackDet);

  if (_event->EventBoxes[EventBoxId::kEventBoxTracker]){
    EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(_event->EventBoxes[EventBoxId::kEventBoxTracker]);
    output().FillVar(BunchTime,   EventBox->BunchCentralTime);
  }

  AnaHNLTrueVertex* trueVertex = static_cast<AnaHNLTrueVertex*>(hnl->Vertex->TrueVertex);
  if(!trueVertex)
    return;

  // true vars
  output().FillVar(dToF,   trueVertex->dToF);


  output().FillVar(HNL_true_prob,   trueVertex->Probability);
  output().FillVar(HNL_true_mass,   trueVertex->Mass);


  output().FillVectorVarFromArray(  HNL_true_position,  trueVertex->Position,   4);
  output().FillVectorVarFromArray(  HNL_true_direction, trueVertex->NuDir,      3);


  output().FillVar( HNL_true_constituents_rel_angle, trueVertex->RelAngle);

}


//********************************************************************
void hnlAnalysis::FillToyVarsInMicroTrees(bool addBase){
  //********************************************************************

  (void)addBase;

  // Fill the common variables
  if (addBase) 
    baseTrackerAnalysis::FillToyVarsInMicroTreesBase(addBase);

}

//********************************************************************
bool hnlAnalysis::CheckFillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************

  return (hnl_analysis_utils::InTPCFV(SubDetId::kTPC, vtx.Position));
}


//********************************************************************
void hnlAnalysis::FillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************

  baseTrackerAnalysis::FillTruthTreeBase(vtx);
  // Fill  the categories according to TREx definitions (FV etc)
  SubDetId::SubDetEnum det=SubDetId::kTPC;
  hnl_categ_utils::FillTruthTreeCategories(vtx, "", det, _AntiNu);

  // Event variables
  output().FillVar(run,    GetSpill().EventInfo->Run);
  output().FillVar(subrun, GetSpill().EventInfo->SubRun);
  output().FillVar(evt,    GetSpill().EventInfo->Event);


  // true variables
  output().FillVar(nu_pdg,        vtx.NuPDG);
  output().FillVar(nu_trueE,      vtx.NuEnergy);
  output().FillVar(nu_truereac,   vtx.ReacCode);
  output().FillVar(TruthVertexID, vtx.ID);
  output().FillVar(RooVtxIndex,   vtx.RooVtxIndex);
  output().FillVar(RooVtxEntry,   vtx.RooVtxEntry);

  // override
  output().FillVectorVarFromArray(HNL_true_direction, vtx.NuDir,    3);
  output().FillVectorVarFromArray(HNL_true_position,  vtx.Position, 4);

  const AnaHNLTrueVertex& trueVertex = static_cast<const AnaHNLTrueVertex&>(vtx); 

  output().FillVectorVar(weight, trueVertex.Weight);
  output().IncrementCounterForVar(weight);

  output().FillVar(HNL_true_prob,   trueVertex.Probability);
  output().FillVar(HNL_true_mass,   trueVertex.Mass);
  output().FillVar( HNL_true_constituents_rel_angle, trueVertex.RelAngle);


}

//********************************************************************
void hnlAnalysis::FillCategories(){
  //********************************************************************
  // Fill the track categories for color drawing
  SubDetId::SubDetEnum det=SubDetId::kTPC;

  /// MainTrack = HMNtrack !!
  hnl_categ_utils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(box().MainTrack),"", det, _AntiNu);

  // for positive charged candidate
  hnl_categ_utils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(box().HMPtrack),"HMP", det, _AntiNu);

}
