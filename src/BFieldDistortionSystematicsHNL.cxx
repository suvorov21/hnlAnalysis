#include "BFieldDistortionSystematicsHNL.hxx"
#include "ND280AnalysisUtils.hxx"

//********************************************************************
void BFieldDistortionSystematicsHNL::Apply(const ToyExperiment& toy, AnaEventC& event){
//********************************************************************

  // Get the SystBox for this event
  SystBoxB* box = GetSystBox(event);

#ifdef DEBUG 
  std::cout << "BFieldDistortionSystematics::Apply(): " << box->nRelevantRecObjects << std::endl;  
#endif

  // Loop over the relevant tracks for this systematic
  for (Int_t itrk=0;itrk<box->nRelevantRecObjects;itrk++){
#ifdef DEBUG 
    std::cout << "itrk = " << itrk << std::endl;
#endif
    AnaTrackB* track = static_cast<AnaTrackB*>(box->RelevantRecObjects[itrk]);    

    Float_t delta = 0;
    Float_t ntpcs = 0;
      // There is no more than 2 TPC segments
      // but we use the first one to correct.
      // The TPC segment
      //      AnaTPCParticleB* tpcTrack = track->TPCSegments[0];

      // Use the TPC segment with more nodes in closest TPC to the start position of the track
      AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*track, SubDetId::kTPC));    
      if (!tpcTrack) continue;
      Float_t gmom=track->Momentum;
      Float_t mom = tpcTrack->Momentum;
      Float_t refmom;
#ifdef DEBUG 
      std::cout<<" mom "<<mom<<std::endl;
#endif
      //the sign represents the charge in the prod6 files!
      if (versionUtils::prod6_systematics){
        refmom = fabs(tpcTrack->RefitMomentum);
        if (refmom == 99999 || !TMath::Finite(refmom)) continue;
      }else{
        refmom = tpcTrack->RefitMomentum;
        if (refmom < 0 || !TMath::Finite(refmom))      continue;
      }
      // Make sure we have a valid momentum
      if (mom < 0 || !TMath::Finite(mom))      continue;
      // the tpc mom should always be smaller
      // than the global momentum (gmom=tpcmom+fgdmom)
      if (mom > gmom || refmom > gmom) continue;
    
      delta += (refmom-mom)*_mean_error*toy.GetToyVariations(_index)->Variations[0];
    ntpcs += 1.0;      

#ifdef DEBUG 
    std::cout <<"  p0 = " << track->Momentum << std::endl;
#endif    
    
    // Apply the variation
    if(ntpcs>0) track->Momentum += delta/(ntpcs);
    
#ifdef DEBUG 
    std::cout << "p = " << track->Momentum << " delta "<<delta<<" ntpcs "<<ntpcs<<" var "<<toy.Variations[0]<< std::endl;
#endif
  }
}