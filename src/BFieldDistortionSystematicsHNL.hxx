#ifndef BFieldDistortionSystematicsHNL_h
#define BFieldDistortionSystematicsHNL_h

#include "BFieldDistortionSystematics.hxx"
#include "EventBoxTracker.hxx"

/// This is the BField distortions systematic
///

class BFieldDistortionSystematicsHNL: public BFieldDistortionSystematics {
public:
  
  /// Instantiate the momentum resolution systematic. nbins is the number of
  /// bins in the PDF. addResol and addResolError describe
  /// the Gaussian distribution from which the resolution of each virtual
  /// analysis is selected from.
  BFieldDistortionSystematicsHNL() : BFieldDistortionSystematics() {}
  
  virtual ~BFieldDistortionSystematicsHNL() {}

  virtual void Apply(const ToyExperiment& toy, AnaEventC& event);

};

#endif
