#ifndef hnlDetectorDefinition_h
#define hnlDetectorDefinition_h

namespace hnlDetDef{

  /// ----- TPC Detector Active Volume definitions ---
  /// ----- Split in two volumes along X (cathode presence),  this is to be able to apply fine-tuning if reuqired 

  const  Float_t tpc1Lmax[3]   =   {  870,  1030,  -71.15 };
  const  Float_t tpc1Lmin[3]   =   {   20,  -930, -724.85 };
  
  const  Float_t tpc1Rmax[3]   =   {  -20,  1030,  -71.15 };
  const  Float_t tpc1Rmin[3]   =   { -870,  -930, -724.85 };

  const  Float_t tpc2Lmax[3]   =   {  870,  1030, 1287.85 };
  const  Float_t tpc2Lmin[3]   =   {   20,  -930,  634.15 };
  
  const  Float_t tpc2Rmax[3]   =   {  -20,  1030, 1287.85 };
  const  Float_t tpc2Rmin[3]   =   { -870,  -930,  634.15 };

  const  Float_t tpc3Lmax[3]   =   {  870,  1030, 2646.85 };
  const  Float_t tpc3Lmin[3]   =   {   20,  -930, 1993.55 };
  
  const  Float_t tpc3Rmax[3]   =   {  -20,  1030, 2646.85 };
  const  Float_t tpc3Rmin[3]   =   { -870,  -930, 1993.55 };
  
  //  ----- TPC gaps, Z coordinates
  const Float_t tpc1MMGapMax     =    -395.0;
  const Float_t tpc1MMGapMin     =    -412.0;

  const Float_t tpc2MMGapMax     =     974.0;
  const Float_t tpc2MMGapMin     =     948.0;

  const Float_t tpc3MMGapMax     =    2333.0;
  const Float_t tpc3MMGapMin     =    2307.0;

}
#endif
