#include "TPCClusterEffSystematicsHNL.hxx"
#include "AnalysisUtils.hxx"
#include "CutUtils.hxx"
#include "SystematicUtils.hxx"
#include "ConstituentsUtils.hxx"
#include "ToyBoxTracker.hxx"

//********************************************************************
TPCClusterEffSystematicsHNL::TPCClusterEffSystematicsHNL(){
//********************************************************************

    // Get the TPC cluster extra inefficiency
    //GetParametersForBin(0, _extraIneff, _extraIneffError);
    //GetParametersForBin(0, _params);
    SetNParameters(2*GetNBins());
}

//********************************************************************
Weight_h TPCClusterEffSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& boxB, const SelectionBase& sel){
  //********************************************************************

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB); 

  // Get the SystBox for this event, and the appropriate selection and branch
  SystBoxB* SystBox = GetSystBox(event,box.SelectionEnabledIndex,box.SuccessfulBranch);

  Weight_h eventWeight = 1.0;

  SubDetId::SubDetEnum vertexDet = anaUtils::GetDetector(box.Vertex->Position);

  if (vertexDet != SubDetId::kTPC1 && vertexDet != SubDetId::kTPC2 && vertexDet != SubDetId::kTPC3)
    return eventWeight;

  bool HMPuseVertexTpc    = (vertexDet == anaUtils::GetDetector(box.HMPtrack->PositionStart));
  bool HMNuseVertexTpc    = (vertexDet == anaUtils::GetDetector(box.HMNtrack->PositionStart));

  AnaTPCParticleB* ClosestTPCPositive = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInClosestTpc(*box.HMPtrack));
  AnaTPCParticleB* ClosestTPCNegative = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInClosestTpc(*box.HMNtrack));

  AnaTPCParticleB* MostTPCPositive = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*box.HMPtrack, SubDetId::kTPC));
  AnaTPCParticleB* MostTPCNegative = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*box.HMNtrack, SubDetId::kTPC));
  
  // if we have at least one long track in vertex TPC, so we don't care about losing pair oh hits
  if (((HMPuseVertexTpc && ClosestTPCPositive->NNodes > 11) || (HMNuseVertexTpc && ClosestTPCNegative->NNodes > 11)) &&
   (MostTPCPositive->NNodes > 20) && (MostTPCNegative->NNodes >20))
    return eventWeight;
  
  std::vector<AnaTrackB*> CloseTracks;
  Weight_h TotalWeightDATA = 0.;
  Weight_h TotalWeightMC = 0.;
  
  if ((HMPuseVertexTpc && ClosestTPCPositive->NNodes < 12) || MostTPCPositive->NNodes < 21)
    CloseTracks.push_back(box.HMPtrack);

  if ((HMNuseVertexTpc && ClosestTPCNegative->NNodes < 12) || MostTPCNegative->NNodes < 21)
    CloseTracks.push_back(box.HMNtrack);

  for (Int_t i = 0; i < CloseTracks.size(); ++i) {
    AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInClosestTpc(*CloseTracks[i]));

    if (!tpcTrack)continue;

    float dir=fabs(tpcTrack->DirectionStart[2]);
    BinnedParamsParams params;
    int index;
    if(!GetBinValues(dir, params, index)) continue;
    int nn= tpcTrack->NNodes;           
   
    //           pvar_data/pvar_mc *pana
    //eff_w   = ----------------------
    //                   pana
    //           1-  pvar_data/pvar_mc *pana
    //ineff_w =---------------------------
    //                1 - pana
    
    //meanDATA  <=> effdata/effmc
    //meanMC    <=> 1
    //meanMCANA <=> 1
    //sigmaMC   <=> 0
    //sigmaDATA <=> ratio error
    /// extra hit inefficiency for MC: 0.0007 = effdata-effMC/effMC  
    /// => effdata/effmc=1-0.0007 
    // weight = effvarWeight ^nn *( (nn+1) * Pineffvar + 1) / ( (nn+1) * Pineff +1)
   
    #if useNewWeights 
      Weight_h eff_w = systUtils::ComputeEffLikeWeight(true, toy.GetToyVariations(_index)->Variations[0], params);//new way with data-mc error included
    #else
      Weight_h eff_w = systUtils::ComputeEffLikeWeight(true, toy.toy.GetToyVariations(_index)->Variations[0], 
          toy.GetToyVariations(_index)->Variations[1], params);
    #endif
     
    Float_t Pnom = params.meanMCANA;
    
    Weight_h Pineff = (eff_w*(-1))+1; //this is true, only if assume that effMC=1 

    TotalWeightDATA  = TotalWeightDATA + Weight_h(TMath::Power(eff_w.Correction,(Float_t)nn)*((nn+1)*(Pineff.Correction)+1.),
        TMath::Power(eff_w.Systematic,(Float_t)nn)*((nn+1)*(Pineff.Systematic)+1.));

    TotalWeightMC = TotalWeightMC+ Weight_h((nn+1)*(1-Pnom)+1., (nn+1)*(1-Pnom)+1.);
        
  }

  
  eventWeight *= TotalWeightDATA/TotalWeightMC;

  return eventWeight;
    
}

