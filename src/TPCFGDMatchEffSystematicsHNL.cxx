#include "TPCFGDMatchEffSystematicsHNL.hxx"
#include "AnalysisUtils.hxx"
#include "SystematicUtils.hxx"
#include "ConstituentsUtils.hxx"
#include "EventBoxTracker.hxx"
#include "ToyBoxTracker.hxx"
#include "SystId.hxx"

//********************************************************************
Weight_h TPCFGDMatchEffSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& box, const SelectionBase& sel){
  //********************************************************************

  
  const AnaEventB& event = static_cast< const AnaEventB&>(eventC);
  
  if(_computecounters)
    InitializeEfficiencyCounter();

  // Get the SystBox for this event, and the appropriate selection and branch
  SystBoxB* SystBox = GetSystBox(event, box.SelectionEnabledIndex, box.SuccessfulBranch);


  Weight_h eventWeight = 1.;


  /// for long tracks crossing the FGD, TPC-FGD matching is working at 100% in production6 therefore we only consider for production 6 very short tracks starting at the edge of the FGD. This is the case that has not been taken into account with the TPC-FGD matching package which use through-going muons crossing TPC1 and TPC2 or TPC2 and TPC3.
  /// for very short tracks in the FGD starting at the edge, tpc-fgd matching will depend on the efficiency to really get a hit in one of the two layers under consideration. Therefore the following propagation.
  ////// NHITS
  /// assume we observe nhits in the reco tracks.
  /// 1) we have the possibility that there were really nhits before (we did not lose anything:
  /// probability is :p = eff^nhits
  /// 2) we have the possibility that one hit is lost, so there nhits+1 before
  /// probability is :p= (nhits+1)*eff^nhits*(1-eff)
  /// 3) we lost 2 hits, so there were nhits+2 before
  /// probability is :p= (nhits+2)*(nhits+1)/2*eff^nhits*(1-eff)^2
  /// sum p= eff^nhits*[1+(nhits+1)*(1-eff)+(nhits+2)*(nhits+1)/2*(1-eff)^2]
  ///      = eff^nhits*[1+(nhhits+1)(1-eff)[1+(nhits+2)/2*(1-eff)]]

  //if( SystBox->nRelevantTracks == 0 ) return eventWeight;
  // Loop over relevant tracks for this systematic
  for (Int_t itrk=0; itrk<SystBox->nRelevantRecObjects;itrk++){
    AnaTrackB* track = static_cast<AnaTrackB*>(SystBox->RelevantRecObjects[itrk]);
    
    if (!track) continue;
    
    // For example in numuCC inclusive selection, only the TrueTrack associated to the muon candidate, and other true muon tracks should be considered
    if (!sel.IsRelevantRecObjectForSystematicInToy(event, box, track, SystId::kTpcFgdMatchEff, box.SuccessfulBranch)) continue;
    
    AnaParticleB* FGDSegment = NULL;
    FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(*track,SubDetId::kFGD1);

    if (!FGDSegment)
      FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(*track,SubDetId::kFGD2);
    
    if (!FGDSegment) continue;
    
    eventWeight *= GetWeight(static_cast<AnaFGDParticleB*>(FGDSegment), toy);
  
  }
  return eventWeight;
}
//THIS IS FOR PROD6 PROP
//**************************************************
bool TPCFGDMatchEffSystematicsHNL::IsRelevantRecObject(const AnaEventC& event, const AnaRecObjectC& object) const{
  //**************************************************

  (void)event;
  
  const AnaTrackB& track = static_cast<const AnaTrackB&>(object);
  

  if(!track.GetTrueParticle()) return false;
  if (track.nFGDSegments==0) return false;
  AnaParticleB* FGDSegment = NULL;

  // Get the most upstream FGD  

  FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(track,SubDetId::kFGD1);

  if (!FGDSegment)
    FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(track,SubDetId::kFGD2);

  if (!FGDSegment) return false;

  // only consider the very short case tracks, since those are suceptible to change something in data
  // normally one hit in the FGD is needed so that there is a matching, 
  if(FGDSegment->NNodes > _prod6_nnodes_cut) return false;
  
  if (track.Momentum < 30) return false;  

  return true;
}


