#ifndef BoxUtilsHNL_h
#define BoxUtilsHNL_h

#include "EventBoxUtils.hxx"

namespace boxUtils{
  
   /// Fill in the EventBox the array of true tracks originating from the HNL vertex
  void FillTrajsChargedFromHNLVertex(AnaEventB& event);

}

#endif
