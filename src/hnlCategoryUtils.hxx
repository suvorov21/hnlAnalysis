#ifndef trexCategoryUtils_h
#define trexCategoryUtils_h

#include "CategoriesUtils.hxx"
#include "hnlAnalysisUtils.hxx"


/// Various utilities to deal with color categories
/// specific for TREx analysis
namespace hnl_categ_utils{
  
  /// add the categories
  void AddCategories(const std::string prefix="");

  /// target nuclei categories specific to TPC-based analysis
  class target_tpc{
  public:
    static const int NTARGET = 8;
    //arrays
    static std::string  target_types[NTARGET];
    static int          target_codes[NTARGET];        
    static int          target_colors[NTARGET];  
    
    /// add categories for track category tools
    static void AddCategories(const std::string prefix = ""){
      //this will replace the original one
      anaUtils::_categ->AddCategory(prefix + "target", NTARGET, target_types, target_codes, target_colors);  
    }
     
    /// get code for a given target (neutrino interaction) 
    static int GetTargetCode(const AnaTrueVertex* vertex);
 
    /// get code for a given target (neutrino interaction)
    static int GetTargetCode(const AnaTrack* track);

  };

  /// Reation type. Use reactions name instead of nu-bar type
  class reaction_all_nu {
  public:
    static const int NREAC = 16;
    static std::string reac_types[NREAC];
    static int         reac_codes[NREAC];
    static int         reac_colors[NREAC];

    static void AddCategories(const std::string prefix = "") {
      anaUtils::_categ->AddCategory(prefix + "reactionAllNu", NREAC, reac_types, reac_codes, reac_colors);
      anaUtils::_categ->AddCategory(prefix + "reactionAllNunofv", NREAC, reac_types, reac_codes, reac_colors);
    }

    static int GetReaction(const AnaTrueVertex* vertex, bool IsAntinu = false);

    static int GetReaction(const AnaTrack* track, bool IsAntinu = false);
  };

  /// Fill truth tree categories, this fills some of the standart ones but treats properly the TPC FV
  void FillTruthTreeCategories(const AnaTrueVertex& trueVertex, const std::string& prefix, const SubDetId::SubDetEnum det=SubDetId::kTPC, bool IsAntinu=false);

  void FillTruthTreeCategories(const AnaTrueVertex& trueVertex, const SubDetId::SubDetEnum det=SubDetId::kTPC, bool IsAntinu=false);

  /// This fills the standart ones but treats properly the TPC FV
  void FillCategories(AnaEventB* event, AnaTrack* track, const std::string& prefix, const SubDetId::SubDetEnum det=SubDetId::kTPC, bool IsAntinu=false);
  void FillCategories(AnaEventB* event, AnaTrack* track, const SubDetId::SubDetEnum det=SubDetId::kTPC, bool IsAntinu = false);
 
}

#endif
