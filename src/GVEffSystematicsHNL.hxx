#ifndef GVEffSystematicsHNL_h
#define GVEffSystematicsHNL_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"

/// This is a normalization systematic. It takes into account the uncertainty on the neutrino flux

class GVEffSystematicsHNL: public EventWeightBase, public BinnedParams {
  public:

    GVEffSystematicsHNL(const std::string& name = "GVEffHNL");  
    virtual ~GVEffSystematicsHNL() {}

    /// Apply this systematic
    using EventWeightBase::ComputeWeight;
    Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box);

};


#endif
