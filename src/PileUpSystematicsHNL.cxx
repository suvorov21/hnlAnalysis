#include "PileUpSystematicsHNL.hxx"
#include "BaseDataClasses.hxx"
#include "SubDetId.hxx"
#include "AnalysisUtils.hxx"
#include "ConstituentsUtils.hxx"

//********************************************************************
PileUpSystematicsHNL::PileUpSystematicsHNL(bool TPCTotal): EventWeightBase(){
//********************************************************************

  char filename[300];
  _TPCTotal = TPCTotal;

  if (_TPCTotal) {
    sprintf(filename, "%s/data/PileUpSystematicsHNLtpcTotal.dat", getenv("HNLANALYSISROOT"));

    std::cout << " PileUp systematic total data per TPC " << filename << std::endl;

  } else {
    sprintf(filename, "%s/data/PileUpSystematicsHNL.dat", getenv("HNLANALYSISROOT"));

    std::cout << " PileUp systematic separate data for veto & TPC start " << filename << std::endl;
  }
  
  FILE *pFile = fopen(filename, "r");

  if (pFile == NULL) {
    printf("Cannot open file.\n");
    exit(1);
  }

  for (unsigned int i = 0; i < _NDET; i++)
    _correction[i] = 0;
 
  int det;
  Float_t corr;
  
  while (fscanf(pFile, "%d%f", &det, &corr) == 2) {
    if (det < 0 || det > (int)(_NDET-1)) continue;
    _correction[det] = corr;
  }

  fclose(pFile);
}

//********************************************************************
Weight_h PileUpSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& box, const SelectionBase& sel){
//********************************************************************

  Weight_h eventWeight = 1.;
  
  (void)box;
  (void)sel;
  (void)toy;
  
  const AnaEventB& event = static_cast<const AnaEventB&>(eventC); 
  
  // No correction for data
  if (!event.GetIsMC()) return eventWeight;

  // Get the detector depending on GV location
  unsigned int index1 = 0;
  unsigned int index2 = 0;
 
  // Should be one vertex per spill
  if (event.nTrueVertices == 0) return eventWeight;
  
  if (!event.TrueVertices[0]) return eventWeight;
 
  SubDetId::SubDetEnum det = anaUtils::GetDetector(event.TrueVertices[0]->Position);
    
  if (!SubDetId::IsTPCDetector(det)) return eventWeight;
   
  if (det == SubDetId::kInvalid) return eventWeight;
 
  switch (det){
    case SubDetId::kTPC1:
      index1 = SubDetId::kTPC1;
      index2 = SubDetId::kP0D;
      break; 
    case SubDetId::kTPC2:
      index1 = SubDetId::kTPC2;
      index2 = SubDetId::kFGD1;
      break;
    case SubDetId::kTPC3:
      index1 = SubDetId::kTPC3;
      index2 = SubDetId::kFGD2;
      break;
    default:
      return eventWeight;
      break;
  } 
  
  // Apply the corection, no systematic at the moment
  if (_TPCTotal) {
    eventWeight *= 1-_correction[index1];
  }
  else {
    eventWeight *= 1-_correction[index1];
    eventWeight *= 1-_correction[index2];
  }
  
  return eventWeight;
}

