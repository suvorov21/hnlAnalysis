#ifndef TPCFGDMatchEffSystematicsHNL_h
#define TPCFGDMatchEffSystematicsHNL_h

#include "TPCFGDMatchEffSystematics.hxx"

class TPCFGDMatchEffSystematicsHNL: public TPCFGDMatchEffSystematics {
public:
  
  TPCFGDMatchEffSystematicsHNL(bool computecounters=false) : TPCFGDMatchEffSystematics(computecounters){}  
  virtual ~TPCFGDMatchEffSystematicsHNL() {}
  
  
  using TPCFGDMatchEffSystematics::ComputeWeight;
  
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);

protected:

  /// Is this track relevant for this systematic ?
  bool IsRelevantRecObject(const AnaEventC& event, const AnaRecObjectC& object) const;

};

#endif
