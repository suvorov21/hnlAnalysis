#include "PionInteractionSystematicHNL.hxx"
#include "TFile.h"
#include "GeometryManager.hxx"
#include "SystId.hxx"


// Pion SI Manager HNL business
Bool_t PionSIManagerHNL::InVOI(SubDetId_h det, Float_t* pos) const{  


  if (det == SubDetId::kTPC1){
    
    /// TPC1 active volume (FV should be enough) + extended to the TPC2GasMixture in downstream Z
    Bool_t xOK = -870.0 < pos[0] && pos[0] < 870.0;
    Bool_t yOK = -930.0 < pos[1] && pos[1] < 1030.0;
    Bool_t zOK = -724.0 < pos[2] && pos[2] < 576.0;
    return xOK && yOK && zOK;
  
  }
    
  else if (det == SubDetId::kTPC2 || det == SubDetId::kTPC3){
   
    Bool_t xOK = -870.0 < pos[0] && pos[0] < 870.0;
    Bool_t yOK = -930.0 < pos[1] && pos[1] < 1030.0;
    Bool_t zOK = 634.0  < pos[2] && pos[2] < 2647.0;
    return xOK && yOK && zOK;
  }


  return false;
}


Bool_t PionSIManagerHNL::InVOI1(Float_t* pos) const{
  
  Bool_t xOK = -870.0 < pos[0] && pos[0] < 870.0;
  Bool_t yOK = -930.0 < pos[1] && pos[1] < 1030.0;
  Bool_t zOK = -724.0 < pos[2] && pos[2] < 576.0;
  
  return xOK && yOK && zOK;
  
}

Bool_t PionSIManagerHNL::InVOI2(Float_t* pos) const{

  Bool_t xOK = -870.0 < pos[0] && pos[0] < 870.0;
  Bool_t yOK = -930.0 < pos[1] && pos[1] < 1030.0;
  Bool_t zOK = 634.0  < pos[2] && pos[2] < 2647.0;
  
  return xOK && yOK && zOK; 

}

