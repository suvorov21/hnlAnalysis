#ifndef PileUpSystematicsHNL_h
#define PileUpSystematicsHNL_h

#include "EventWeightBase.hxx"



class PileUpSystematicsHNL: public EventWeightBase{

public:

  PileUpSystematicsHNL(bool TPCTotal = true);
  virtual ~PileUpSystematicsHNL(){}

  /// Apply this systematic
  using EventWeightBase::ComputeWeight;
  Weight_h ComputeWeight(const ToyExperiment&, const AnaEventC&, const ToyBoxB&){return 1;}
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);


private:
    
  static const unsigned int _NDET = 5; // FGD1, FGD2, TPC1, TPC2, TPC3, P0D following SubDetEnum
 
  // Not a highland style
  Float_t _correction[_NDET]; 
  bool _TPCTotal;
};

#endif
