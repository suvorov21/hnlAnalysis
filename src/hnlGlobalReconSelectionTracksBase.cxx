#include "hnlGlobalReconSelectionTracksBase.hxx"
#include "EventBoxUtils.hxx"
#include "AnalysisUtils.hxx"
#include "hnlAnalysisUtils.hxx"
#include "baseSelection.hxx"
/*

//********************************************************************
void hnlGlobalReconSelectionTracksBase::DefineSteps(){
  //********************************************************************

  // Cuts must be added in the right order
  // last "true" means the step sequence is broken if cut is not passed (default is "false")
  AddStep(StepBase::kCut,     "event quality",                new EventQualityCut(),           true);

  // find tracks wich starts in each TPC
  AddStep(StepBase::kAction,     "Find tracks in each TPC",   new hnl_global_TracksBase_selection::Find_HNL_candidate());

  // fill track candidates: constituents of the global vertex
  AddStep(StepBase::kAction,  "fill event summary",           new hnl_global_selection::FillSummaryAction());

  // Cuts 
  // 1. Should have a candidate in FV and with good quality of both daughter tracks 
  AddStep(StepBase::kCut,  "HNL candidate exists",            new hnl_global_TracksBase_selection::HNL_CandidateCut());

  // 2. Upstream veto
  AddStep(StepBase::kCut,  "Upstream veto",                   new hnl_global_selection::UpstreamActivityVetoCut());

  // 3. TPC Additional Activity Cut
  AddStep(StepBase::kCut,  "TPCAdditionalActivity",           new hnl_global_selection::TPCAdditionalActivityCut());

  // 4. HMP or HMN use the same detector as vertex
  AddStep(StepBase::kCut,  "Track use Vertex TPC",             new hnl_global_selection::EventUseVertexTPC());
  
  //split the selection into two groups: HMP is a pion/HMN is a pion
  AddSplit(2);

  //5.0
  AddStep(0, StepBase::kCut,        "HMP  PID pion cut",      new hnl_global_selection::PositivePionPIDCut());

  // mu- + pi+ and e- + pi+ modes
  AddSplit(2, 0);

  //6.0.0 mu- + pi+
  AddStep(0, 0, StepBase::kCut,     "HMN  PID muon cut",            new hnl_global_selection::MuonPIDCut());
  //7.0.0 invariant mass
  AddStep(0, 0, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kMumPip));
  //8.0.0 HNL reconstructed angle theta to Z axis 
  AddStep(0, 0, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kMumPip));
  //9.0.0 Reconstructed relative angle between products of HNL decay 
  AddStep(0, 0, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kMumPip));

  

  //6.0.1 e- + pi+
  AddStep(0, 1, StepBase::kCut,     "HMN  PID electron cut",        new hnl_global_selection::ElectronPIDCut());
  //7.0.1
  AddStep(0, 1, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kElePip));
  //8.0.1 HNL reconstructed angle theta to Z axis 
  AddStep(0, 1, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kElePip));
  //9.0.1 Reconstructed relative angle between products of HNL decay 
  AddStep(0, 1, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kElePip));

  //5.1
  AddStep(1, StepBase::kCut,        "HMN  PID pion cut",      new hnl_global_selection::NegativePionPIDCut());

  // mu+ + pi- and e+ + pi- modes
  AddSplit(2, 1);

  //6.1.0 mu+ + pi-
  AddStep(1, 0, StepBase::kCut,     "HMP  PID muon cut",            new hnl_global_selection::AntiMuonPIDCut());
  //7.1.0
  AddStep(1, 0, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kMupPim));
  //8.1.0 HNL reconstructed angle theta to Z axis 
  AddStep(1, 0, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kMupPim));
  //9.1.0 Reconstructed relative angle between products of HNL decay 
  AddStep(1, 0, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kMupPim));

  //6.1.1 e+ + pi-
  AddStep(1, 1, StepBase::kCut,     "HMP  PID electron cut",        new hnl_global_selection::PositronPIDCut());
  //7.1.1
  AddStep(1, 1, StepBase::kCut,     "invariant mass cut",           new hnl_global_selection::HNLInvMassCut(AnaHNLCandidate::kPosPim));
  //8.1.1 HNL reconstructed angle theta to Z axis 
  AddStep(1, 1, StepBase::kCut,     "HNL theta cut",                new hnl_global_selection::HNLThetaCut(AnaHNLCandidate::kPosPim));
  //9.1.1 Reconstructed relative angle between products of HNL decay 
  AddStep(1, 1, StepBase::kCut,     "Products relative angle cut",  new hnl_global_selection::HNLRelAngleCut(AnaHNLCandidate::kPosPim));

  SetBranchAlias(0, "mum + pip", 0, 0);
  SetBranchAlias(1, "ele + pip", 0, 1);
  SetBranchAlias(2, "mup + pim", 1, 0);
  SetBranchAlias(3, "pos + pim", 1, 1);
}

//**************************************************
bool hnl_global_TracksBase_selection::Find_HNL_candidate::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  // reset the vertex 
  if (box.Vertex) delete box.Vertex;
  box.Vertex = NULL;

  // reset the candidate
  ToyHNLBoxB& hnlBox = static_cast<ToyHNLBoxB&>(box);
  if (hnlBox.hnlCandidate) delete hnlBox.hnlCandidate;
  hnlBox.hnlCandidate = NULL;

  AnaVertexB* vertex_tmp = NULL;
  for (int i=0; i<event.nVertices; i++){
    vertex_tmp = event.Vertices[i];
    if (vertex_tmp)
      break;
  }

  //should have a valid vertex
  if (!vertex_tmp)
    return false;  

  // look at verticies with 1 or 2 constituents. second condition in order to prevent some possible errors. Not sure it can happen, but...
  if (vertex_tmp->nTracks > 2 || vertex_tmp->nTracks < 1) return false;

  AnaVertexB* vertex = new AnaVertexB();
  vertex = vertex_tmp->Clone();
  utils::CreateArray(vertex->Tracks, 2);
  vertex->nTracks = 0;

  vertex->Tracks[0] = vertex_tmp->Tracks[0];
  ++vertex->nTracks;

  Double_t range = -1.;

  if (vertex_tmp->nTracks == 2) {
    vertex->Tracks[1] = vertex_tmp->Tracks[1];
    ++vertex->nTracks;
  } else {
  // look for second track if the vertex has one constituent
    bool SecondTrackFound = false;
    for (Int_t i = 0; i < event.nTracks; ++i) {
      if (!event.Tracks[i]) continue;

      if (event.Tracks[i]->UniqueID == vertex_tmp->Tracks[0]->UniqueID)
        continue;

      // find range between two linear extrapolations of the tracks
      TVector3 Track1Dir   = utils::ArrayToTVector3(event.Tracks[i]->DirectionStart);
      TVector3 Track1Start(event.Tracks[i]->PositionStart[0], event.Tracks[i]->PositionStart[1], event.Tracks[i]->PositionStart[2]);

      TVector3 Track2Dir   = utils::ArrayToTVector3(vertex->Tracks[0]->DirectionStart);
      TVector3 Track2Start(vertex->Tracks[0]->PositionStart[0], vertex->Tracks[0]->PositionStart[1], vertex->Tracks[0]->PositionStart[2]);      

      TVector3 norm = Track1Dir.Cross(Track2Dir);
      norm = norm.Mag();

      range = abs(norm.Dot(Track2Start - Track1Start));
   
      if (range < _assign_track_range) {
        if (!SecondTrackFound) {
          vertex->Tracks[1] = event.Tracks[i]->Clone();
          ++vertex->nTracks;
          SecondTrackFound = true;
          // if third track found:
        } else return false;
      }        
    }
  }

  if (vertex->nTracks != 2) return false;

  hnlBox.range = range;

  // if one of the tracks starts before the vertex move the vertex position in order to pass FV cut
  if (vertex->Tracks[0]->PositionStart[2] < vertex->Position[2])
    for (Int_t i = 0; i < 4; ++i)
      vertex->Position[i] = vertex->Tracks[0]->PositionStart[i];

  if (vertex->Tracks[1]->PositionStart[2] < vertex->Position[2])
    for (Int_t i = 0; i < 4; ++i)
      vertex->Position[i] = vertex->Tracks[1]->PositionStart[i];
  
  box.Vertex = vertex->Clone();
  hnlBox.hnlCandidate = new AnaHNLCandidate(box.Vertex);

  // fill the main track 
  box.MainTrack = hnlBox.hnlCandidate->trackNegative;
  box.HMPtrack  = hnlBox.hnlCandidate->trackPositive;

  // define veto type 
  if (anaUtils::InFiducialVolume(SubDetId::kTPC1, vertex->Position)){
    hnlBox.VetoType = SubDetId::kTPC1;
  }
  else if (anaUtils::InFiducialVolume(SubDetId::kTPC2, vertex->Position)){
    hnlBox.VetoType = SubDetId::kTPC2;
  }
  else if (anaUtils::InFiducialVolume(SubDetId::kTPC3, vertex->Position)){
    hnlBox.VetoType = SubDetId::kTPC3;
    // out of FV:
  } else return false;

  // initialize array 
  for (Int_t i = 0; i < 3; ++i)
    utils::CreateArray(hnlBox.SameTPCTracks, event.nTracks);

  // fill TPC tracks arrays
  for (Int_t i = 0; i < event.nTracks; ++i) {
    if (!event.Tracks[i]) continue;

    // should start in the same TPC
    if (hnl_analysis_utils::InTPCFV(hnlBox.VetoType, event.Tracks[i]->PositionStart)) {
      hnlBox.SameTPCTracks[hnlBox.nSameTPCTracks] = event.Tracks[i]->Clone(); ++hnlBox.nSameTPCTracks;
    }
  }

  // set the proper size for array
  utils::ResizeArray(hnlBox.SameTPCTracks, hnlBox.nSameTPCTracks);
  

  return true;
}

//**************************************************
bool hnl_global_TracksBase_selection::HNL_CandidateCut::Apply(AnaEventB& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  ToyHNLBoxB* hnlBox = static_cast<ToyHNLBoxB*>(&box);

  if (!hnlBox->hnlCandidate) return false;
  if (!hnlBox->hnlCandidate->IsSuccess()) return false; 

  // should have good tracks
  if (!hnl_global_actions::TrackQualityCut(*hnlBox->hnlCandidate->trackNegative, _min_tpc_nodes)) return false;
  if (!hnl_global_actions::TrackQualityCut(*hnlBox->hnlCandidate->trackPositive, _min_tpc_nodes)) return false;
  if (hnlBox->hnlCandidate->trackNegative->Momentum < _min_momentum) return false;
  if (hnlBox->hnlCandidate->trackPositive->Momentum < _min_momentum) return false;

  // number of good quality tracks in the same TPC
  for(Int_t i = 0; i < hnlBox->nSameTPCTracks - 1; ++i) {
    if (!hnl_global_actions::TrackQualityCut(*hnlBox->SameTPCTracks[i], _min_tpc_nodes)) continue;
    if (hnlBox->SameTPCTracks[i]->Momentum < _min_momentum) continue;
    ++hnlBox->nSameTPCqualtyTracks;
  }

  // second track should start not farther next detector
  if (hnlBox->VetoType == SubDetId::kTPC1) {
    if (!anaUtils::InFiducialVolume(SubDetId::kTPC1, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart) &&
      !anaUtils::InFiducialVolume(SubDetId::kFGD1, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart))
      return false;
  } else if (hnlBox->VetoType == SubDetId::kTPC2) {
    if (!anaUtils::InFiducialVolume(SubDetId::kTPC2, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart) &&
      !anaUtils::InFiducialVolume(SubDetId::kFGD2, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart))
      return false;
  } else if (hnlBox->VetoType == SubDetId::kTPC3) {
    if (!anaUtils::InFiducialVolume(SubDetId::kTPC3, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart) &&
      !anaUtils::InFiducialVolume(SubDetId::kDSECAL, hnlBox->hnlCandidate->Vertex->Tracks[1]->PositionStart))
      return false;
  }*/

//  return true;
//}


