#ifndef oaAnalysisHNL_Converter_h
#define oaAnalysisHNL_Converter_h

#include "oaAnalysisTreeConverter.hxx"
#include "DataClassesHNL.hxx"
#include "Parameters.hxx"
#include "BeamBunchingHNL.hxx"

#include "TH1D.h"

class oaAnalysisHNL_Converter: public oaAnalysisTreeConverter{

 public:

  oaAnalysisHNL_Converter(){
  	oaAnalysisTreeConverter();
    _apply_tof_corr = (bool)ND::params().GetParameterI("hnlAnalysis.Corrections.EnableToFcorr");
   }
  virtual ~oaAnalysisHNL_Converter(){}
 
  //----------------
  virtual AnaTrueVertexB*   MakeTrueVertex()  { return new AnaHNLTrueVertex(); }

  /// Override to make it simplier so not to care about various local stuff 
  virtual void FillBunchInfo(AnaSpill* spill);
  
  /// Override the functions in order to use HNL specific bunching
  virtual void GetBunchPIDs();
  virtual void GetBunchVertices();
  
  /// Override the base class so to be able to retrieve info for HNL
  virtual bool FillTrueVertexInfo(ND::TTruthVerticesModule::TTruthVertex* true_vertex, AnaTrueVertex* vertex, int v, AnaSpill* spill);
  
  /// Override the base class function to set the time of true/reco objects
  virtual void FillInfo(AnaSpill* spill);

  // For TREx analysis need to fill NNodes var in a correct way
  virtual void FillTpcTrackInfo(ND::TGlobalReconModule::TTPCObject& tpcTrack, AnaTPCParticle* seg);

  protected:
  
  /// HNL bunching
  BeamBunchingHNL _bunchingHNL;
  
  /// Whether to account for TOF effects
  bool _apply_tof_corr;

  /// Histoes for flux reweight
  /// wheather to reweight flux
  bool _FluxReweight;
  /// nominal flux weight
  TH1D* _NomHisto;
  /// reweight for neutrino from kaons
  TH1D* _ReweightHisto;
};


#endif

