#include "hnlCategoryUtils.hxx"
#include "AnalysisUtils.hxx"

using namespace hnl_categ_utils;


//********************************************************************
void hnl_categ_utils::AddCategories(const std::string prefix){
  //********************************************************************
  // create standart categories
  anaUtils::AddStandardCategories(prefix);

  target_tpc::AddCategories(prefix); 

  reaction_all_nu::AddCategories(prefix);
  
}

// Target
std::string target_tpc::target_types[NTARGET]   = { "Argon" , "Fluorine", "Carbon", "Oxygen", "Hydrogen", "Aluminium",    "other"};
int         target_tpc::target_codes[NTARGET]   = { 18      , 9         , 6       , 8       , 1         , 13         ,    CATOTHER};
int         target_tpc::target_colors[NTARGET]  = { 2       , 3         , 4       , 6       , 7         , 31         ,    COLOTHER};

//********************************************************************
int target_tpc::GetTargetCode(const AnaTrueVertex* trueVertex){
  //********************************************************************
  int code = CATNOTRUTH;
  
  if (!trueVertex)
    return code;

  // Nucleus PDG codes are 10LZZZAAAI, and we want to extract the Z value to
  // identify the element.
  
  if (trueVertex->TargetPDG == 2212) // fix for bug in oaAnalysis, see bugzilla 1015
    code = 1;
  else
    code = (trueVertex->TargetPDG / 10000) % 1000;

  return code;
}

//********************************************************************
int target_tpc::GetTargetCode(const AnaTrack* track){
  //********************************************************************
  if (!track)
    return CATNOTRUTH;

  if (!track->GetTrueParticle())
    return CATNOTRUTH;

  if (!track->GetTrueParticle()->TrueVertex)
    return CATNOTRUTH;

  AnaTrueVertex* trueVertex = static_cast<AnaTrueVertex*>(track->GetTrueParticle()->TrueVertex);
  return GetTargetCode(trueVertex); 

}

// Reactions
std::string reaction_all_nu::reac_types[NREAC] = {"CCQE", "2p2h" , "RES", "DIS", "COH", "NC", "#bar{#nu}_{#mu} CCQE", "#bar{#nu}_{#mu} 2p2h", "#bar{#nu}_{#mu} RES", "#bar{#nu}_{#mu} DIS", "#bar{#nu}_{#mu} COH", "#bar{#nu}_{#mu} NC", "#nu_{e}, #bar{#nu}_{e}", "other" , "out FV"};
int reaction_all_nu::reac_codes[NREAC]         = { 0    , CAT2P2H, 1    , 2    , 3    , 4   , 50                    , CAT2P2H + 50          , 51                   , 52                   , 53                   , 54                  , 6                       , CATOTHER, CATOUTFV};
int reaction_all_nu::reac_colors[NREAC]        = { 2    , COL2P2H, 3    , 4    , 7    , 6   , 31                    , 41                    , 46                   , 38                   , 30                   , 49                  , 65                      , COLOTHER, COLOUTFV};
  
//********************************************************************
int reaction_all_nu::GetReaction(const AnaTrueVertex* trueVertex, bool IsAntinu) {
  /* Classify reaction types
     -1 = no tru vertex associated to track
     0  = CCQE
     1  = CC RES
     2  = CC DIS
     3  = CC COH
     4  = NC
     50 = (anti-)numu CCQE
     51 = (anti-)numu RES
     52 = (anti-)numu DIS
     53 = (anti-)numu COH
     54 = (anti-)numu NC
     6 = nue/anti-nue
     999 = other... nutau?
     -1 = no truth
  */
  Int_t reac   = trueVertex->ReacCode;
  Int_t nutype = trueVertex->NuPDG;

 // nue/anti-nue BKG
  if (abs(nutype) == 12)                 return 6;

  // (anti-)nu BKG
  else if (IsAntinu && nutype == 14) {    // nu BKG if antinu
    if (abs(reac) == 1)                  return 50;
    if (abs(reac) == 2 )                 return CAT2P2H + 50;
    // in NuWro prod 5 2p2h code is 70 (Neut does not have 70 at all)
    if (abs(reac) == 70 ) return CAT2P2H;
    if (abs(reac) >10 && abs(reac)<14)   return 51;
    if (abs(reac) >16 && abs(reac)<30)   return 52;
    if (abs(reac) ==16)                  return 53;
    if (abs(reac) >30)                   return 54;
  }
  else if ( ! IsAntinu && nutype == -14) { // antinu BKG if nu
    if (abs(reac) == 1)                  return 50;
    if (abs(reac) == 2 )                 return CAT2P2H + 50;
    // in NuWro prod 5 2p2h code is 70 (Neut does not have 70 at all)
    if (abs(reac) == 70 ) return CAT2P2H;
    if (abs(reac) >10 && abs(reac)<14)   return 51;
    if (abs(reac) >16 && abs(reac)<30)   return 52;
    if (abs(reac) ==16)                  return 53;
    if (abs(reac) >30)                   return 54;
  }

  // (anti-)numu
  else if (abs(nutype) == 14) {
    if (abs(reac) == 1)                  return 0;
    if (abs(reac) == 2 )                 return CAT2P2H;
    // in NuWro prod 5 2p2h code is 70 (Neut does not have 70 at all)
    if (abs(reac) == 70 ) return CAT2P2H;
    if (abs(reac) >10 && abs(reac)<14)   return 1;
    if (abs(reac) >16 && abs(reac)<30)   return 2;
    if (abs(reac) ==16)                  return 3;
    if (abs(reac) >30)                   return 4;
  }

  return CATOTHER; // nu-tau??
}

//********************************************************************
int reaction_all_nu::GetReaction(const AnaTrack* track, bool IsAntinu){
  //********************************************************************
  
  (void)IsAntinu;
  
  if (!track)
    return CATNOTRUTH;

  if (!track->GetTrueParticle())
    return CATNOTRUTH;

  if (!track->GetTrueParticle()->TrueVertex)
    return CATNOTRUTH;

  AnaTrueVertex* trueVertex = static_cast<AnaTrueVertex*>(track->GetTrueParticle()->TrueVertex);
  return GetReaction(trueVertex); 

}

//********************************************************************
void hnl_categ_utils::FillTruthTreeCategories(const AnaTrueVertex& trueVertex, const SubDetId::SubDetEnum det, bool IsAntinu){
//********************************************************************
  FillTruthTreeCategories(trueVertex, "", det, IsAntinu);
}

//********************************************************************
void hnl_categ_utils::FillTruthTreeCategories(const AnaTrueVertex& trueVertex, const std::string& prefix, const SubDetId::SubDetEnum det, 
    bool IsAntinu){
//********************************************************************
  
  using namespace anaUtils;

  //general ones,  no dependency on FV
  anaUtils::_categ->SetCode(prefix + "nutype",        trueVertex.NuPDG,     CATOTHER); 
  
  anaUtils::_categ->SetCode(prefix + "particle",      trueVertex.LeptonPDG, CATOTHER);

  anaUtils::_categ->SetCode(prefix + "detector",      trueVertex.Detector,  CATOTHER);

  int target          = target_tpc::GetTargetCode(&trueVertex);
  anaUtils::_categ->SetCode(prefix + "target",        target,               CATOTHER);
  
  //this assumes psycheUtils TPC volume definition is wider than the one in trex!
  bool InTPCFV = hnl_analysis_utils::InTPCFV(det, trueVertex.Position);
   
  int reac, reacCC, reac_AllNu;

  // hnlDetectorDefinition.hxx content refined TPCs size data
  if (InTPCFV) {
    reac = GetReactionNoFgdFv(    trueVertex, IsAntinu);
    reac_AllNu = reaction_all_nu::GetReaction(&trueVertex, IsAntinu);
    if (reac >= 0 && reac < 4) reacCC = 1;
    else if (reac == CAT2P2H) reacCC = 1;
    else reacCC = reac;
  } else {
    reac       = CATOUTFV;
    reac_AllNu = CATOUTFV;
    reacCC     = CATOUTFV;
  }

  int reacnofv    = GetReactionNoFgdFv(    trueVertex, IsAntinu);
  int reacnAllNunofv  = reaction_all_nu::GetReaction(&trueVertex, IsAntinu);

  anaUtils::_categ->SetCode(prefix + "reaction",          reac, CATOTHER);
  
  anaUtils::_categ->SetCode(prefix + "reactionCC",        reacCC, CATOTHER);

  anaUtils::_categ->SetCode(prefix + "reactionAllNu",     reac_AllNu, CATOTHER);

  anaUtils::_categ->SetCode(prefix + "reactionAllNunofv", reacnAllNunofv, CATOTHER);

  anaUtils::_categ->SetCode(prefix + "reactionnofv",  reacnofv,             CATOTHER); 
 
  int topol                   = !InTPCFV ? CATOUTFV : GetTopology(          trueVertex, det, IsAntinu);
  anaUtils::_categ->SetCode(prefix + "topology",          topol);

  int topol_no1pi             = !InTPCFV ? CATOUTFV : GetTopology_no1pi(    trueVertex, det, IsAntinu);
  anaUtils::_categ->SetCode(prefix + "topology_no1pi",    topol_no1pi);

  int topol_withpi0           = !InTPCFV ? CATOUTFV : GetTopology_withpi0(  trueVertex, det, IsAntinu);
  anaUtils::_categ->SetCode(prefix + "topology_withpi0",  topol_withpi0);
  
  int mectopol                = !InTPCFV ? CATOUTFV : GetMECTopology(       trueVertex, det, IsAntinu);
  anaUtils::_categ->SetCode(prefix + "mectopology",       mectopol);

  int topol_ccpizero          = !InTPCFV ? CATOUTFV : GetTopologyCCPiZero(  trueVertex, det, IsAntinu);
  anaUtils::_categ->SetCode(prefix + "topology_ccpizero", topol_ccpizero);
  
}

//********************************************************************
void hnl_categ_utils::FillCategories(AnaEventB* event, AnaTrack* track, const SubDetId::SubDetEnum det, bool IsAntinu){
//********************************************************************
  FillCategories(event, track, "", det, IsAntinu);
}

//********************************************************************
void hnl_categ_utils::FillCategories(AnaEventB* event, AnaTrack* track, const std::string& prefix, const SubDetId::SubDetEnum det, 
    bool IsAntinu){
//********************************************************************

  // For each classification 

  anaUtils::_categ->SetCode(prefix + "reaction",                  CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "reactionCC",                CATNOTRUTH);
  //ToDo
  anaUtils::_categ->SetCode(prefix + "reactionnofv",              CATNOTRUTH);
  //
  anaUtils::_categ->SetCode(prefix + "topology",                  CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "topology_no1pi",            CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "topology_withpi0",          CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "mectopology",               CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "topology_ccpizero",         CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "detector",                  CATNOTRUTH);
  anaUtils::_categ->SetCode(prefix + "target",                    CATNOTRUTH);

  if (!track) return;
  
  using namespace anaUtils;

  // ----- Particle ------------------------------
  if (track->GetTrueParticle()) {

    if (track->GetTrueParticle()->PDG != 0) {
      AnaTrueParticle* trueTrack = static_cast<AnaTrueParticle*>(track->GetTrueParticle());
      AnaTrueParticleB* primary = anaUtils::GetTrueParticleByID(*event, trueTrack->PrimaryID);

      anaUtils::_categ->SetCode(prefix + "particle",  trueTrack->PDG,         CATOTHER);
      anaUtils::_categ->SetCode(prefix + "parent",    trueTrack->ParentPDG,   CATOTHER);
      anaUtils::_categ->SetCode(prefix + "gparent",   trueTrack->GParentPDG,  CATOTHER);

      if (primary) {
        anaUtils::_categ->SetCode(prefix + "primary", primary->PDG, CATOTHER);
      }

      if (trueTrack->TrueVertex) {
        
        //this assumes psycheUtils TPC volume definition is wider than the one in trex!
        bool InTPCFV = hnl_analysis_utils::InTPCFV(det, trueTrack->TrueVertex->Position);

        int topol                   = !InTPCFV ? CATOUTFV : GetTopology(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex),          det, IsAntinu);
        anaUtils::_categ->SetCode(prefix + "topology",            topol);

        int topol_no1pi             = !InTPCFV ? CATOUTFV : GetTopology_no1pi(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex),    det, IsAntinu);
        anaUtils::_categ->SetCode(prefix + "topology_no1pi",      topol_no1pi);

        int topol_withpi0         = !InTPCFV ? CATOUTFV :   GetTopology_withpi0(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex),  det, IsAntinu);
        anaUtils::_categ->SetCode(prefix + "topology_withpi0",    topol_withpi0);
        
        int mectopol                = !InTPCFV ? CATOUTFV : GetMECTopology(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex),       det, IsAntinu);
        anaUtils::_categ->SetCode(prefix + "mectopology",         mectopol);

        int topol_ccpizero          = !InTPCFV ? CATOUTFV : GetTopologyCCPiZero(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex),  det);
        anaUtils::_categ->SetCode(prefix + "topology_ccpizero",   topol_ccpizero); 

        anaUtils::_categ->SetCode(prefix+"nutype", static_cast<AnaTrueVertex*>(trueTrack->TrueVertex)->NuPDG);

        int reac, reacCC, reac_AllNu;

        // hnlDetectorDefinition.hxx content refined TPCs size data
        if (InTPCFV) {
          reac = GetReactionNoFgdFv(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex), IsAntinu);
          reac_AllNu = reaction_all_nu::GetReaction(static_cast<AnaTrueVertex*>(trueTrack->TrueVertex), IsAntinu);
          if (reac >= 0 && reac < 4) reacCC = 1;
          else if (reac == CAT2P2H) reacCC = 1;
          else reacCC = reac;
        } else {
          reac       = CATOUTFV;
          reac_AllNu = CATOUTFV;
          reacCC     = CATOUTFV;
        }
      
        int reacnofv    = GetReactionNoFgdFv(*static_cast<AnaTrueVertex*>(trueTrack->TrueVertex), IsAntinu);
        int reacnAllNunofv  = reaction_all_nu::GetReaction(static_cast<AnaTrueVertex*>(trueTrack->TrueVertex), IsAntinu);
      
        anaUtils::_categ->SetCode(prefix + "reaction",          reac, CATOTHER);
        
        anaUtils::_categ->SetCode(prefix + "reactionCC",        reacCC, CATOTHER);
      
        anaUtils::_categ->SetCode(prefix + "reactionAllNu",     reac_AllNu, CATOTHER);
      
        anaUtils::_categ->SetCode(prefix + "reactionAllNunofv", reacnAllNunofv, CATOTHER);
      
        anaUtils::_categ->SetCode(prefix + "reactionnofv",  reacnofv,             CATOTHER);       
 

        anaUtils::_categ->SetCode(prefix + "detector",  static_cast<AnaTrueVertex*>(trueTrack->TrueVertex)->Detector,       CATOTHER);
        anaUtils::_categ->SetCode(prefix + "target",    target_tpc::GetTargetCode(static_cast<AnaTrueVertex*>(trueTrack->TrueVertex)),  CATOTHER);
      }
    }
  }
}







