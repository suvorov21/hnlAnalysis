#include "EventBoxUtilsHNL.hxx"
#include "DataClassesHNL.hxx"
#include "EventBoxId.hxx"

//********************************************************************
void boxUtils::FillTrajsChargedFromHNLVertex(AnaEventB& event){
//********************************************************************

  EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  // Don't fill it when already filled by other selection
  if (EventBox->TrueObjectsInGroup[EventBoxHNL::kTrueTracksChargedFromHNLVertex]) return;

  AnaTrueParticleB* trajs[NMAXTRUEPARTICLES];
  
  int nTraj = 0;
  for (int i = 0; i< event.nTrueParticles; i++){
    if (!event.TrueParticles[i]) continue;
    if (!event.TrueParticles[i]->TrueVertex) continue;
    if (event.TrueParticles[i]->TrueVertex->Bunch != event.Bunch) continue;
    if (event.TrueParticles[i]->Charge == 0)continue;
    
    // Just want primary particles
    if (event.TrueParticles[i]->ParentPDG != 0) continue; 
  
    trajs[nTraj++] = event.TrueParticles[i];

  }
  
  if (NMAXTRUEPARTICLES < (UInt_t)nTraj) nTraj = NMAXTRUEPARTICLES;
  anaUtils::CreateArray(EventBox->TrueObjectsInGroup[EventBoxHNL::kTrueTracksChargedFromHNLVertex], nTraj);
  anaUtils::CopyArray(trajs, EventBox->TrueObjectsInGroup[EventBoxHNL::kTrueTracksChargedFromHNLVertex], nTraj);
  EventBox->nTrueObjectsInGroup[EventBoxHNL::kTrueTracksChargedFromHNLVertex] = nTraj;
}


