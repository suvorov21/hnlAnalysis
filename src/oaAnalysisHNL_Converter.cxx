#include "oaAnalysisHNL_Converter.hxx"
#include <sstream>
#include "TFile.h"

//#define TRExAnalysis

//*****************************************************************************
void oaAnalysisHNL_Converter::FillInfo(AnaSpill* spill){
  //*****************************************************************************

  oaAnalysisTreeConverter::FillInfo(spill);

  if (!spill) return;

  // Use HNL to set bunches for true object
  // Loop over true tracks and vertices
  std::vector<AnaTrueParticleB*>::iterator  it1 = spill->TrueParticles.begin();
  std::vector<AnaTrueVertexB*>::iterator it2 = spill->TrueVertices.begin();


  for (; it1 != spill->TrueParticles.end(); it1++){
    if (!*it1) continue;
    AnaTrueParticle* track =  static_cast<AnaTrueParticle*>(*it1);
    
    track->Position[3]    += _bunchingHNL.GetDeltaTOF();
    track->PositionEnd[3] += _bunchingHNL.GetDeltaTOF();
    
    track->Bunch = _bunchingHNL.GetBunch(track->Position[3] + _trueBunchShift, spill->EventInfo->Run, IsMC, cosmic_mode); 
  }

  for (; it2 != spill->TrueVertices.end(); it2++){
    if (!*it2) continue;
    AnaTrueVertex* vertex = static_cast<AnaTrueVertex*>(*it2);
    
    vertex->Position[3] += _bunchingHNL.GetDeltaTOF();
    
    vertex->Bunch = _bunchingHNL.GetBunch(vertex->Position[3] + _trueBunchShift, spill->EventInfo->Run, IsMC, cosmic_mode); 
  }

  // Reco objects
  for (size_t i = 0; i < spill->Bunches.size(); ++i) {
    if (!static_cast<AnaBunchB*>(spill->Bunches[i]))
      continue;

    // loop over verticies
    for (size_t j = 0; j < static_cast<AnaBunchB*>(spill->Bunches[i])->Vertices.size(); ++j) {
      if (!static_cast<AnaBunchB*>(spill->Bunches[i])->Vertices[j])
        continue;
      static_cast<AnaBunchB*>(spill->Bunches[i])->Vertices[j]->Position[3] += _bunchingHNL.GetDeltaTOF();
    }

    // loop over tracks. Change only start/end time. Don't care about segments. Should be carefull here.
    for (size_t j = 0; j < static_cast<AnaBunchB*>(spill->Bunches[i])->Particles.size(); ++j) {
      if (!static_cast<AnaBunchB*>(spill->Bunches[i])->Particles[j])
        continue;
      static_cast<AnaBunchB*>(spill->Bunches[i])->Particles[j]->PositionStart[3] += _bunchingHNL.GetDeltaTOF();
      static_cast<AnaBunchB*>(spill->Bunches[i])->Particles[j]->PositionEnd[3]   += _bunchingHNL.GetDeltaTOF();
    }
  }

  // Loop over out of bunch objects 
  if (spill->OutOfBunch) {
    for (size_t j = 0; j < spill->OutOfBunch->Vertices.size(); ++j) {
      if (!spill->OutOfBunch->Vertices[j])
        continue;
      spill->OutOfBunch->Vertices[j]->Position[3] += _bunchingHNL.GetDeltaTOF();
    }
    for (size_t j = 0; j < spill->OutOfBunch->Particles.size(); ++j) {
      if (!spill->OutOfBunch->Particles[j])
        continue;
      spill->OutOfBunch->Particles[j]->PositionStart[3] += _bunchingHNL.GetDeltaTOF();
      spill->OutOfBunch->Particles[j]->PositionEnd[3]   += _bunchingHNL.GetDeltaTOF();
    }
  }

  // Loop over FGDtimeBins
  for (size_t j = 0; j < spill->FgdTimeBins.size(); ++j) {
    if (!spill->FgdTimeBins[j])
      continue;
    
    AnaFgdTimeBin* bin = static_cast<AnaFgdTimeBin*>(spill->FgdTimeBins[j]); 
    
    // Apply the shift only if a true track exist --> i.e. not noise
    if (bin->G4ID <= 0) continue;
    
    bin->MinTime += _bunchingHNL.GetDeltaTOF();
    bin->MaxTime += _bunchingHNL.GetDeltaTOF();
    
  }

}

//*****************************************************************************
bool oaAnalysisHNL_Converter::FillTrueVertexInfo(ND::TTruthVerticesModule::TTruthVertex* true_vertex, AnaTrueVertex* vertex, int v, AnaSpill* spill){
  //*****************************************************************************
 
  if (!oaAnalysisTreeConverter::FillTrueVertexInfo(true_vertex, vertex, v, spill))
    return false;

  //HNL are saved in NEUT RooTracker format
  // Should return true to work normally with Genie input files
  if (!Neut)
    return true;



  AnaHNLTrueVertex* trueVertex = static_cast<AnaHNLTrueVertex*>(vertex); 

  bool status = false;

  //loop throught the vertex: yep, this will be additional loop to base class call, 
  //but dont want to copy the full method to this converter
  for (int roov = 0; roov < NNVtx; roov++) {
    ND::NRooTrackerVtx *lvtx = (ND::NRooTrackerVtx*) (*NVtx)[roov];

    // this is needed otherwise when running with highland for pro6 over a prod5 file
    // it crashes before doing the versioning check
    if ( ! lvtx->StdHepPdg) continue;

    // look for the current vertex
    if (vertex->ID != lvtx->TruthVertexID) continue;

    //fill the vars
    trueVertex->Weight      = lvtx->EvtWght;
    trueVertex->Probability = lvtx->EvtProb;
    //this how we store it
    // multiply because originaly XSec was saved with this multipliyer
    trueVertex->Mass        = lvtx->EvtXSec * 1E-33;
    /// Energy of ordinary neutrino, we need it for kaon systematics analysis
    trueVertex->OriginalNuEnergy    = lvtx->EvtDXSec;

    /*if (_FluxReweight) {
      Int_t _nomBin = _NomHisto->FindBin(trueVertex->OriginalNuEnergy);
      Int_t _newBin = _ReweightHisto->FindBin(trueVertex->OriginalNuEnergy);

      if (_NomHisto->GetBinContent(_nomBin) != 0) 
        trueVertex->Weight *= _ReweightHisto->GetBinContent(_newBin) / _NomHisto->GetBinContent(_nomBin);
    }*/

    Float_t arr1[3] = {lvtx->StdHepP4[2][0], lvtx->StdHepP4[2][1], lvtx->StdHepP4[2][2]};
    TVector3 vect1 = anaUtils::ArrayToTVector3(arr1);

    Float_t arr2[3] = {lvtx->StdHepP4[3][0], lvtx->StdHepP4[3][1], lvtx->StdHepP4[3][2]};
    TVector3 vect2 = anaUtils::ArrayToTVector3(arr2);

    trueVertex->RelAngle = vect1.Angle(vect2);

    // ToF correction calculations
    TVector3 MesonDecay(lvtx->NuParentDecX4[0], lvtx->NuParentDecX4[1], lvtx->NuParentDecX4[2]); // in cm from target
    TVector3 HNLDecay(lvtx->EvtVtx[0],lvtx->EvtVtx[1], lvtx->EvtVtx[2]); // in mm from ND center
    HNLDecay = HNLDecay * 0.1; // in cm
    HNLDecay.SetX(HNLDecay.X() - 322.2292);
    HNLDecay.SetY(HNLDecay.Y() - 814.557);
    HNLDecay.SetZ(HNLDecay.Z() + 28010.0);
    TVector3 HNLpath = HNLDecay - MesonDecay; // in cm

    Double_t p = sqrt(lvtx->StdHepP4[0][3] * lvtx->StdHepP4[0][3] - 1E-6*trueVertex->Mass * trueVertex->Mass); // in GeV
    Double_t beta = p / lvtx->StdHepP4[0][3];

    Double_t dTOF = HNLpath.Mag() * 0.01 * (1-beta)*1E3;
    dTOF /= beta*units::c_light; // in ns

    trueVertex->dToF = dTOF;

    // set the dTOF for bunching for massive HNL
    // assume one such a vertex present

    // Mass is stored as a cross-section in RooTracker tree. 
    // >1 is a cross-check that we analize signal samples but not active Nu MC
    if (trueVertex->Mass > 1 && _apply_tof_corr)
      _bunchingHNL.SetDeltaTOF(dTOF);

    status = true;

    break; 
  }

  return status;
}

//*****************************************************************************
void oaAnalysisHNL_Converter::FillTpcTrackInfo(ND::TGlobalReconModule::TTPCObject& tpcTrack, AnaTPCParticle* seg){
  //*****************************************************************************
  oaAnalysisTreeConverter::FillTpcTrackInfo(tpcTrack, seg);

  // in case of TREx analysis use proper nodes number calculations
#ifdef TRExAnalysis
    seg->NNodes = tpcTrack.NbFittedVerticalClusters + tpcTrack.NbFittedHorizontalClusters;
#endif

}

//*****************************************************************************
void oaAnalysisHNL_Converter::FillBunchInfo(AnaSpill* spill){
  //*****************************************************************************

  spill->OutOfBunch = MakeBunch();

  spill->OutOfBunch->Bunch = -1;

  // Distribute all global tracks in bunches
  GetBunchPIDs();

  // Distribute all global vertices in bunches
  GetBunchVertices();

  //loop over bunches
  for (unsigned int ibunch=0; ibunch<NBUNCHES+1; ibunch++) {

    if ( _bunchPIDs[ibunch].size() == 0 &&
        _bunchVertices[ibunch].size() == 0) continue;

    // warnings
    if ((unsigned int)_bunchPIDs[ibunch].size() > NMAXPARTICLES) {
      std::cout << "INFO: event " << EventID << " has " << (unsigned int)_bunchPIDs[ibunch].size() << " recon tracks (too many), "
        << "only the first " << NMAXPARTICLES << " will be stored (=> some warnings might appear)" << std::endl;
    }
    if ((unsigned int)_bunchVertices[ibunch].size() > NMAXVERTICES) {
      std::cout << "INFO: event " << EventID << " has " << (unsigned int)_bunchVertices[ibunch].size() << " recon vertices (too many), "
        << "only the first " << NMAXVERTICES << " will be stored (=> some warnings might appear)" << std::endl;
    }

    // create the bunch
    AnaBunch* bunch = NULL;
    if (ibunch < NBUNCHES) {

      bunch = static_cast<AnaBunch*>(MakeBunch());

      spill->Bunches.push_back(bunch);
      bunch->Bunch = ibunch;
    } else {
      bunch = static_cast<AnaBunch*>(spill->OutOfBunch);
    }


    // Fill PIDs information
    if(_bunchPIDs[ibunch].size()>0)
      FillPIDs(bunch, ibunch);

  } // End of loop over bunches

  // need to loop again over bunches to fill global vertices
  // (allowing the global vertices to point to PIDs in other bunches)
  FillGlobalVertices(spill);
}

//*****************************************************************************
void oaAnalysisHNL_Converter::GetBunchPIDs(){
  //*****************************************************************************
  for (unsigned int i=0;i<NBUNCHES+1;i++)
    _bunchPIDs[i].clear();

  for( int j=0; j<NPIDs; j++) {

    ND::TGlobalReconModule::TGlobalPID *globalTrack = (ND::TGlobalReconModule::TGlobalPID*)PIDs->UncheckedAt(j);
    if(!globalTrack) std::cout << "debug error 1100 in oaAnalysisConverter" << std::endl; // shouldn't happen!
    if(!globalTrack) continue; // if no global track is found, go to the next PID

    // check that the track is valid (the momentum is not NaN nor 0, etc)
    //    if (!IsValidTrack(globalTrack)) continue;

    // --- Get the bunch number -----
    double tTrack = GetVertexTime(*globalTrack);
    int ibunch = _bunchingHNL.GetBunchHNL(tTrack,RunID,IsMC, cosmic_mode);

    if (ibunch==-1){
      ibunch = NBUNCHES;
    }

    _bunchPIDs[ibunch].push_back(globalTrack);
  }
}

//*****************************************************************************
void oaAnalysisHNL_Converter::GetBunchVertices(){
  //*****************************************************************************

  for (unsigned int i=0;i<NBUNCHES+1;i++)  _bunchVertices[i].clear();

  for( int j=0; j<NVertices; j++) {
    ND::TGlobalReconModule::TGlobalVertex *globalVertex = (ND::TGlobalReconModule::TGlobalVertex*)Vertices->UncheckedAt(j);
    if(!globalVertex) std::cout << "debug error 1101 in oaAnalysisConverter" << std::endl; // shouldn't happen!
    if(!globalVertex) continue; // if no global vertex is found, go to the next vertex

    // check that the track is valid (the momentum is not NaN nor 0, etc)
    //    if (!IsValidTrack(globalTrack)) continue;

    // --- Get the bunch number -----
    double tVertex = (*globalVertex).Position.T();
    int ibunch = _bunchingHNL.GetBunchHNL(tVertex, RunID, IsMC, cosmic_mode);

    if (ibunch==-1){
      ibunch = NBUNCHES;
    }


    _bunchVertices[ibunch].push_back(globalVertex);
  }
}
