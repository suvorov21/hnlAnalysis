#include "pileUpSelectionHNL.hxx"
#include "baseSelection.hxx"
#include "CutUtils.hxx"
#include "SubDetId.hxx"
#include "EventBoxUtils.hxx"
#include "hnlGlobalReconSelection.hxx"
#include "hnlAnalysisUtils.hxx"
#include "EventBoxId.hxx"


//********************************************************************
pileUpSelectionHNL::pileUpSelectionHNL(bool forceBreak): SelectionBase(forceBreak, EventBoxId::kEventBoxTracker) {
  //********************************************************************
  
  // define the steps
  //DefineSteps();
}

//********************************************************************
void pileUpSelectionHNL::DefineDetectorFV() {
  //********************************************************************
  SetDetectorFV(SubDetId::kTPC);
}

//********************************************************************
void pileUpSelectionHNL::DefineSteps(){
  //********************************************************************

  AddStep(StepBase::kCut,    "Event quality",      new EventQualityCut(), true);
  AddStep(StepBase::kAction, "Fill true vertex",   new FindTrueVertex());
	
  AddStep(StepBase::kAction, "Find FGD time bins", new FindFGDTimeBins());
  
	AddSplit(6);	// P0D FGD1 FGD2 TPC1 TPC2 TPC3
		
	AddStep(0, StepBase::kAction, "Find TPC1 P0D Veto Vertex",            new FindTPC1_P0DVeto());
  AddStep(0, StepBase::kCut,    "Find TPC1 P0D Veto",           				new TPC1_P0DVetoCut());
	
	AddStep(1, StepBase::kCut,    "Find TPC2 FGD1 Veto",           				new TPC2_FGD1VetoCut());
	
	AddStep(2, StepBase::kCut,    "Find TPC3 FGD2 Veto",                  new TPC3_FGD2VetoCut());

  AddStep(3, StepBase::kAction, "Find tracks starts in TPC1",           new FindTPC1start());
  AddStep(3, StepBase::kCut,    "TPC1 tracks starts cut",               new TPC1startCut());  

  AddStep(4, StepBase::kAction, "Find tracks starts in TPC2",           new FindTPC2start());
  AddStep(4, StepBase::kCut,    "TPC2 tracks starts cut",               new TPC2startCut()); 

  AddStep(5, StepBase::kAction, "Find tracks starts in TPC3",           new FindTPC3start());
  AddStep(5, StepBase::kCut,    "TPC3 tracks starts cut",               new TPC3startCut()); 

	SetBranchAlias(0,"TPC1_P0DVeto",   0);
	SetBranchAlias(1,"TPC2_FGD1Veto",  1);
	SetBranchAlias(2,"TPC3_FGD2Veto",  2);
  SetBranchAlias(3,"TPC1_StartVeto", 3);
  SetBranchAlias(4,"TPC2_StartVeto", 4);
  SetBranchAlias(5,"TPC3_StartVeto", 5);
}

//********************************************************************
bool pileUpSelectionHNL::FillEventSummary(AnaEventC& eventC, Int_t allCutsPassed[]){
  //********************************************************************

  (void)allCutsPassed;
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  // Should set event sample enumeration here
  static_cast<AnaEventSummaryB*>(event.Summary)->EventSample = SampleId::kUnassigned; 
  return (static_cast<AnaEventSummaryB*>(event.Summary)->EventSample !=  SampleId::kUnassigned);
  
}

//**************************************************
void pileUpSelectionHNL::InitializeEvent(AnaEventC& eventC){
//**************************************************
 
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  // Create the appropriate EventBox if it does not exist yet
  if (!event.EventBoxes[EventBoxId::kEventBoxTracker])
    event.EventBoxes[EventBoxId::kEventBoxTracker] = new EventBoxHNL();

  EventBoxHNL* EventBox = static_cast<EventBoxHNL*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);
 
  boxUtils::FillTrajsChargedInTPC(event);
 
}

//********************************************************************    
bool FindTrueVertex::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //********************************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyBoxPileUp& box = static_cast<ToyBoxPileUp&>(boxB);

 
  // read just one true vertex for proper MC weigth
  if (box.Vertex) 
    delete box.Vertex;
  box.Vertex = NULL;
  AnaTrackB* track = NULL;

  for (Int_t i = 0; i < event.nParticles; ++i) {
    track = static_cast<AnaTrackB*>(event.Particles[i]);
    if (track)
      break;
  }

  if (!track)
    return false;

  box.Vertex = new AnaVertexB();
  anaUtils::CreateArray(box.Vertex->Particles, 1);

  box.Vertex->nParticles = 0;
  box.Vertex->Particles[0] = track;
  ++box.Vertex->nParticles;
   
  for(int i = 0; i < 4; ++i)
    box.Vertex->Position[i] = track->PositionStart[i];

  if( track->GetTrueParticle() )
    if (track->GetTrueParticle()->TrueVertex)
      box.Vertex->TrueVertex = track->GetTrueParticle()->TrueVertex;
  
  return true;
  
}

//********************************************************************    
bool FindFGDTimeBins::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //********************************************************************
    
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyBoxPileUp& box = static_cast<ToyBoxPileUp&>(boxB);
  
  // Fill FGD time bins   
  Float_t selTime = _bunchingHNL.GetBunchCentralTime(event, event.Bunch); 
  
  // Initialize arrays
  anaUtils::CreateArray(box.FgdTimeBins[0], event.nFgdTimeBins);
  anaUtils::CreateArray(box.FgdTimeBins[1], event.nFgdTimeBins);

  // Loop through the FGD time bins and get those satisfying time cuts
  for( Int_t i = 0; i < event.nFgdTimeBins; i++ ){

    AnaFgdTimeBinB *FgdTimeBin = event.FgdTimeBins[i];

    if (!FgdTimeBin) continue;

    Float_t binTime = FgdTimeBin->MinTime - _binTimeOffsetToBunch;

    
    // Should correspond to the current bunch
    // false --> i.e. not cosmic mode
    if (_bunchingHNL.GetBunch(binTime, event.EventInfo.Run, event.GetIsMC(), false) != event.Bunch) continue;


    box.FgdTimeBins[0][box.nFgdTimeBins[0]++] = FgdTimeBin;
    box.FgdTimeBins[1][box.nFgdTimeBins[1]++] = FgdTimeBin;

  }
  // resize the arrays
  anaUtils::ResizeArray(box.FgdTimeBins[0], box.nFgdTimeBins[0], event.nFgdTimeBins);
  anaUtils::ResizeArray(box.FgdTimeBins[1], box.nFgdTimeBins[1], event.nFgdTimeBins);


  // sort separately FGD1 and FGD2 time bins
  if (box.nFgdTimeBins[0]>0){ 
    std::sort(box.FgdTimeBins[0], box.FgdTimeBins[0] + box.nFgdTimeBins[0], 
        hnl_analysis_utils::CompareFgdBinsNHits(SubDetId::kFGD1));
  }

  if (box.nFgdTimeBins[1]>0){ 
    std::sort(box.FgdTimeBins[1], box.FgdTimeBins[1] + box.nFgdTimeBins[1], 
        hnl_analysis_utils::CompareFgdBinsNHits(SubDetId::kFGD2));
  }

}


//********************************************************************    
bool FindTPC1_P0DVeto::Apply(AnaEventC& eventC, ToyBoxB& box) const{
//********************************************************************
 
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);

  mybox->nP0Dtracks = 0;
  
  for (int it = 0; it < event.nParticles; it++) {
    AnaTrackB* track = static_cast<AnaTrackB*>(event.Particles[it]);
    if (!track) continue;
    if (!anaUtils::TrackUsesOnlyDet(*track, SubDetId::kP0D)) continue;

    //should have an end close to downstream edge
    if (!hnl_global_selection::IsP0dDownstream(track->PositionStart, _trim_p0d) &&
        !hnl_global_selection::IsP0dDownstream(track->PositionEnd, _trim_p0d)) continue;

    if (mybox->nP0Dtracks >= (int)NMAXPARTICLES) continue;

    mybox->P0Dtracks[mybox->nP0Dtracks] = track;
    ++mybox->nP0Dtracks;
  }

  return true;

}

//********************************************************************
bool TPC1_P0DVetoCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //********************************************************************
   
  (void)event;
  
  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);
  return (mybox->nP0Dtracks != 0);
}

//********************************************************************
bool TPC2_FGD1VetoCut::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //********************************************************************
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyBoxPileUp& box = static_cast<ToyBoxPileUp&>(boxB);
  
  if (box.nFgdTimeBins[0] == 0)
    return false;
  
  if (box.FgdTimeBins[0][0]->NHits[0] > _max_fgd1_hits)
    return true;
  
  return false;
}

//********************************************************************
bool TPC3_FGD2VetoCut::Apply(AnaEventC& eventC, ToyBoxB& boxB) const{
  //********************************************************************
   
  
  AnaEventB& event = static_cast<AnaEventB&>(eventC);
  
  ToyBoxPileUp& box = static_cast<ToyBoxPileUp&>(boxB);

  if (box.nFgdTimeBins[1] == 0)
    return false;
  
  if (box.FgdTimeBins[1][0]->NHits[1] > _max_fgd2_hits)
    return true;
  
  return false;
}

//********************************************************************
bool pileUpUtils::FindTPCstart(AnaEventC& eventC, ToyBoxB& box, SubDetId::SubDetEnum det){
//********************************************************************
   
  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  int tpc = -1;
  if (det == SubDetId::kTPC1) tpc = 0;
  else if (det == SubDetId::kTPC2) tpc = 1;
  else if (det == SubDetId::kTPC3) tpc = 2;
  if (tpc == -1) return false;

  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);
  mybox->nTpcTracks[tpc] = 0;

  for (Int_t i = 0; i < event.nParticles; ++i) {
    AnaTrackB* track = static_cast<AnaTrackB*>(event.Particles[i]);
    if (!track) continue;
    if (anaUtils::GetDetector(track->PositionStart) != det) continue;
    
    mybox->TPCtracks[tpc][mybox->nTpcTracks[tpc]] = track;
    ++mybox->nTpcTracks[tpc];
  }

  return true;    
}

//********************************************************************
bool FindTPC1start::Apply(AnaEventC& event, ToyBoxB& box) const{
//********************************************************************
  
  (void)event;
  
  return pileUpUtils::FindTPCstart(event, box, SubDetId::kTPC1);  
}

//********************************************************************
bool TPC1startCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //********************************************************************
   
  (void)event;
  
  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);
  return (mybox->nTpcTracks[0] != 0);
}

//********************************************************************
bool FindTPC2start::Apply(AnaEventC& event, ToyBoxB& box) const{
//********************************************************************

  (void)event;
  
  return pileUpUtils::FindTPCstart(event, box, SubDetId::kTPC2);  
}

//********************************************************************
bool TPC2startCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //********************************************************************
  
  (void)event;
  
  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);
  return (mybox->nTpcTracks[1] != 0);
}

//********************************************************************
bool FindTPC3start::Apply(AnaEventC& event, ToyBoxB& box) const{
//********************************************************************
  
  (void)event;
  
  return pileUpUtils::FindTPCstart(event, box, SubDetId::kTPC3);  
}

//********************************************************************
bool TPC3startCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //********************************************************************
 
  (void)event;
  
  ToyBoxPileUp* mybox = static_cast<ToyBoxPileUp*>(&box);
  return (mybox->nTpcTracks[2] != 0);
}
