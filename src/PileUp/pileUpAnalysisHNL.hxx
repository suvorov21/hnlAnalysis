#ifndef pileUpAnalysisHNL_h
#define pileUpAnalysisHNL_h

#include "baseTrackerAnalysis.hxx"
#include "AnalysisUtils.hxx"
#include "pileUpSelectionHNL.hxx"
#include "oaAnalysisHNL_Converter.hxx"

class pileUpAnalysisHNL: public baseTrackerAnalysis {
 public:
  pileUpAnalysisHNL(AnalysisAlgorithm* ana=NULL);
  virtual ~pileUpAnalysisHNL(){}


  //---- These are mandatory functions
  void DefineSelections();
  void DefineInputConverters();
  void DefineMicroTrees(bool addBase=true);
  void FillMicroTrees(bool addBase=true);
  bool CheckFillTruthTree(const AnaTrueVertex& vtx){
    (void)vtx;
    return false;
  }
    
  void FillToyVarsInMicroTrees(bool addBase=true){(void)addBase;}
  void FillTruthTree(const AnaTrueVertex&){}
    
  void FillP0DVariables();
  void FillFGD1Variables();
  void FillFGD2Variables();

  void FillTPCVariables();
  
  bool Initialize();
  
  const ToyBoxPileUp& mybox(){return *static_cast<const ToyBoxPileUp*>( &box() );}
    
protected:

    /// Access to the flux weighting.
    FluxWeighting* _flux;
    
    enum enumStandardMicroTrees_pileUpAnalysisHNL{

    // first candidate
    NumFGD1Tracks = baseTrackerAnalysis::enumStandardMicroTreesLast_baseTrackerAnalysis+1,
    FGD1TrackMom,
    FGD1TrackCostheta,
    FGD1TrackPhi,
    FGD1TrackVId,
    FGD1TrackTId,
    FGD1TrackParentTId,
    FGD1TrackGParentTId,
    FGD1TrackStart,
    FGD1TrackEnd,
    NumFGD2Tracks,
    FGD2TrackMom,
    FGD2TrackCostheta,
    FGD2TrackPhi,
    FGD2TrackVId,
    FGD2TrackTId,
    FGD2TrackParentTId,
    FGD2TrackGParentTId,
    FGD2TrackStart,
    FGD2TrackEnd, 
    NumP0DTracks,
    P0DTrackMom,
    P0DTrackCostheta,
    P0DTrackPhi,
    P0DTrackVId,
    P0DTrackTId,
    P0DTrackParentTId,
    P0DTrackGParentTId,
    P0DTrackStart,
    P0DTrackEnd,   
    
    FGD1MaxHit,
    FGD2MaxHit,

    runPeriod 
  };
};

#endif
