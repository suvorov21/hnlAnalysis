#include "pileUpAnalysisHNL.hxx"
#include "BasicUtils.hxx"
#include "CategoriesUtils.hxx"
#include "Parameters.hxx"

#include "BasicUtils.hxx"
#include "CutUtils.hxx"
#include "AnalysisUtils.hxx"

//********************************************************************
pileUpAnalysisHNL::pileUpAnalysisHNL(AnalysisAlgorithm* ana) : baseTrackerAnalysis(ana) {
//********************************************************************

    // Add the package version
  ND::versioning().AddPackage("hnlAnalysis", 
      anaUtils::GetSoftwareVersionFromPath((std::string)getenv("HNLANALYSISROOT")));
  
  _flux= NULL;

}

//********************************************************************
void pileUpAnalysisHNL::DefineSelections(){
//********************************************************************

  sel().AddSelection("HNLPileUp",           "HNL PileUp Systematic Sel",     new pileUpSelectionHNL(false));

}


//********************************************************************
bool pileUpAnalysisHNL::Initialize(){
//********************************************************************

  // Initialize the baseTrackerAnalysis
  if(!baseTrackerAnalysis::Initialize()) return false;  

  // Minimum accum level to save event into the output tree
  SetMinAccumCutLevelToSave(ND::params().GetParameterI("pileUpAnalysisHNL.MinAccumLevelToSave"));

  return true;
}

//********************************************************************
void pileUpAnalysisHNL::DefineInputConverters(){
//********************************************************************
  baseTrackerAnalysis::DefineInputConverters();
  
  input().ReplaceConverter("oaAnalysisTree", new oaAnalysisHNL_Converter());

}


//********************************************************************
void pileUpAnalysisHNL::DefineMicroTrees(bool addBase){
//********************************************************************

  // -------- Add variables to the analysis tree ----------------------
  //---- Variables from the base class
  if (addBase) baseTrackerAnalysis::DefineMicroTrees(addBase);

       
  // ---- P0D1PileUp Tracks ----
  AddVarVF(  output(), P0DTrackMom,       "P0D Track Momentum",      NumP0DTracks);
  AddVarVF(  output(), P0DTrackCostheta,  "P0D Track Costheta",      NumP0DTracks);
  AddVarVF(  output(), P0DTrackPhi,       "P0D Track Phi",           NumP0DTracks);
  AddVarVI(  output(), P0DTrackVId,       "P0D Track VertexID",      NumP0DTracks);
  AddVarVI(  output(), P0DTrackTId,       "P0D TrackID",             NumP0DTracks);
  AddVarVI(  output(), P0DTrackParentTId, "P0D Parent TrackID",      NumP0DTracks);
  AddVarVI(  output(), P0DTrackGParentTId,"P0D Grandparent TrackID", NumP0DTracks);
  AddVarMF( output(),  P0DTrackStart,     "P0D Track Start Position",NumP0DTracks, -NMAXPARTICLES, 4);
  AddVarMF( output(),  P0DTrackEnd,       "P0D Track End Position",  NumP0DTracks, -NMAXPARTICLES, 4);
        
  // ---- FGD1PileUp Tracks ----
  AddVarVF(  output(), FGD1TrackMom,       "FGD1 Track Momentum",      NumFGD1Tracks);
  AddVarVF(  output(), FGD1TrackCostheta,  "FGD1 Track Costheta",      NumFGD1Tracks);
  AddVarVF(  output(), FGD1TrackPhi,       "FGD1 Track Phi",           NumFGD1Tracks);
  AddVarVI(  output(), FGD1TrackVId,       "FGD1 Track VertexID",      NumFGD1Tracks);
  AddVarVI(  output(), FGD1TrackTId,       "FGD1 TrackID",             NumFGD1Tracks);
  AddVarVI(  output(), FGD1TrackParentTId, "FGD1 Parent TrackID",      NumFGD1Tracks);
  AddVarVI(  output(), FGD1TrackGParentTId,"FGD1 Grandparent TrackID", NumFGD1Tracks);
  AddVarMF( output(),  FGD1TrackStart,     "FGD1 Track Start Position",NumFGD1Tracks, -NMAXPARTICLES, 4);
  AddVarMF( output(),  FGD1TrackEnd,       "FGD1 Track End Position",  NumFGD1Tracks, -NMAXPARTICLES, 4);

  // ---- FGD2PileUp Tracks ----
  AddVarVF(  output(), FGD2TrackMom,       "FGD2 Track Momentum",      NumFGD2Tracks);
  AddVarVF(  output(), FGD2TrackCostheta,  "FGD2 Track Costheta",      NumFGD2Tracks);
  AddVarVF(  output(), FGD2TrackPhi,       "FGD2 Track Phi",           NumFGD2Tracks);
  AddVarVI(  output(), FGD2TrackVId,       "FGD2 Track VertexID",      NumFGD2Tracks);
  AddVarVI(  output(), FGD2TrackTId,       "FGD2 TrackID",             NumFGD2Tracks);
  AddVarVI(  output(), FGD2TrackParentTId, "FGD2 Parent TrackID",      NumFGD2Tracks);
  AddVarVI(  output(), FGD2TrackGParentTId,"FGD2 Grandparent TrackID", NumFGD2Tracks);
  AddVarMF( output(),  FGD2TrackStart,     "FGD2 Track Start Position",NumFGD2Tracks, -NMAXPARTICLES, 4);
  AddVarMF( output(),  FGD2TrackEnd,       "FGD2 Track End Position",  NumFGD2Tracks, -NMAXPARTICLES, 4);

  AddVarI(  output(),  FGD1MaxHit,         "Max num of hits in FGD1");
  AddVarI(  output(),  FGD2MaxHit,         "Max num of hits in FGD2");    

  AddVarI(  output(),  runPeriod,         "Max num of hits in FGD2");
}

//********************************************************************
void pileUpAnalysisHNL::FillMicroTrees(bool addBase){
//********************************************************************

  // Fill Variables from the base class
  if(addBase) baseTrackerAnalysis::FillMicroTreesBase(addBase);

  Int_t runPer = anaUtils::GetRunPeriod(GetSpill().EventInfo->Run);
  output().FillVar (runPeriod, runPer);

  const ToyBoxPileUp* mybox = static_cast< const ToyBoxPileUp*>(&box());
  if (mybox->nFgdTimeBins[0] > 0)
    output().FillVar(FGD1MaxHit,   mybox->FgdTimeBins[0][0]->NHits[0]);
  if (mybox->nFgdTimeBins[1] > 0)
    output().FillVar(FGD2MaxHit,   mybox->FgdTimeBins[1][0]->NHits[1]);

  //FillTPCVariables();        
}
     
//********************************************************************
void pileUpAnalysisHNL::FillTPCVariables(){
//********************************************************************
  // Fill TPC1PileUp track variables
  /*for (UInt_t tpc = 0; tpc < 3; ++tpc) {
    for( UInt_t i = 0; i <mybox().nTpcTracks[tpc]; i++ ) {
      // can't cope with more than 20 tracks so ignore any extras
      if(i>=NMAXPARTICLES) continue;
      AnaTrackB* track = mybox().TPCtracks[tpc][i];
      
      output().FillVectorVar(TPC1TrackPhi, (Float_t)TMath::ATan2(track->DirectionStart[1],track->DirectionStart[0]));
      output().FillVectorVar(TPC1TrackCostheta, track->DirectionStart[2]);
      
      if( track->TrueTrack ){
        if( track->TrueTrack->TrueVertex ){
            output().FillVectorVar(TPC1TrackVId,    track->TrueTrack->VertexID);
        }
        output().FillVectorVar(TPC1TrackTId,       track->TrueTrack->PDG);
        output().FillVectorVar(TPC1TrackParentTId, track->TrueTrack->ParentPDG);
        output().FillVectorVar(TPC1TrackGParentTId,track->TrueTrack->GParentPDG);
      }
      output().FillMatrixVarFromArray(TPC1TrackStart,track->PositionStart, 4);
      output().FillMatrixVarFromArray(TPC1TrackEnd,track->PositionEnd,   4);
      output().FillVectorVar(TPC1TrackMom, track->Momentum);
      output().IncrementCounter(NumTPC1Tracks);
    }
  }*/
}
