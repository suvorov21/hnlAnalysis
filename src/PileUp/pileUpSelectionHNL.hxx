#ifndef pileUpSelectionHNL_h
#define pileUpSelectionHNL_h

#include "SelectionBase.hxx"
#include "SubDetId.hxx"
#include "Parameters.hxx"
#include "baseSelection.hxx"
#include "BeamBunchingHNL.hxx"
#include "ToyBoxTracker.hxx"

class pileUpSelectionHNL: public SelectionBase{
 public:
  pileUpSelectionHNL(bool forceBreak=true);
  virtual ~pileUpSelectionHNL(){}

    ///========= These are mandatory functions ==================
    
    /// In this method all steps are added to the selection
    void DefineSteps();
    void DefineDetectorFV();
    
    /// Create a proper instance of the box (ToyBoxB) to store all relevent information to be passed from one step to the next
    inline ToyBoxB* MakeToyBox();
    
    /// Fill the EventBox with the objects needed by this selection
    void InitializeEvent(AnaEventC& event);  //---- These are mandatory functions

    /// Fill the event summary information, which is needed by the fitters (BANFF, Mach3)
    bool FillEventSummary(AnaEventC& event, Int_t allCutsPassed[]);
    
    /// Return the appropriate sample type (only needed by fitters)
    SampleId::SampleEnum GetSampleEnum(){return SampleId::kUnassigned;} 

    Int_t GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const {return 0;} 
    Int_t GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const {return 0;}
};

namespace pileUpUtils{
  bool FindTPCstart(AnaEventC& event, ToyBoxB& box, SubDetId::SubDetEnum det);
}

class ToyBoxPileUp: public ToyBoxTracker{

public:

  ToyBoxPileUp():ToyBoxTracker(){
    
    FgdTimeBins[0] = NULL;
    FgdTimeBins[1] = NULL;
    
    nFgdTimeBins[0] = 0;
    nFgdTimeBins[1] = 0;
    Reset();
  };

  void Reset(){
    
    ToyBoxTracker::ResetBase();
    
    nTpcTracks[0]=0;
    nTpcTracks[1]=0;
    nTpcTracks[2]=0;
    
    MainTPC1Track = NULL;
    MainTPC2Track = NULL;
    MainTPC3Track = NULL;

    for(UInt_t i=0; i<NMAXPARTICLES; ++i){
        TPCtracks[0][i] = NULL;
        TPCtracks[1][i] = NULL;
        TPCtracks[2][i] = NULL;
    }

    nP0Dtracks = 0;
    for(UInt_t i=0; i<NMAXPARTICLES; ++i)
      P0Dtracks[i] = NULL;

    nfgd1tracks = 0;
    for(UInt_t i=0; i<NMAXPARTICLES; ++i)
      FGD1tracks[i] = NULL;

    nfgd2tracks = 0;
    for(UInt_t i=0; i<NMAXPARTICLES; ++i)
      FGD2tracks[i] = NULL;
  
  
    // delete FGD1 and FGD2 time-bin arrays, bins themselves are removed elsewhere
    for (int i = 0; i < 2; i++){
      nFgdTimeBins[i] = 0;    
      if (FgdTimeBins[i]) delete [] FgdTimeBins[i];
      FgdTimeBins[i] = NULL;
    }
    
  }
    
  AnaTrackB* MainTPC1Track;
  AnaTrackB* MainTPC2Track;
  AnaTrackB* MainTPC3Track;

  AnaTrackB* TPCtracks[3][NMAXPARTICLES];
  int nTpcTracks[3];

  AnaTrackB* P0Dtracks[NMAXPARTICLES];
  int nP0Dtracks;
  AnaTrackB* FGD1tracks[NMAXPARTICLES];
  int nfgd1tracks;
  AnaTrackB* FGD2tracks[NMAXPARTICLES];
  int nfgd2tracks;
  
  
  /// Time bins selected for veto purposes split between two FGDs
  AnaFgdTimeBinB** FgdTimeBins[2];
  int nFgdTimeBins[2];

  virtual ~ToyBoxPileUp(){}
};

inline ToyBoxB* pileUpSelectionHNL::MakeToyBox() {return new ToyBoxPileUp();}

class FindTPC1_P0DVeto: public StepBase{
 public:
  FindTPC1_P0DVeto() {
    _trim_p0d             = (Float_t) ND::params().GetParameterD("hnlAnalysis.Veto.P0D.Trim");
  }
   using StepBase::Apply;
   bool Apply(AnaEventC& event, ToyBoxB& box) const;
   StepBase* MakeClone(){return new FindTPC1_P0DVeto();}
 private:
  Float_t _trim_p0d;
};

class FindTrueVertex: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindTrueVertex();}
};

class FindFGDTimeBins: public StepBase{
public:
  
  FindFGDTimeBins(){
    _binTimeOffsetToBunch = (Float_t) ND::params().GetParameterD("hnlAnalysis.Veto.FGDTimeBin.TimeOffsetToBunch");
  }
    
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindFGDTimeBins();}
  
protected:
  
  mutable BeamBunchingHNL _bunchingHNL;
  Float_t         _binTimeOffsetToBunch;
};

class TPC1_P0DVetoCut: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC1_P0DVetoCut();}
};


class TPC2_FGD1VetoCut: public StepBase{
  public:
    TPC2_FGD1VetoCut() {
      _max_fgd1_hits     = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Veto.FGD1.NHits");
    }
   using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC2_FGD1VetoCut();}
  private:
    Int_t _max_fgd1_hits;
};

class TPC3_FGD2VetoCut: public StepBase{
public:
    TPC3_FGD2VetoCut() {
      _max_fgd2_hits     = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Veto.FGD2.NHits");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC3_FGD2VetoCut();}
  private:
    Int_t _max_fgd2_hits;
};

class FindTPC1start: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindTPC1start();}
};

class TPC1startCut: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC1startCut();}
};

class FindTPC2start: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindTPC2start();}
};

class TPC2startCut: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC2startCut();}
};

class FindTPC3start: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindTPC3start();}
};

class TPC3startCut: public StepBase{
public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPC3startCut();}
};

#endif
