#include "TPCTrackEffSystematicsHNL.hxx"
#include "SystematicUtils.hxx"
#include "SubDetId.hxx"
#include "DataClassesHNL.hxx"
#include "Parameters.hxx"
#include "ToyBoxTracker.hxx"
#include "SystId.hxx"
#include "AnalysisUtils.hxx"

//********************************************************************
TPCTrackEffSystematicsHNL::TPCTrackEffSystematicsHNL(bool comp): TPCTrackEffSystematics(comp){
  //********************************************************************

  _distanceCut = (Float_t) ND::params().GetParameterD("hnlAnalysis.Systematics.TPCTrackEff.DistanceCut");
  _distanceCut *= _distanceCut;

}

//********************************************************************
Weight_h TPCTrackEffSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& boxB, const SelectionBase& sel){
  //********************************************************************

  const AnaEventB& event = static_cast<const AnaEventB&>(eventC);

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB); 

  // Get the SystBox for this event, and the appropriate selection and branch
  SystBoxB* SystBox = GetSystBox(event,box.SelectionEnabledIndex,box.SuccessfulBranch);

  if(_computecounters)
    InitializeEfficiencyCounter();


  BinnedParamsParams params;
  Weight_h eventWeight;

  // Loop over all TrueTracks in the TPC
  for (Int_t itrue = 0; itrue < SystBox->nRelevantTrueObjects; itrue++){      
    AnaTrueParticleB* trueTrack = static_cast<AnaTrueParticleB*>(SystBox->RelevantTrueObjects[itrue]); 
    if (!trueTrack) continue;

    // For example in numuCC inclusive selection, only the TrueTrack associated to the muon candidate, and other true muon tracks should be considered
    if (!sel.IsRelevantTrueObjectForSystematicInToy(event, box, trueTrack, SystId::kTpcTrackEff, box.SuccessfulBranch)) continue;

    // Is there any reconstructed track associated to this true track ?
    bool found = false;

    SubDetId::SubDetEnum tpc_true  = GetFirstTPCForRecon(*trueTrack);  

    if (tpc_true == SubDetId::kInvalid) continue; // failed to evaluate the TPC for a true track

    SubDetId::SubDetEnum tpc_rec = SubDetId::kInvalid;

    for (Int_t irec = 0; irec < (int)SystBox->nRelevantRecObjects; irec++){

      bool match_tmp = false;

      AnaTrackB* track = static_cast<AnaTrackB*>(SystBox->RelevantRecObjects[irec]);

      for(int ii = 0; ii < track->nTPCSegments; ii++){

        AnaTPCParticleB* tpc_track = track->TPCSegments[ii];

        if (!tpc_track->GetTrueParticle()) continue;

        if (trueTrack->ID == tpc_track->GetTrueParticle()->ID || 
            trueTrack->ID == track->GetTrueParticle()->ID ||
            (trueTrack->ID > tpc_track->GetTrueParticle()->ID && 
             (trueTrack->PDG == tpc_track->GetTrueParticle()->ParentPDG ||trueTrack->PDG == tpc_track->GetTrueParticle()->GParentPDG))){            

          tpc_rec = SubDetId::GetSubdetectorEnum(tpc_track->Detector);

          if (tpc_true == tpc_rec){ //search for the segment that match the one we are looking at, at the true level
            match_tmp = true;
            break;
          }
        }
      }   
      if (match_tmp) break; // break the loop over recon tracks
    }

    if (tpc_true == tpc_rec) found = true;

    // Get the TPC tracking efficiency for bin 0 (there is a single bin);
    int index;
    if(!GetBinValues((Float_t)tpc_true, params, index))	  continue;

#if useNewWeights 
    eventWeight *= systUtils::ComputeEffLikeWeight(found, toy.GetToyVariations(_index)->Variations[index], params);// new way including data-mc diff
#else
    eventWeight *= systUtils::ComputeEffLikeWeight(found, toy.GetToyVariations(_index)->Variations[2*index],
        toy.GetToyVariations(_index)->Variations[2*index+1], params);
#endif

    if (_computecounters)
      UpdateEfficiencyCounter(index, found);  

    // if found a successful reco no need to keep looping

  }

  return eventWeight;
}

//********************************************************************
SubDetId::SubDetEnum TPCTrackEffSystematicsHNL::GetFirstTPCForRecon(const AnaTrueParticleB& trueTrack) const{
  //********************************************************************

  SubDetId::SubDetEnum det = SubDetId::kInvalid;

  // Assume that crossings follow the track 
  for (Int_t idet = 0; idet < trueTrack.nDetCrossings; idet++){
    bool found = false;

    // I.e crossing the active part of the tpc
    for (int j = SubDetId::kTPC1; j < SubDetId::kP0D; j++){

      if (!SubDetId::GetDetectorUsed(trueTrack.DetCrossings[idet]->Detector, static_cast<SubDetId::SubDetEnum>(j)) || 
          !trueTrack.DetCrossings[idet]->InActive) continue;

      Float_t sep_tmp = anaUtils::GetSeparationSquared(trueTrack.DetCrossings[idet]->EntrancePosition, trueTrack.DetCrossings[idet]->ExitPosition);
      if (sep_tmp > _distanceCut){ 
        det = static_cast<SubDetId::SubDetEnum>(j);
        found = true;
        break;
      }
    }
    if (found) break;

  }

  return det;

}


