#include "FluxWeightSystematicsHNL.hxx"
#include "FluxWeightSystematics.hxx"
#include "ND280AnalysisUtils.hxx"
#include "DataClassesHNL.hxx"
#include "ToyBoxTracker.hxx"

//#define DEBUG

//********************************************************************
FluxWeightSystematicsHNL::FluxWeightSystematicsHNL(const std::string& name): BinnedParams(){
//********************************************************************

 	BinnedParams::SetName(name);

  BinnedParams::SetType(k2D_SYMMETRIC);

  char dirname[256];
 	sprintf(dirname,"%s/data",getenv("HNLANALYSISROOT"));
 	BinnedParams::Read(dirname);

  SetNParameters(GetNBins());

  _flux = new FluxWeighting();


}

//********************************************************************
Weight_h FluxWeightSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& boxB){
//********************************************************************

  (void)eventC;

  Weight_h eventWeight = 1.;

  const AnaEventB& event = *static_cast<const AnaEventB*>(&eventC);

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB);

  if (!box.Vertex) return eventWeight;

  if (!box.Vertex->TrueVertex) return eventWeight;         // True vertex associated to the recon vertex should exist

  AnaHNLTrueVertex* vertex = static_cast<AnaHNLTrueVertex*>(box.Vertex->TrueVertex);

  // Get the original neutrino energy
  Float_t enu  = vertex->OriginalNuEnergy;
  int     type = vertex->ParentMode; // mode

  // Set for the moment to Kmu2 from heavy neutrino
  if (type == 12 && vertex->Mass > 1) {

    // Get the flux error values for this energy and mode
    Float_t mean_dummy;
    Float_t sigma;
    Int_t index;
    if(!GetBinValues(enu, type, mean_dummy, sigma, index)) return eventWeight;

    // Actual mean from true vertex
    Float_t mean =  vertex->Weight;

    // Compute the weight
    eventWeight.Systematic = 1 + (mean-1) + sigma * toy.GetToyVariations(_index)->Variations[index];
    eventWeight.Correction = 1 + (mean-1);
  } else {
    Float_t enu  = box.Vertex->TrueVertex->NuEnergy;
    Float_t sigma;
    Int_t index;
    if(!GetBinSigmaValue(anaUtils::GetRunPeriod(event.EventInfo.Run), enu, type, sigma, index)) return eventWeight;

    // Actual mean from FluxWeighting class
    Float_t mean =  _flux->GetWeight(box.Vertex->TrueVertex, anaUtils::GetRunPeriod(event.EventInfo.Run));

    // Compute the weight
    eventWeight.Systematic = 1 + (mean - 1) + sigma * toy.GetToyVariations(_index)->Variations[index];
    eventWeight.Correction = 1 + (mean - 1);

  }

#ifdef DEBUG
  std::cout << " Enu: " << enu << " type: " << type << " mean " << mean << " sigma " << sigma
        << " index " << index << " variation " << toy.GetToyVariations(_index)->Variations[index] << " weight " << eventWeight << std::endl;
#endif

  return eventWeight;

}


