#ifndef hnlAnalysis_h
#define hnlAnalysis_h

#include "baseTrackerAnalysis.hxx"
#include "hnlGlobalReconSelection.hxx"
#include "hnlGlobalReconSelectionTracksBase.hxx"
#include "hnlAnalysisUtils.hxx"
#include "OutputManager.hxx"

class hnlAnalysis: public baseTrackerAnalysis {
public:
  hnlAnalysis(AnalysisAlgorithm* ana=NULL);
  virtual ~hnlAnalysis(){}
  
  //---- These are mandatory functions
  bool Initialize();
  void DefineSelections();
  void DefineSystematics();
  void DefineCorrections();
  void DefineInputConverters();
  void DefineConfigurations();
  void DefineMicroTrees(bool addBase=true);
  void DefineTruthTree();

  void FillMicroTrees(bool addBase=true);
  void FillToyVarsInMicroTrees(bool addBase=true);

  bool CheckFillTruthTree(const AnaTrueVertex& vtx);
  void FillTruthTree(const AnaTrueVertex& vtx);
  //--------------------

  void FillCategories();

  void FinalizeToy();

  bool _AntiNu;

  enum enumStandardMicroTrees_hnlAnalysis{
 
    HNL_position = baseTrackerAnalysis::enumStandardMicroTreesLast_baseTrackerAnalysis + 1, 
    HNL_direction,                
    HNL_inv_mass,  
    HNL_constituents_rel_angle,

    /// Veto objects
    NP0DVetoTracks, 
    P0DVetoEndPos, 
    P0DVetoStartPos, 
    NFGD1VetoBins, 
    NFGD2VetoBins,
    NFGD1VetoHits, 
    NFGD2VetoHits, 
    FGD1VetoMinTime, 
    FGD2VetoMinTime,
    FGD1VetoRawCharge, 
    FGD2VetoRawCharge, 

    // tracks relations in TPC
    nSameTPCTracks,
    nSameTPCQualityTracks,
    nConstituents,
    dz1,
    dz2,

    // traverse vars
    dpt,
    dphit,
    dat,
    p1,
    p2,
    dpt_true,

    // PID vars
    cut1_mu,
    cut2_mu,
    mip_lhood,
    pion_lhood,
    pullelec,
    pullpos,
    pullpion,
    pullmuon,
    pullprot,

    // Vertex vars
    VertexChi2,
    VarianceX,
    VarianceY,
    VarianceZ,
    VarianceR,
 
    /// HNL simulation
    HNL_true_position,                
    HNL_true_direction,               
    HNL_true_constituents_rel_angle,   

    // timing vars
    dToF,  
    BunchTime,

    // End track detector
    HMNEndTrackDet,
    HMNEndTrackDetTrue,
    HMNEndTrackPos,
    HMNStartTrackDet,

    HMPEndTrackDet,
    HMPEndTrackDetTrue,
    
    HNL_true_prob, 
    HNL_true_mass, 
    
    enumStandardMicroTreesLast_hnlAnalysis 
  };
  
  enum enumConf_hnlAnalysis{
    flux_hnl_syst=baseTrackerAnalysis::enumConfLast_baseTrackerAnalysis+1,
    gv_eff_syst,
    pileup_hnl_syst, 
    enumConfLast_hnlAnalysis
  };

};


#endif
