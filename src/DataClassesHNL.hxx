#pragma once
#ifndef DataClassesHNL_hxx
#define DataClassesHNL_hxx

#include "DataClasses.hxx"
#include "EventBoxTracker.hxx"

const Int_t   kUnassigned      = 0xDEAD;

/// a very simple class to represent a HNL candidate build out of a GV vertex
class AnaHNLCandidate{
public: 
  enum ModeEnum{
    kMumPip = 0, 
    kMupPim, 
    kElePip, 
    kPosPim, 
    kDiMuon,
    kModeCounter
  };
  
  /// a canidate needs a GV with at least two tracks of opposite charge
  AnaHNLCandidate(AnaVertexB* vertex){
    trackNegative = NULL;
    trackPositive = NULL;
    Vertex        = NULL;
    ReadInfo(vertex);
  }
  virtual ~AnaHNLCandidate(){};

  bool IsSuccess() const{ 
    return _success;
  }
  
  void GetDirection(Float_t* dir) const {
    dir[0] = _direction[0]; 
    dir[1] = _direction[1];
    dir[2] = _direction[2];
  }

  Float_t GetMomentum() const {
    return _momentum;
  }
  
  Float_t GetMass(AnaHNLCandidate::ModeEnum mode) const{
    if (mode == kModeCounter){
      std::cout << "AnaHNLCandidate::GetMass() wrong mode provided " << mode << std::endl; 
      return kUnassigned;
    }
    return _masses[mode];
  }
  
  void GetMasses(Float_t* masses) const{
    for (int i=kMumPip; i<kModeCounter; i++){
     masses[i] = _masses[i]; 
    }
  
  }
  
  Float_t GetRelAngle() const{
    return _relativeAngle;
  }

  AnaTrackB*  trackNegative;
  AnaTrackB*  trackPositive;
  AnaVertexB* Vertex;
  
private:
  void ReadInfo(AnaVertexB* vertex);
  // status bit
  bool _success;
  // pre-computed kinematics
  Float_t _relativeAngle;
  Float_t _direction[3];
  Float_t _momentum;
  Float_t _masses[kModeCounter];
   
};

class EventBoxHNL: public EventBoxTracker{
public:
  EventBoxHNL(){
    FgdTimeBins[0] = NULL;
    FgdTimeBins[1] = NULL;
    
    nFgdTimeBins[0] = 0;
    nFgdTimeBins[1] = 0;

    BunchCentralTime = 0.;

  }
  virtual ~EventBoxHNL();
  
     
  enum TrackGroupEnumHNL{
    kP0DVetoTracks = EventBoxTracker::kTracksWithECal+1,
  };
  
  enum TrueTrackGroupEnumHNL{
    kTrueTracksChargedFromHNLVertex = EventBoxTracker::kTrueParticlesInECalInBunch+1,
  };

 
  /// time bins selected for veto purposes split between two FGDs
  AnaFgdTimeBinB** FgdTimeBins[2];
  int nFgdTimeBins[2];

  Float_t BunchCentralTime;
};


/// true vertex with the information specific to HNL search: addded
class AnaHNLTrueVertex: public AnaTrueVertex{
  
public:
  AnaHNLTrueVertex(){ 
    Mass             = kUnassigned;
    Weight           = kUnassigned;
    Probability      = kUnassigned;
    OriginalNuEnergy = kUnassigned;
    ParentMode       = kUnassigned;
    dToF             = kUnassigned;
  }
  virtual ~AnaHNLTrueVertex(){}
  
  void Print() const;
  
  /// Clone this object.
  virtual AnaHNLTrueVertex* Clone() {
    return new AnaHNLTrueVertex(*this);
  }

  ///This is the information relevant for HNL analsysis, can create one more class for this but seems fine to have it here
  ///Mass of the HNL that corresponds to vertex
  Float_t Mass;
  
  ///These two vars exist e.g. for G4PrimaryVertex (and corresponding rooTracker)
  ///Will be used with more or less the same meaning
   
  /// The weight of the interaction.  This will be set to one if the
  /// interaction is not reweighted.  If the vertex is oversampled, this
  /// will be less than one. This is used if the generator is using
  /// a weighted sample (e.g. over sampling the high energy part of the
  /// neutrino spectrum smooth out the distributions)

  // weight of the whole event, including HNL decay kinematics & width
  Float_t Weight;

  /// The overall probability of the interaction (HNL decay) that created this vertex.
  /// This includes the effect of the cross section, path length through the
  /// material, etc.  This will be one if it is not filled.

  /// Weight of the HNL particle, when it crosses the front plane of detector
  Float_t Probability;

  /// True cos relative angle between  HNL decay products
  Float_t RelAngle;

  /// True energy of the original neutrino (used for re-weighting)
  Float_t OriginalNuEnergy;
  
  /// The original mode used for reveighting (K+/-mu2,  pion? etc)
  Int_t ParentMode;

  Float_t dToF;
  
protected:
  /// Copy constructor is protected, as Clone() should be used to copy this object.
  AnaHNLTrueVertex(const AnaHNLTrueVertex& vertex);


}; 


#endif


