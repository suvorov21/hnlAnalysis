#include "SIPionSystematicsHNL.hxx"
#include "PionInteractionSystematicHNL.hxx"
#include "DataClassesHNL.hxx"

//********************************************************************
void SIPionSystematicsHNL::Initialize(){
//********************************************************************
  
  
  if (_initialized) return;
 
  if (!_pionSIManager)
    _pionSIManager = new PionSIManagerHNL();
 
  _initialized   = true;
  
}


//********************************************************************
void SIPionSystematicsHNL::FillSystBox(const AnaEventC& eventC, const SelectionBase& sel, Int_t ibranch){
//********************************************************************

  (void)sel;
  (void)ibranch;

  Int_t uniqueID = 0;    
#ifdef MULTITHREAD
  uniqueID = event.UniqueID;
#endif

  
  const AnaEventB& event = static_cast<const AnaEventB&>(eventC);
  
  /// Simply make the decision based on the true vertex location 
  if (event.nTrueVertices == 0) return;
  
  if (!event.TrueVertices[0]) return;
  
  SubDetId::SubDetEnum det = anaUtils::GetDetector(event.TrueVertices[0]->Position);
    
  if (!SubDetId::IsTPCDetector(det)) return;
   
  if (det == SubDetId::kInvalid) return;
 

  // Compute Pion weight info needed by PionSISystematics (TODO, only when this systematic is enabled)
  _pionWeightInfo[uniqueID] = _pionSIManager->ComputePionWeightInfo(event, det);

}


