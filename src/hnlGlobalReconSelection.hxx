#ifndef hnlGlobalReconSelection_h
#define hnlGlobalReconSelection_h

#include "SelectionBase.hxx"
#include "Parameters.hxx"
#include "DataClassesHNL.hxx"
#include "BeamBunchingHNL.hxx"
#include "ToyBoxTracker.hxx"

class hnlGlobalReconSelection: public SelectionBase{
public:
  hnlGlobalReconSelection(bool forceBreak=true);
  virtual ~hnlGlobalReconSelection(){}

  //---- These are mandatory functions
  virtual void DefineSteps();
  virtual void DefineDetectorFV();
  inline ToyBoxB* MakeToyBox();
  virtual bool FillEventSummary(AnaEventC& event, Int_t allCutsPassed[]);
  SampleId::SampleEnum GetSampleEnum(){return SampleId::kUnassigned;}
  virtual void InitializeEvent(AnaEventC& event);
  
  /// Functions related to systematics propagation
  bool IsRelevantRecObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaRecObjectC* track, SystId_h syst_index, Int_t branch) const;
  bool IsRelevantTrueObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaTrueObjectC* trueObj, SystId_h syst_indexsyst_index, Int_t branch) const;
  bool IsRelevantSystematic(const AnaEventC& event, const ToyBoxB& box, SystId_h syst_index, Int_t branch=0) const;

  Int_t GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const; 
  Int_t GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const;


protected:
  BeamBunchingHNL _bunchingHNL;
  Float_t         _binTimeOffsetToBunch;
 // Int_t         _nBinSigma;
//  Float_t       _binWidth;   
  Float_t         _trim_p0d;
  
};

///actions
namespace hnl_global_actions{
  bool TrackQualityCut(AnaTrackB& track, Int_t min_tpc_nodes = 18); 
}

class ToyHNLBoxB: public ToyBoxTracker{
public:
  ToyHNLBoxB(){
    VetoType       = SubDetId::kInvalid;
    hnlCandidate   = NULL; 
    nSameTPCTracks = 0;
    range = 0;
    constituent = 0;

    relAngle = 0;
    ClosestSeparation = 0;
    ClosestSeparationX = 0;
    ClosestSeparationY = 0;
    ClosestSeparationZ = 0;
    dz1 = -1;
    dz2 = -1;
    nRecoTracks = -1;

    dpt = dphit = dat = -1e6;

    cut1_mu    = -1e6;
    cut2_mu    = -1e6;
    mip_lhood  = -1e6;
    pion_lhood = -1e6;
    pullelec   = -1e6;
    pullmuon   = 1e6;
    pullprot   = 1e6;
    pullpion   = 1e6;
    pullpos    = -1e6;

    VertexChi2 = -1.;

    HMPEndTrackDetTrue = -1;
    HMNEndTrackDetTrue = -1;
    HMPEndTrackDet = -1;
    HMNEndTrackDet = -1;
  }
  
  virtual void Reset(){
    ToyBoxTracker::ResetBase();
    if (hnlCandidate) delete hnlCandidate;
    hnlCandidate = NULL;
    VetoType = SubDetId::kInvalid;
    nSameTPCTracks = 0;
    nSameTPCqualtyTracks = 0;
    range = 0;
    rangeZ = 0;
    constituent = 0;
    relAngle = 0;
    
    ClosestSeparation = 0;
    ClosestSeparationX = 0;
    ClosestSeparationY = 0;
    ClosestSeparationZ = 0;
    
    dz1 = -1;
    dz2 = -1;
    
    nRecoTracks = -1;

    dpt = dphit = dat = dpt_true = -1.;

    cut1_mu    = -1e6;
    cut2_mu    = -1e6;
    mip_lhood  = -1e6;
    pion_lhood = -1e6;
    pullelec   = -1e6;
    pullmuon   = 1e6;
    pullprot   = 1e6;
    pullpion   = 1e6;
    pullpos    = -1e6;

    VertexChi2 = -1.;

    HMPEndTrackDetTrue = -1;
    HMNEndTrackDetTrue = -1;
    HMPEndTrackDet = -1;
    HMNEndTrackDet = -1;
  }
  
  SubDetId::SubDetEnum VetoType;
  AnaHNLCandidate* hnlCandidate;
  int nSameTPCTracks;
  int nSameTPCqualtyTracks;
  int constituent;
  int nRecoTracks;
  float VertexChi2;

  AnaTrackB** SameTPCTracks;
  Float_t range;
  Float_t rangeZ;

  Float_t ClosestSeparation;
  Float_t ClosestSeparationX;
  Float_t ClosestSeparationY;
  Float_t ClosestSeparationZ;

  Float_t relAngle;
  Float_t dz1, dz2;

  Float_t dpt, dphit, dat, dpt_true;

  Float_t cut1_mu;
  Float_t cut2_mu;
  Float_t mip_lhood;
  Float_t pion_lhood;
  Float_t pullelec, pullpos, pullmuon, pullprot, pullpion;

  Int_t HMPEndTrackDetTrue, HMNEndTrackDetTrue;
  Int_t HMPEndTrackDet, HMNEndTrackDet;
  Int_t HMNStartTrackDet;

};

inline ToyBoxB* hnlGlobalReconSelection::MakeToyBox(){return new ToyHNLBoxB();}


/// out all the classes into a dedicated namespace so to allow solving the name conflicts later
namespace hnl_global_selection{
  
  // Utils
  AnaTrackB* GetNegativeTrack(ToyHNLBoxB& box);
  
  AnaTrackB* GetPositiveTrack(ToyHNLBoxB& box);
  
  /// Check that a point is inside P0D downstream (with a given trim)
  bool IsP0dDownstream(   Float_t* pos, Float_t trim);
  bool IsFgd1Downstream(  Float_t* pos, Float_t trim); 
  bool IsFgd2Downstream(  Float_t* pos, Float_t trim);
  
  /// Actions and cuts
  
  class FillSummaryAction: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;  
    StepBase* MakeClone(){return new FillSummaryAction();}
  };
  
  /// find primary global vertex
  class FindHNLCandidateAction: public StepBase{
  public:
    FindHNLCandidateAction() {
      _BunchTimeDiffL = (Float_t)ND::params().GetParameterI("hnlAnalysis.TimeBunching.TimeCutL");
      _BunchTimeDiffH = (Float_t)ND::params().GetParameterI("hnlAnalysis.TimeBunching.TimeCutH");
      _ApplyToFCorr   = (bool)ND::params().GetParameterI("hnlAnalysis.Corrections.EnableToFcorr");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindHNLCandidateAction();}
    private:
      bool _ApplyToFCorr;
      Float_t _BunchTimeDiffL;
      Float_t _BunchTimeDiffH;
  };

  /// find tracks in the same TPC as vertex
  class FindSameTPCTracks: public StepBase{
  public:
    FindSameTPCTracks() {
      _min_tpc_nodes = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.TrackQuality.MinTPCNodes");
      _min_momentum = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.HNL.MinConstMom");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new FindSameTPCTracks();}
  private:
    Int_t _min_tpc_nodes;
    Float_t _min_momentum;
  };
  
  /// find primary global vertex
  class HNL_QualityFiducialCut: public StepBase{
  public:
    HNL_QualityFiducialCut(){
      _min_tpc_nodes = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.TrackQuality.MinTPCNodes");
      _min_momentum  = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.HNL.MinConstMom");
      _max_chi2      = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.VertexQuality.MaxChi2");
      _max_var       = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.VertexQuality.MaxVar");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNL_QualityFiducialCut();}
  private:
    Int_t _min_tpc_nodes;
    Float_t _min_momentum;
    Float_t _max_chi2;
    Float_t _max_var;
  };
/*
  class HNL_VertexTimeCut: public StepBase{
  public:
    HNL_VertexTimeCut(){
      _TimeCutLow      = (Float_t)ND::params().GetParameterI("hnlAnalysis.TimeBunching.TimeCutL");
      _TimeCutHigh     = (Float_t)ND::params().GetParameterI("hnlAnalysis.TimeBunching.TimeCutH");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNL_VertexTimeCut();}
  private:
    Float_t _TimeCutLow;
    Float_t _TimeCutHigh;
  };
*/

  class UpstreamActivityVetoCut: public StepBase{
  public:
    UpstreamActivityVetoCut() {
      _max_fgd1_hits = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Veto.FGD1.NHits");
      _max_fgd2_hits = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Veto.FGD2.NHits");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new UpstreamActivityVetoCut();}
  private:
    Int_t _max_fgd1_hits;
    Int_t _max_fgd2_hits;
  };

  class TPCAdditionalActivityCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new TPCAdditionalActivityCut();}
  };

  class EventUseVertexTPC: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new EventUseVertexTPC();}
  };
    
  /// cut on HNL angle w.r.t. to beam axis
  class HNLThetaCut: public StepBase{
  public:
    HNLThetaCut(AnaHNLCandidate::ModeEnum mode){
      _mode = mode;
      
      switch (_mode){
        case AnaHNLCandidate::kElePip:  
        case AnaHNLCandidate::kPosPim:
          _min_cos_theta = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.kinem.Electron.MinCosTheta");
          break; 
        case AnaHNLCandidate::kMumPip:  
        case AnaHNLCandidate::kMupPim:
          _min_cos_theta = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.kinem.Muon.MinCosTheta");
          break; 
        case AnaHNLCandidate::kDiMuon:
          _min_cos_theta = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.kinem.DiMuon.MinCosTheta");
          break;
        default:
          std::cout << "Error in HNLThetaCut. Unknown mode" << std::endl;
      }
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNLThetaCut(_mode);}
  private:
    Float_t _min_cos_theta;    
    AnaHNLCandidate::ModeEnum _mode;
  };

  ///  Cut events with too high relative angle of products
  class HNLRelAngleCut: public StepBase {
  public:
    HNLRelAngleCut(AnaHNLCandidate::ModeEnum mode){
      _mode = mode;
      
      switch (_mode){
        case AnaHNLCandidate::kElePip:  
        case AnaHNLCandidate::kPosPim:
          _min_rel_cos = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.kinem.Electron.MinCosRel");
          break; 
        case AnaHNLCandidate::kMumPip:  
        case AnaHNLCandidate::kMupPim:
          _min_rel_cos = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.kinem.Muon.MinCosRel");
          break; 
        default:
          std::cout << "Error in HNLRelAngleCut. Unknown mode" << std::endl;
      }
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNLRelAngleCut(_mode);}
  private:
    Float_t _min_rel_cos;
    AnaHNLCandidate::ModeEnum _mode;
  };
  
  /// cut on the reconstructed HNL invariant mass
  class HNLInvMassCut: public StepBase{
  public:
  
    HNLInvMassCut(AnaHNLCandidate::ModeEnum mode){
      _mode = mode;
      
      switch (_mode){
        case AnaHNLCandidate::kElePip:  
        case AnaHNLCandidate::kPosPim:
          _min_inv_mass = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.HNL.InvMass.Electron.Min");
          _max_inv_mass = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.HNL.InvMass.Electron.Max");
          break; 
        default:
          _min_inv_mass = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.HNL.InvMass.Muon.Min");
          _max_inv_mass = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.HNL.InvMass.Muon.Max");
          break; 
      }
    }
    
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNLInvMassCut(_mode);}
    
    
  private: 
    Float_t _min_inv_mass;
    Float_t _max_inv_mass; 
    AnaHNLCandidate::ModeEnum _mode;
  };
  

  /// ECal segment cut. Check both tracks have segment in ECal
  class ECaLSegmentCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new ECaLSegmentCut();}
  };

    /// ECal shower cut. Check it's not EM like
  class ECaLShowerCut: public StepBase{
  public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new ECaLShowerCut();}
  };

  /// muon PID
  class MuonPIDCut: public StepBase{
  public:
    MuonPIDCut(){
      _mom_cut      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.MuonPID.MomCut");
      _cut1_mu      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.MuonPID.Cut1");
      _cut2_mu      = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.MuonPID.Cut2");
      _cut1_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.AntiMuonPID.Cut1");  
      _cut2_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.AntiMuonPID.Cut2");
      _cut3_antimu  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.AntiMuonPID.Cut3");
    }
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new MuonPIDCut();}
    
    virtual AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetNegativeTrack(box); 
    }
 
  private:
    bool ApplyMuonPID(const AnaTrackB& track) const;
    bool ApplyAntiMuonPID(const AnaTrackB& track) const;

    Float_t _cut1_mu;
    Float_t _cut2_mu;
    Float_t _cut1_antimu;
    Float_t _cut2_antimu;
    Float_t _cut3_antimu;
    Float_t _mom_cut; 
  };
  
  /// anti-muon PID
  class AntiMuonPIDCut: public MuonPIDCut{
  public:
    AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetPositiveTrack(box); 
    }
  };
 
  /// pion PID
  class PionPIDCut: public StepBase{
  public:
    PionPIDCut(){
      _mip_lhood_cut  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.PionPID.MipLHood");
      _pion_lhood_cut = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.PionPID.PionLHood");
      _min_mom_cut    = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.PionPID.MomentumCut");
      
    }
    virtual ~PionPIDCut(){}
    
    virtual AnaTrackB* GetTrack(ToyHNLBoxB& box) const = 0;
    
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return NULL;}
  
  private:
    bool ApplyPionPID(const AnaTrackB& track) const;
    Float_t _mip_lhood_cut;
    Float_t _pion_lhood_cut;
    Float_t _min_mom_cut; 
  };
  
  /// positive pion PID
  class PositivePionPIDCut: public PionPIDCut{
  public:
    AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetPositiveTrack(box); 
    }
  };
  
  /// negative pion PID
  class NegativePionPIDCut: public PionPIDCut{
  public:
    AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetNegativeTrack(box); 
    }
  };
  
  /// electron PID
  class ElectronPIDCut: public StepBase{
  public:
    ElectronPIDCut(){
    _pullmu_reject_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullMuonMin");
    _pullmu_reject_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullMuonMax");
    _pullpi_reject_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullPionMin");
    _pullpi_reject_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullPionMax");
    _pullel_accept_min        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullElecMin");
    _pullel_accept_max        = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullElecMax");
    _pullel_accept_tight_min  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullElecTightMin");
    _pullel_accept_tight_max  = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.PullElecTightMax");

    _min_momentum         = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.MinMomentum");
    _max_momentum_pos     = (Float_t)ND::params().GetParameterD("hnlAnalysis.Cuts.ElePos.MaxMomentum.Pos");
    }
    virtual ~ElectronPIDCut(){}
    
    virtual AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetNegativeTrack(box); 
    }

    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new ElectronPIDCut();}
  
  private:
    bool ApplyElectronPID(const AnaTrackB& track) const;
    //cuts to be read from NuE selection
    Float_t _pullmu_reject_min;
    Float_t _pullmu_reject_max;
    Float_t _pullpi_reject_min;
    Float_t _pullpi_reject_max;
    Float_t _pullel_accept_min;
    Float_t _pullel_accept_max;  
    Float_t _pullel_accept_tight_min;
    Float_t _pullel_accept_tight_max;
    Float_t _min_momentum;
    Float_t _max_momentum_pos;

  };

  /// positron PID
  class PositronPIDCut: public ElectronPIDCut{
  public:
    AnaTrackB* GetTrack(ToyHNLBoxB& box) const{
      return hnl_global_selection::GetPositiveTrack(box); 
    }
  };


}
#endif
