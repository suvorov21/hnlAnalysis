#ifndef SystId_HNL_hxx_seen
#define SystId_HNL_hxx_seen

#include "SystId.hxx"

class SystIdHNL{

public:

  SystIdHNL(){};
  ~SystIdHNL(){};
  

  enum SystEnumHNL {
    kFluxWeightHNL =  SystId::SystEnumLast_SystId + 1,        
    kGVEffHNL,
    kSIPionHNL, 
    kPileUpHNL,
    SystHNL_EnumLast_SystId
  };
};

#endif
