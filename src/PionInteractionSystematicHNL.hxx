#ifndef PionInteractionSystematicHNL_h
#define PionInteractionSystematicHNL_h

#include "PionInteractionSystematic.hxx"
#include "BasicTypes.hxx"

/// Combine everything that may be analysis/selection dependent int a simple class, 
/// so that can extended 
class PionSIManagerHNL: public PionSIManager{

  
public:
   
  /// ctor
  PionSIManagerHNL(){}
  
  /// dtor
  virtual ~PionSIManagerHNL(){}

  /// Functions related to the volumes of interest to the analysis
  Bool_t InVOI(SubDetId_h, Float_t* pos) const;
  Bool_t InVOI1(Float_t* pos) const;
  Bool_t InVOI2(Float_t* pos) const;
 
}; 


#endif
