#ifndef hnlAnalysisUtils_h
#define hnlAnalysisUtils_h

#include "BaseDataClasses.hxx"
#include "SubDetId.hxx"

namespace hnl_analysis_utils{
  
  Float_t GetPIDLikelihood(const AnaTrackB&,  int hypo);
  bool InTPCFV(SubDetId::SubDetEnum det, const Float_t* pos, const Float_t* FVdefmin, const Float_t* FVdefmax);
  bool InTPCFV(SubDetId::SubDetEnum det, const Float_t* pos);

  
  /// comparator for FgdTimeBins, to sort them in decreasing number of hits 
  class CompareFgdBinsB : public std::binary_function<const AnaFgdTimeBinB*, const AnaFgdTimeBinB*, bool>{
  public:
    CompareFgdBinsB(SubDetId::SubDetEnum det){
      _det = det;
      
      if (_det != SubDetId::kFGD1 && _det != SubDetId::kFGD2){
        std::cout << "hnl_analysis_utils::CompareBase() non-FGD detector provided: " << det << std::endl; 
        exit(1);
      }
  
    }
    virtual ~CompareFgdBinsB(){}
    
    bool operator()(const AnaFgdTimeBinB* lhs, const AnaFgdTimeBinB* rhs) const{
      return Compare(lhs, rhs);
    };
   
  protected:
    virtual bool Compare(const AnaFgdTimeBinB* lhs, const AnaFgdTimeBinB* rhs) const = 0;
    
    SubDetId::SubDetEnum _det;
  };

  class CompareFgdBinsNHits : public CompareFgdBinsB{
  public:
    CompareFgdBinsNHits(SubDetId::SubDetEnum det): CompareFgdBinsB(det){}
    
    virtual ~CompareFgdBinsNHits(){}
    
    virtual bool Compare(const AnaFgdTimeBinB* lhs, const AnaFgdTimeBinB* rhs) const;
    
  };




}

#endif
