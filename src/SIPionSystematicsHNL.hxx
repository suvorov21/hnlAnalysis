#ifndef SIPionSystematicsHNL_h
#define SIPionSystematicsHNL_h

#include "SIPionSystematics.hxx"


class SIPionSystematicsHNL: public SIPionSystematics{
  
public:

  SIPionSystematicsHNL(){}
  
  virtual ~SIPionSystematicsHNL(){}
  
  /// Initilaize the systematics itself, basically the manager
  void Initialize();
    
protected:
 
  /// Fill the SystBox for this event, selection and branch
  void FillSystBox(const AnaEventC& event, const SelectionBase& sel, Int_t ibranch);

    
};

#endif
