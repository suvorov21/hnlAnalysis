#include "DataClassesHNL.hxx"
#include "HEPConstants.hxx"
#include "KinematicsUtils.hxx" 

#define _array4(par) std::cout<< #par << ": \t" << par[0] << " " << par[1] << " " << par[2] << " " << par[3] << std::endl;  
#define _array3(par) std::cout<< #par << ": \t" << par[0] << " " << par[1] << " " << par[2] << std::endl; 




//********************************************************************
void AnaHNLCandidate::ReadInfo(AnaVertexB* vertex){
  //********************************************************************

  _success = false;

  //should be a valid vertex
  if (!vertex) return; 

  //first two tracks should be valid and of different charge

  for (int i=0; i<2; i++){
    if (!vertex->Particles[i]) return;
    if (static_cast<AnaTrackB*>(vertex->Particles[i])->Charge<0) trackNegative = static_cast<AnaTrackB*>(vertex->Particles[i]);
    if (static_cast<AnaTrackB*>(vertex->Particles[i])->Charge>0) trackPositive = static_cast<AnaTrackB*>(vertex->Particles[i]);
  } 

  if (!trackNegative) return;
  if (!trackPositive) return;

  //calculate various vars
  //invariant masses
  _masses[kMumPip] = anaUtils::ComputeInvariantMass(*trackNegative, *trackPositive,
      units::pdgBase->GetParticle(13)->Mass()*units::GeV, 
      units::pdgBase->GetParticle(211)->Mass()*units::GeV);

  _masses[kMupPim] = anaUtils::ComputeInvariantMass(*trackPositive, *trackNegative,
      units::pdgBase->GetParticle(13)->Mass()*units::GeV, 
      units::pdgBase->GetParticle(211)->Mass()*units::GeV);

  _masses[kElePip] = anaUtils::ComputeInvariantMass(*trackNegative, *trackPositive,
      units::pdgBase->GetParticle(11)->Mass()*units::GeV, 
      units::pdgBase->GetParticle(211)->Mass()*units::GeV);

  _masses[kPosPim] = anaUtils::ComputeInvariantMass(*trackPositive, *trackNegative,
      units::pdgBase->GetParticle(11)->Mass()*units::GeV, 
      units::pdgBase->GetParticle(211)->Mass()*units::GeV);

  TVector3 vect1 = anaUtils::ArrayToTVector3(trackNegative->DirectionStart);
  TVector3 vect2 = anaUtils::ArrayToTVector3(trackPositive->DirectionStart);

  // Just a cross-check. DirectionStart should be 1
  vect1 = vect1.Unit();
  vect2 = vect2.Unit();

  vect1 *= trackNegative->Momentum;
  vect2 *= trackPositive->Momentum;

  _relativeAngle = vect1.Angle(vect2);

  anaUtils::VectorToArray((vect1+vect2).Unit(), _direction);
  _momentum = (vect1 + vect2).Mag();

  Vertex = vertex;

  _success = true;

}

//********************************************************************
AnaHNLTrueVertex::AnaHNLTrueVertex(const AnaHNLTrueVertex& vertex):AnaTrueVertex(vertex){
  //********************************************************************

  Mass             = vertex.Mass;
  Weight           = vertex.Weight;
  Probability      = vertex.Probability;
  RelAngle         = vertex.RelAngle;
  OriginalNuEnergy = vertex.OriginalNuEnergy;
  ParentMode       = vertex.ParentMode;
  dToF             = vertex.dToF;

}

//********************************************************************
void AnaHNLTrueVertex::Print() const{
  //********************************************************************

  std::cout << "-------- AnaHNLTrueVertex --------- " << std::endl;

  AnaTrueVertex::Print();

  std::cout << "Mass:             " << Mass             << std::endl;
  std::cout << "Weight:           " << Weight           << std::endl;
  std::cout << "Probability:      " << Probability      << std::endl;
  std::cout << "RelAngle:         " << RelAngle         << std::endl;
  std::cout << "OriginalNuEnergy: " << OriginalNuEnergy << std::endl;
  std::cout << "ParentMode:       " << ParentMode       << std::endl;
  std::cout << "dToF              " << dToF             << std::endl;
}


//********************************************************************
EventBoxHNL::~EventBoxHNL(){
  //********************************************************************
  
  // delete FGD1 and FGD2 time-bin arrays, bins themselves are removed elsewhere
  for (int i = 0; i < 2; i++){
    nFgdTimeBins[i] = 0;    
    if (FgdTimeBins[i]) delete [] FgdTimeBins[i];
    FgdTimeBins[i] = NULL;
  }

}

