#include "BeamBunchingHNL.hxx"

//*****************************************************************************
Int_t BeamBunchingHNL::GetBunchHNL(Float_t tTrack, Int_t run, bool isMC, bool cosmic_mode, Float_t lCut, Float_t hCut) const {
  //*****************************************************************************

  if (cosmic_mode) {
    // In cosmics mode, everything gets put into one bunch.
    return 0;
  }

  Int_t bunch = -1;
  //  Float_t sigma;
  Float_t time;

  // Find Bunch Period 

  Int_t bunchperiod = -1; 

  if( !isMC ) {
    for(UInt_t j = 0 ; j < bunchrunperiod.size(); j++ ) {
      if( run <= bunchrunperiod[j].second && run >= bunchrunperiod[j].first ){
        bunchperiod = j; 
        break; 
      }
    }
    if( bunchperiod < 0 ) return bunch; 
  }

  // Apply offset
  tTrack += _dTOF;

  for (UInt_t i = 0; i < NBUNCHES; i++){


    if (isMC){
      time = bunch_time_mc[i];
    }
    else{
      time = bunch_time_data[bunchperiod][i];
    }

    if ( time < 0 ) return bunch;
        
    if (tTrack-time < fabs(hCut) &&  // hcut
        time-tTrack < fabs(lCut)){ // lcut
      bunch = i;
      break;
    }
  }
  
  return bunch;
}

