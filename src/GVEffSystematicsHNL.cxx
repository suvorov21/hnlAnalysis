#include "GVEffSystematicsHNL.hxx"
#include "SystematicUtils.hxx"
#include "DataClassesHNL.hxx"
#include "ToyBoxTracker.hxx"
#include "ConstituentsUtils.hxx"
//#define DEBUG

//********************************************************************
GVEffSystematicsHNL::GVEffSystematicsHNL(const std::string& name): BinnedParams(){
//********************************************************************
  
  BinnedParams::SetName(name);
   
  BinnedParams::SetType(k1D_EFF_ASSYMMETRIC);
 	 
 	char dirname[256];
 	sprintf(dirname,"%s/data",getenv("HNLANALYSISROOT"));
 	BinnedParams::Read(dirname);
  
  SetNParameters(2*GetNBins());
   
   
}

//********************************************************************
Weight_h GVEffSystematicsHNL::ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& boxB){
//********************************************************************
  
  (void)event;
  
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB); 
  
  BinnedParamsParams params;
  Weight_h eventWeight = 1.;
  
  // Get two track candidates from the box
  AnaTrack* trackNegative = static_cast<AnaTrack*>(box.HMNtrack);
  
  AnaTrack* trackPositive = static_cast<AnaTrack*>(box.HMPtrack);
  
  
  // Since we select the most primary vertex, use it for each track
  // two tracks belong to the same vertex for us is they share the same primary --> guess should be (always?) the case
  
  if (!trackPositive || !trackNegative) return eventWeight; 
   
  bool found = true;
  
  // In general should not happen
  if (!box.Vertex) found = false;
  if (box.Vertex->nParticles != 2) found = false;
  else if ((box.Vertex->Particles[0] != trackNegative && box.Vertex->Particles[1] != trackNegative) || 
           (box.Vertex->Particles[0] != trackPositive && box.Vertex->Particles[1] != trackPositive)) 
    found = false;

  SubDetId::SubDetEnum det = SubDetId::kInvalid;
  if (found) {
    det = anaUtils::GetDetector(box.Vertex->Position);
  }
  if (det == SubDetId::kInvalid) {
    found = false;
    det = SubDetId::kTPC3;
  }
 
  int index;
  if(!GetBinValues((Float_t)det, params, index))	  return eventWeight; 
 
#if useNewWeights 
      eventWeight *= systUtils::ComputeEffLikeWeight(found,  toy.GetToyVariations(_index)->Variations[index], params); // new way including data-mc diff
#else
      eventWeight *= systUtils::ComputeEffLikeWeight(found, sigma*toy.GetToyVariations(_index)->Variations[2*index], 
           sigma*toy.GetToyVariations(_index)->Variations[2*index+1], params);
#endif

  //std::cout << eventWeight << std::endl;
  return eventWeight;

}


