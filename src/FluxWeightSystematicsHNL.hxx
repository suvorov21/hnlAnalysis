#ifndef FluxWeightSystematicsHNL_h
#define FluxWeightSystematicsHNL_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"
#include "FluxWeightSystematics.hxx"
#include "FluxWeighting.hxx"

/// This is a normalization systematic. It takes into account the uncertainty on the neutrino flux

class FluxWeightSystematicsHNL: public EventWeightBase, public BinnedParams {
  public:

    FluxWeightSystematicsHNL(const std::string& name = "FluxWeightHNL");
    virtual ~FluxWeightSystematicsHNL() {}

    /// Apply this systematic
    using EventWeightBase::ComputeWeight;
    Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& box);
  protected:
    FluxWeighting* _flux;

};


#endif
