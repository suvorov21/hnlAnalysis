#ifndef TPCTrackEffSystematicsHNL_h
#define TPCTrackEffSystematicsHNL_h

#include "TPCTrackEffSystematics.hxx"

class TPCTrackEffSystematicsHNL: public TPCTrackEffSystematics {
public:
  
  /// this is only 1 toy experiment, as we only store the weight for a change of one sigma.
  /// The mean and sigma for each momentum bin is defined in a data file.
    /// the Gaussian distribution from which the resolution of each virtual
    /// analysis is selected from.
  TPCTrackEffSystematicsHNL(bool computecounters = false);  
  
  virtual ~TPCTrackEffSystematicsHNL(){}

  Weight_h ComputeWeight(const ToyExperiment&, const AnaEventC&, const ToyBoxB&){return 1;}
  
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);  
  
private:
  
  /// Get the first TPC where a true has a min length to potentially reconstructable
  SubDetId::SubDetEnum GetFirstTPCForRecon(const AnaTrueParticleB& trueTrack) const;
   
  Float_t _distanceCut;

};

#endif
