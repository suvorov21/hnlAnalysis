#ifndef hnlGlobalReconSelectionTracksBase_h
#define hnlGlobalReconSelectionTracksBase_h

#include "hnlGlobalReconSelection.hxx"
#include "SelectionBase.hxx"
/*
class hnlGlobalReconSelectionTracksBase: public hnlGlobalReconSelection{
public:
  hnlGlobalReconSelectionTracksBase(bool forceBreak=true) : hnlGlobalReconSelection(forceBreak){}
  virtual ~hnlGlobalReconSelectionTracksBase(){}
  virtual void DefineSteps();

};

namespace hnl_global_TracksBase_selection{

  /// find HNL candidate
  class Find_HNL_candidate: public StepBase{
  public:
    Find_HNL_candidate() {
      _assign_track_range = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.Vertex.AssignTrackRange");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new Find_HNL_candidate();}
  private:
    Float_t _assign_track_range;
  };

  class HNL_CandidateCut: public StepBase{
  public:
    HNL_CandidateCut(){
      _min_tpc_nodes     = (Int_t)ND::params().GetParameterI("hnlAnalysis.Cuts.TrackQuality.MinTPCNodes");
      _min_momentum      = (Float_t)ND::params().GetParameterI("hnlAnalysis.Cuts.HNL.MinConstMom");
    }
    using StepBase::Apply;
    bool Apply(AnaEventB& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new HNL_CandidateCut();}
  private:
    Int_t _min_tpc_nodes;
    Float_t _min_momentum;
  };
    
}

*/

#endif
