# echo "cleanup hnlAnalysis v0r4 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /Users/suvorov/T2K/SOFT/CMT/v1r26p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmthnlAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmthnlAnalysistempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmthnlAnalysistempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmthnlAnalysistempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmthnlAnalysistempfile}
  unset cmthnlAnalysistempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmthnlAnalysistempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmthnlAnalysistempfile}
unset cmthnlAnalysistempfile
exit $cmtcleanupstatus

