# echo "cleanup hnlAnalysis v0r4 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if test "${CMTROOT}" = ""; then
  CMTROOT=/Users/suvorov/T2K/SOFT/CMT/v1r26p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmthnlAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmthnlAnalysistempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmthnlAnalysistempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmthnlAnalysistempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmthnlAnalysistempfile}
  unset cmthnlAnalysistempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmthnlAnalysistempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmthnlAnalysistempfile}
unset cmthnlAnalysistempfile
return $cmtcleanupstatus

