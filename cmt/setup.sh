# echo "setup hnlAnalysis v0r4 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if test "${CMTROOT}" = ""; then
  CMTROOT=/Users/suvorov/T2K/SOFT/CMT/v1r26p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmthnlAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmthnlAnalysistempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmthnlAnalysistempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=hnlAnalysis -version=v0r4 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmthnlAnalysistempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmthnlAnalysistempfile}
  unset cmthnlAnalysistempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmthnlAnalysistempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmthnlAnalysistempfile}
unset cmthnlAnalysistempfile
return $cmtsetupstatus

