#include "hnlAnalysis.hxx"
#include "AnalysisLoop.hxx"

int main(int argc, char *argv[]){
  hnlAnalysis* ana = new hnlAnalysis();
  AnalysisLoop loop(ana, argc, argv); 
  loop.Execute();
}
