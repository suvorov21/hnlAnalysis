#include "PileUp/pileUpAnalysisHNL.hxx"
#include "AnalysisLoop.hxx"

int main(int argc, char *argv[]){
  pileUpAnalysisHNL* ana = new pileUpAnalysisHNL();
  AnalysisLoop loop(ana, argc, argv); 
  loop.Execute();
}
