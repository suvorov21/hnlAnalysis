{ 
  DrawingTools drawPosElePi("AnalysisResults/Signal/ElePi_vM.root");
  DataSample samplePosElePi("AnalysisResults/Signal/ElePi_vM.root");
  DrawingTools drawPosPosPi("AnalysisResults/C_check/v2/PosPosPi.root");
  DataSample samplePosPosPi("AnalysisResults/C_check/v2/PosPosPi.root");
  DrawingTools drawEleElePi("AnalysisResults/C_check/v2/EleElePi.root");
  DataSample sampleEleElePi("AnalysisResults/C_check/v2/EleElePi.root");
  DrawingTools drawElePosPi("AnalysisResults/C_check/v2/ElePosPi.root");
  DataSample sampleElePosPi("AnalysisResults/C_check/v2/ElePosPi.root");
  DrawingTools drawMuElePi("AnalysisResults/C_check/v2/MuElePi.root");
  DataSample sampleMuElePi("AnalysisResults/C_check/v2/MuElePi.root");
  //DrawingTools draw("AnalysisResults/PosElePiNd5.root");
  //DataSample sample("AnalysisResults/PosElePiNd5.root");

  std::string filename = "PDF/Efficiency1.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  std::string cutPast = "(accum_level[1] > 9 || accum_level[3] > 9)";//"  && ((HNL_true_position[0] > 20 && HNL_true_position[0] < 870) || (HNL_true_position[0] > -870 && HNL_true_position[0] < -20)) && (HNL_true_position[1] > -930 && HNL_true_position[1] < 1030) && ((HNL_true_position[2] > -724.85 && HNL_true_position[2] < -71.15) || (HNL_true_position[2] > 634.15 && HNL_true_position[2] < 1287.85) || (HNL_true_position[2] > 1993.55 && HNL_true_position[2] < 2646.85))";
  std::string cutPre  = "accum_level[1] > -1 && ((HNL_true_position[0] > 20 && HNL_true_position[0] < 870) || (HNL_true_position[0] > -870 && HNL_true_position[0] < -20)) && (HNL_true_position[1] > -930 && HNL_true_position[1] < 1030) && ((HNL_true_position[2] > -724.85 && HNL_true_position[2] < -71.15) || (HNL_true_position[2] > 634.15 && HNL_true_position[2] < 1287.85) || (HNL_true_position[2] > 1993.55 && HNL_true_position[2] < 2646.85))";

  drawPosElePi.SetTitleX("Mass, MeV");
  drawPosElePi.SetTitleY("Efficiency");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  TMultiGraph* mg = new TMultiGraph();
  BGcanvas->SetGrid();

  //draw.DumpCuts();

  //draw.DrawEffVSCut(sample, 3, "HNL_true_mass", "", -1, -1, "", "now");
  //BGcanvas->Print(filenamefirst.c_str());
  //draw.DrawEffVSCut(sample, 3, "HNL_true_mass", "", -1, -1, "", "");
  //BGcanvas->Print(filename.c_str());

  drawPosElePi.DrawEff(samplePosElePi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph1(drawPosElePi.GetLastGraph());
  WeightGraph1->SetLineColor(kRed);
  WeightGraph1->SetMarkerColor(kRed);
  WeightGraph1->SetTitle("K^{+}#rightarrow e^{+}(e^{-}#pi^{+})");
  mg->Add(WeightGraph1, "XLPE"); 

  drawPosPosPi.DrawEff(samplePosPosPi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph0(drawPosPosPi.GetLastGraph());
  WeightGraph0->SetLineColor(kMagenta);
  WeightGraph0->SetMarkerColor(kMagenta);
  WeightGraph0->SetTitle("K^{+}#rightarrow e^{+}(e^{+}#pi^{-})");
  mg->Add(WeightGraph0, "XLP"); 


  drawEleElePi.DrawEff(sampleEleElePi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph2(drawEleElePi.GetLastGraph());
  WeightGraph2->SetLineColor(kBlue);
  WeightGraph2->SetMarkerColor(kBlue);
  WeightGraph2->SetTitle("K^{-}#rightarrow e^{-}(e^{-}#pi^{+})");
  mg->Add(WeightGraph2, "XLP");


  drawElePosPi.DrawEff(sampleElePosPi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph3(drawElePosPi.GetLastGraph());
  WeightGraph3->SetLineColor(kGreen);
  WeightGraph3->SetMarkerColor(kGreen);
  WeightGraph3->SetTitle("K^{-}#rightarrow e^{-}(e^{+}#pi^{-})");
  mg->Add(WeightGraph3, "XLP");

  drawMuElePi.DrawEff(sampleMuElePi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph4(drawMuElePi.GetLastGraph());
  WeightGraph4->SetLineColor(kBlack);
  WeightGraph4->SetMarkerColor(kBlack);
  WeightGraph4->SetTitle("K^{+}#rightarrow #mu^{+}(e^{-}#pi^{+})");
  mg->Add(WeightGraph4, "XLP");


  BGcanvas->Clear();
  BGcanvas->SetGrid();

  //mg->SetTitle("Efficiency for N#rightarrow e^{#pm}+#pi^{#mp}");
  mg->SetMinimum(0.);
  mg->SetMaximum(0.6);
  //mg->GetYaxis()->SetNdivisions(510);
  //mg->GetXaxis()->SetNdivisions(510);
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetTitleAlign(13);
  //gStyle->SetTitleXOffset(20);
  mg->Draw("a");

  mg->GetXaxis()->SetTitle("HNL mass, MeV");
  mg->GetYaxis()->SetTitle("Efficiency");
  mg->GetXaxis()->SetTitleSize(0.05);
  mg->GetYaxis()->SetTitleSize(0.05);
  
  gPad->Update();
  BGcanvas->BuildLegend();
  BGcanvas->Print(filenamelast.c_str());
}