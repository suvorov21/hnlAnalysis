{ 
  //DrawingTools draw("AnalysisResults/Signal/Sys/MuPi_prod5_Sys_old.root");
  //DataSample sample("AnalysisResults/Signal/Sys/MuPi_prod5_Sys_old.root");

  DrawingTools draw("$bars/AnalysisResults/hnlAnalysis/Signal/Sys/ElePi_new_PID.root");
  DataSample sample("$bars/AnalysisResults/hnlAnalysis/Signal/Sys/ElePi_new_PID.root");
  //DrawingTools draw("AnalysisResults/Signal/Sys/MuPi/all.root");
  //DataSample sample("AnalysisResults/Signal/Sys/MuPi/all.root");
  std::string filename = "$bars/dev/figures/hnlAnalysis/Systematics.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("Mass, MeV");
  draw.SetTitleY("Relative error");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

  //draw.DumpConfigurations();
  draw.DumpConfiguration("all_syst");
  //draw.DumpConfiguration("default");
  //draw.DumpCuts();
  //draw.DumpSystematics();

  std::string cut = "accum_level[][1]>9 || accum_level[][3]>9";

  double bins[12] = {260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480};

  //draw.ListOptions();
  
  //std::cout << "total:" << std::endl;
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS");
  //BGcanvas->Print(filenamefirst.c_str());

  //draw.SetTitleY("#events");
  //draw.Draw(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, "all", "accum_level[][0]>8", "", "");
  //BGcanvas->Print(filename.c_str());

  //draw.DrawErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, "accum_level[][0]>8", "", "SSYS NOW");
  //BGcanvas->Print(filename.c_str());

  draw.SetMaxY(0.4);
  
  //std::cout << "variation" << std::endl;
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SSYS", "variation");
  std::cout << "Var+ Charge ID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS0", "Var + Charge ID");
  std::cout << "+ TPC cluster" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1", "+ TPC cluster");
  std::cout << "+ TPC track" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2", "+ TPC track");
  std::cout << "+ TPC FGD match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  std::cout << "+ GV eff" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5", "+ GV eff");
  std::cout << "+ SI Pion" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5 WS6", "+ pion SI");
  /*std::cout << "+ TPC ECal match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5 WS6 WS7", "+ Tpc ECal match");
  std::cout << "+ ECal PID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5 WS6 WS7 WS8", "+ ECal PID");
 */
  BGcanvas->Print(filename.c_str());
  

  /*
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS0 WS1 WS2 WS3 WS5 WS6 ", "Detector sys");
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6", "+ Flux");
  BGcanvas->Print(filename.c_str());
  */

}