{ 
  DrawingTools draw("AnalysisResults/Signal/MuPi_vM.root");
  DataSample sample("AnalysisResults/Signal/MuPi_vM.root");
  DrawingTools drawEleMumPi("AnalysisResults/C_check/v2/EleMumPi.root");
  DataSample sampleEleMumPi("AnalysisResults/C_check/v2/EleMumPi.root");
  DrawingTools drawEleMuPi("AnalysisResults/C_check/v2/EleMuPi.root");
  DataSample sampleEleMuPi("AnalysisResults/C_check/v2/EleMuPi.root");
  DrawingTools drawPosMupPi("AnalysisResults/C_check/v2/PosMupPi.root ");
  DataSample samplePosMupPi("AnalysisResults/C_check/v2/PosMupPi.root ");

  std::string filename = "PDF/Efficiency0.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  std::string cutPast = "(accum_level[0] > 9 || accum_level[2] > 9)";
  std::string cutPre  = "accum_level[0] > -1 && ((HNL_true_position[0] > 20 && HNL_true_position[0] < 870) || (HNL_true_position[0] > -870 && HNL_true_position[0] < -20)) && (HNL_true_position[1] > -930 && HNL_true_position[1] < 1030) && ((HNL_true_position[2] > -724.85 && HNL_true_position[2] < -71.15) || (HNL_true_position[2] > 634.15 && HNL_true_position[2] < 1287.85) || (HNL_true_position[2] > 1993.55 && HNL_true_position[2] < 2646.85))";

  drawEleMuPi.SetTitleX("Mass, MeV");
  drawEleMuPi.SetTitleY("Efficiency");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  TMultiGraph* mg = new TMultiGraph();
  BGcanvas->SetGrid();

  //draw.DumpCuts();

  //draw.DrawEffVSCut(sample, 3, "HNL_true_mass", "", -1, -1, "", "now");
  //BGcanvas->Print(filenamefirst.c_str());
  //draw.DrawEffVSCut(sample, 3, "HNL_true_mass", "", -1, -1, "", "");
  //BGcanvas->Print(filename.c_str());

  drawEleMuPi.DrawEff(sampleEleMuPi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph0(drawEleMuPi.GetLastGraph());
  WeightGraph0->SetLineColor(kRed);
  WeightGraph0->SetMarkerColor(kRed);
  WeightGraph0->SetTitle("K^{-}#rightarrow e^{-}(#mu^{+}#pi^{-})");
  mg->Add(WeightGraph0, "XLP"); 

  drawEleMumPi.DrawEff(sampleEleMumPi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph1(drawEleMumPi.GetLastGraph());
  WeightGraph1->SetLineColor(kMagenta);
  WeightGraph1->SetMarkerColor(kMagenta);
  WeightGraph1->SetTitle("K^{-}#rightarrow e^{-}(#mu^{-}#pi^{+})");
  mg->Add(WeightGraph1, "XLP"); 

  draw.DrawEff(sample.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph2(draw.GetLastGraph());
  WeightGraph2->SetLineColor(kBlue);
  WeightGraph2->SetMarkerColor(kBlue);
  WeightGraph2->SetTitle("K^{+}#rightarrow e^{+}(#mu^{-}#pi^{+})");
  mg->Add(WeightGraph2, "XLP"); 

  drawPosMupPi.DrawEff(samplePosMupPi.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPast.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph3(drawPosMupPi.GetLastGraph());
  WeightGraph3->SetLineColor(kGreen);
  WeightGraph3->SetMarkerColor(kGreen);
  WeightGraph3->SetTitle("K^{+}#rightarrow e^{+}(#mu^{+}#pi^{-})");
  mg->Add(WeightGraph3, "XLP"); 
 


  BGcanvas->Clear();
  BGcanvas->SetGrid();

  //mg->SetTitle("Efficiency for N#rightarrow e^{#pm}+#pi^{#mp}");
  mg->SetMinimum(0.);
  mg->SetMaximum(0.6);
  //mg->GetYaxis()->SetNdivisions(510);
  //mg->GetXaxis()->SetNdivisions(510);
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetTitleAlign(13);
  //gStyle->SetTitleXOffset(20);
  mg->Draw("a");

  mg->GetXaxis()->SetTitle("HNL mass, MeV");
  mg->GetYaxis()->SetTitle("Efficiency");
  mg->GetXaxis()->SetTitleSize(0.05);
  mg->GetYaxis()->SetTitleSize(0.05);
  
  gPad->Update();
  BGcanvas->BuildLegend();
  BGcanvas->Print(filenamelast.c_str());
}