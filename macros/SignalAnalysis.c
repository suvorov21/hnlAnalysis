{ 
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(1,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  t2kStyle->SetOptTitle(1);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  DrawingTools draw("AnalysisResults/Signal/MuPi.root");
  DataSample sample("AnalysisResults/Signal/MuPi.root");
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();
  //DrawingTools draw("AnalysisResults/SignalEvent_AnalysisResult1_Sys_Full.root");
  //DataSample sample("AnalysisResults/SignalEvent_AnalysisResult1_Sys_Full.root");
  //DrawingTools draw("AnalysisResults/out.root");
  //DataSample sample("AnalysisResults/out.root");
  //DrawingTools draw("AnalysisResults/prod6B_magnet_neut_AnalResultsPart1.root");
  //DataSample sample("AnalysisResults/prod6B_magnet_neut_AnalResultsPart1.root");
  //DrawingTools draw("AnalysisResults/C_check/MuElePi.root");
  //DataSample sample("AnalysisResults/C_check/MuElePi.root");
  //DrawingTools draw("AnalysisResults/DiMuon/SignalDiMuon.root");
  //DataSample sample("AnalysisResults/DiMuon/SignalDiMuon.root");
  sample.SetCurrentTree("default");
  //DataSample sample_truth("prod6B_magnet_neut_AnalResults.root");
  //sample_truth.SetCurrentTree("truth");


  std::string filename = "PDF/SignalAnalysis.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("M_{reco}-M_{true}, MeV");
  draw.SetTitleY("N");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,1000); 
    

  draw.DumpCuts();
  //draw.DumpSteps();
  draw.DumpPOT(sample);
  //draw.DrawEventsVSCut(sample, 0, "accum_level[][0]>0");
  //draw.DrawEventsVSCut(sample, 1, "accum_level[][1]>0");
  //draw.DrawEventsVSCut(sample, 2, "accum_level[][2]>0");
  //draw.DrawEventsVSCut(sample, 3, "accum_level[][3]>0");
  //draw.DumpCategories("AnalusisResults/VETO_OFF/BGanal2.root");
  //draw.ListOptions();
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  //draw.SetOptStat("RM");
  //gStyle->SetOptStat(2200);
  //draw.Draw(sample, "HNL_inv_mass[0]-HNL_true_mass", 200, -250., 250., "all", "accum_level[][0]>9 && HNL_true_mass==480", "histo", "");
  //BGcanvas->Clear();
  //TH1D* histo(draw.GetLastHisto());
  //gStyle->SetOptStat(2200);
  //histo->Draw("histo");
  //BGcanvas->Print(filename.c_str());

  //draw.Draw(sample, "1", 2, 0., 2., "all", "accum_level[][]>-1", "","now");
  //draw.Draw(sample, "1", 2, 0., 2., "all", "accum_level[][0]>-1", "","now");
  //draw.Draw(sample, "1", 2, 0., 2., "all", "accum_level[][0]>-1 && accum_level[][1]>-1", "","now");

  /*draw.SetTitle("NNodes in longest TPC segment 1");  
  draw.SetTitleX("N");
  draw.Draw(sample, "FirstTpcNodes1", 100, 0., 100., "all", "accum_level[][0]>1", "HIST","under over");
  BGcanvas->Print(filenamefirst.c_str());
  draw.SetTitle("Analysing nodes");  
  draw.SetTitleX("N");
  draw.Draw(sample, "ANAnodes", 100, 0., 100., "all", "accum_level[][0]>8", "HIST","under over");
  BGcanvas->Print(filenamefirst.c_str());
  draw.SetTitle("NNodes in longest TPC segment (nodes in first TPC)");  
  draw.SetTitleX("N");
  draw.Draw(sample, "MostTpcNodes2:FirstTpcNodes2", 100, 0., 100., 100, 0., 100., "all", "accum_level[][]>1", "HIST","under over");
  BGcanvas->Print(filenamelast.c_str());*/


  /*draw.SetTitle("NNodes in longest TPC segment 2");  
  draw.SetTitleX("N");
  draw.Draw(sample, "FirstTpcNodes2", 100, 0., 100., "all", "1", "HIST","under over");
  BGcanvas->Print(filenamelast.c_str());*/

  /*draw.SetTitle("HMP End Detector");  
  draw.SetTitleX("N");
  draw.Draw(sample, "HMPEndTrackDet", 27, 0., 27., "all", "accum_level[][4]>6", "HIST","");
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("HMN end detector");  
  draw.SetTitleX("N");
  draw.Draw(sample, "HMNEndTrackDet", 27, 0., 27., "all", "accum_level[][4]>6", "HIST","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Relative angle");  
  draw.SetTitleX("cos(#theta)");
  draw.Draw(sample, "cos(HNL_constituents_rel_angle)", 100, -1., 1., "all", "accum_level[][4]>6", "HIST","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("HNL dir");  
  draw.SetTitleX("cos(#theta)");
  draw.Draw(sample, "HNL_direction[2]", 100, 0.95, 1., "all", "accum_level[][4]>6", "HIST","UNDER");
  BGcanvas->Print(filenamelast.c_str());*/



  /*draw.SetTitle("Reco tracks associated with HNL constituents");  
  draw.SetTitleX("N");
  draw.Draw(sample, "nRecoTracks", 15, 0., 15., "all", "accum_level[][0]>0", "","now");
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("Reco tracks associated with HNL constituents");  
  draw.SetTitleX("N");
  draw.Draw(sample, "TrueFilter", 10, -2., 8., "all", "accum_level[][0]>0", "","now");
  BGcanvas->Print(filename.c_str());
  draw.SetTitle("VertexChi2");  
  draw.SetTitleX("VertexChi2");
  draw.Draw(sample, "VertexChi2", 100, 0., 1000., "all", "accum_level[][1]>0", "","OVER now");
  BGcanvas->Print(filenamefirst.c_str());
  
  draw.SetTitle("VarianceX");  
  draw.SetTitleX("VarianceX");
  draw.Draw(sample, "VarianceX", 100, 0., 100., "all", "accum_level[][1]>0", "","OVER now");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("VarianceY");  
  draw.SetTitleX("VarianceY");
  draw.Draw(sample, "VarianceY", 100, 0., 100., "all", "accum_level[][1]>0", "","OVER now");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("VarianceZ");  
  draw.SetTitleX("VarianceZ");
  draw.Draw(sample, "VarianceZ", 100, 0., 500., "all", "accum_level[][1]>0", "","OVER now");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("VarianceR");  
  draw.SetTitleX("VarianceR");
  draw.Draw(sample, "VarianceR", 100, 0., 2000., "all", "accum_level[][1]>0", "","OVER now");
  BGcanvas->Print(filename.c_str());*/

  /*draw.SetTitle("Pullmuon");  
  draw.SetTitleX("Pullmuon");
  draw.Draw(sample, "pullmuon", 200, -20., 20., "all", "accum_level[][1]>5", "histo","AREA1");
  draw.DrawCutLineVertical(-2.5);
  draw.DrawCutLineVertical(2.5);
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("Pullpion");  
  draw.SetTitleX("Pullpion");
  draw.Draw(sample, "pullpion", 200, -20., 20., "all", "accum_level[][1]>5", "histo","AREA1");
  draw.DrawCutLineVertical(-2.);
  draw.DrawCutLineVertical(2.);
  BGcanvas->Print(filename.c_str());*/

  /*draw.SetTitle("Pullprot");  
  draw.SetTitleX("Pullprot");
  draw.Draw(sample, "pullprot", 200, -20., 20., "all", "accum_level[][1]>5", "","");
  BGcanvas->Print(filename.c_str());*/

  /*draw.SetTitle("PullElec");  
  draw.SetTitleX("PullElec");
  draw.Draw(sample, "pullelec", 200, -20., 20., "all", "accum_level[][1]>5", "histo","AREA1");
  draw.DrawCutLineVertical(-2.);
  draw.DrawCutLineVertical(2.5);
  BGcanvas->Print(filenamelast.c_str());*/


  /*draw.SetTitle("#delta p_{T}");  
  draw.SetTitleX("#delta p_{T}, MeV");
  draw.Draw(sample, "dpt", 100, 0., 800., "all", "accum_level[][1]>9", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("#delta p_{T}");  
  draw.SetTitleX("#delta p_{T, true}, MeV");
  draw.Draw(sample, "dpt_true", 100, 0., 800., "all", "accum_level[][1]>9", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("#delta #phi_{T}");  
  draw.SetTitleX("#delta #phi_{T}, deg");
  draw.Draw(sample, "dphit*57.295", 100, 0., 180., "all", "accum_level[][1]>9", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("#delta #alpha_{T}");  
  draw.SetTitleX("#delta #alpha_{T}, deg");
  draw.Draw(sample, "dat*57.295", 100, 0., 180., "all", "accum_level[][1]>9", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Resolution of HNL direction");  
  draw.SetTitleX("#alpha, deg");
  draw.Draw(sample, "acos((HNL_direction[0]*HNL_true_direction[0]+HNL_direction[1]*HNL_true_direction[1]+HNL_direction[2]*HNL_true_direction[2])/(sqrt(HNL_direction[0]*HNL_direction[0]+HNL_direction[1]*HNL_direction[1]+HNL_direction[2]*HNL_direction[2])*sqrt(HNL_true_direction[0]*HNL_true_direction[0]+HNL_true_direction[1]*HNL_true_direction[1]+HNL_true_direction[2]*HNL_true_direction[2])))*57.295", 100, 0., 180., "all", "accum_level[][0]>9", "","");
  BGcanvas->Print(filenamelast.c_str());

  /*draw.SetTitle("method of vertex reco before cut");  
  draw.SetTitleX("N");
  draw.Draw(sample, "method", 5, 0., 5., "all", "accum_level[][0]>0", "","");
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("method of vertex reco after cut");  
  draw.SetTitleX("N");
  draw.Draw(sample, "method", 5, 0., 5., "all", "accum_level[][0]>1", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("method of vertex reco after all cut");  
  draw.SetTitleX("N");
  draw.Draw(sample, "method", 5, 0., 5., "all", "accum_level[][0]>9", "","");
  BGcanvas->Print(filenamelast.c_str());*/

  draw.SetTitle("Muon PID (low momentum only)");  
  draw.SetTitleX("(L_{#mu}+L_{#pi})/(1-L_{p})");
  draw.Draw(sample, "cut1_mu", 200, 0.7, 1.1, "all", "cut1[0] == 1 && cut2[0] == 1 && cut3[0] == 1 && cut4[0] == 1 && cut7[0] == 1 && cut8[0] == 1 && cut9[0] == 1" , "hist","AREA1");
  draw.DrawCutLineVertical(0.8, true, "r");
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("Muon PID");  
  draw.SetTitleX("L_{#mu}");
  draw.Draw(sample, "cut2_mu", 200, 0., 1.1, "all", "cut1[0] == 1 && cut2[0] == 1 && cut3[0] == 1 && cut4[0] == 1 && cut7[0] == 1 && cut8[0] == 1 && cut9[0] == 1" , "hist","AREA1");
  draw.DrawCutLineVertical(0.05, true, "r");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("pion PID (low momentum only)");  
  draw.SetTitleX("(L_{#mu}+L_{#pi})/(1-L_{p})");
  draw.Draw(sample, "mip_lhood", 200, 0.7, 1.1, "all", "cut1[0] == 1 && cut2[0] == 1 && cut3[0] == 1 && cut4[0] == 1 && cut7[0] == 1 && cut8[0] == 1 && cut9[0] == 1" , "hist","AREA1");
  draw.DrawCutLineVertical(0.8, true, "r");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Pion PID");  
  draw.SetTitleX("L_{#pi}");
  draw.Draw(sample, "pion_lhood", 200, 0., 1.1, "all", "pion_lhood > 0.001 && cut1[0] == 1 && cut2[0] == 1 && cut3[0] == 1 && cut4[0] == 1 && cut7[0] == 1 && cut8[0] == 1 && cut9[0] == 1" , "hist","AREA1");
  draw.DrawCutLineVertical(0.3, true, "r");
  BGcanvas->Print(filenamelast.c_str());

  /*draw.SetTitle("Vertex accuracy (reco - true)");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "HNL_position[2] - HNL_true_position[2]", 200, -70., 70., "all", "accum_level[][0]>9", "","");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Distance between CS and track 1");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "dz1", 200, 0., 400., "all", "TrueFilter == 2 && dz1 != 0", "","OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Distance between CS and track 2");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "dz2", 200, 0., 400., "all", "TrueFilter == 2 && dz2 != 0", "","OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("#chi^{2} of reco GV (cut is < 100)");  
  draw.SetTitleX("#chi^{2}");
  draw.Draw(sample, "VertexChi2", 200, 0., 20., "all", "accum_level[][0]>1", "","OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("method of vertex reco after ALL cuts");  
  draw.SetTitleX("N");
  draw.Draw(sample, "method", 5, 0., 5., "all", "accum_level[][0]>9", "","now");
  BGcanvas->Print(filenamelast.c_str());*/

  /*draw.SetTitle("True vertex Z");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "HNL_true_position[2]", 200, -1700, 3000., "all", "accum_level[][0]>0");
  BGcanvas->Print(filenamefirst.c_str());*/

  /*draw.SetTitle("ClosestSeparation 3D");
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "ClosestSeparation", 200, 0., 200., "all", "accum_level[][0]>1 && ClosestSeparation!=0", "", "OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("ClosestSeparation X");
  draw.Draw(sample, "ClosestSeparationX", 200, 0., 200., "all", "accum_level[][0]>1 && ClosestSeparationX!=0", "", "OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("ClosestSeparation Y");
  draw.Draw(sample, "ClosestSeparationY", 200, 0., 200., "all", "accum_level[][0]>1 && ClosestSeparationY!=0", "", "OVER");
  draw.DrawCutLineVertical(200);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("ClosestSeparation Z");
  draw.Draw(sample, "ClosestSeparationZ", 200, 0., 200., "all", "accum_level[][0]>1 && ClosestSeparationZ!=0", "", "OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("dz1");
  draw.Draw(sample, "dz1", 200, 0, 700., "all", "accum_level[][0]>1 && dz1!=0", "", "OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("dz2");
  draw.Draw(sample, "dz2", 200, 0, 700., "all", "accum_level[][0]>1 && dz2!=0", "", "OVER");
  draw.DrawCutLineVertical(100);
  BGcanvas->Print(filename.c_str());

  draw.SetTitleX("N constituent");
  draw.SetTitle("N constituents");
  draw.Draw(sample, "nConstituents", 6, 0, 6., "all", "accum_level[][0]>1");
  BGcanvas->Print(filenamelast.c_str());*/

  /*draw.SetTitle("Opening angle");
  draw.SetTitleX("cos#theta");
  draw.Draw(sample, "cos(rel_angle)", 0.9, 1.1, 200., "all", "accum_level[][0]>1");
  BGcanvas->Print(filenamelast.c_str());*/

  /*draw.SetTitle("Recon vertex Z");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "HNL_position[2]", 200, -1700, 3000., "all", "accum_level[][0]>1", "", "under over");
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());*/

  /*draw.SetTitle("Recon outFV vertex Z");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "HNL_position[2]", 200, -1700, 3000., "all", "accum_level[][0]>1 && ((HNL_position[2]<=-724.85) || (HNL_position[2]>=-71.15 && HNL_position[2]<=634.15) || (HNL_position[2]>=1287.85 && HNL_position[2]<=1993.55) || (HNL_position[2]>=2646.85))", "", "under over");
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Range between 2 tracks");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "Range", 100, -1, 600., "all", "accum_level[][0]>0 && Range != -1", "", "now");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("RangeZ between 2 tracks");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "RangeZ", 100, -1, 300., "all", "accum_level[][0]>0 && RangeZ != -1", "", "now");
  BGcanvas->Print(filenamelast.c_str());*/
  
  /*draw.SetTitle("Recon relative angle HMP & HMN, theta cut success");
  draw.SetTitleX("cos(#theta)");
  draw.SetMinY(1E13);
  draw.SetLogY();
  draw.Draw(sample, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", "accum_level[][0]>2");
  draw.DrawCutLineVertical(0., true, "r");
  BGcanvas->Print(filename.c_str());*/

  //draw.SetTitle("Efficiency N#rightarrow #mu#pi");
  //draw.SetTitleY("N");
  //draw.SetTitleX("Momentum, MeV/c");
  //draw.DrawEff(sample.GetTree("truth"), "sqrt(nu_trueE*nu_trueE - HNL_true_mass*HNL_true_mass)", 50, 0, 5000., "accum_level[1]>10 || accum_level[3]>10", "accum_level[1]>1", "", "now");
  //BGcanvas->Print(filename.c_str());

  /*draw.SetTitle("Efficiency N#rightarrow #mu#pi");
  draw.SetTitleX("cos(#theta)");
  //draw.SetTitleX("Momentum, GeV/c");
  draw.DrawEff(sample.GetTree("truth"), "cos(HNL_true_constituents_rel_angle)", 50, -1., 1., "accum_level[0]>9 || accum_level[2]>9", "accum_level[0]>-1", "", "now");
  BGcanvas->Print(filenamelast.c_str());*/

  //draw.SetOptStat("m");
  //draw.SetOptStat("RM");
  //gStyle->SetOptStat(2200);
  /*draw->SetTitleX("T_{vertex} - T_{bunch}, ns");
  draw.Draw(sample, "HNL_position[3] - BunchTime+dToF", 150, -1100, 600, "all", "accum_level[][0]>1 && HNL_true_mass == 460 && BunchTime > 0", "", "OVER ");
  BGcanvas->Print(filenamefirst.c_str());

  draw->SetTitleX("T_{bunch}, ns");
  draw.Draw(sample, "BunchTime", 15, 2500, 7500, "all", "accum_level[][0]>1 && HNL_true_mass == 460 && BunchTime > 0", "", "OVER ");
  BGcanvas->Print(filename.c_str());

  draw->SetTitleX("T_{vertex}, ns");
  draw.Draw(sample, "HNL_position[3]", 500, 2500, 7500, "all", "accum_level[][0]>1 && HNL_true_mass == 460 && BunchTime > 0", "", "OVER ");
  BGcanvas->Print(filenamelast.c_str());*/



  //draw.SetLineColor(kBlack);
  //gStyle->SetOptStat(0);
  //draw->SetTitleX("HNL direction, degree");
  //draw.Draw(sample, "acos(HNL_true_direction[2])*180/3.1415", 100, 0., 20., "all", "accum_level[][0]>7", "histo", "AREA1");
  //draw.SetLineColor(kRed);
  //draw.Draw(sample, "acos(HNL_direction[2])*180/3.1415", 100, 0., 20., "all", "accum_level[][0]>7", "same histo", "AREA1");
  //draw.DrawCutLineVertical(acos(0.97)*180/3.1415);
  //BGcanvas->Print(filename.c_str());

  /*draw.SetOptStat("m");
  draw.SetOptStat("RM");
  gStyle->SetOptStat(2200);
  draw->SetTitleX("T_{reco} - T_{true}, ns");*/
  //draw.Draw(sample, "HNL_position[3] - HNL_true_position[3]-2690", 100, -100, 100, "all", "accum_level[][1]>-1 && HNL_true_mass == 460");
  
  //BGcanvas->Print(filenamelast.c_str());

  /*draw.SetTitleY("cos(#theta)");
  draw.SetTitleX("Momentum, GeV/c");
  draw.DrawEff(sample.GetTree("truth"), "sqrt(nu_trueE*nu_trueE - HNL_true_mass*HNL_true_mass):cos(HNL_true_constituents_rel_angle)", 50, 0, 5000., 50, -1., 1., "accum_level[0]>9 || accum_level[2]>9", "accum_level[0]>-1", "colz", "now");
  BGcanvas->Print(filenamelast.c_str());*/

  /*draw.SetTitle("Recon polar angle of Nu, vertex in FV");
  draw.Draw(sample, "HNL_direction[2]", 200, 0.8, 1.1, "all", "accum_level[][0]>1");
  draw.DrawCutLineVertical(0.97, true, "r");
  BGcanvas->Print(filename.c_str());

  draw.SetTitleX("Z, mm");
  draw.SetTitle("Recon Vertex Z position. inv_mass success");
  draw.SetLogY(0);
  draw.Draw(sample, "HNL_position[2]", 200, -1700, 3000., "all", "accum_level[][0]>9");
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("True Vertex Z position. inv_mass success");
  draw.Draw(sample, "HNL_true_position[2]", 200, -1700, 3000., "all", "accum_level[][0]>9");
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("HMN Particle. inv_mass cut");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "particle", "accum_level[][0]>9");
  //TLegend* legend(draw.GetLastLegend());
 
  //legend->Draw();
  //draw.GetLastLegend()->PaintPave(0.8,0.8,0.9,0.9);

  BGcanvas->Print(filename.c_str());
  draw.SetTitle("HMP Particle. inv_mass cut");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "HMPparticle", "accum_level[][0]>9");
  BGcanvas->Print(filename.c_str());
  

  draw.SetTitle("FGD1 hits. inv_mass success");
  draw.SetTitleX("Hints");
  draw.Draw(sample, "NFGD1VetoHits", 11, -1, 10., "all", "accum_level[][0]>9", "", "under over");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("FGD2 hits. inv_mass success");
  draw.Draw(sample, "NFGD2VetoHits", 11, -1, 10., "all", "accum_level[][0]>9", "", "under over");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("P0DVetoEndPos Z . inv_mass success");
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "P0DVetoEndPos[][2]", 100, -1500., -900., "all", "accum_level[][0]>9");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("P0DVetoStartPos Z . inv_mass success");
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "P0DVetoStartPos[][2]", 100, -1500., -900., "all", "accum_level[][0]>9");
  BGcanvas->Print(filename.c_str());
  
  //draw.SetOptStat("e");
  draw.SetTitle("Energy spectrum of parent. inv_mass success");
  draw.SetTitleX("E, MeV");
  draw.Draw(sample, "nu_trueE", 200, 0., 12000., "all", "accum_level[][0]>9", "", "under over");
  BGcanvas->Print(filenamelast.c_str());*/
 }