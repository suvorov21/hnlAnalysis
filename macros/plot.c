{
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  DrawingTools drawS0("AnalysisResults/SignalEvent_AnalysisResult0.root");
  DataSample sampleS0("AnalysisResults/SignalEvent_AnalysisResult0.root");

  DrawingTools drawS1("AnalysisResults/SignalEvent_AnalysisResult1.root");
  DataSample sampleS1("AnalysisResults/SignalEvent_AnalysisResult1.root");

  DrawingTools drawBG("AnalysisResults/prod6B_magnet_neut_AnalResults_new.root");
  DataSample sampleBG("AnalysisResults/prod6B_magnet_neut_AnalResults_new.root");

  std::string filename = "PDF/AngleCut.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();
  
  drawS0.Draw(sampleS0, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", "accum_level[][0]>7", "", "AREA1");
  TH1F* SignalMuonRel(drawS0.GetLastHisto());

  drawBG.Draw(sampleBG, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", "accum_level[][0]>7", "", "AREA1");
  TH1F* BGMuonRel(drawBG.GetLastHisto());

  drawS1.Draw(sampleS1, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", "accum_level[][1]>7", "HIST", "AREA1");
  TH1F* SignalEleRel(drawS1.GetLastHisto());

  drawBG.Draw(sampleBG, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", "accum_level[][1]>7", "HIST", "AREA1");
  TH1F* BGEleRel(drawBG.GetLastHisto());

  drawS0.Draw(sampleS0, "HNL_direction[2]", 200, -1.1, 1.1, "all", "accum_level[][0]>7", "HIST", "AREA1");
  TH1F* SignalMuonPol(drawS0.GetLastHisto());

  drawS1.Draw(sampleS1, "HNL_direction[2]", 200, -1.1, 1.1, "all", "accum_level[][1]>7", "HIST", "AREA1");
  TH1F* SignalElePol(drawS1.GetLastHisto());

  drawBG.Draw(sampleBG, "HNL_direction[2]", 200, -1.1, 1.1, "all", "accum_level[][0]>7", "HIST", "AREA1");
  TH1F* BGMuonPol(drawBG.GetLastHisto());

  drawBG.Draw(sampleBG, "HNL_direction[2]", 200, -1.1, 1.1, "all", "accum_level[][1]>7", "HIST", "AREA1");
  TH1F* BGElenPol(drawBG.GetLastHisto());

  /*SignalMuonRel->SetOption("HIST");
  BGMuonRel->SetOption("HIST");
  SignalEleRel->SetOption("HIST");
  SignalMuonPol->SetOption("HIST");
  SignalElePol->SetOption("HIST");
  BGMuonPol->SetOption("HIST");
  BGElenPol->SetOption("HIST");
  BGEleRel->SetOption("HIST");*/

  SignalMuonRel->SetTitle("Relative angle. Backgoround (black) N#rightarrow #mu #pi (red)");
  SignalMuonRel->SetLineColor(kRed);
  BGMuonRel->SetLineColor(kBlack);
  BGMuonRel->Draw("HIST");
  SignalMuonRel->Draw("SAME HIST");
  BGcanvas->Print(filenamefirst.c_str());
  BGcanvas->Clear();

  SignalEleRel->SetTitle("Relative angle. Backgoround (black) N#rightarrow e #pi (red)");
  SignalEleRel->SetLineColor(kRed);
  BGEleRel->SetLineColor(kBlack);
  BGEleRel->Draw("HIST");
  SignalEleRel->Draw("SAME HIST");
  BGcanvas->Print(filename.c_str());
  BGcanvas->Clear();

  SignalMuonPol->SetTitle("Polar angle. Backgoround (black) N#rightarrow #mu #pi (red)");
  SignalMuonPol->SetLineColor(kRed);
  BGMuonPol->SetLineColor(kBlack);
  SignalMuonPol->Draw("HIST");
  BGMuonPol->Draw("SAME HIST");
  BGcanvas->Print(filename.c_str());
  BGcanvas->Clear();

  SignalElePol->SetTitle("Polar angle. Backgoround (black) N#rightarrow e #pi (red)");
  SignalElePol->SetLineColor(kRed);
  BGElenPol->SetLineColor(kBlack);
  SignalElePol->Draw("HIST");
  BGElenPol->Draw("SAME HIST");
  BGcanvas->Print(filenamelast.c_str());
  BGcanvas->Clear();

}