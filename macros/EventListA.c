{
  std::cout << "Listing started" << std::endl;
  //TFile* file = new TFile("AnalysisResults/SignalEvent_AnalysisResult1.root", "READ");
  //TFile* file = new TFile("AnalysisResults/Timing/prod6B_magnet_antineut_AnalResults1.root", "READ");
  //TFile* file = new TFile("AnalysisResults/Timing/prod6B_magnet_genie_AnalResults.root");
  //TFile* file = new TFile("AnalysisResults/Timing/prod6E_rdp_AnalResults.root", "READ");
  //TFile* file = new TFile("AnalysisResults/prod6E_rdp_AnalResults_ev.root", "READ");
  //TFile* file = new TFile("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root", "READ");
  TFile* file = new TFile("AnalysisResults/BG/ANEUT/prod6B_magnet_antineut_AnalResults_1.root", "READ");
  //TFile* file = new TFile("AnalysisResults/prod6E_rdp_AnalResults_add.root", "READ");
  
  //TFile* file = new TFile("out.root", "READ");
  TTree* tree = (TTree*)file->Get("default");

  std::string form = "(accum_level[][0]>9 || accum_level[][2]>9) && HNL_direction[2] > 0.998440764181981";
  //std::string form = "cut2[0]";
  Int_t mode = 0;

  TTreeFormula* formula = new TTreeFormula("cut", form.c_str(), tree);
  formula->GetNdata();
  Int_t Nev = 0;

  Float_t HNL_position[3];
  Float_t HNL_true_position[3];
  Float_t Inv_mass[3];
  Int_t reactionnofv, reaction, target, parent, particle, HMPparticle, reaction_all_nu;
  Int_t NFGD1VetoBins, NFGD2VetoBins, nSameTPCTracks, TrueFilter;  
  Int_t cut0[4],cut1[4],cut2[4],cut3[4],cut4[4],cut5[4],cut6[4],cut7[4],cut8[4],cut9[4];
  Float_t dpt, dphit, dat, p1, p2, dpt_true, pullelec, VertexChi2, VarianceR;

  const Int_t NMAXFGDBINS = 50;

  //Int_t NFGD1VetoHits[NMAXFGDBINS];
  //Int_t NFGD2VetoHits[NMAXFGDBINS];
  //Int_t NFGD1VetoHits, NFGD2VetoHits;

  tree->SetBranchAddress("HNL_position",       &HNL_position);
  tree->SetBranchAddress("HNL_true_position",  &HNL_true_position);
  tree->SetBranchAddress("reactionnofv",       &reactionnofv);
  tree->SetBranchAddress("reaction",           &reaction);
  tree->SetBranchAddress("reactionAllNunofv",    &reaction_all_nu);
  tree->SetBranchAddress("target",             &target);
  tree->SetBranchAddress("parent",             &parent);
  tree->SetBranchAddress("particle",           &particle);
  tree->SetBranchAddress("HMPparticle",        &HMPparticle);
  //tree->SetBranchAddress("HNL_inv_mass",       &Inv_mass);
  //tree->SetBranchAddress("NFGD2VetoBins",      &NFGD2VetoBins);
  //tree->SetBranchAddress("NFGD1VetoBins",      &NFGD1VetoBins);
  //tree->SetBranchAddress("NFGD1VetoHits",      &NFGD1VetoHits);
  //tree->SetBranchAddress("NFGD2VetoHits",      &NFGD2VetoHits);*/
  /*tree->SetBranchAddress("cut0",      &cut0);
  tree->SetBranchAddress("cut1",      &cut1);
  tree->SetBranchAddress("cut2",      &cut2);
  tree->SetBranchAddress("cut3",      &cut3);
  tree->SetBranchAddress("cut4",      &cut4);
  tree->SetBranchAddress("cut5",      &cut5);
  tree->SetBranchAddress("cut6",      &cut6);
  tree->SetBranchAddress("cut7",      &cut7);
  tree->SetBranchAddress("cut8",      &cut8);
  tree->SetBranchAddress("cut9",      &cut9);
  tree->SetBranchAddress("nSameTPCTracks",     &nSameTPCTracks);
  tree->SetBranchAddress("TrueFilter",     &TrueFilter);
  tree->SetBranchAddress("dpt",       &dpt);
  tree->SetBranchAddress("dpt_true",       &dpt_true);
  tree->SetBranchAddress("dphit",     &dphit);
  tree->SetBranchAddress("dat",       &dat);
  tree->SetBranchAddress("p1",        &p1);
  tree->SetBranchAddress("p2",        &p2);*/
  //tree->SetBranchAddress("pullelec",        &pullelec);
  //tree->SetBranchAddress("VertexChi2",        &VertexChi2);
  //tree->SetBranchAddress("VarianceR",        &VarianceR);


  Int_t run, subrun, evt;
  tree->SetBranchAddress("run",                &run);
  tree->SetBranchAddress("subrun",             &subrun);
  tree->SetBranchAddress("evt",                &evt);

  //Int_t CutLevel;
  //TBranch *accum_level = tree->GetBranch("accum_level");

  std::cout << "Initialize complete. Start LOOP" << std::endl;
  std::cout << "Formula = " << form << "         mode = " << mode << std::endl;

  Long64_t entries = tree->GetEntries();
  Int_t CheckProgress = floor(entries / 5);

  TCanvas canva("canva", "", 300, 400);
  TH2F* dpt_dat = new TH2F("name", "", 300, 0., 800., 300, 0.,180.);
  TH2F* dpt_p1 = new TH2F("name1", "", 300, 0., 800., 300, 0.,10000.);
  TH2F* dpt_p2 = new TH2F("name2", "", 300, 0., 800., 300, 0.,10000.);
  TH2F* dpt_p1p2 = new TH2F("name3", "", 300, 0., 800., 300, 0.,15000.);

  for (Long64_t i = 0; i < entries; ++i) {

    /*if (i % CheckProgress == 0){
      Double_t ratio = (double)i/entries;
      std::cout << "Entry: " << i << " of " << entries << " (" << int(100*ratio + 0.5) << "%)" << std::endl;
    }*/

    tree->GetEntry(i); 
    
    if (formula->EvalInstance()) {
      /*dpt_dat->Fill(dpt, dat*57.295);
      dpt_p1->Fill(dpt, p1);
      dpt_p2->Fill(dpt, p2);
      dpt_p1p2->Fill(dpt, p1+p2);*/
     
    //if (TrueFilter == 1 && cut1[0] == 0) {
      //tree->GetEntry(i);
      //if (subrun != 2) continue;
      /*if (nSameTPCTracks > 2) continue;*/
      std::cout << "--------------------------------------" << std::endl;

      std::cout << "Background event N = " << Nev+1 << std::endl;
      std::cout << "Run " << run << "    Subrun " << subrun << "      Evt " << evt << std::endl;
      std::cout << run << "-";
      if (subrun < 10)
        std::cout << "000";
      else if (subrun < 100)
        std::cout << "00";
      else if (subrun < 1000)
        std::cout << "0";

      std::cout << subrun << std::endl;

      std::cout << "Recon vertex   = " << HNL_position[0] << "     " << HNL_position[1] << "     " << HNL_position[2] << "     " << HNL_position[3] << std::endl;
      //std::cout << "Chi2 = " << VertexChi2 << "       Var = " << VarianceR << std::endl;
      std::cout << "True vertex    = " << HNL_true_position[0] << "     " << HNL_true_position[1] << "     " << HNL_true_position[2] << "     " << HNL_true_position[3] << std::endl;
      std::cout << "Det (only Z)   = ";
      if  ((HNL_true_position[2] > -724.85) && (HNL_true_position[2]< -71.15))
        std::cout << "TPC1" << std::endl;
      else if  ((HNL_true_position[2] > 634.15) && (HNL_true_position[2]< 1287.85))
        std::cout << "TPC2" << std::endl;
      else if  ((HNL_true_position[2] > 1993.55) && (HNL_true_position[2]< 2646.85))
        std::cout << "TPC3" << std::endl;

      else if  ((HNL_true_position[2] > -71.15) && (HNL_true_position[2]< 634.15))
        std::cout << "FGD1" << std::endl;
      else if  ((HNL_true_position[2] > 1287.85) && (HNL_true_position[2]< 1993.55))
        std::cout << "FGD2" << std::endl;

      else if ((HNL_true_position[2] < -938.753) && (HNL_true_position[2] > -3296.48))
        std::cout << "P0D" << std::endl;
      else std::cout << "Unknown" << std::endl;

      /*std::cout << "FGD time bins =  " << NFGD1VetoBins << std::endl;
      if (NFGD1VetoBins != 0) {
        std::cout << "FGD1 hits arr = ";
        for (Int_t j = 0; j < NFGD1VetoBins; ++j)
          std::cout << NFGD1VetoHits[j] << "    ";
        std::cout << std::endl;
      }

      //std::cout << "FGD2 time bins = " << NFGD2VetoBins << std::endl;
      if (NFGD2VetoBins != 0) {
        std::cout << "FGD2 hits arr = ";
        for (Int_t j = 0; j < NFGD2VetoBins; ++j)
          std::cout << NFGD2VetoHits[j] << "    ";
        std::cout << std::endl;
      }*/

      //std::cout << "SameTPC tracks = " << nSameTPCTracks << std::endl;
      
      //std::cout << "FGD1 hits = " << NFGD1VetoHits << std::endl;
      //std::cout << "FGD2 hits = " << NFGD2VetoHits << std::endl;

      std::string par, reac, tar, part, HMPpart;

      if (parent == 13)   par = "mu-";
      if (parent == 11)   par = "e-";
      if (parent == -211) par = "pi-";
      if (parent == -13)  par = "mu+";
      if (parent == -11)  par = "e+";
      if (parent == 211)  par = "pi+";
      if (parent == 2212) par = "p";
      if (parent == 0)    par = "nu";
      if (parent == 22)   par = "gamma";
      if (parent == 111)  par = "pi0";
      if (parent == 2112) par = "n";
      if (parent == 999)  par = "other";
      if (parent == -1)   par = "no truth";

      std::cout << "Parent code    = " << par << std::endl;

      if (reaction == 0)   reac = "CCQE";
      if (reaction == 9)   reac = "2p2h";
      if (reaction == 1)   reac = "RES";
      if (reaction == 2)   reac = "DIS";
      if (reaction == 3)   reac = "COH";
      if (reaction == 4)   reac = "NC";
      if (reaction == 5)   reac = "anti nu-mu";
      if (reaction == 6)   reac = "nu-e, anti nu-e";
      if (reaction == 999) reac = "other";
      if (reaction == 7)   reac = "out of FV";
      if (reaction == -1)  reac = "no truth";

      std::cout << "Reaction code  = " << reac << std::endl;

      if (reactionnofv == 0)   reac = "CCQE";
      if (reactionnofv == 9)   reac = "2p2h";
      if (reactionnofv == 1)   reac = "RES";
      if (reactionnofv == 2)   reac = "DIS";
      if (reactionnofv == 3)   reac = "COH";
      if (reactionnofv == 4)   reac = "NC";
      if (reactionnofv == 5)   reac = "anti nu-mu";
      if (reactionnofv == 6)   reac = "nu-e, anti nu-e";
      if (reactionnofv == 999) reac = "other";
      if (reactionnofv == 7)   reac = "out of FV";
      if (reactionnofv == -1)  reac = "no truth";

      std::cout << "Reaction outFV = " << reac << std::endl;

      if (reaction_all_nu == 0)   reac = "CCQE";
      if (reaction_all_nu == 9)   reac = "2p2h";
      if (reaction_all_nu == 1)   reac = "RES";
      if (reaction_all_nu == 2)   reac = "DIS";
      if (reaction_all_nu == 3)   reac = "COH";
      if (reaction_all_nu == 4)   reac = "NC";
      if (reaction_all_nu == 5)   reac = "anti nu-mu";
      if (reaction_all_nu == 6)   reac = "nu-e, anti nu-e";
      if (reaction_all_nu == 999) reac = "other";
      if (reaction_all_nu == 7)   reac = "out of FV";
      if (reaction_all_nu == -1)  reac = "no truth";

      std::cout << "Reaction all Nu = " << reac << std::endl;

      tar = "other";
      if (target == 18)  tar = "Argon";
      if (target == 9)   tar = "Fluorine";
      if (target == 6)   tar = "Carbon";
      if (target == 8)   tar = "Oxygen";
      if (target == 1)   tar = "Hydrogen";
      if (target == 13)  tar = "Aluminium";
      if (target == 999) tar = "other";
      if (target == -1)  tar = "no truth";

      std::cout << "Target code    = " << tar << std::endl;

      if (HMPparticle == 13)   HMPpart = "mu-";
      if (HMPparticle == 11)   HMPpart = "e-";
      if (HMPparticle == -211) HMPpart = "pi-";
      if (HMPparticle == -13)  HMPpart = "mu+";
      if (HMPparticle == -11)  HMPpart = "e+";
      if (HMPparticle == 211)  HMPpart = "pi+";
      if (HMPparticle == 2212) HMPpart = "p";
      if (HMPparticle == 999)  HMPpart = "other";
      if (HMPparticle == -1)   HMPpart = "no truth";

      std::cout << "HMP particle   = " << HMPpart << std::endl;

      if (particle == 13)   part = "mu-";
      if (particle == 11)   part = "e-";
      if (particle == -211) part = "pi-";
      if (particle == -13)  part = "mu+";
      if (particle == -11)  part = "e+";
      if (particle == 211)  part = "pi+";
      if (particle == 2212) part = "p";
      if (particle == 999)  part = "other";
      if (particle == -1)   part = "no truth";

      std::cout << "HMN particle   = " << part << std::endl;
      //std::cout << "Invariant mass = " << Inv_mass[mode] << std::endl;
      ++Nev;
    }
  }
  /*gStyle->SetOptStat(0);
  dpt_dat->Draw("COLZ");
  canva->Print("upi.pdf(");
  dpt_p1->Draw("COLZ");
  canva->Print("upi.pdf");
  dpt_p2->Draw("COLZ");
  canva->Print("upi.pdf");
  dpt_p1p2->Draw("COLZ");
  canva->Print("upi.pdf)");*/

  std::cout << "Total events caught = " << Nev << std::endl;
  std::cout << "End of listing" << std::endl;
}