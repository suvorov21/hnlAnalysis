{ 
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(1,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);



  DrawingTools draw("filename.root");
  DataSample sample("filename.root");


  std::string filename = "PDF/PDF_fileName.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("M_{reco}-M_{true}, MeV");
  draw.SetTitleY("N");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);     

  draw.DumpCuts();
  draw.DumpSteps();
  draw.DumpPOT(sample);
  draw.DrawEventsVSCut(sample, "accum_level[][0]>0");

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  draw.SetOptStat("RM");
  gStyle->SetOptStat(2200);
  draw.SetTitle("NNodes in longest TPC segment 1");  
  draw.SetTitleX("N");
  draw.Draw(sample, "FirstTpcNodes1", 100, 0., 100., "all", "accum_level[][0]>1", "HIST","under over");
  BGcanvas->Print(filenamefirst.c_str());
  draw.SetTitle("Analysing nodes");  
  draw.SetTitleX("N");
  draw.Draw(sample, "ANAnodes", 100, 0., 100., "all", "accum_level[][0]>8", "HIST","under over");
  BGcanvas->Print(filename.c_str());
  draw.SetTitle("NNodes in longest TPC segment (nodes in first TPC)");  
  draw.SetTitleX("N");
  draw.Draw(sample, "MostTpcNodes2:FirstTpcNodes2", 100, 0., 100., 100, 0., 100., "all", "accum_level[][]>1", "HIST","under over");
  BGcanvas->Print(filenamelast.c_str());

 }