{ 
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  DrawingTools draw("AnalysisResults/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_all.root");
  DataSample sample("AnalysisResults/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_all.root");
  
  //DrawingTools draw("AnalysisResults/prod6E_rdp_AnalResults.root");
  //DataSample sample("AnalysisResults/prod6E_rdp_AnalResults.root");
  
  //DrawingTools draw("AnalysisResults/Timing/prod6B_magnet_antineut_AnalResults1.root");
  //DataSample sample("AnalysisResults/Timing/prod6B_magnet_antineut_AnalResults1.root");

  //DrawingTools draw("AnalysisResults/prod6E_rdp_AnalResults_add.root");
  //DataSample sample("AnalysisResults/prod6E_rdp_AnalResults_add.root");

  //DrawingTools draw("AnalysisResults/Timing/prod6B_magnet_genie_AnalResults.root");
  //DataSample sample("AnalysisResults/Timing/prod6B_magnet_genie_AnalResults.root");

  //DrawingTools draw("out.root");
  //DataSample sample("out.root");
  //sample.SetCurrentTree("default");
  //DataSample sample_truth("prod6B_magnet_neut_AnalResults.root");
  //sample_truth.SetCurrentTree("truth");
  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800); 
  draw.DumpPOT(sample);
  draw.Draw(sample.GetTree("truth"), "truelepton_costheta", 50, -1., 1., "topology", "accum_level[0]>0 && truelepton_costheta < 0.1");
  BGcanvas->Print("test.png");

  // mode to draw
  /*Int_t mode = 0;
  std::string cut = "accum_level[][";
  std::stringstream stream;
  stream.str("");
  stream << mode;
  cut += stream.str();
  cut += "]";
  std::string cutLevel = cut + ">9";
  std::cout << cutLevel << std::endl;


  std::string filename = "PDF/BGAnalysisGENIE.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("Invariant mass, MeV");
  draw.SetTitleY("N");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800); 
    
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();
  draw.SetOptStat("e");
  //sample.DumpPOT();
  //draw.DumpSteps();
  int mode = 0;
  std::stringstream stream;
  stream << mode;

  draw.DumpPOT(sample);
  draw.DumpCuts();
  /*draw.SetMinY(0.1);
  draw.SetLogY();
  draw.SetTitle("mu- pi+");*/
  //draw.DrawEventsVSCut(sample, 4, "accum_level[][4]>0");
  //BGcanvas->Print(filenamefirst.c_str());
  /*draw.SetTitle("e- pi+");
  draw.DrawEventsVSCut(sample, 1, "accum_level[][1]>0");
  BGcanvas->Print(filename.c_str());
  draw.SetTitle("mu+ pi-");
  draw.DrawEventsVSCut(sample, 2, "accum_level[][2]>0");
  BGcanvas->Print(filename.c_str());
  draw.SetTitle("e+ pi-");
  draw.DrawEventsVSCut(sample, 3, "accum_level[][3]>0");
  BGcanvas->Print(filenamelast.c_str());*/
  //draw.DumpCategories("AnalusisResults/VETO_OFF/BGanal2.root");
  //draw.ListOptions();
 
  /*draw.SetTitle("Range between 2 tracks");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "Range", 100, -1, 600., "all", "accum_level[][0]>9 && Range != -1", "", "now");
  BGcanvas->Print(filenamefirst.c_str());

  draw.SetTitle("RangeZ between 2 tracks");  
  draw.SetTitleX("Z, mm");
  draw.Draw(sample, "RangeZ", 100, -1, 300., "all", "accum_level[][0]>9 && RangeZ != -1", "", "now");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("VertexChi2");  
  draw.SetTitleX("VertexChi2");
  draw.Draw(sample, "VertexChi2", 100, -1, 300., "all", "accum_level[][0]>9 || accum_level[][2]>9", "", "");
  BGcanvas->Print(filename.c_str());*/

  //draw.SetTitle("Invariant mass, AllCuts success");
  //draw.SetTitleX("Invariant mass, MeV");
  //draw.SetLogY(0);
  //cutLevel = cut + ">9";
  //draw.Draw(sample,"1",100,0.,800.,"all", "accum_level[][4]>7");
  //BGcanvas->Print(filenamelast.c_str());

  //draw.SetTitle("Invariant mass, AllCuts success");
  //draw.SetTitleX("Invariant mass, MeV");
  //draw.SetLogY(0);
  //cutLevel = cut + ">9";
  //draw.Draw(sample,"HNL_inv_mass[0]",100,0.,800.,"all", "accum_level[][1]>9 || accum_level[][3]>9");
  //BGcanvas->Print(filename.c_str());

  /*draw.SetTitle("True vertex time. AllCuts cut");  
  draw.SetTitleX("T");
  draw.Draw(sample, "HNL_true_position[3]", 500, -500., 5000., "all", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Recon vertex time. AllCuts cut");  
  draw.SetTitleX("T");
  draw.Draw(sample, "HNL_position[3]", 500, 2000., 8000., "all", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());
  
  draw.SetTitle("Recon relative angle HMP & HMN, AllCuts cut");
  draw.SetTitleX("cos(#theta)");
  draw.SetMinY(0.1);
  draw.SetLogY();
  draw.Draw(sample, "cos(HNL_constituents_rel_angle)", 200, -1.1, 1.1, "all", cutLevel.c_str());
  draw.DrawCutLineVertical(0., true, "l");
  BGcanvas->Print(filename.c_str());
  
  draw.SetTitle("Recon polar angle of Nu, AllCuts cut");
  draw.Draw(sample, "HNL_direction[2]", 200, -1.1, 1.1, "all", cutLevel.c_str());
  draw.DrawCutLineVertical(0.97, true, "l");
  BGcanvas->Print(filename.c_str());

  draw.SetTitleX("Z, mm");
  draw.SetTitle("Recon Vertex Z position. AllCuts success");
  draw.SetLogY(0);
  draw.Draw(sample, "HNL_position[2]", 200, -3300, 3000., "all", cutLevel.c_str());
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("True Vertex Z position. AllCuts success");
  draw.Draw(sample, "HNL_true_position[2]", 200, -3300, 3000., "all", cutLevel.c_str(), "", "under over");
  draw.DrawCutLineVertical(-724.85);
  draw.DrawCutLineVertical(-71.15);
  draw.DrawCutLineVertical(634.15);
  draw.DrawCutLineVertical(1287.85);
  draw.DrawCutLineVertical(1993.55);
  draw.DrawCutLineVertical(2646.85);
  BGcanvas->Print(filename.c_str());

  draw.SetTitleX("Invariant mass, MeV");
  draw.SetTitle("HMN Parent. AllCuts succes");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "parent", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Reaction. AllCuts succes");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120,100.,1200., "reaction", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("topology. AllCuts succes");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120,100.,1200., "topology", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Reaction. AllCuts succes");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120,100.,1200., "reactionAllNu", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("Reaction outFV. AllCuts succes");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120,100.,1200., "reactionnofv", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitleX("Invariant mass, MeV");
  draw.SetTitle("Target. AllCuts success");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "target", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("HMN Particle. AllCuts cut");
  draw.SetTitleX("Invariant mass, MeV");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "particle", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());
  draw.SetTitle("HMP Particle. AllCuts cut");
  draw.Draw(sample, "HNL_inv_mass[0]", 120, 100., 1200., "HMPparticle", cutLevel.c_str());
  BGcanvas->Print(filename.c_str());
  

  draw.SetTitle("FGD1 max hits. AllCuts success");
  draw.SetTitleX("Hints");
  draw.Draw(sample, "NFGD1VetoHits[0]", 51, -1, 50., "all", cutLevel.c_str(), "", "under over");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("nTPC. AllCuts success");
  draw.SetTitleX("nSameTPCTracks");
  draw.Draw(sample, "nSameTPCTracks", 51, -1, 10., "all", cutLevel.c_str(), "", "under over");
  BGcanvas->Print(filename.c_str());

  draw.SetTitle("FGD2 max hits. AllCuts success");
  draw.Draw(sample, "NFGD2VetoHits[0]", 51, -1, 50., "all", cutLevel.c_str(), "", "under over");
  BGcanvas->Print(filename.c_str());
  
  //draw.SetOptStat("e");
  draw.SetTitle("Energy spectrum of parent. AllCuts success");
  draw.SetTitleX("E, MeV");
  draw.Draw(sample, "nu_trueE", 200, 0., 12000., "all", cutLevel.c_str(), "", "under over");
  BGcanvas->Print(filenamelast.c_str());*/
 }