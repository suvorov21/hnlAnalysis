{
  std::string rootFile = "$bars/AnalysisResults/hnlAnalysis/PileUp/data_run7c.root";
  DrawingTools draw(rootFile.c_str());
  DataSample sample(rootFile.c_str());

 draw.DumpPOT(sample);
 draw.DumpCuts();
 std::cout << rootFile << std::endl;
 //draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][0]>1 || accum_level[][1]>1 || accum_level[][2]>1 || accum_level[][3]>1 || accum_level[][4]>1 || accum_level[][5]>1" );
 std::cout << "TPC1 start" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][3]>1");
 std::cout << "TPC1 veto" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][0]>1");
 std::cout << "TPC1 complete" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][3]>1 || accum_level[][0]>1");

 std::cout << "TPC2 start" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][4]>1");
 std::cout << "TPC2 veto" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][1]>1");
 std::cout << "TPC2 complete" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][4]>1 || accum_level[][1]>1");

 std::cout << "TPC3 start" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][5]>1");
 std::cout << "TPC3 veto" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][2]>1");
 std::cout << "TPC3 complete" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "accum_level[][5]>1 || accum_level[][2]>1");
 /*std::string filename = "PDF/PileUp.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";
 TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

 std::cout << "TPC1 complete" << std::endl;
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "");
 draw.Draw(sample.GetTree("default"), "1", 2, 0., 2., "all", "(accum_level[][3]>1 || accum_level[][0]>1)");
 //std::cout << "run0" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 0", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 0");
 BGcanvas->Print(filename.c_str());
 /*std::cout << "run1" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 1", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 1");
 std::cout << "run2" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 2", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 2");
 std::cout << "run3" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 3", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 3");
 std::cout << "run4" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 4", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 4");
 std::cout << "run5" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 5", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 5");
 std::cout << "run6" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 6", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 6");
 std::cout << "run7" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 7", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 7");
 std::cout << "run8" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 8", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 8");
 std::cout << "run9" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 9", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 9");
 std::cout << "run10" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 10", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 10");
 std::cout << "run11" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 11", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 11");
 std::cout << "run12" << std::endl; draw.DrawRatio(sample.GetTree("default"), "1", 2, 0., 2., "(accum_level[][3]>1 || accum_level[][0]>1) && runPeriod == 12", "(accum_level[][3]>-1 || accum_level[][0]>-1) && runPeriod == 12");*/
}