std::string GetReaction(Double_t reaction) {
  std::string reac = "no truth";
  if (reaction == 0+0.5)        reac = "CCQE";
  else if (reaction == 9+0.5)   reac = "2p2h";
  else if (reaction == 1+0.5)   reac = "RES";
  else if (reaction == 2+0.5)   reac = "DIS";
  else if (reaction == 3+0.5)   reac = "COH";
  else if (reaction == 4+0.5)   reac = "NC";
  else if (reaction == 5+0.5)   reac = "anti nu-mu";
  else if (reaction == 6+0.5)   reac = "nu-e, anti nu-e";
  else if (reaction == 999+0.5) reac = "other";
  else if (reaction == 7+0.5)   reac = "out of FV";
  else if (reaction == -1+0.5)  reac = "no truth";

  return reac;
}

void DumpBG2(){
  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  //std::string micro_path = "/t2k/users/suvorov/AnalysisResults/hnlAnalysis/BG/NEUT/6H/";
  //std::string file_name  = "prod6B_magnet_neut_AnalResults_";

  std::string micro_path = "/t2k/users/suvorov/AnalysisResults/hnlAnalysis/BG/GENIE/6H/";
  std::string file_name  = "prod6B_magnet_genie_AnalResults_";


  Experiment exp("nd280");

  SampleGroup neut_1("neut_1");
  SampleGroup neut_2("neut_2");
  SampleGroup neut_3("neut_3");
  SampleGroup neut_4("neut_4");
  SampleGroup neut_5("neut_5");
  //SampleGroup neut_6("neut_6");

  DataSample* neut_1_ds  = new DataSample((micro_path + file_name + "1.root").c_str());
  DataSample* neut_2_ds  = new DataSample((micro_path + file_name + "2.root").c_str());
  DataSample* neut_3_ds  = new DataSample((micro_path + file_name + "3.root").c_str());
  DataSample* neut_4_ds  = new DataSample((micro_path + file_name + "4.root").c_str());
  DataSample* neut_5_ds  = new DataSample((micro_path + file_name + "5.root").c_str());
  //DataSample* neut_6_ds  = new DataSample((micro_path + file_name + "6.root").c_str());

  neut_1.AddMCSample("magnet", neut_1_ds);
  neut_2.AddMCSample("magnet", neut_2_ds);
  neut_3.AddMCSample("magnet", neut_3_ds);
  neut_4.AddMCSample("magnet", neut_4_ds);
  neut_5.AddMCSample("magnet", neut_5_ds);
  //neut_6.AddMCSample("magnet", neut_6_ds);

  exp.AddSampleGroup("neut_1",  neut_1);
  exp.AddSampleGroup("neut_2",  neut_2);
  exp.AddSampleGroup("neut_3",  neut_3);
  exp.AddSampleGroup("neut_4",  neut_4);
  exp.AddSampleGroup("neut_5",  neut_5);
  //exp.AddSampleGroup("neut_6",  neut_6);

  DrawingTools drawBG(exp, false);
  SetT2KStyle();
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  // normalization for prod6B neut
  //Float_t norm = 1/6.498769;
  // normalization for neut prod6H
  Float_t norm = 1E21;

  std::string var1, var2, var3;
  var1 = var2 = var3 = "1";

  Int_t nBins = 4;
  Float_t start = 0.;
  Float_t end = 4.;

  drawBG.DumpPOT(exp, "neut_1");
  drawBG.DumpPOT(exp, "neut_2");
  drawBG.DumpPOT(exp, "neut_3");
  drawBG.DumpPOT(exp, "neut_4");
  drawBG.DumpPOT(exp, "neut_5");
  //drawBG.DumpPOT(exp, "neut_6");

  exp.GetOverallPOTRatio();

  std::string cut1 = "(accum_level[][0]>9 || accum_level[][2]>9) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 3.7";
  std::string cut2 = "(accum_level[][1]>9 || accum_level[][3]>9)  && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 8.";
  std::string cut3 = "(accum_level[][4]>8) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 17.";

  std::cout << "drawing NEUT" << std::endl;
  drawBG.Draw(exp,var1, nBins, start, end,"all", cut1, "histo", "");

  //std::cout << "drawing GENIE" << std::endl;
  //drawBG.Draw(exp,var1, nBins, start, end,"all", cut3, "histo", "POTNORM", norm);
  //drawBG.CompareSampleGroups(exp,"magnet", var1, nBins, start, end,"all", cut3, "histo", "POTNORM", norm);







  /*std::string filename = "PDF/BGAnalysis.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  std::string cut1 = "(accum_level[][0]>9 || accum_level[][2]>9) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 3.7";
  std::string cut2 = "(accum_level[][1]>9 || accum_level[][3]>9)  && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 8.";
  std::string cut3 = "(accum_level[][4]>8) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 17.";

  //std::string cut1 = "(accum_level[][0]>1 || accum_level[][2]>1)";
  //std::string cut2 = "(accum_level[][1]>1 || accum_level[][3]>1)";

  /*std::string var1 = "HNL_inv_mass[0]";
  std::string var2 = "HNL_inv_mass[2]";
  std::string var3 = "1";*/

  //std::string var1, var2, var3;
  //var1 = var2 = var3 = "reaction";

  /*var1 = var2 = var3 = "1";

  Int_t nBins = 4;
  Float_t start = 0.;
  Float_t end = 4.;

  /*Int_t nBins = 72;
  Float_t start = -2.;
  Float_t end = 70.;*/

  /*DataSample BGsample_NEUT_1("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");
  DataSample BGsample_NEUT_2("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_2.root");
  DataSample BGsample_NEUT_3("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_3.root");
  DataSample BGsample_NEUT_4("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_4.root");
  DataSample BGsample_NEUT_5("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root");
  DataSample BGsample_NEUT_6("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_6.root");

  drawBG.SetTitle("Invariant mass");
  drawBG.SetTitleX("M, MeV");
  drawBG.Draw(BGsample_NEUT_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NEUT_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_3,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_MuPi->Scale(1/6.5);
  NEUT_MuPi->SetTitle("MuPi Neut");
  NEUT_MuPi->Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());

  drawBG.Draw(BGsample_NEUT_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NEUT_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_3,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_ElePi->Scale(1/6.5);
  NEUT_ElePi->SetTitle("ElePi Neut");
  NEUT_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_NEUT_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NEUT_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_3,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_DiMuon->Scale(1/6.5);
  NEUT_DiMuon->SetTitle("DiMu Neut");
  NEUT_DiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  /*DataSample BGsample_NuWro_1("AnalysisResults/BG/NuWro/NuWro_tpc1.root");
  DataSample BGsample_NuWro_2("AnalysisResults/BG/NuWro/NuWro_tpc2.root");
  DataSample BGsample_NuWro_3("AnalysisResults/BG/NuWro/NuWro_tpc3.root");

  drawBG.Draw(BGsample_NuWro_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro1_MuPi(drawBG.GetLastHisto());
  NuWro1_MuPi->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro2_MuPi(drawBG.GetLastHisto());
  NuWro2_MuPi->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro3_MuPi(drawBG.GetLastHisto());
  NuWro3_MuPi->Scale(1/46.87725);

  TH1D* NuWroMuPi(NuWro1_MuPi);
  NuWroMuPi->Add(NuWro2_MuPi);
  NuWroMuPi->Add(NuWro3_MuPi);
  NuWroMuPi->SetTitle("MuPi nuWro");
  NuWroMuPi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_NuWro_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro1_ElePi(drawBG.GetLastHisto());
  NuWro1_ElePi->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro2_ElePi(drawBG.GetLastHisto());
  NuWro2_ElePi->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro3_ElePi(drawBG.GetLastHisto());
  NuWro3_ElePi->Scale(1/46.87725);

  TH1D* NuWroElePi(NuWro1_ElePi);
  NuWroElePi->Add(NuWro2_ElePi);
  NuWroElePi->Add(NuWro3_ElePi);
  NuWroElePi->SetTitle("ElePi nuWro");
  NuWroElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_NuWro_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro1_DiMuon(drawBG.GetLastHisto());
  NuWro1_DiMuon->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro2_DiMuon(drawBG.GetLastHisto());
  NuWro2_DiMuon->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro3_DiMuon(drawBG.GetLastHisto());
  NuWro3_DiMuon->Scale(1/46.87725);

  TH1D* NuWroDiMuon(NuWro1_DiMuon);
  NuWroDiMuon->Add(NuWro2_DiMuon);
  NuWroDiMuon->Add(NuWro3_DiMuon);
  NuWroDiMuon->SetTitle("DiMu nuWro");
  NuWroDiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  DataSample BGsample_GENIE_1("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_1.root");
  DataSample BGsample_GENIE_2("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_2.root");
  DataSample BGsample_GENIE_3("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_3.root");
  DataSample BGsample_GENIE_4("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_4.root");
  DataSample BGsample_GENIE_5("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_5.root");

  drawBG.Draw(BGsample_GENIE_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* GENIE_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_MuPi->Scale(1./5.);
  GENIE_MuPi->SetTitle("MuPi Genie");
  GENIE_MuPi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_GENIE_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* GENIE_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_ElePi->Scale(1./5.);
  GENIE_ElePi->SetTitle("ElePi Genie");
  GENIE_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_GENIE_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* GENIE_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_DiMuon->Scale(1./5.);
  GENIE_DiMuon->SetTitle("DiMu Genie");
  GENIE_DiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  DataSample BGsample_ANEUT_1("AnalysisResults/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_1.root");
  DataSample BGsample_ANEUT_2("AnalysisResults/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_2.root");
  DataSample BGsample_ANEUT_3("AnalysisResults/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_3.root");
  DataSample BGsample_ANEUT_4("AnalysisResults/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_4.root");

  drawBG.Draw(BGsample_ANEUT_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* ANEUT_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_MuPi->Scale(1/5.794);
  ANEUT_MuPi->SetTitle("MuPi ANEUT");
  ANEUT_MuPi->Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());

  drawBG.Draw(BGsample_ANEUT_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* ANEUT_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_ElePi->Scale(1/5.794);
  ANEUT_ElePi->SetTitle("ElePi ANEUT");
  ANEUT_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_ANEUT_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* ANEUT_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_DiMuon->Scale(1/5.794);
  ANEUT_DiMuon->SetTitle("DiMu ANEUT");
  ANEUT_DiMuon->Draw("histo");
  BGcanvas->Print(filenamelast.c_str());

  /*DataSample BGsample_DATA("AnalysisResults/test.root");
  
  drawBG.Draw(BGsample_DATA,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* DATA_MuPi(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_MuPi.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_DATA,var1, nBins, start, end,"all", cut2, "histo", "");
  TH1D* DATA_ElePi(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_ElePi.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_DATA,var1, nBins, start, end,"all", cut3, "histo", "");
  TH1D* DATA_DiMuon(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_DiMuon.Draw("histo");
  BGcanvas->Print(filenamelast.c_str());*/

  /*std::cout << "--------------------------------------------------------------" << std::endl;
  std::cout << "----------- NEUT statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_NEUT_1);
  drawBG.DumpPOT(BGsample_NEUT_2);
  drawBG.DumpPOT(BGsample_NEUT_3);
  drawBG.DumpPOT(BGsample_NEUT_4);
  drawBG.DumpPOT(BGsample_NEUT_5);
  drawBG.DumpPOT(BGsample_NEUT_6);
  std::cout << "----------- GENIE statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_GENIE_1);
  drawBG.DumpPOT(BGsample_GENIE_2);
  drawBG.DumpPOT(BGsample_GENIE_3);
  drawBG.DumpPOT(BGsample_GENIE_4);
  drawBG.DumpPOT(BGsample_GENIE_5);
  std::cout << "----------- ANEUT statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_ANEUT_1);
  drawBG.DumpPOT(BGsample_ANEUT_2);
  drawBG.DumpPOT(BGsample_ANEUT_3);
  drawBG.DumpPOT(BGsample_ANEUT_4);*/
  //std::cout << "----------- DATA statistics-----------" << std::endl;
  //drawBG.DumpPOT(BGsample_DATA);

  /*std::cout << "----------------------------------------------------------" << std::endl;
  std::cout << "---- NEUT generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NEUT_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NEUT_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NEUT_DiMuon->Integral() << std::endl;
  /*std::cout << "---- GENIE generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << GENIE_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << GENIE_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << GENIE_DiMuon->Integral() << std::endl;
  std::cout << "---- ANEUT generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << ANEUT_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << ANEUT_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << ANEUT_DiMuon->Integral() << std::endl;
  std::cout << "---- NuWro generator -------" << std::endl;
  std::cout << "N -> mu pi :    " << NuWroMuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWroElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWroDiMuon->Integral() << std::endl;
  std::cout << "---- TPC 1 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro1_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro1_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro1_DiMuon->Integral() << std::endl;
  std::cout << "---- TPC 2 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro2_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro2_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro2_DiMuon->Integral() << std::endl;
  std::cout << "---- TPC 3 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro3_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro3_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro3_DiMuon->Integral() << std::endl;
  std::cout << "----------------------------------------------------------" << std::endl;*/
  /*std::cout << "------- DATA ---------" << std::endl;
  std::cout << "N -> mu pi :    " << DATA_MuPi.Integral() << std::endl;
  std::cout << "N -> e pi :     " << DATA_ElePi.Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << DATA_DiMuon.Integral() << std::endl;*/

  /*std::string reac;
  Double_t reaction;
  std::cout << "----------------------------------------------------------" << std::endl;
  std::cout << "---- NEUT generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_MuPi->GetNbinsX(); ++i) {
    reaction = NEUT_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << NEUT_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_ElePi->GetNbinsX(); ++i) {
    reaction = NEUT_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << NEUT_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_DiMuon->GetNbinsX(); ++i) {
    reaction = NEUT_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << NEUT_DiMuon->Integral() << std::endl;

  /*std::cout << "---- GENIE generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_MuPi->GetNbinsX(); ++i) {
    reaction = GENIE_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << GENIE_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_ElePi->GetNbinsX(); ++i) {
    reaction = GENIE_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << GENIE_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_DiMuon->GetNbinsX(); ++i) {
    reaction = GENIE_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << GENIE_DiMuon->Integral() << std::endl;

  std::cout << "---- NuWro generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= NuWroMuPi->GetNbinsX(); ++i) {
    reaction = NuWroMuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroMuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << NuWroMuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= NuWroElePi->GetNbinsX(); ++i) {
    reaction = NuWroElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << NuWroElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= NuWroDiMuon->GetNbinsX(); ++i) {
    reaction = NuWroDiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroDiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << NuWroDiMuon->Integral() << std::endl;*/



  /*std::cout << "---- ANEUT generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_MuPi->GetNbinsX(); ++i) {
    reaction = ANEUT_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << ANEUT_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_ElePi->GetNbinsX(); ++i) {
    reaction = ANEUT_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << ANEUT_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_DiMuon->GetNbinsX(); ++i) {
    reaction = ANEUT_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << ANEUT_DiMuon->Integral() << std::endl;*/
}

TStyle* SetT2KStyle(Int_t WhichStyle = 1, TString styleName = "T2K") {
  TStyle *t2kStyle= new TStyle(styleName, "T2K approved plots style");
  
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper

  Int_t FontStyle = 22;
  Float_t FontSizeLabel = 0.035;
  Float_t FontSizeTitle = 0.05;
  Float_t YOffsetTitle = 1.3;
 
  switch(WhichStyle) {
  case 1:
    FontStyle = 42;
    FontSizeLabel = 0.05;
    FontSizeTitle = 0.065;
    YOffsetTitle = 1.19;
    break;
  case 2:
    FontStyle = 42;
    FontSizeLabel = 0.035;
    FontSizeTitle = 0.05;
    YOffsetTitle = 1.6;
    break;
  case 3:
    FontStyle = 132;
    FontSizeLabel = 0.035;
    FontSizeTitle = 0.05;
    YOffsetTitle = 1.6;
    break;
  }

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetCanvasBorderSize(0);
  t2kStyle->SetFrameBorderSize(0);
  t2kStyle->SetDrawBorder(0);
  t2kStyle->SetTitleBorderSize(0);

  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetFillColor(0);

  t2kStyle->SetEndErrorSize(4);
  t2kStyle->SetStripDecimals(kFALSE);

  t2kStyle->SetLegendBorderSize(1);
  t2kStyle->SetLegendFont(FontStyle);

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20, 26);
  t2kStyle->SetPadTopMargin(0.1);
  t2kStyle->SetPadBottomMargin(0.17);
  t2kStyle->SetPadRightMargin(0.13); // 0.075 -> 0.13 for colz option
  t2kStyle->SetPadLeftMargin(0.16);//to include both large/small font options
  
  // Fonts, sizes, offsets
  t2kStyle->SetTextFont(FontStyle);
  t2kStyle->SetTextSize(0.08);

  t2kStyle->SetLabelFont(FontStyle, "x");
  t2kStyle->SetLabelFont(FontStyle, "y");
  t2kStyle->SetLabelFont(FontStyle, "z");
  t2kStyle->SetLabelFont(FontStyle, "t");
  t2kStyle->SetLabelSize(FontSizeLabel, "x");
  t2kStyle->SetLabelSize(FontSizeLabel, "y");
  t2kStyle->SetLabelSize(FontSizeLabel, "z");
  t2kStyle->SetLabelOffset(0.015, "x");
  t2kStyle->SetLabelOffset(0.015, "y");
  t2kStyle->SetLabelOffset(0.015, "z");

  t2kStyle->SetTitleFont(FontStyle, "x");
  t2kStyle->SetTitleFont(FontStyle, "y");
  t2kStyle->SetTitleFont(FontStyle, "z");
  t2kStyle->SetTitleFont(FontStyle, "t");
  t2kStyle->SetTitleSize(FontSizeTitle, "y");
  t2kStyle->SetTitleSize(FontSizeTitle, "x");
  t2kStyle->SetTitleSize(FontSizeTitle, "z");
  t2kStyle->SetTitleOffset(1.14, "x");
  t2kStyle->SetTitleOffset(YOffsetTitle, "y");
  t2kStyle->SetTitleOffset(1.2, "z");

  t2kStyle->SetTitleStyle(0);
  t2kStyle->SetTitleFontSize(0.06);//0.08
  t2kStyle->SetTitleFont(FontStyle, "pad");
  t2kStyle->SetTitleBorderSize(0);
  t2kStyle->SetTitleX(0.1f);
  t2kStyle->SetTitleW(0.8f);

  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth( Width_t(2.5) );
  t2kStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes
  
  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);
  
  // do not display any of the standard histogram decorations
  t2kStyle->SetOptTitle(1);
  t2kStyle->SetOptStat(1);
  t2kStyle->SetOptFit(0);
  
  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);
  
  // -- color --
  // functions blue
  t2kStyle->SetFuncColor(600-4);

  t2kStyle->SetFillColor(1); // make color fillings (not white)
  // - color setup for 2D -
  // - "cold"/ blue-ish -
  Double_t red[]   = { 0.00, 0.00, 0.00 };
  Double_t green[] = { 1.00, 0.00, 0.00 };
  Double_t blue[]  = { 1.00, 1.00, 0.25 };
  // - "warm" red-ish colors -
  //  Double_t red[]   = {1.00, 1.00, 0.25 };
  //  Double_t green[] = {1.00, 0.00, 0.00 };
  //  Double_t blue[]  = {0.00, 0.00, 0.00 };

  Double_t stops[] = { 0.25, 0.75, 1.00 };
  const Int_t NRGBs = 3;
  const Int_t NCont = 500;

  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  t2kStyle->SetNumberContours(NCont);

  // - Rainbow -
  //  t2kStyle->SetPalette(1);  // use the rainbow color set

  // -- axis --
  t2kStyle->SetStripDecimals(kFALSE); // don't do 1.0 -> 1
  //  TGaxis::SetMaxDigits(3); // doesn't have an effect 
  // no supressed zeroes!
  t2kStyle->SetHistMinimumZero(kTRUE);


 return(t2kStyle);
}


void CenterHistoTitles(TH1 *thisHisto){
  thisHisto->GetXaxis()->CenterTitle();
  thisHisto->GetYaxis()->CenterTitle();
  thisHisto->GetZaxis()->CenterTitle();
}


int AddGridLinesToPad(TPad *thisPad) {
  thisPad->SetGridx();
  thisPad->SetGridy();
  return(0);
}




