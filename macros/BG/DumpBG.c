std::string GetReaction(Double_t reaction) {
  std::string reac = "no truth";
  if (reaction == 0+0.5)        reac = "CCQE";
  else if (reaction == 9+0.5)   reac = "2p2h";
  else if (reaction == 1+0.5)   reac = "RES";
  else if (reaction == 2+0.5)   reac = "DIS";
  else if (reaction == 3+0.5)   reac = "COH";
  else if (reaction == 4+0.5)   reac = "NC";
  else if (reaction == 5+0.5)   reac = "anti nu-mu";
  else if (reaction == 6+0.5)   reac = "nu-e, anti nu-e";
  else if (reaction == 999+0.5) reac = "other";
  else if (reaction == 7+0.5)   reac = "out of FV";
  else if (reaction == -1+0.5)  reac = "no truth";

  return reac;
}

void DumpBG(){
  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
	DrawingTools drawBG("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run8w.root", true);

  std::string filename = "PDF/BGAnalysis.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  std::string cut1 = "(accum_level[][0]>9 || accum_level[][2]>9) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 3.7";
  std::string cut2 = "(accum_level[][1]>9 || accum_level[][3]>9)  && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 8.";
  std::string cut3 = "(accum_level[][4]>8) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 17.";

  //std::string cut1 = "(accum_level[][0]>1 || accum_level[][2]>1)";
  //std::string cut2 = "(accum_level[][1]>1 || accum_level[][3]>1)";

  /*std::string var1 = "HNL_inv_mass[0]";
  std::string var2 = "HNL_inv_mass[2]";
  std::string var3 = "1";*/

  std::string var1, var2, var3;
  //var1 = var2 = var3 = "reaction";

  var1 = var2 = var3 = "1";

  Int_t nBins = 4;
  Float_t start = 0.;
  Float_t end = 4.;

  /*Int_t nBins = 72;
  Float_t start = -2.;
  Float_t end = 70.;*/


  DataSample BGsample_NEUT_1("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_2a.root");
  DataSample BGsample_NEUT_2("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_2w.root");
  DataSample BGsample_NEUT_3("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_3b.root");
  DataSample BGsample_NEUT_4("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_3c.root");
  DataSample BGsample_NEUT_5("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_4a.root");
  DataSample BGsample_NEUT_6("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_4w.root");
  DataSample BGsample_NEUT_7("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_5c.root");
  DataSample BGsample_NEUT_8("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_6b.root ");
  DataSample BGsample_NEUT_9("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_6c.root ");
  DataSample BGsample_NEUT_10("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_6d.root ");
  DataSample BGsample_NEUT_11("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_7b.root ");
  DataSample BGsample_NEUT_12("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_8a.root ");
  DataSample BGsample_NEUT_13("AnalysisResults/hnlAnalysis/BG/NEUT/v2/prod6B_magnet_neut_AnalResults_8w.root ");

  drawBG.SetTitle("Invariant mass");
  drawBG.SetTitleX("M, MeV");
  drawBG.Draw(BGsample_NEUT_7,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NEUT_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_3,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_7,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_9,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_10,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_11,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_12,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_13,var1, nBins, start, end,"all", cut1, "histo", "");
  NEUT_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_MuPi->Scale(1/20.936);

  NEUT_MuPi->SetTitle("MuPi Neut");
  NEUT_MuPi->Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());

  drawBG.Draw(BGsample_NEUT_7,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NEUT_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  NEUT_ElePi->Scale(1/6.287);
  /*
  drawBG.Draw(BGsample_NEUT_3,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_7,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_9,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_10,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_10,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
   drawBG.Draw(BGsample_NEUT_11,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
   drawBG.Draw(BGsample_NEUT_12,var2, nBins, start, end,"all", cut2, "histo", "");
  NEUT_ElePi->Add(drawBG.GetLastHisto());
   drawBG.Draw(BGsample_NEUT_13,var2, nBins, start, end,"all", cut2, "histo", "");

  NEUT_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_ElePi->Scale(1/20.936);
  NEUT_ElePi->SetTitle("ElePi Neut");
  NEUT_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());
*/
  drawBG.Draw(BGsample_NEUT_7,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NEUT_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  NEUT_DiMuon->Scale(1/6.287);
  /*drawBG.Draw(BGsample_NEUT_3,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_4,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_5,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_6,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_7,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_8,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_9,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_10,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_11,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_12,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_13,var3, nBins, start, end,"all", cut3, "histo", "");
  NEUT_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  NEUT_DiMuon->Scale(1/20.936);
  NEUT_DiMuon->SetTitle("DiMu Neut");
  NEUT_DiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  /*DataSample BGsample_NuWro_1("AnalysisResults/hnlAnalysis/BG/NuWro/NuWro_tpc1.root");
  DataSample BGsample_NuWro_2("AnalysisResults/hnlAnalysis/BG/NuWro/NuWro_tpc2.root");
  DataSample BGsample_NuWro_3("AnalysisResults/hnlAnalysis/BG/NuWro/NuWro_tpc3.root");

  drawBG.Draw(BGsample_NuWro_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro1_MuPi(drawBG.GetLastHisto());
  NuWro1_MuPi->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro2_MuPi(drawBG.GetLastHisto());
  NuWro2_MuPi->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* NuWro3_MuPi(drawBG.GetLastHisto());
  NuWro3_MuPi->Scale(1/46.87725);

  TH1D* NuWroMuPi(NuWro1_MuPi);
  NuWroMuPi->Add(NuWro2_MuPi);
  NuWroMuPi->Add(NuWro3_MuPi);
  NuWroMuPi->SetTitle("MuPi nuWro");
  NuWroMuPi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_NuWro_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro1_ElePi(drawBG.GetLastHisto());
  NuWro1_ElePi->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro2_ElePi(drawBG.GetLastHisto());
  NuWro2_ElePi->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* NuWro3_ElePi(drawBG.GetLastHisto());
  NuWro3_ElePi->Scale(1/46.87725);

  TH1D* NuWroElePi(NuWro1_ElePi);
  NuWroElePi->Add(NuWro2_ElePi);
  NuWroElePi->Add(NuWro3_ElePi);
  NuWroElePi->SetTitle("ElePi nuWro");
  NuWroElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_NuWro_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro1_DiMuon(drawBG.GetLastHisto());
  NuWro1_DiMuon->Scale(1/47.0191);
  drawBG.Draw(BGsample_NuWro_2,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro2_DiMuon(drawBG.GetLastHisto());
  NuWro2_DiMuon->Scale(1/47.64396);
  drawBG.Draw(BGsample_NuWro_3,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* NuWro3_DiMuon(drawBG.GetLastHisto());
  NuWro3_DiMuon->Scale(1/46.87725);

  TH1D* NuWroDiMuon(NuWro1_DiMuon);
  NuWroDiMuon->Add(NuWro2_DiMuon);
  NuWroDiMuon->Add(NuWro3_DiMuon);
  NuWroDiMuon->SetTitle("DiMu nuWro");
  NuWroDiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  DataSample BGsample_GENIE_1("AnalysisResults/hnlAnalysis/BG/GENIE/new/prod6B_magnet_genie_AnalResults_1.root");
  DataSample BGsample_GENIE_2("AnalysisResults/hnlAnalysis/BG/GENIE/new/prod6B_magnet_genie_AnalResults_2.root");
  DataSample BGsample_GENIE_3("AnalysisResults/hnlAnalysis/BG/GENIE/new/prod6B_magnet_genie_AnalResults_3.root");
  DataSample BGsample_GENIE_4("AnalysisResults/hnlAnalysis/BG/GENIE/new/prod6B_magnet_genie_AnalResults_4.root");
  DataSample BGsample_GENIE_5("AnalysisResults/hnlAnalysis/BG/GENIE/new/prod6B_magnet_genie_AnalResults_5.root");

  drawBG.Draw(BGsample_GENIE_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* GENIE_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var1, nBins, start, end,"all", cut1, "histo", "");
  GENIE_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_MuPi->Scale(1./5.);
  GENIE_MuPi->SetTitle("MuPi Genie");
  GENIE_MuPi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_GENIE_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* GENIE_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var2, nBins, start, end,"all", cut2, "histo", "");
  GENIE_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_ElePi->Scale(1./5.);
  GENIE_ElePi->SetTitle("ElePi Genie");
  GENIE_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_GENIE_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* GENIE_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_2,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_3,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_4,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_GENIE_5,var3, nBins, start, end,"all", cut3, "histo", "");
  GENIE_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  GENIE_DiMuon->Scale(1./5.);
  GENIE_DiMuon->SetTitle("DiMu Genie");
  GENIE_DiMuon->Draw("histo");
  BGcanvas->Print(filename.c_str());

  DataSample BGsample_ANEUT_1("AnalysisResults/hnlAnalysis/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_1.root");
  DataSample BGsample_ANEUT_2("AnalysisResults/hnlAnalysis/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_2.root");
  DataSample BGsample_ANEUT_3("AnalysisResults/hnlAnalysis/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_3.root");
  DataSample BGsample_ANEUT_4("AnalysisResults/hnlAnalysis/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_4.root");
  DataSample BGsample_ANEUT_5("AnalysisResults/hnlAnalysis/BG/ANEUT/v2/prod6B_magnet_antineut_AnalResults_7b.root");


  drawBG.Draw(BGsample_ANEUT_1,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* ANEUT_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_5,var1, nBins, start, end,"all", cut1, "histo", "");
  ANEUT_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_MuPi->Scale(1/8.9947);
  ANEUT_MuPi->SetTitle("MuPi ANEUT");
  ANEUT_MuPi->Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());

  drawBG.Draw(BGsample_ANEUT_1,var2, nBins, start, end,"all", cut2, "histo", "");
  TH1D* ANEUT_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_5,var2, nBins, start, end,"all", cut2, "histo", "");
  ANEUT_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_ElePi->Scale(1/8.9947);
  ANEUT_ElePi->SetTitle("ElePi ANEUT");
  ANEUT_ElePi->Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_ANEUT_1,var3, nBins, start, end,"all", cut3, "histo", "");
  TH1D* ANEUT_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_2,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_3,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_4,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_ANEUT_5,var3, nBins, start, end,"all", cut3, "histo", "");
  ANEUT_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  ANEUT_DiMuon->Scale(1/8.9947);
  ANEUT_DiMuon->SetTitle("DiMu ANEUT");
  ANEUT_DiMuon->Draw("histo");
  BGcanvas->Print(filenamelast.c_str());*/

  /*DataSample BGsample_DATA_1w("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run1w.root");
  DataSample BGsample_DATA_2a("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run2a.root");
  DataSample BGsample_DATA_2w("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run2w.root");
  DataSample BGsample_DATA_3b("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run3b.root");
  DataSample BGsample_DATA_3c("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run3c.root");
  DataSample BGsample_DATA_4a("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run4a.root");
  DataSample BGsample_DATA_4w("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run4w.root");
  DataSample BGsample_DATA_5w("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run5w.root");
  DataSample BGsample_DATA_5wAnu("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run5wAnu.root");
  DataSample BGsample_DATA_6a("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run6a.root");
  DataSample BGsample_DATA_6b("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run6b.root");
  DataSample BGsample_DATA_6c("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run6c.root");
  DataSample BGsample_DATA_6d("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run6d.root");
  DataSample BGsample_DATA_6e("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run6e.root");
  DataSample BGsample_DATA_7b("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run7b.root");
  DataSample BGsample_DATA_7c("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run7c.root");
  DataSample BGsample_DATA_8a("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run8aa_v2.root");
  DataSample BGsample_DATA_8w("/t2k/users/suvorov/AnalysisResults/hnlAnalysis/data/run8ww_v2.root");

  drawBG.Draw(BGsample_DATA_1w,var1, nBins, start, end,"all", cut1, "histo", "");
  TH1D* DATA_MuPi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2a,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2w,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3b,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3c,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4a,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4w,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5w,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5wAnu,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6a,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6b,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6c,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6d,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6e,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7b,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7c,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8a,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8w,var1, nBins, start, end,"all", cut1, "histo", "");
  DATA_MuPi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_MuPi.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_DATA_1w,var1, nBins, start, end,"all", cut2, "histo", "");
  TH1D* DATA_ElePi(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2a,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2w,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3b,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3c,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4a,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4w,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5w,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5wAnu,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6a,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6b,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6c,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6d,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6e,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7b,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7c,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8a,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8w,var1, nBins, start, end,"all", cut2, "histo", "");
  DATA_ElePi->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_ElePi.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.Draw(BGsample_DATA_1w,var1, nBins, start, end,"all", cut3, "histo", "");
  TH1D* DATA_DiMuon(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2w,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_2a,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3b,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_3c,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4a,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_4w,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5w,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_5wAnu,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6a,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6b,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6c,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6d,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_6e,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7b,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_7c,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8a,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_DATA_8w,var1, nBins, start, end,"all", cut3, "histo", "");
  DATA_DiMuon->Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  DATA_DiMuon.Draw("histo");
  BGcanvas->Print(filenamelast.c_str());
*/
  std::cout << "--------------------------------------------------------------" << std::endl;
  std::cout << "----------- NEUT statistics-----------" << std::endl;
  //drawBG.DumpPOT(BGsample_NEUT_1);
  //drawBG.DumpPOT(BGsample_NEUT_2);
  //drawBG.DumpPOT(BGsample_NEUT_3);
  //drawBG.DumpPOT(BGsample_NEUT_4);
  //drawBG.DumpPOT(BGsample_NEUT_5);
  //drawBG.DumpPOT(BGsample_NEUT_6);
  drawBG.DumpPOT(BGsample_NEUT_7);
  drawBG.DumpPOT(BGsample_NEUT_8);
  drawBG.DumpPOT(BGsample_NEUT_9);
  drawBG.DumpPOT(BGsample_NEUT_10);
  drawBG.DumpPOT(BGsample_NEUT_11);
  drawBG.DumpPOT(BGsample_NEUT_12);
  drawBG.DumpPOT(BGsample_NEUT_13);
  /*std::cout << "----------- GENIE statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_GENIE_1);
  drawBG.DumpPOT(BGsample_GENIE_2);
  drawBG.DumpPOT(BGsample_GENIE_3);
  drawBG.DumpPOT(BGsample_GENIE_4);
  drawBG.DumpPOT(BGsample_GENIE_5);

  std::cout << "----------- ANEUT statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_ANEUT_1);
  drawBG.DumpPOT(BGsample_ANEUT_2);
  drawBG.DumpPOT(BGsample_ANEUT_3);
  drawBG.DumpPOT(BGsample_ANEUT_4);
  drawBG.DumpPOT(BGsample_ANEUT_5);*/
  /*std::cout << "----------- DATA statistics-----------" << std::endl;
  drawBG.DumpPOT(BGsample_DATA_1w);
  drawBG.DumpPOT(BGsample_DATA_2a);
  drawBG.DumpPOT(BGsample_DATA_2w);
  drawBG.DumpPOT(BGsample_DATA_3b);
  drawBG.DumpPOT(BGsample_DATA_3c);
  drawBG.DumpPOT(BGsample_DATA_4a);
  drawBG.DumpPOT(BGsample_DATA_4w);
  drawBG.DumpPOT(BGsample_DATA_5w);
  drawBG.DumpPOT(BGsample_DATA_5wAnu);
  drawBG.DumpPOT(BGsample_DATA_6a);
  drawBG.DumpPOT(BGsample_DATA_6b);
  drawBG.DumpPOT(BGsample_DATA_6c);
  drawBG.DumpPOT(BGsample_DATA_6d);
  drawBG.DumpPOT(BGsample_DATA_6e);
  drawBG.DumpPOT(BGsample_DATA_7b);
  drawBG.DumpPOT(BGsample_DATA_7c);
  drawBG.DumpPOT(BGsample_DATA_8a);
  drawBG.DumpPOT(BGsample_DATA_8w);*/

  std::cout << "----------------------------------------------------------" << std::endl;
  std::cout << "---- NEUT generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NEUT_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NEUT_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NEUT_DiMuon->Integral() << std::endl;
  /*std::cout << "---- GENIE generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << GENIE_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << GENIE_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << GENIE_DiMuon->Integral() << std::endl;
  std::cout << "---- ANEUT generator -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << ANEUT_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << ANEUT_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << ANEUT_DiMuon->Integral() << std::endl;
  std::cout << "---- NuWro generator -------" << std::endl;
  std::cout << "N -> mu pi :    " << NuWroMuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWroElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWroDiMuon->Integral() << std::endl;
  std::cout << "---- TPC 1 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro1_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro1_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro1_DiMuon->Integral() << std::endl;
  std::cout << "---- TPC 2 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro2_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro2_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro2_DiMuon->Integral() << std::endl;
  std::cout << "---- TPC 3 -------" << std::endl;
  std::cout << "Event number for 1e21 " << std::endl;
  std::cout << "N -> mu pi :    " << NuWro3_MuPi->Integral() << std::endl;
  std::cout << "N -> e pi :     " << NuWro3_ElePi->Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << NuWro3_DiMuon->Integral() << std::endl;
  std::cout << "----------------------------------------------------------" << std::endl;*/
  /*std::cout << "------- DATA ---------" << std::endl;
  std::cout << "N -> mu pi :    " << DATA_MuPi.Integral() << std::endl;
  std::cout << "N -> e pi :     " << DATA_ElePi.Integral() << std::endl;
  std::cout << "N -> mu mu nu : " << DATA_DiMuon.Integral() << std::endl;

  /*std::string reac;
  Double_t reaction;
  std::cout << "----------------------------------------------------------" << std::endl;
  std::cout << "---- NEUT generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_MuPi->GetNbinsX(); ++i) {
    reaction = NEUT_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << NEUT_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_ElePi->GetNbinsX(); ++i) {
    reaction = NEUT_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << NEUT_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= NEUT_DiMuon->GetNbinsX(); ++i) {
    reaction = NEUT_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NEUT_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << NEUT_DiMuon->Integral() << std::endl;*/

  /*std::cout << "---- GENIE generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_MuPi->GetNbinsX(); ++i) {
    reaction = GENIE_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << GENIE_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_ElePi->GetNbinsX(); ++i) {
    reaction = GENIE_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << GENIE_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= GENIE_DiMuon->GetNbinsX(); ++i) {
    reaction = GENIE_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  GENIE_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << GENIE_DiMuon->Integral() << std::endl;

  std::cout << "---- NuWro generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= NuWroMuPi->GetNbinsX(); ++i) {
    reaction = NuWroMuPi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroMuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << NuWroMuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= NuWroElePi->GetNbinsX(); ++i) {
    reaction = NuWroElePi->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << NuWroElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= NuWroDiMuon->GetNbinsX(); ++i) {
    reaction = NuWroDiMuon->GetBinCenter(i);
    reac = GetReaction(reaction);
    std::cout << reac << "    " <<  NuWroDiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << NuWroDiMuon->Integral() << std::endl;*/



  /*std::cout << "---- ANEUT generator -------" << std::endl;
  std::cout << "---- MuPi -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_MuPi->GetNbinsX(); ++i) {
    reaction = ANEUT_MuPi->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_MuPi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu pi :    " << ANEUT_MuPi->Integral() << std::endl;
  std::cout << "---- ElePi -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_ElePi->GetNbinsX(); ++i) {
    reaction = ANEUT_ElePi->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_ElePi->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> e pi :     " << ANEUT_ElePi->Integral() << std::endl;
  std::cout << "---- DiMuon -------" << std::endl;
  for (Int_t i = 1; i <= ANEUT_DiMuon->GetNbinsX(); ++i) {
    reaction = ANEUT_DiMuon->GetBinCenter(i);
    reac = GetReaction(reaction-50);
    if (reac != "no truth")
      std::cout << reac << "    " <<  ANEUT_DiMuon->GetBinContent(i) << std::endl;;
  }
  std::cout << "N -> mu mu nu : " << ANEUT_DiMuon->Integral() << std::endl;*/
}


