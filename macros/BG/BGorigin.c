{  
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  t2kStyle->SetStatX(0.7);
  t2kStyle->SetStatY(0.1);

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  DrawingTools drawBG("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root", true);

  DataSample BGsample1("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");
  DataSample BGsample2("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_2.root");
  DataSample BGsample3("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_3.root");
  DataSample BGsample4("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_4.root");
  DataSample BGsample5("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root");
  DataSample BGsample6("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_6.root");

  std::string filename = "PDF/BGAnalysisNEUT.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  std::string cut1 = "accum_level[][0]>7 || accum_level[][2]>7";
  std::string var  = "";
  std::string cat  = "";
  Int_t num = 0;
  Float_t min = 0;
  Float_t max = 0;

  drawBG.SetTitle("Reaction");
  drawBG.SetTitleX("cos(#theta)");
  var = "HNL_direction[2]";
  cat = "target";
  num = 100;
  min = -1.;
  max = 1.;

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  drawBG.DumpCategories("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");

  drawBG.Draw(BGsample1, "HNL_direction[2]", 100, -1., 1., "target", "accum_level[][0]>7 || accum_level[][2]>7", "", "");
  //TH1D* histo_target(drawBG.GetLastStackTotal());
  BGcanvas->Print(filenamefirst.c_str());
  drawBG.Draw(BGsample2, var.c_str(), num, min, max, cat.c_str(), cut1.c_str(), "histo", "");
  //histo_target.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample3, var.c_str(), num, min, max, cat.c_str(), cut1.c_str(), "histo", "");
  //histo_target.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample4, var.c_str(), num, min, max, cat.c_str(), cut1.c_str(), "histo", "");
  //histo_target.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample5, var.c_str(), num, min, max, cat.c_str(), cut1.c_str(), "histo", "");
  //histo_target.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample6, var.c_str(), num, min, max, cat.c_str(), cut1.c_str(), "histo", "");
  //histo_target.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());

  /*BGcanvas->Clear();
  histo_target.Scale(1/6.5);
  histo_target.Draw();
  BGcanvas->Print(filenamefirst.c_str());*/

  cat = "reaction";
  drawBG.SetTitle("Reaction");
  drawBG.Draw(BGsample1, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample2, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample3, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample4, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample5, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample6, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());

  cat = "reactionnofv";
  drawBG.SetTitle("Reaction OutFV");
  drawBG.Draw(BGsample1, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample2, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample3, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample4, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample5, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample6, var, num, min, max, cat, cut1, "histo", "");
  BGcanvas->Print(filename.c_str());

  cat = "particle";
  drawBG.SetTitle("Particle");
  drawBG.SetTitleX("cos(#theta)");
  drawBG.Draw(BGsample1, var, num, min, max, cat, cut1, "histo", "");
  //TH1D* BGpolarAngle(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample2, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample3, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample4, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample5, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample6, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastStackTotal());
  BGcanvas->Print(filename.c_str());

  /*BGcanvas->Clear();
  BGpolarAngle.Scale(1/6.5);
  BGpolarAngle.Draw("histo");
  BGcanvas->Print(filenamelast.c_str());*/

  cat = "MMPparticle";
  drawBG.SetTitle("HMP Particle");
  drawBG.Draw(BGsample1, var, num, min, max, cat, cut1, "histo", "");
  //TH1D* BGpolarAngle(drawBG.GetLastHisto());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample2, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastHisto());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample3, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastHisto());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample4, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastHisto());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample5, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastHisto());
  BGcanvas->Print(filename.c_str());
  drawBG.Draw(BGsample6, var, num, min, max, cat, cut1, "histo", "");
  //BGpolarAngle.Add(drawBG.GetLastHisto());
  BGcanvas->Print(filenamelast.c_str());

  /*BGcanvas->Clear();
  BGpolarAngle.Scale(1/6.5);
  BGpolarAngle.Draw("histo");
  BGcanvas->Print(filenamelast.c_str());*/
}