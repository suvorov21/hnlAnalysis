{  
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  t2kStyle->SetStatX(0.7);
  t2kStyle->SetStatY(0.1);

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.075);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  DrawingTools drawBG("", true);

  DataSample BGsample1("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");
  DataSample BGsample2("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_2.root");
  DataSample BGsample3("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_3.root");
  DataSample BGsample4("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_4.root");
  DataSample BGsample5("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root");
  DataSample BGsample6("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_6.root");

  std::string filename = "PDF/BGAnalysisNEUT.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

  //DrawingTools drawSig("AnalysisResults/Signal/SignalElePi.root", true);
  //DataSample sampleSig("AnalysisResults/Signal/SignalElePi.root");

  /*drawBG.SetTitle("HNL candidate polar angle.");
  drawBG.SetTitleX("cos(#theta)");
  std::string cut1 = "accum_level[][0]>7 || accum_level[][2]>7";
  drawBG.Draw(BGsample1,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  TH1D* BGpolarAngle(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  BGpolarAngle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  BGpolarAngle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  BGpolarAngle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  BGpolarAngle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"HNL_direction[2]", 1000, -1., 1.,"all", cut1, "histo", "");
  BGpolarAngle.Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  BGpolarAngle.Scale(1/BGpolarAngle.Integral());
  BGpolarAngle.Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());
  BGpolarAngle.GetXaxis()->SetRangeUser(0.99, 1.);
   

  drawSig.SetLineColor(kRed);
  drawSig.Draw(sampleSig, "HNL_direction[2]", 1000, 0.9, 1., "all", cut1, "histo", "AREA1");
  BGcanvas->Print(filename.c_str());
  TH1D* SigPolarAngle(drawSig.GetLastHisto());
  SigPolarAngle.GetXaxis()->SetRangeUser(0.99, 1.);
  BGcanvas->Clear();
  SigPolarAngle.Draw("histo ");
  BGpolarAngle.Draw("histo same");  

  BGcanvas->Print(filename.c_str());*/


/////////
  /*drawBG.SetTitle("HNL candidate polar angle. BG");
  drawBG.SetTitleX("#theta, degree");
  std::string cut1 = "accum_level[][1]>7 || accum_level[][3]>7";
  drawBG.Draw(BGsample1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180.,"all", cut1, "histo", "");
  TH1D* BGpolarAngleA(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());

  
  BGcanvas->Clear();
  BGpolarAngleA.Scale(1/BGpolarAngleA.Integral());
  BGpolarAngleA.Draw("histo");
  BGcanvas->Print(filenamefirst.c_str());
  BGpolarAngleA.GetXaxis()->SetRangeUser(0., 10.);
   

  drawSig.SetLineColor(kRed);
  drawSig.SetTitle("HNL candidate polar angle. Signal");
  drawSig.Draw(sampleSig, "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 500, 0., 50., "all", cut1, "histo", "AREA1");
  BGcanvas->Print(filename.c_str());
  TH1D* SigPolarAngleA(drawSig.GetLastHisto());
  SigPolarAngleA.GetXaxis()->SetRangeUser(0., 10.);
  BGcanvas->Clear();

  SigPolarAngleA.Draw("histo");
  BGpolarAngleA.Draw("histo same");  

  BGcanvas->Print(filename.c_str());*/

  /*drawBG.SetTitle("HNL direction wrt Y axis. BG");
  drawBG.SetTitleX("Y, degree");

  std::string cut2 = "(accum_level[][0]>7 || accum_level[][2]>7) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 10";
  drawBG.Draw(BGsample1,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  TH1D* Yhisto(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Yhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Yhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Yhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Yhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Yhisto.Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  Yhisto.Scale(1/6.5);
  Yhisto.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawBG.SetTitle("HNL direction wrt X axis. BG");
  drawBG.SetTitleX("X, degree");

  drawBG.Draw(BGsample1,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  TH1D* Xhisto(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Xhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Xhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Xhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Xhisto.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "");
  Xhisto.Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  Xhisto.Scale(1/6.5);
  Xhisto.Draw("histo");
  BGcanvas->Print(filename.c_str());

  drawSig.SetOptStat("iRM");
  //drawSig.SetOptStat(2210);

  drawSig.SetTitle("HNL direction wrt Y");
  drawSig.SetTitleX("Y, degree");
  drawSig.Draw(sampleSig,"TMath::ASin(HNL_direction[1])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "AREA1");
  TPaveStats *st2 = (TPaveStats*)BGcanvas->GetPrimitive("stats"); 
  st2->SetX1NDC(0.6); 
  st2->SetX2NDC(0.9); 
  st2->SetY1NDC(0.3); 
  st2->SetY2NDC(0.6);
  BGcanvas->Print(filename.c_str());

  drawSig.SetTitle("HNL direction wrt X");
  drawSig.SetTitleX("X, degree");
  drawSig.Draw(sampleSig,"TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 100, -10., 10.,"all", cut2, "", "AREA1");
  TPaveStats *st2 = (TPaveStats*)BGcanvas->GetPrimitive("stats"); 
  st2->SetX1NDC(0.6); 
  st2->SetX2NDC(0.9); 
  st2->SetY1NDC(0.3); 
  st2->SetY2NDC(0.6);
  BGcanvas->Print(filename.c_str());

  drawSig.SetTitle("HNL direction wrt Z");
  drawSig.SetTitleX("Z, degree");
  drawSig.Draw(sampleSig,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10.,"all", cut2, "", "AREA1");
  TPaveStats *st2 = (TPaveStats*)BGcanvas->GetPrimitive("stats"); 
  st2->SetX1NDC(0.6); 
  st2->SetX2NDC(0.9); 
  st2->SetY1NDC(0.3); 
  st2->SetY2NDC(0.6);
  BGcanvas->Print(filename.c_str());

  drawSig.SetTitle("HNL direction wrt beam");
  drawSig.SetTitleX("#theta, degree");
  drawSig.Draw(sampleSig,"angleWRTbeam*180/TMath::Pi()", 100, -5., 5.,"all", cut2, "", "AREA1");
  TPaveStats *st2 = (TPaveStats*)BGcanvas->GetPrimitive("stats"); 
  st2->SetX1NDC(0.6); 
  st2->SetX2NDC(0.9); 
  st2->SetY1NDC(0.3); 
  st2->SetY2NDC(0.6);
  BGcanvas->Print(filename.c_str());

  drawSig.SetTitle("HNL true direction wrt Z");
  drawSig.SetTitleX("Z, degree");
  drawSig.Draw(sampleSig,"TMath::ACos(HNL_true_direction[2])*180/TMath::Pi()", 100, 0., 10.,"all", cut2, "", "AREA1");
  TPaveStats *st2 = (TPaveStats*)BGcanvas->GetPrimitive("stats"); 
  st2->SetX1NDC(0.6); 
  st2->SetX2NDC(0.9); 
  st2->SetY1NDC(0.3); 
  st2->SetY2NDC(0.6);
  BGcanvas->Print(filename.c_str());

  drawSig.SetTitle("HNL true direction in XY plane");
  drawSig.SetTitleY("Y, degree");
  drawSig.Draw(sampleSig,"TMath::ASin(HNL_direction[1])*180/TMath::Pi():TMath::ASin(HNL_direction[0])*180/TMath::Pi()", 50, -10., 10., 50, -10., 10.,"all", cut2, "colz", "");
  BGcanvas->Print(filename.c_str());*/


  cut2 = "(accum_level[][0]>6 || accum_level[][2]>6) && HNL_direction[2] > 0.992 && cos(HNL_constituents_rel_angle) > 0. && HNL_inv_mass[0] > 250 && HNL_inv_mass[0] < 700";
  drawBG.SetTitle("Invariant mass");
  drawBG.SetTitleX("M, MeV");
  drawBG.Draw(BGsample1,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  TH1D* BGInvMass(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  BGInvMass.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  BGInvMass.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  BGInvMass.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  BGInvMass.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"HNL_inv_mass[0]", 100, 0., 5000.,"all", cut2, "histo", "");
  BGInvMass.Add(drawBG.GetLastHisto());

  BGcanvas->Clear();
  BGInvMass.Scale(1/6.5);
  BGInvMass.Draw("histo");
  BGcanvas->Print(filenamelast.c_str());
  std::cout << "Event number for 1e21 = " << BGInvMass.Integral() << std::endl;
}

  

