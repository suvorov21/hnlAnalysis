{
  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  DrawingTools drawBG("", true);

  DataSample BGsample_NEUT_1("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_1.root");
  DataSample BGsample_NEUT_2("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_2.root");
  DataSample BGsample_NEUT_3("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_3.root");
  DataSample BGsample_NEUT_4("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_4.root");
  DataSample BGsample_NEUT_5("AnalysisResults/BG/GENIE/new/prod6B_magnet_genie_AnalResults_5.root");

  std::string cut = "(accum_level[][1]>6 || accum_level[][3]>6) && (HNL_inv_mass[2] < 850 && HNL_inv_mass[2] > 140)&& TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 8. && cos(HNL_constituents_rel_angle) > 0.";//" && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 8.";
  std::string dir = "PDF/reactionType/9/";
  std::string print = "";

  std::string var = "reactionAllNu";
  Int_t nx     = 12;
  Float_t xmin = -2.;
  Float_t xmax = 10.;

  drawBG.Draw(BGsample_NEUT_1, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.0191);
  TH1D* histo1(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.64396);
  histo1->Add(drawBG.GetLastHisto(), 1.);  
  drawBG.Draw(BGsample_NEUT_3, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/46.87725);
  histo1->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_4, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo1->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_5, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo1->Add(drawBG.GetLastHisto(), 1.);
  //drawBG.Draw(BGsample_NEUT_6, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //histo1->Add(drawBG.GetLastHisto(), 1.);
  histo1->Draw();
  print = dir + "GenEleReaction1.png";
  BGcanvas->Print(print.c_str());

  nx   = 20;
  xmin = 50.;
  xmax = 70.;
  drawBG.Draw(BGsample_NEUT_1, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.0191);
  TH1D* histo2(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.64396);
  histo2->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_3, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/46.87725);
  histo2->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_4, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo2->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_5, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo2->Add(drawBG.GetLastHisto(), 1.);
  //drawBG.Draw(BGsample_NEUT_6, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //histo2->Add(drawBG.GetLastHisto(), 1.);
  histo2->Draw();
  print = dir + "GenEleReaction2.png";
  BGcanvas->Print(print.c_str());

  nx   = 200;
  xmin = 800.;
  xmax = 1000.;
  drawBG.Draw(BGsample_NEUT_1, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.0191);
  TH1D* histo3(drawBG.GetLastHisto());
  drawBG.Draw(BGsample_NEUT_2, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/47.64396);
  histo3->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_3, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //drawBG.GetLastHisto().Scale(1/46.87725);
  histo3->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_4, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo3->Add(drawBG.GetLastHisto(), 1.);
  drawBG.Draw(BGsample_NEUT_5, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  histo3->Add(drawBG.GetLastHisto(), 1.);
  //drawBG.Draw(BGsample_NEUT_6, var.c_str(), nx, xmin, xmax, "all", cut.c_str(), "", "");
  //histo3->Add(drawBG.GetLastHisto(), 1.);
  histo3->Draw();
  print = dir + "GenEleReaction3.png";
  BGcanvas->Print(print.c_str());
}