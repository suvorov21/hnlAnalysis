{
  DrawingTools draw("AnalysisResults/Signal/Sys/ElePi.root");
  DataSample sample("AnalysisResults/Signal/Sys/ElePi.root");

  std::string filename = "PDF/InvMass.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("M_{reco}-M_{true}, MeV");
  draw.SetTitleY("N");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

  draw.SetOptStat("RM");
  gStyle->SetOptStat(2200);
  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==160", "histo", "");
  BGcanvas->Print(filenamefirst.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==180", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==200", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==220", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==240", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==260", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==280", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==300", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==320", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==340", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==360", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[0]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==380", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[0]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==400", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==420", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==440", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==460", "histo", "");
  BGcanvas->Print(filename.c_str());

  draw.Draw(sample, "HNL_inv_mass[2]-HNL_true_mass", 200, -250., 250., "all", "(accum_level[][1]>9  || accum_level[][3]>9) && HNL_true_mass==480", "histo", "");
  BGcanvas->Print(filenamelast.c_str());
}