{
  /*TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  const Int_t n = 12;
  Double_t x[n]  = {0.26, 0.28, 0.30, 0.32, 0.34, 0.36, 0.38, 0.40, 0.42, 0.44, 0.46, 0.48};
  Double_t y[n]  = {35.0303, 33.289, 38.9486, 45.4958, 51.9357, 50.7265, 55.1208, 56.1579, 59.9736, 63.4565, 62.9672, 64.1807};
  Double_t ex[n] = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01};
  Double_t ey[n] = {1.21564, 0.848813, 0.882097, 0.974282, 1.00029, 0.940214, 0.978361, 0.9625, 1.01224, 1.04988, 1.06165, 1.26884};
  gr = new TGraphErrors(n,x,y,ex,ey);
  gr->SetTitle("HNL invariant mass resolution");
  gr->GetXaxis()->SetTitle("M_{HNL}, GeV");
  gr->GetYaxis()->SetTitle("Invariant mass RMS, MeV");

  //gr->SetMarkerColor(4);
  gr->SetMarkerStyle(21);
  gr->Draw("ALP");
  BGcanvas->Print("PDF/InvMass.pdf");*/

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  const Int_t n = 17;
  Double_t x[n]  = {0.16, 0.18, 0.20, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 0.36, 0.38, 0.40, 0.42, 0.44, 0.46, 0.48};
  Double_t y[n]  = {24.2725, 28.3972, 32.7394, 42.6802, 47.2118, 52.7185, 57.3912, 62.5741, 67.4059, 72.3497, 75.1837, 75.2163, 78.4596, 85.3618, 88.2744, 85.185, 87.1324};
  Double_t ex[n] = {0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01};
  Double_t ey[n] = {1.3263, 1.235, 1.02557, 1.32596, 1.32581, 1.46939, 1.43887, 1.51134, 1.640698, 1.73012, 1.67631, 1.67119, 1.6549, 1.70504, 1.78249, 1.75549, 2.0712};
  gr = new TGraphErrors(n,x,y,ex,ey);
  gr->SetTitle("HNL invariant mass resolution");
  gr->GetXaxis()->SetTitle("M_{HNL}, GeV");
  gr->GetYaxis()->SetTitle("Invariant mass RMS, MeV");

  //gr->SetMarkerColor(4);
  gr->SetMarkerStyle(21);
  gr->Draw("ALP");
  BGcanvas->Print("PDF/InvMass.pdf");

}