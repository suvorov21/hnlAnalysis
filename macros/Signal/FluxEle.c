{
	DrawingTools draw("AnalysisResults/Signal/Sys/ElePi/ElePi_fluxND13.root");
	DataSample sample("AnalysisResults/Signal/Sys/ElePi/ElePi_fluxND13.root");

	TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

	draw.DumpConfiguration("all_syst");
	draw.SetTitleX("HNL mass, MeV");

	std::string cut = "accum_level[][1]>9 || accum_level[][3]>9";
  	double bins[18] = {160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500};
  
	std::cout << "total:" << std::endl;
	draw.SetMaxY(0.2);
	draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 17, bins, cut.c_str(), "", "SYS WS0 WS1 WS2 WS3 WS5 WS6", "Det sys");
	draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 17, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6", "+ Flux sys");
	BGcanvas->Print("PDF/FluxEle.pdf");
}