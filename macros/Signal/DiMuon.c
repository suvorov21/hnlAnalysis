{
  DrawingTools drawBG("", true);

  DataSample BGsample1("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");
  DataSample BGsample2("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_2.root");
  DataSample BGsample3("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_3.root");
  DataSample BGsample4("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_4.root");
  DataSample BGsample5("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root");
  DataSample BGsample6("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_6.root");

	std::string filename = "PDF/SignalAnalysis1.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  TCanvas *Canvas = new TCanvas("Efficiency","",50,50,1000,800); 

  std::string cut1 = "accum_level[][4]>8";
  /*drawBG.SetTitle("DiMuon. All cuts");
  drawBG.Draw(BGsample1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  TH1D* BGpolarAngleD(drawBG.GetLastHisto());
  drawBG.Draw(BGsample2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsample6,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());

  BGpolarAngleD.Scale(1/BGpolarAngleD.Integral());
  BGpolarAngleD.Draw("histo"); 
  Canvas->Print(filenamefirst.c_str());*/

  DataSample sampleDiMuon("AnalysisResults/Signal/Sys/DiMuon.root");
  drawBG.SetLineColor(kRed);
  drawBG.Draw(sampleDiMuon, "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 180., "all", cut1, "histo", "AREA1");
  //BGpolarAngleD.Draw("histo same"); 
  Canvas->Print(filenamefirst.c_str());

  cut1 = "accum_level[][4]>6";
  drawBG.Draw(sampleDiMuon, "HMPEndTrackDet", 27, 0., 27., "all", cut1, "histo", "AREA1");
  Canvas->Print(filename.c_str());

  drawBG.Draw(sampleDiMuon, "HMNStartTrackDet", 27, 0., 27., "all", "accum_level[][4]>6 && HMNEndTrackDet == 4", "", "AREA1");
  Canvas->Print(filename.c_str());

  drawBG.Draw(sampleDiMuon, "HMNEndTrackDet", 27, 0., 27., "all", "accum_level[][4]>6 && HMNEndTrackDet < 5", "histo", "AREA1");
  Canvas->Print(filename.c_str());

  drawBG.Draw(sampleDiMuon, "HMNEndTrackPos[2]", 50, -1000., 3500., "all", "accum_level[][4]>6 && HMNEndTrackDet < 5", "histo", "AREA1");
  drawBG.DrawCutLineVertical(-724.85);
  drawBG.DrawCutLineVertical(-71.15);
  drawBG.DrawCutLineVertical(634.15);
  drawBG.DrawCutLineVertical(1287.85);
  drawBG.DrawCutLineVertical(1993.55);
  drawBG.DrawCutLineVertical(2646.85);
  drawBG.DrawCutLineVertical(2855.03);
  Canvas->Print(filename.c_str());

  drawBG.SetTitleX("HNL mass, MeV");
  Canvas->SetGrid();
  drawBG.DrawEff(sampleDiMuon.GetTree("truth"), "HNL_true_mass", 200, 0, 700., "(accum_level[4]>8)", "accum_level[4]>-1", "", "");
  Canvas->Print(filename.c_str());
  drawBG.DrawEff(sampleDiMuon.GetTree("truth"), "HNL_true_mass", 200, 0, 700., "(accum_level[4]>9)", "accum_level[4]>-1", "", "");
  Canvas->Print(filename.c_str());
  Canvas->SetGrid(0, 0);

  drawBG.SetMaxY(0.24);

  drawBG.DumpConfigurations("AnalysisResults/Signal/Sys/DiMuon.root");
  DrawingTools drawE("AnalysisResults/Signal/Sys/DiMuon.root");
  drawE.DumpConfiguration("all_syst");
  drawE.DumpSystematics();
  drawE.DumpWeightSystematics();
  double bins[14] = {220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480};
  drawBG.SetLineColor(kBlack);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "", "SSYS", "variation");
  drawBG.SetLineColor(kRed);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0", "+ Charge ID");
  drawBG.SetLineColor(kYellow);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1", "+ TPC cluster");
  drawBG.SetLineColor(kBlue);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2", "+ TPC track");
  drawBG.SetLineColor(kViolet);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  drawBG.SetLineColor(kCyan);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4", "+ GV eff");
  //drawBG.SetLineColor(kGreen);
  //drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5", "+ SI Pion");
  drawBG.SetLineColor(kMagenta);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6", "+ TpcECalMatchEff");
  //drawBG.SetLineColor(kAzure);
  //drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6 WS7", "+ ECalTrackEff");
  drawBG.SetLineColor(kGreen);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6 WS8", "+ ECalPID");
  Canvas->Print(filename.c_str());

  drawBG.SetMaxY();
  drawBG.SetLineColor(kBlack);
  drawBG.DrawRelativeErrors(sampleDiMuon.GetTree("all_syst"), "HNL_true_mass", 13, bins, "accum_level[][4]>9", "", "SYS");
  Canvas->Print(filenamelast.c_str());

}