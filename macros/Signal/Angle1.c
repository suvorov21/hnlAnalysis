{
  std::string filename = "PDF/SignalAnalysis1.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  TCanvas *Canvas = new TCanvas("Efficiency","",50,50,1000,800); 

  DrawingTools drawE("", true);
  DataSample sampleEle("AnalysisResults/Signal/Sys/Zero/prod3/ElePi_All.root");
  DataSample sampleEle_Mom("AnalysisResults/Signal/Sys/Zero/prod3/ElePi_Mom.root");

  drawE.Draw(sampleEle_Mom.GetTree("all_syst"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 20, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && cos(HNL_constituents_rel_angle)>0.", "", "SYS E2 AREA1");

  Canvas->Print(filename.c_str());

}