{
  DrawingTools draw("", true);
  DataSample sampleCorr("AnalysisResults/Signal/ElePi_Corr.root");
  DataSample sampleWOCorr("AnalysisResults/Signal/ElePi_woCorr.root");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);
  BGcanvas->SetGrid();

  std::string cutPass    = "(accum_level[1]>9 || accum_level[3]>9)";
  std::string cutPre     =  "accum_level[1]>-1";

  draw.DrawEff(sampleCorr.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPass.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph(draw.GetLastGraph());
  WeightGraph->SetMaximum(0.15);
  WeightGraph->Draw("ap");
  //draw.Draw(sampleWOCorr, "HNL_true_mass", 200, 0, 700., "all", cutPass.c_str(), "", "");
  BGcanvas->Print("PDF/ElePiEffcorr.png");

  draw.DrawEff(sampleWOCorr.GetTree("truth"), "HNL_true_mass", 200, 0, 700., cutPass.c_str(), cutPre.c_str(), "", "");
  TGraphAsymmErrors* WeightGraph1(draw.GetLastGraph());
  WeightGraph1->SetMaximum(0.15);
  WeightGraph1->Draw("ap");
  WeightGraph->SetLineColor(kRed);
  WeightGraph->SetMarkerColor(kRed);
  WeightGraph->Draw("same");
  BGcanvas->Print("PDF/ElePiEffWOcorr.png");


}