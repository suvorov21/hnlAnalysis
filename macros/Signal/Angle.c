{ 
  gROOT->Reset();
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1); 

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.08);
  t2kStyle->SetPadRightMargin(0.12);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);

  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(1,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");

  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t"); 
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.04);
  t2kStyle->SetTitleFont(132,"pad");

  t2kStyle->SetTitleBorderSize(1);    
  t2kStyle->SetPadBorderSize(1);    
  t2kStyle->SetCanvasBorderSize(1);    
 
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth(1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);

  // do not display any of the standard histogram decorations
  //t2kStyle->SetOptTitle(0);
  t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(0);

  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;

  TColor **theCols = new TColor*[ncol];

  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

  for (int j = 0; j < ncol; j++) {
    theCols[j]->SetRGB(gray,gray,gray);
    gray -= dcol;
  }

  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  DrawingTools draw("");
  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();
  //DataSample sampleMuMom("AnalysisResults/Signal/Sys/Zero/MuPi_Mom.root");
  DataSample sampleMu("AnalysisResults/Signal/Dimuon.root");

  DrawingTools drawBG("");

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();

  DataSample BGsampleN1("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_1.root");
  DataSample BGsampleN2("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_2.root");
  DataSample BGsampleN3("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_3.root");
  DataSample BGsampleN4("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_4.root");
  DataSample BGsampleN5("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_5.root");
  DataSample BGsampleN6("AnalysisResults/BG/NEUT/prod6B_magnet_neut_AnalResults_6.root");

  /*DataSample BGsampleG1("AnalysisResults/BG/GENIE/prod6B_magnet_genie_AnalResults_1.root");
  DataSample BGsampleG2("AnalysisResults/BG/GENIE/prod6B_magnet_genie_AnalResults_2.root");
  DataSample BGsampleG3("AnalysisResults/BG/GENIE/prod6B_magnet_genie_AnalResults_3.root");
  DataSample BGsampleG4("AnalysisResults/BG/GENIE/prod6B_magnet_genie_AnalResults_4.root");
  DataSample BGsampleG5("AnalysisResults/BG/GENIE/prod6B_magnet_genie_AnalResults_5.root");

  DataSample BGsampleAN1("AnalysisResults/BG/ANEUT/prod6B_magnet_antineut_AnalResults_1.root");
  DataSample BGsampleAN2("AnalysisResults/BG/ANEUT/prod6B_magnet_antineut_AnalResults_2.root");
  DataSample BGsampleAN3("AnalysisResults/BG/ANEUT/prod6B_magnet_antineut_AnalResults_3.root");
  DataSample BGsampleAN4("AnalysisResults/BG/ANEUT/prod6B_magnet_antineut_AnalResults_4.root");*/

  TCanvas *Canvas = new TCanvas("Efficiency","",50,50,1000,800); 

  gROOT->SetStyle("T2K");
  gROOT->ForceStyle();
  
  sampleMu.SetCurrentTree("default");

  std::string filename = "PDF/SignalAnalysis.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  drawBG.SetTitleX("cos(#theta)");
  std::string cut1 = "(accum_level[][0]>6 || accum_level[][2]>6) && HNL_inv_mass[0] > 250 && HNL_inv_mass[0] < 750 && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 3.4";
  drawBG.Draw(BGsampleN1,"cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  TH1D* BGpolarAngleA(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1, "histo", "");
  BGpolarAngleA.Add(drawBG.GetLastHisto());

  /*drawBG.Draw(BGsampleG1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  TH1D* BGpolarAngleA1(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA1.Add(drawBG.GetLastHisto());

  drawBG.Draw(BGsampleAN1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  TH1D* BGpolarAngleA2(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA2.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA2.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleA2.Add(drawBG.GetLastHisto());*/

  BGpolarAngleA.Scale(1/6.5);
  std::cout << "Integral = " << BGpolarAngleA.Integral() << std::endl;
  //BGpolarAngleA.GetXaxis()->SetRangeUser(0., 40.);
  //BGpolarAngleA1.Scale(1/BGpolarAngleA1.Integral());
  //BGpolarAngleA2.Scale(1/BGpolarAngleA2.Integral());

  //draw.SetTitle("Polar Angle #mu#pi. Zero var. MomScale+MomRes+Bfield");
  draw.SetTitleX("cos(#theta)");
  draw.SetLineColor(kViolet);
  draw.Draw(sampleMu.GetTree("default"), "TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", cut1.c_str(), "hist", "AREA1");

  TH1D* SigPolarAngleA(draw.GetLastHisto());
  
  BGpolarAngleA.Draw("histo");
  draw.DrawCutLineVertical(0., true, "r");
  Float_t rightmax = 1.1*SigPolarAngleA->GetMaximum();
  Float_t scale = BGpolarAngleA->GetMaximum()/rightmax;
  SigPolarAngleA->SetLineColor(kRed);
  SigPolarAngleA->Scale(scale);
  SigPolarAngleA->Draw("same histo");

  TGaxis *axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(),
  gPad->GetUxmax(), BGpolarAngleA->GetMaximum(),0,rightmax,510,"+L");
  axis->SetLineColor(kRed);
  axis->SetTextColor(kRed);
  axis->Draw();

  Canvas->Print(filename.c_str());
   
  //BGpolarAngleA1.Draw("histo"); 
  //Canvas->Print(filename.c_str());  
  //BGpolarAngleA2.Draw("histo");
  //Canvas->Print(filename.c_str());  

  /*draw.SetLineColor(kBlack);

  cut1 = "(accum_level[][0]>8 || accum_level[][2]>8) && TMath::ACos(HNL_direction[2])*180/TMath::Pi() < 3.4";

  drawBG.Draw(BGsampleN1,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  TH1D* BGprelAngleA(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  BGprelAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  BGprelAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  BGprelAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  BGprelAngleA.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1., "all", cut1, "histo", "");
  BGprelAngleA.Add(drawBG.GetLastHisto());
  BGprelAngleA.Scale(1/6.5);

  draw.SetTitleX("cos(#theta), degree");
  draw.SetLineColor(kViolet);
  draw.Draw(sampleMu.GetTree("default"), "TMath::Cos(HNL_constituents_rel_angle)", 100, -1.1, 1.1, "all", "(accum_level[][0]>8 || accum_level[][2]>8)", "hist", "AREA1");

  TH1D* SigRelAngleA(draw.GetLastHisto());

  BGprelAngleA.Draw("histo");
  draw.DrawCutLineVertical(0., true, "r");
  rightmax = 1.1*SigRelAngleA->GetMaximum();
  scale = BGprelAngleA->GetMaximum()/rightmax;
  SigRelAngleA->SetLineColor(kRed);
  SigRelAngleA->Scale(scale);
  SigRelAngleA->Draw("same histo");

  TGaxis *axis1 = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(),
  gPad->GetUxmax(), BGprelAngleA->GetMaximum(),0,rightmax,510,"+L");
  axis1->SetLineColor(kRed);
  axis1->SetTextColor(kRed);
  axis1->Draw();
  Canvas->Print(filenamelast.c_str());
  //draw.DrawRelativeErrors(sampleMuMom.GetTree("all_syst"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 40, 0., 10., "(accum_level[][0]>7 || accum_level[][2]>7) && cos(HNL_constituents_rel_angle)>0.", "", "SSYS");
  //Canvas->Print(filename.c_str());  

  /*draw.SetTitle("#mu#pi 480 MeV");
  draw.SetLineColor(kBlack);
  draw.Draw(sampleMu.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][0]>7 || accum_level[][2]>7) && HNL_true_mass==480 && cos(HNL_constituents_rel_angle)>0.", " histo", "now");
  draw.SetTitle("400 MeV");
  draw.SetLineColor(kRed);
  draw.Draw(sampleMu.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][0]>7 || accum_level[][2]>7) && HNL_true_mass==400 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  draw.SetTitle("360 MeV");
  draw.SetLineColor(kGreen);
  draw.Draw(sampleMu.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][0]>7 || accum_level[][2]>7) && HNL_true_mass==360 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  draw.SetTitle("300 MeV");
  draw.SetLineColor(kBlue);
  draw.Draw(sampleMu.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][0]>7 || accum_level[][2]>7) && HNL_true_mass==300 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  Canvas->BuildLegend();
  Canvas->Print(filename.c_str());

  Canvas->SetGrid();
  draw.SetTitle("Efficiency. #mu#pi.");
  draw.SetTitleX("HNL mass, MeV");
  draw.SetTitleY("Efficiency. N#rightarrow#mu#pi");
  draw.DrawEff(sampleMu.GetTree("truth"), "HNL_true_mass", 200, 0, 700., "(accum_level[0]>9 || accum_level[2]>9)", "accum_level[0]>-1", "", "");
  Canvas->Print(filename.c_str());

  Canvas->SetGrid(0, 0);
  draw.SetTitle("#mu#pi Systematic");
  draw.SetMaxY(0.26);
  double bins[13] = {260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500};
  draw.SetLineColor(kBlack);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "", "SSYS", "variation");
  draw.SetLineColor(kRed);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0", "+ Charge ID");
  draw.SetLineColor(kYellow);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0 WS1", "+ TPC cluster");
  draw.SetLineColor(kBlue);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0 WS1 WS2", "+ TPC track");
  draw.SetLineColor(kViolet);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  draw.SetLineColor(kCyan);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4", "+ GV eff");
  draw.SetLineColor(kGreen);
  draw.DrawRelativeErrors(sampleMu.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>9 || accum_level[][2]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5", "+ SI Pion");
  Canvas->Print(filename.c_str());
  draw.SetMaxY();


  /*drawBG.SetTitleX("M, MeV");
  cut1 = "(accum_level[][0]>9 || accum_level[][2]>9)";
  drawBG.Draw(BGsampleN1,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  TH1D* BGMassMu(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"HNL_true_mass[0]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassMu.Add(drawBG.GetLastHisto());

  BGMassMu->Scale(1/6.5);
  t2kStyle->SetOptStat(10);
  BGMassMu->Draw("histo");
  Canvas->Print(filename.c_str());*/

  ////////////////////////////////////////////////////////////////////////////////////////////////////

  /*DrawingTools drawE("", true);
  DataSample sampleEle("AnalysisResults/Signal/SignalElePi_prod3.root");
  //DataSample sampleEle_Mom("AnalysisResults/Signal/Sys/Zero/prod3/ElePi_Mom.root");
  DataSample sampleEleEff("AnalysisResults/Signal/SignalElePi_prod3.root");

  std::string cut2 = "(accum_level[][1]>7 || accum_level[][3]>7) && cos(HNL_constituents_rel_angle)>0.";
  drawBG.Draw(BGsampleN1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  TH1D* BGpolarAngleB(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB.Add(drawBG.GetLastHisto());

  drawBG.Draw(BGsampleG1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  TH1D* BGpolarAngleB1(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB1.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleG5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB1.Add(drawBG.GetLastHisto());

  drawBG.Draw(BGsampleAN1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  TH1D* BGpolarAngleB2(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB2.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB2.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleAN4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleB2.Add(drawBG.GetLastHisto());

  BGpolarAngleB.Scale(1/BGpolarAngleB.Integral());
  BGpolarAngleB1.Scale(1/BGpolarAngleB1.Integral());
  BGpolarAngleB2.Scale(1/BGpolarAngleB2.Integral());

  //drawE.SetTitle("Polar Angle e#pi. Zero var. MomScale+MomRes+Bfield");
  drawE.SetTitleX("#theta, degree");
  drawE.SetLineColor(kViolet);
  drawE.Draw(sampleEle.GetTree("default"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 50, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && cos(HNL_constituents_rel_angle)>0.", "hist", "AREA1");

  TH1D* SigPolarAngleB(drawE.GetLastHisto());
  SigPolarAngleB.Draw("histo");
  BGpolarAngleB.Draw("histo same"); 
  drawE.DrawCutLineVertical(8.2);
  Canvas->Print(filename.c_str()); 

  BGpolarAngleB.GetXaxis()->SetRangeUser(0., 10.);
  BGpolarAngleB1.GetXaxis()->SetRangeUser(0., 10.);
  BGpolarAngleB2.GetXaxis()->SetRangeUser(0., 10.);

  BGpolarAngleB.Draw("histo"); 
  Canvas->Print(filename.c_str());   
  BGpolarAngleB1.Draw("histo"); 
  Canvas->Print(filename.c_str());  
  BGpolarAngleB2.Draw("histo");
  Canvas->Print(filename.c_str());  

  drawE.SetLineColor(kBlack);
  //drawE.DrawRelativeErrors(sampleEle_Mom.GetTree("all_syst"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 40, 0., 10., "(accum_level[][1]>7 || accum_level[][3]>7) && cos(HNL_constituents_rel_angle)>0.", "", "SSYS");
  //Canvas->Print(filename.c_str()); 

  /*drawE.SetTitle("e#pi 480 MeV");
  drawE.SetLineColor(kBlack);
  drawE.Draw(sampleEle.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && HNL_true_mass==480 && cos(HNL_constituents_rel_angle)>0.", " histo", "now");
  drawE.SetTitle("400 MeV");
  drawE.SetLineColor(kRed);
  drawE.Draw(sampleEle.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && HNL_true_mass==400 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  drawE.SetTitle("360 MeV");
  drawE.SetLineColor(kGreen);
  drawE.Draw(sampleEle.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && HNL_true_mass==360 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  drawE.SetTitle("300 MeV");
  drawE.SetLineColor(kBlue);
  drawE.Draw(sampleEle.GetTree("zero_var"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 100, 0., 10., "all", "(accum_level[][1]>7 || accum_level[][3]>7) && HNL_true_mass==300 && cos(HNL_constituents_rel_angle)>0.", "same histo", "now");
  Canvas->BuildLegend();
  Canvas->Print(filename.c_str());

  Canvas->SetGrid();
  drawE.SetTitle("Efficiency. #e#pi.");
  drawE.SetTitleY("Efficiency N#rightarrowe#pi");
  drawE.SetTitleX("HNL mass, MeV");
  drawE.DrawEff(sampleEleEff.GetTree("truth"), "HNL_true_mass", 200, 0, 700., "(accum_level[1]>9 || accum_level[3]>9)", "accum_level[1]>-1", "", "");
  Canvas->Print(filename.c_str());

  Canvas->SetGrid(0, 0);
  drawE.SetTitle("e#pi Systematic");
  drawE.SetMaxY(0.28);
  double binsE[18] = {160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480, 500};
  drawE.SetLineColor(kBlack);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "", "SSYS", "variation");
  drawE.SetLineColor(kRed);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0", "+ Charge ID");
  drawE.SetLineColor(kYellow);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0 WS1", "+ TPC cluster");
  drawE.SetLineColor(kBlue);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0 WS1 WS2", "+ TPC track");
  drawE.SetLineColor(kViolet);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  drawE.SetLineColor(kCyan);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4", "+ GV eff");
  drawE.SetLineColor(kGreen);
  drawE.DrawRelativeErrors(sampleEle.GetTree("all_syst"), "HNL_true_mass", 17, binsE, "accum_level[][1]>9 || accum_level[][3]>9", "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5", "+ SI Pion");
  Canvas->Print(filename.c_str());
  drawE.SetMaxY();

  drawBG.SetTitleX("M, MeV");
  cut1 = "(accum_level[][1]>9 || accum_level[][3]>9)";
  drawBG.Draw(BGsampleN1,"HNL_true_mass[1]", 100, 100., 800.,"all", cut1, "histo", "");
  TH1D* BGMassEle(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"HNL_true_mass[1]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassEle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"HNL_true_mass[1]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassEle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"HNL_true_mass[1]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassEle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"HNL_true_mass[1]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassEle.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"HNL_true_mass[1]", 100, 100., 800., "all", cut1, "histo", "");
  BGMassEle.Add(drawBG.GetLastHisto());

  BGMassEle->Scale(1/6.5);
  t2kStyle->SetOptStat(10);
  BGMassEle->Draw("histo");
  Canvas->Print(filename.c_str());*/

  /*DataSample sampleDiMuon("AnalysisResults/Signal/SignalDiMuon.root");
  cut2 = "(accum_level[][4]>8)";

  //drawE.SetTitle("Polar Angle DiMuon.");
  drawE.SetTitleX("#theta, degree");
  drawBG.Draw(BGsampleN1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  TH1D* BGpolarAngleC(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleC.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleC.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleC.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleC.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 720, 0., 180., "all", cut2, "histo", "");
  BGpolarAngleC.Add(drawBG.GetLastHisto());

  BGpolarAngleC.Scale(1/BGpolarAngleC.Integral());

  drawE.SetLineColor(kViolet);
  drawE.Draw(sampleDiMuon.GetTree("default"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 30, 0., 40., "all", cut2, "hist", "AREA1");
  TH1D* SigPolarAngleC(drawE.GetLastHisto());
  SigPolarAngleC.Draw("hist");
  BGpolarAngleC.Draw("histo same");  
  drawE.DrawCutLineVertical(17.);

  Canvas->Print(filenamelast.c_str());

  /*std::string cut1 = "accum_level[][4]>8";
  //drawE.SetTitle("DiMuon. All cuts");
  drawE.SetTitleX("#theta, degree");
  drawBG.Draw(BGsampleN1,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  TH1D* BGpolarAngleD(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 900, 0., 180., "all", cut1, "histo", "");
  BGpolarAngleD.Add(drawBG.GetLastHisto());

  BGpolarAngleD.Scale(1/BGpolarAngleD.Integral());
  BGpolarAngleD.Draw("histo"); 
  //Canvas->Print(filename.c_str());

  drawE.SetLineColor(kViolet);
  drawE.Draw(sampleDiMuon.GetTree("default"), "TMath::ACos(HNL_direction[2])*180/TMath::Pi()", 50, 0., 40., "all", cut1, "histo", "AREA1");
  BGpolarAngleD.Draw("histo same");  

  Canvas->Print(filename.c_str());

  /*drawBG.SetTitleX("M, MeV");
  
  drawBG.Draw(BGsampleN1,"200", 100, 100., 800.,"all", cut1, "histo", "");
  TH1D* BGMassDiMu(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN2,"200", 100, 100., 800., "all", cut1, "histo", "");
  BGMassDiMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN3,"200", 100, 100., 800., "all", cut1, "histo", "");
  BGMassDiMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN4,"200", 100, 100., 800., "all", cut1, "histo", "");
  BGMassDiMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN5,"200", 100, 100., 800., "all", cut1, "histo", "");
  BGMassDiMu.Add(drawBG.GetLastHisto());
  drawBG.Draw(BGsampleN6,"200", 100, 100., 800., "all", cut1, "histo", "");
  BGMassDiMu.Add(drawBG.GetLastHisto());

  BGMassDiMu->Scale(1/6.5);
  t2kStyle->SetOptStat(10);
  BGMassDiMu->Draw("histo");
  Canvas->Print(filenamelast.c_str());*/

}