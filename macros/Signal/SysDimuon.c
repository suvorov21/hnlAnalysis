{ 
  //DrawingTools draw("AnalysisResults/Signal/Sys/MuPi_prod5_Sys_old.root");
  //DataSample sample("AnalysisResults/Signal/Sys/MuPi_prod5_Sys_old.root");

  DrawingTools draw("AnalysisResults/Signal/Sys/Dimuon/Dimuon_v2.root");
  DataSample sample("AnalysisResults/Signal/Sys/Dimuon/Dimuon_v2.root");
  std::string filename = "PDF/Systematics0.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("Mass, MeV");
  draw.SetTitleY("Relative error");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

  draw.DumpConfigurations();
  draw.DumpConfiguration("all_syst");
  draw.DumpConfiguration("default");
  //draw.DumpCuts();
  //draw.DumpSystematics();

  std::string cut = "accum_level[][4]>9";

  double bins[12] = {260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480};

  //draw.ListOptions();
  
  std::cout << "total:" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS NWS4 NWS6");
  BGcanvas->Print(filenamefirst.c_str());

  //draw.SetTitleY("#events");
  //draw.Draw(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, "all", "accum_level[][0]>8", "", "");
  //BGcanvas->Print(filename.c_str());

  //draw.DrawErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, "accum_level[][0]>8", "", "SSYS NOW");
  //BGcanvas->Print(filename.c_str());

  draw.SetMaxY(0.04);
  //std::cout << "variation" << std::endl;
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SSYS", "variation");
  std::cout << "Var+ Charge ID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS0", "Var + Charge ID");
  std::cout << "+ TPC cluster" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1", "+ TPC cluster");
  std::cout << "+ TPC track" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2", "+ TPC track");
  std::cout << "+ TPC FGD match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  std::cout << "+ GV eff" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5", "+ GV eff");
  //std::cout << "+ SI Pion" << std::endl;
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5", "+ SI Pion");
  std::cout << "+ TPC ECaL match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5 WS7", "+ TpcECalMatchEff");
  std::cout << "+ Ecal PID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS5 WS7 WS8", "+ ECalPID");
  //std::cout << "+ Pile Up" << std::endl;
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6 WS7 WS8", "+ Pile Up");
  draw.SetTitleY("");
  BGcanvas->Print(filenamelast.c_str());

  //draw.SetMaxY();
  /*std::cout << "-------------------------------" << std::endl;
  std::cout << "Charge ID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS0", "variation + Charge");
  BGcanvas->Print(filename.c_str());
  std::cout << "TPC cluster" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS1", "variation + Cluster");
  BGcanvas->Print(filename.c_str());
  std::cout << "TPC track" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS2", "variation + TPC track");
  BGcanvas->Print(filename.c_str());
  std::cout << "TPC FGD match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS3", "variation + TPC FGD match");
  BGcanvas->Print(filename.c_str());
  std::cout << "GV eff" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS4", "variation + GV eff");
  BGcanvas->Print(filename.c_str());
  std::cout << "+ SI Pion" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS5", "variation + Pion SI");
  BGcanvas->Print(filename.c_str());
  std::cout << "+ PileUp" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 11, bins, cut.c_str(), "", "SYS WS6", "variation + Pile Up");
  BGcanvas->Print(filenamelast.c_str());*/
}