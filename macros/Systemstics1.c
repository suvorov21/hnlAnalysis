{ 
  //DrawingTools draw("AnalysisResults/Signal/Sys/flux.root");
  //DataSample sample("AnalysisResults/Signal/Sys/flux.root");

  DrawingTools draw("AnalysisResults/Signal/Sys/ElePi/PosPi.root");
  DataSample sample("AnalysisResults/Signal/Sys/ElePi/PosPi.root");
  std::string filename = "PDF/Systematics1.pdf";
  std::string filenamefirst = filename + "(";
  std::string filenamelast = filename + ")";

  draw.SetTitleX("Mass, MeV");
  draw.SetTitleY("Relative error");

  TCanvas *BGcanvas = new TCanvas("Efficiency","",50,50,1000,800);

  draw.DumpConfigurations();
  draw.DumpConfiguration("all_syst");
  draw.DumpConfiguration("default");
  //draw.DumpCuts();
  draw.DumpSystematics();
  draw.ListOptions();

  std::string cut = "accum_level[][1]>9 || accum_level[][3]>9";
  double bins[17] = {160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420, 440, 460, 480};
  
  std::cout << "total:" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "", "SYS");
  BGcanvas->Print(filenamefirst.c_str());

  //draw.SetTitleY("#events");
  //draw.Draw(sample.GetTree("all_syst"), "HNL_true_mass", 12, bins, "all", "accum_level[][0]>8", "", "");
  //BGcanvas->Print(filename.c_str());

  //draw.DrawErrors(sample.GetTree("all_syst"), "HNL_true_mass", 12, bins, "accum_level[][0]>8", "", "SSYS NOW");
  //BGcanvas->Print(filename.c_str());

  //std::cout << "variation" << std::endl;
  draw.SetMaxY(0.12);
  //draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WCORR", "variation");
  std::cout << "Var + Charge ID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "", "SYS WS0", "Var + Charge ID");
  std::cout << "+ TPC cluster" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1", "+ TPC cluster");
  std::cout << "+ TPC track" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2", "+ TPC track");
  std::cout << "+ TPC FGD match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3", "+ TPC FGD match");
  std::cout << "+ GV eff" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4", "+ GV eff");
  std::cout << "+ SI Pion" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5", "+ SI Pion");
  std::cout << "+ Pile Up" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 16, bins, cut.c_str(), "same", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6", "+ Pile Up");
  BGcanvas->Print(filenamelast.c_str());

  /*draw.SetMaxY();
  std::cout << "-------------------------------" << std::endl;
  std::cout << "Charge ID" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WS0", "variation + Charge");
  BGcanvas->Print(filename.c_str());
  std::cout << "TPC cluster" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WS1", "variation + Cluster");
  BGcanvas->Print(filename.c_str());
  std::cout << "TPC FGD match" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WS2", "variation + track");
  BGcanvas->Print(filename.c_str());
  std::cout << "GV eff" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WS3", "variation + TPC FGD");
  BGcanvas->Print(filename.c_str());
  std::cout << "+ SI Pion" << std::endl;
  draw.DrawRelativeErrors(sample.GetTree("all_syst"), "HNL_true_mass", 14, bins, cut.c_str(), "", "WS4", "variation + Pion SI");
  BGcanvas->Print(filenamelast.c_str());*/
}