{
  DrawingTools draw("AnalysisResults/rdp.root");
  DataSample sample("AnalysisResults/rdp.root");
  draw.DrawEff(sample.GetTree("default"), "1", 2, 0., 2., "accum_level[][0]>5", "accum_level[][0]>2");
  DrawingTools draw1("AnalysisResults/mcp.root");
  DataSample sample1("AnalysisResults/mcp.root");
  draw1.DrawEff(sample1.GetTree("default"), "1", 2, 0., 2., "accum_level[][0]>5", "accum_level[][0]>2");
}